#include <incr.h>

#ifdef DMALLOC
#include "dmalloc.h"
#endif

ilbool ildispatch(ILview_t *view, ILobj_t *spec, ILfnlist_t *f) {
    ilbool	rv;
    rv = FALSE;

    switch(spec->tag) {

            case ILNODE:

            if (f->n)
                rv = (*(f->n))(view,(ILnode_t*)spec);

            break;

            case ILEDGE:
            if (f->e)
                rv = (*(f->e))(view,(ILedge_t*)spec);

            break;

            case ILHYPER:
            if (f->m)
                rv = (*(f->m))(view,(ILhyper_t*)spec);

            break;

            case ILVIEW:
            if (f->v)
                rv = (*(f->v))(view,(ILview_t*)spec);

            break;

            default:
            break;
    }

    return rv;
}

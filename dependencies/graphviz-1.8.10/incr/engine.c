#include <engine.h>

#ifdef DMALLOC
#include "dmalloc.h"
#endif

/* view operations and stubs to invoke the engine */

#define ENG(view)	(((engview_t*)(view->pvt))->engine)

ilbool		ilopen(ILengine_t *engine, ILview_t *view) {
    return engine->open(view);
}

void		ilclose(ILview_t *view) {
    ENG(view)->close(view);
}

ilbool		ilinsert(ILview_t *view, ILobj_t *spec) {
    return ENG(view)->ins(view,spec);
}

ilbool 		iloptimize(ILview_t *view, ILobj_t *spec) {
    return ENG(view)->mod(view,spec);
}

ilbool		ilmodify(ILview_t *view, ILobj_t *spec) {
    return ENG(view)->mod(view,spec);
}

ilbool		ildelete(ILview_t *view, ILobj_t *spec) {
    return ENG(view)->del(view,spec);
}

Agraph_t	*ilmodel(ILview_t *view) {
    return (Agraph_t*) (ENG(view)->spec_to_mdlobj(view,(ILobj_t*)view));
}

Agobj_t	*ilspec_to_mdlobj(ILview_t *view, ILobj_t *spec) {
    switch (spec->tag) {

            case ILNODE:
            return (Agobj_t*)il_find_node((engview_t*)(view->pvt),(ILnode_t*)spec);

            case ILEDGE:
            return (Agobj_t*)il_find_edge((engview_t*)(view->pvt),(ILedge_t*)spec);

            case ILVIEW:
            return (Agobj_t*)(((engview_t*)(view->pvt))->model.main);

            default:
            return NIL(Agobj_t*);
    }
}

ILobj_t *ilmdlobj_to_spec(ILview_t *view, Agobj_t *mdlobj) {
    switch (AGTYPE(mdlobj)) {

            case AGNODE:
            return (ILobj_t*)il_node((Agnode_t*)mdlobj);

            case AGEDGE:
            return (ILobj_t*)il_edge((Agedge_t*)mdlobj);

            case AGRAPH:

            default:
            return (ILobj_t*)0;	/* UNIMPLEMENTED */
    }
}

void il_register_node_callback(engview_t *view, ILnode_t *spec, int kind) {
    Agnode_t	*n;

    if ((n = il_find_node(view,spec))) {
        if (kind == IL_MOD) {
            if (agsubnode(view->model.v[IL_INS],n,FALSE))
                return;

            if (agsubnode(view->model.v[IL_DEL],n,FALSE))
                return;
        }

        (void) agsubnode(view->model.v[kind], n, TRUE);

    } else
        abort();
}

void il_register_edge_callback(engview_t *view, ILedge_t *spec, int kind) {
    Agedge_t	*e;

    if ((e = il_find_edge(view,spec))) {
        if (kind == IL_MOD) {
            if (agsubedge(view->model.e[IL_INS],e,FALSE))
                return;

            if (agsubedge(view->model.e[IL_DEL],e,FALSE))
                return;
        }

        (void) agsubedge(view->model.e[kind], e, TRUE);

    } else
        abort();
}

ilbool ilcallback(ILview_t *view) {
    return ENG(view)->callback(view);
}

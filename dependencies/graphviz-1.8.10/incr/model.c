#include <stdio.h>
#include <engine.h>

#ifdef DMALLOC
#include "dmalloc.h"
#endif

/* bind model nodes  and client IDs/descriptors */

char ILrec_name[] = "ILrec";

/* for mapping unsigned long IDs to model edges */

typedef struct {
    Dtdisc_t	base;
    engview_t	*view;
}

edgemapdisc_t;

void ilfreemapsym(Dict_t *dict, void *obj, Dtdisc_t *argdisc) {
    edgemapdisc_t	*disc;

    disc = (edgemapdisc_t*)argdisc;
    agfree(disc->view->model.main,obj);
}

void *ilmakeobjfn(Dict_t *dict, void *obj, Dtdisc_t *argdisc) {
    return obj;
}

Dtdisc_t IL_dict_id_disc = {
                               offsetof(Agedge_t,base.tag.id),	/* use id as key */
                               sizeof(unsigned long),				/* key size */
                               -1,							/* use holder objects */
                               NIL(Dtmake_f),
                               NIL(Dtfree_f),				/*ilfreemapsym,*/
                               NIL(Dtcompar_f),
                               NIL(Dthash_f),
                               NIL(Dtmemory_f),
                               NIL(Dtevent_f)
                           };

/* trivial ID discipline */
static void	*idopen(Agraph_t *g) {
    return NIL(void*);
}

static long idmap(void *state, int objtype, char *str, unsigned long *id, int createflag) {
    *id = (unsigned long)str;
    return TRUE;
}

static long idalloc(void *state, int objtype, unsigned long id) {
    return TRUE;
}

static void idfree(void *state, int objtype, unsigned long id) {}

static char *idprint(void *state, int objtype, unsigned long id) {
    return NILstr;
}

static void	idclose(void *state) {}

static Agiddisc_t iddisc = { idopen,
                             idmap,
                             idalloc,
                             idfree,
                             idprint,
                             idclose};

Agdisc_t IL_graph_id_disc = {0, &iddisc, 0};

engview_t *il_open_view(ILengine_t *engine, ILview_t *client, Agdesc_t desc, unsigned int *recsize) {
    engview_t		 *rv;
    edgemapdisc_t	*disc;
    Agraph_t		*model;
    int				i;
    unsigned long			id = 1;

    model = agopen(NILstr, desc, &IL_graph_id_disc);
    rv = agalloc(model,recsize[IL_RECSIZE_VIEW]);

    rv->client = client;
    rv->engine = engine;

    for (i = 0; i < IL_RECSIZE_N; i++)
        rv->recsize[i] = recsize[i];

    rv->model.main = model;

    for (i = 0; i < IL_N_OPS; i++) {
        rv->model.v[i] = agidsubg(rv->model.main,id++,TRUE);
        rv->model.e[i] = agidsubg(rv->model.main,id++,TRUE);
    }

    disc = agalloc(model,sizeof(*disc));
    disc->base = IL_dict_id_disc;
    disc->view = rv;
    rv->model.e_dict = dtopen((Dtdisc_t*)disc,Dttree);
    client->pvt = rv;
    return rv;
}

void il_close_view(engview_t *view) {
    Agraph_t	*G;
    Agnode_t	*n, *nn;
    Agedge_t	*e, *ne;
    Dtdisc_t	*edgemapdisc;
    ILedge_t	*espec;
    ILnode_t	*nspec;

    G = view->model.main;

    for (n = agfstnode(G); n; n = nn) {
        for (e = agfstout(n); e; e = ne) {
            ne = agnxtout(e);
            espec = il_edge(e);
            /* this is done in clear_callbacks */
            /* il_close_edge(view, espec); */
            il_register_edge_callback(view, espec, IL_DEL);
        }

        nn = agnxtnode(n);
        nspec = il_node(n);
        il_register_node_callback(view, nspec, IL_DEL);
        /* il_close_node(view, il_node(n)); */
    }

    (void) il_issue_callbacks(view);

    view->client->pvt = NIL(void*);
    edgemapdisc = dtdisc(view->model.e_dict,NIL(Dtdisc_t*),0);
    dtclose(view->model.e_dict);
    agfree(G,edgemapdisc);
    agfree(G,view);
    agclose(G);
}

/* model nodes */
Agnode_t *il_open_node(engview_t *view, ILnode_t *client) {
    Agraph_t	*model;
    Agnode_t	*n;
    engnode_t	*desc;

    model = view->model.main;
    n = agidnode(model,(unsigned long)client,TRUE);
    desc = (engnode_t *)agbindrec(n,ILrec_name,view->recsize[IL_RECSIZE_NODE],TRUE);
    desc->client = client;
    return n;
}

Agnode_t *il_find_node(engview_t *view, ILnode_t *node) {
    return agidnode(view->model.main,(unsigned long)node,FALSE);
}

void il_close_node(engview_t *view, ILnode_t *node) {
    Agnode_t	*n;

    if ((n = il_find_node(view,node)))
        agdelete(view->model.main,n);
}

/* model edges */
Agedge_t *il_open_edge(engview_t *view, ILedge_t *client) {
    Agraph_t	*model;
    Agnode_t	*u, *v;
    Agedge_t	*e;
    engedge_t	*desc;

    model = view->model.main;
    u = il_find_node(view,(ILnode_t*)(client->tail.term));
    v = il_find_node(view,(ILnode_t*)(client->head.term));

    if (u && v && ((e = agidedge(u,v,(unsigned long)client,TRUE)))) {
        assert (AGID(e) == (unsigned long)client);
        desc = (engedge_t *)agbindrec(e,ILrec_name,view->recsize[IL_RECSIZE_EDGE],TRUE);
        desc->client = client;	/* shallow copy OK for most */
        dtinsert(view->model.e_dict,e);
#if 0 /* suspected of memory leak */
        /* OLD DEBUG CODE TO REMOVE SOMEDAY */
        { Agedge_t *f;
            ILedge_t	*probe;
            probe = 0;
            il_find_edge(view,probe);
            probe = (ILedge_t*) (~(unsigned long)0);
            il_find_edge(view,probe);
            probe = client;
            f = il_find_edge(view,probe);
            assert (e == f);
        }
#endif

    } else
        e = NILedge;

    return e;
}

Agedge_t *il_find_edge(engview_t *view, ILedge_t *edge) {

    static Agedge_t	template

    ;	/* static to avoid purify warnings */

    template.base.tag.id = (unsigned long)
                           edge;

    return (Agedge_t*)dtsearch(view->model.e_dict,&template
                              )

           ;
}

void il_close_edge(engview_t *view, ILedge_t *edge) {
    Agedge_t	*e;

    if ((e = il_find_edge(view, edge))) {
        /* do not call il_freeshape on edge->pos because
         * it is the client's responsibility */
        /*
        il_freeshape(agheap(view->model.main),edge->pos);
        edge->pos = NIL(ilshape_t*);
        */
        dtdelete(view->model.e_dict,e);
        agdelete(view->model.main,e);
        assert(il_find_edge(view,edge) == NILedge);
    }
}

ILnode_t *ilnextnode(ILview_t *view, ILnode_t *spec) {
    Agnode_t	*model_n;
    ILnode_t	*rv;

    if (spec) {
        model_n = il_find_node(view->pvt,spec);

        if (model_n)
            model_n = agnxtnode(model_n);

    } else
        model_n = agfstnode(ilmodel(view));

    if (model_n)
        rv = (ILnode_t*)ilmdlobj_to_spec(view,(Agobj_t*)model_n);
    else
        rv = NIL(ILnode_t*);

    return rv;
}

ILedge_t *ilnextedge(ILview_t *view, ILnode_t *endpoint, ILedge_t *spec) {
    Agnode_t	*model_n;
    Agedge_t	*model_e;
    ILedge_t	*rv;

    model_n = il_find_node(view->pvt,endpoint);

    if (model_n == NILnode)
        rv = NIL(ILedge_t*);
    else {
        if (spec) {
            model_e = il_find_edge(view->pvt,spec);

            if (model_e)
                model_e = agnxtedge(model_e, model_n);

        } else
            model_e = agfstedge(model_n);

        if (model_e)
            rv = (ILedge_t*)ilmdlobj_to_spec(view,(Agobj_t*)model_e);
        else
            rv = NIL(ILedge_t*);
    }

    return rv;
}

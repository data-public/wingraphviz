#ifndef AGINCR_H
#define AGINCR_H

/* Abstract interface for an incremental layout engine.
 * "Downcall" means from the canvas manager to layout engine.
 * "Upcall" means a callback from layout engine to client.
 */

#ifdef HAVE_CONFIG_H
#include "gvconfig.h"
#endif

#include <agraph.h>

#ifdef HAVE_VALUES_H
#include <values.h>
#else
#include <limits.h>
#ifndef MAXINT
#define MAXINT INT_MAX
#endif
#include <float.h>
#ifndef MAXDOUBLE
#define MAXDOUBLE DBL_MAX
#endif
#ifndef MAXFLOAT
#define MAXFLOAT FLT_MAX
#endif
#endif

/* Coordinates and offsets are stored as floats.  The unit (pixels,
 * inches, centimeters, etc.) is entirely up to the client.  Layout
 * engines must work in any scale the client chooses.  The coordinate
 * system places the origin at the upper left-hand corner of the canvas.
 * Axes are positive right and down.
 */

typedef unsigned char ilbool;

typedef struct ilcoord_s {
    double x,y;
}

ilcoord_t;

typedef struct {
    ilcoord_t ll,ur;
}

ilrect_t;
typedef ilcoord_t	ildelta;	/* no _t ? */
typedef ilrect_t	ilbox;

typedef struct ilshape_s ilshape_t;

typedef struct ilcurve_s ilcurve_t;

/* Types of diagram objects. */
typedef enum {ILNODE=AGNODE, ILEDGE=AGEDGE, ILHYPER, ILVIEW=AGRAPH} ILtag_t;

/* Specifiers for nodes, edges, hyperedges, and layouts.
 * These descriptors are passed in both down- and up-calls.
 * The client is responsible for storage management.  The descriptor
 * must be kept live (and at the same address) from the time of new
 * object insertion until a delete callback is received.
 *
 * Client can add its own fields to the base structure.
 *
 * immutable - stays the same for the lifetime of the object.
 * mutable - may be updated by client or engine (unless otherwise restricted.)
 * client informs engine of changes by calling its mod() function.
 */

/* generic object/event specifier */

typedef struct ILobj_s {
    ILtag_t		tag;
}

ILobj_t;

#define	IL_UPD_MOVE			(1 << 0)
#define IL_UPD_NAIL			(1 << 1)
#define	IL_UPD_SHAPE		(1 << 2)
#define IL_UPD_TAIL			(1 << 3)
#define IL_UPD_HEAD			(1 << 4)
#define IL_UPD_LENGTH		(1 << 5)
#define IL_UPD_COST			(1 << 6)
#define IL_UPD_WIDTH		(1 << 7)
#define IL_UPD_CONSTRAINT	(1 << 8)
#define IL_UPD_NAILX		(1 << 9)
#define IL_UPD_NAILY		(1 << 10)

/* node event specifier */

typedef struct ILnode_s {
    ILobj_t		base;
    unsigned long	update;		/* owned by both, mutable				*/
    ilcoord_t	pos;		/* owned by both, mutable 				*/
    ilbool		pos_valid;	/* owned by both, mutable             	*/
    ilshape_t	*shape;		/* owned by application, resize allowed */
}

ILnode_t;

/* an endpoint of an edge or hyperedge */

typedef struct ILcon_s {	/* owned by application, immutable for now */
    ILobj_t		*term;		/* usually node, but may be other types */
    ilcoord_t	port;		/* offset from center of object			*/
    ilbool		clipped;	/* owned by application 				*/
}

ILcon_t;

/* edge specifier */

typedef struct ILedge_s {
    ILobj_t		base;		/* owned by application, immutable      */
    ILcon_t		tail, head;	/* owned by application, immutable      */
    float		width;		/* owned by application, mutable        */
    float		length_hint;/* owned by application, mutable		*/
    float		cost;		/* owned by application, mutable 		*/
    /* how to draw the edge- a hint in downcall, return value in upcall.*/
    ilshape_t	*pos;		/* owned by both, mutable 				*/
    ilbool		constraint;	/* owned by application, immutable		*/
    unsigned long	update;		/* owned by both, mutable				*/
}

ILedge_t;

typedef struct ILhyper_s {		/* this is not finished yet */
    ILobj_t		base;
    int			subtype;	/* 0 = primitive, others are engine dependent,  */
    ilshape_t	*pos;		/* how to draw */
    int			n;			/* number of terminals */
    ILcon_t		*V;			/* list of terminals */
}

ILhyper_t;

/* event/callback function */

typedef struct ILview_s ILview_t;		/* forward decl */
typedef ilbool (*ILevf_t)(ILview_t *view, ILobj_t *spec);

/* A client defines this structure to create a new view.  The client must
 * maintain this struct for the lifetime of the view, but it's OK to share
 * between multiple views.
 */

struct ILview_s {
    ILobj_t		base;			/* defined by clients, immutable. */

    /* If this flag is TRUE, the engine makes callbacks while
     * executing graph operations.  If FALSE, callbacks are
     * only given on demand.  This allows switching between
     * online and offline graph update.
     */
    ilbool		enable_immediate_callbacks;	/* owned by client, mutable */

    /* client call back functions */
    ILevf_t		ins;			/* owned by clients, mutable */
    ILevf_t		mod;			/* owned by clients, mutable */
    ILevf_t		del;			/* owned by clients, mutable */

    /* quanitization unit for coordinates */
    float		resolution;		/* owned by clients, immutable. */

    /* minimum separation between nodes */
    ildelta		separation;		/* owned by clients, immutable. */

    /* desired bounding box, if available, else (0,0) */
    ilcoord_t	max_bb;

    /* actual bounding box */
    ilrect_t	actual_bb;

    /* a broad hint about time spent per iteration */
    float		ticks;			/* owned by clients, mutable */

    struct engview_s *pvt;			/* owned by engines, immutable */
};

/* Interface to a layout engine. */

typedef struct ILengine_s {
    ilbool		(*open)(ILview_t *view);
    void		(*close)(ILview_t *view);

    ILevf_t		ins;
    ILevf_t		mod;
    ILevf_t		del;

    /* Issue all pending callbacks.  Return TRUE if there were any. */
    ilbool		(*callback)(ILview_t *view);

    /* Look up spec from model node, edge, or subgraph */
    ILobj_t		*(*mdlobj_to_spec)(ILview_t *view, Agobj_t *model_obj);

    Agobj_t 	*(*spec_to_mdlobj)(ILview_t *view, ILobj_t *spec);
}

ILengine_t;

/* function dispatch utilities */
typedef ilbool (*ILnodefn_t)(ILview_t *view, ILnode_t *obj);
typedef ilbool (*ILedgefn_t)(ILview_t *view, ILedge_t *obj);
typedef ilbool (*ILhyperfn_t)(ILview_t *view, ILhyper_t *obj);
typedef ilbool (*ILviewfn_t)(ILview_t *view, ILview_t *obj);

typedef struct ILfnlist_s {
    ILnodefn_t	n;
    ILedgefn_t	e;
    ILhyperfn_t	m;
    ILviewfn_t	v;
}

ILfnlist_t;

ilbool			ildispatch(ILview_t *view, ILobj_t *spec, ILfnlist_t *f);

/* Services provided by an incremental layout engine. */
/* These are stubs that invoke engine functions. */
/*   ...Maybe these should be deleted. */
ilbool			ilopen(ILengine_t *engine, ILview_t *view);
void			ilclose(ILview_t *view);

ilbool			ilinsert(ILview_t *view, ILobj_t *spec);
ilbool			ilmodify(ILview_t *view, ILobj_t *spec);
ilbool			iloptimize(ILview_t *view, ILobj_t *spec);	/* deprecated */
ilbool			ildelete(ILview_t *view, ILobj_t *spec);
ilbool			ilcallback(ILview_t*);
Agraph_t       	*ilmodel(ILview_t *view);	/* special case of ... */
Agobj_t			*ilspec_to_mdlobj(ILview_t *view, ILobj_t *spec);
ILobj_t			*ilmdlobj_to_spec(ILview_t *view, Agobj_t *obj);

ILnode_t		*ilnextnode(ILview_t *view, ILnode_t *spec);
ILedge_t		*ilnextedge(ILview_t *view, ILnode_t *endpoint, ILedge_t *spec);

#endif	/* AGINCR_H */

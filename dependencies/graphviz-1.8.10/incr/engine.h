#ifndef ENGINE_H
#define ENGINE_H

/* basic services (e.g. callback manager) for all engines */

#include <incr.h>
#include <agraph.h>
#include <shape.h>

#ifndef offsetof
#define offsetof(typ,fld)  ((int)(&(((typ*)0)->fld)))
#endif

#define ROUND(f)        ((f>=0)?(int)(f + .5):(int)(f - .5))

#ifndef NIL
#define NIL(t)		((t)0)
#endif
#define NILnode		NIL(Agnode_t*)
#define NILedge		NIL(Agedge_t*)
#define NILgraph	NIL(Agraph_t*)
#define NILstr		NIL(char*)

#define IL_INS		0
#define IL_MOD		1
#define IL_DEL		2
#define IL_N_OPS	3

extern char ILrec_name[];

typedef struct bb_s {	/* bounding box */
    ilrect_t	size;
    ilbool		valid;
}

bb_t;


typedef struct ILmodel_s {
    Agraph_t	*main;				/* main graph */
    Agraph_t	*v[IL_N_OPS],*e[IL_N_OPS];	/* pending node and edge callbacks */
    Dict_t		*e_dict;			/* map ILedge_t poitner to (main) edge */
}

ILmodel_t ;

#define IL_RECSIZE_VIEW	0
#define IL_RECSIZE_NODE 1
#define IL_RECSIZE_EDGE	2
#define IL_RECSIZE_N 	3

typedef struct engview_s {	/* base class for model graph attributes */
    Agrec_t			h;
    ILview_t		*client;
    ILengine_t		*engine;
    ILmodel_t		model;	/* client's model graph and callback sets */
    unsigned int	recsize[IL_RECSIZE_N];	/* for allocation */
    bb_t			bb;
}

engview_t ;

typedef struct engnode_s {	/* base clase for model node attributes */
    Agrec_t			base;
    ILnode_t			*client;
    bb_t				bb;
}

engnode_t;

typedef struct engedge_s {	/* base class for model edge attributes */
    Agrec_t			h;
    ILedge_t		*client;
}

engedge_t;

/* data access */
engnode_t 		*il_nd(Agnode_t *n);
engedge_t		*il_ed(Agedge_t *e);
ILnode_t		*il_node(Agnode_t*);
ILedge_t		*il_edge(Agedge_t*);

/* views */
engview_t		*il_open_view(ILengine_t *eng, ILview_t *client, Agdesc_t desc, unsigned int *rec_size);
void 			il_close_view(engview_t *view);

/* batch operations */
ilbool			il_batch_ins(ILview_t *view, ILobj_t *spec);
ilbool			il_batch_mod(ILview_t *view, ILobj_t *spec);
ilbool			il_batch_del(ILview_t *view, ILobj_t *spec);

/* nodes */
Agnode_t		*il_find_node(engview_t *view, ILnode_t *node);
Agnode_t		*il_open_node(engview_t *view, ILnode_t *node);
void			il_close_node(engview_t *view, ILnode_t *node);

/* edges */
Agedge_t		*il_find_edge(engview_t *view, ILedge_t *edge);
Agedge_t		*il_open_edge(engview_t *view, ILedge_t *edge);
void			il_close_edge(engview_t *view, ILedge_t *edge);

/* node and edge placement */
ilcoord_t		il_pos(Agnode_t *n);
void			il_set_pos(Agnode_t *n, ilcoord_t pos);
ilcoord_t		il_nodesize(engview_t *view, Agnode_t *node);
void			il_set_nodesize(Agnode_t *n, ilcoord_t size);
ilcoord_t		il_nodesep(engview_t *view);

/* this goes into shapes? - curves */
ilcoord_t		Bezier (ilcoord_t *V, int degree, double t,
                   ilcoord_t *Left, ilcoord_t *Right);
ilbool			il_test_y_intersection(ilcurve_t *spl, double y, ilcoord_t *rv);
ilcoord_t		il_intersect_at_y(ilcurve_t *spl, double y);
int				il_get_seg(ilcurve_t *spl, double y);
ilcurve_t		*il_clip_endpoints(engview_t *view, ilcurve_t *curve,
                              ILnode_t *tl, ILnode_t *hd);
ilcurve_t		*il_getcurve(Agedge_t *e);
void			ilroutem(ILview_t *view);

/* callbacks */
void			il_register_node_callback(engview_t *view, ILnode_t *spec, int kind);
void			il_register_edge_callback(engview_t *view, ILedge_t *spec, int kind);
ilbool			il_issue_callbacks(engview_t *view);
void			il_clear_callbacks(engview_t *view);

/* trivial ID discipline (for dynadag) */

extern Agdisc_t IL_graph_id_disc;

extern Dtdisc_t IL_dict_id_disc;

/* for scanning dot attributes */

typedef struct ildotspline_s {

    struct {
        ilbool		arrow;
        ilcoord_t	coord;
    }

    start, end;
    ilcurve_t	*spline;
}

ildotspline_t;

#endif

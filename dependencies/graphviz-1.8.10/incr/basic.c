/* Common functions for layout engines. */

#include <engine.h>

#ifdef DMALLOC
#include "dmalloc.h"
#endif

engnode_t *il_nd(Agnode_t *n) {
    return (engnode_t*)AGDATA(n);
}

engedge_t *il_ed(Agedge_t *e) {
    return (engedge_t*)AGDATA(e);
}

ILnode_t *il_node(Agnode_t *n) {
    return ((engnode_t*)(AGDATA(n)))->client;
}

ILedge_t *il_edge(Agedge_t *e) {
    return ((engedge_t*)(AGDATA(e)))->client;
}

ilcoord_t il_pos(Agnode_t *n) {
    return il_nd(n)->client->pos;
}

void il_set_pos(Agnode_t *n, ilcoord_t pos) {
    il_nd(n)->client->pos = pos;
}

double il_edgewidth(engview_t *view, Agedge_t *e) {
    ILedge_t    *spec;

    spec = il_edge(e);

    if (spec)
        return spec->width;
    else
        return 0.0;
}

ilcoord_t il_nodesize(engview_t *view, Agnode_t *node) {
    engnode_t              *nd;
    ilcoord_t               rv;

    if ((node) && ((nd = il_nd(node)))) {
        if (NOT(nd->bb.valid)) {
            if (nd->client->shape)
                nd->bb.size = il_get_bounding_rect(nd->client->shape);
            else {
                nd->bb.size.ll.x = nd->bb.size.ll.y = nd->bb.size.ur.y = 0.0;
                nd->bb.size.ur.x = 2 * view->client->separation.x;
            }

            nd->bb.valid = TRUE;
        }

        rv.x = nd->bb.size.ur.x - nd->bb.size.ll.x;
        rv.y = nd->bb.size.ur.y - nd->bb.size.ll.y;

    } else {
        rv.x = rv.y = 0.0;
    }

    return rv;
}

ilcoord_t il_nodesep(engview_t *view) {
    return view->client->separation;
}

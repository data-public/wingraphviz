#include <engine.h>
#include <math.h>
#define EPSILON	(.005)

#ifdef DMALLOC
#include "dmalloc.h"
#endif

static int seg_size(ilcurve_t *curve) {
    switch (curve->type) {

            case IL_SPLINE :
            return 4;

            case IL_POLYLINE :
            return 2;

            case IL_NOCURVE :
            break;
    }

    abort();
    return 0;
}

static ilbool pt_eq(ilcoord_t p, ilcoord_t q) {
    return ((p.x == q.x) && (p.y == q.y));
}

static ilcoord_t sub_pt(ilcoord_t p0, ilcoord_t p1) {
    ilcoord_t	rv;
    rv.x = p0.x - p1.x;
    rv.y = p0.y - p1.y;
    return rv;
}

static void install_seg(ilcoord_t *src, int n, ilcurve_t *result) {
    int		i;

    if (result->n == 0)
        result->p[result->n++] = src[0];
    else {
        /* check that curve is continuous */
        assert(pt_eq(result->p[result->n - 1], src[0]));
    }

    for (i = 1; i < n; i++)
        result->p[result->n++] = src[i];
}

#define W_DEGREE 5
ilcoord_t Bezier (ilcoord_t *V, int degree, double t,
                  ilcoord_t *Left, ilcoord_t *Right) {
    int			i, j;	/* Index variables  */
    ilcoord_t	Vtemp[W_DEGREE + 1][W_DEGREE + 1];

    /* Copy control points  */

    for (j = 0; j <= degree; j++) {
        Vtemp[0][j] = V[j];
    }

    /* Triangle computation */
    for (i = 1; i <= degree; i++) {
        for (j = 0; j <= degree - i; j++) {
            Vtemp[i][j].x =
                (1.0 - t) * Vtemp[i-1][j].x + t * Vtemp[i-1][j+1].x;
            Vtemp[i][j].y =
                (1.0 - t) * Vtemp[i-1][j].y + t * Vtemp[i-1][j+1].y;
        }
    }

    if (Left != NIL(ilcoord_t*))
        for (j = 0; j <= degree; j++)
            Left[j] = Vtemp[j][0];

    if (Right != NIL(ilcoord_t*))
        for (j = 0; j <= degree; j++)
            Right[j] = Vtemp[degree-j][j];

    return (Vtemp[degree][0]);
}

static ilbool inshape(ILnode_t *spec, ilcoord_t coord) {
    return il_inshape(spec->shape,sub_pt(coord,spec->pos));
}

static ilcoord_t *clip(ilcoord_t *A, int n, ILnode_t *node, ilcoord_t *out) {
    double		high, low, t;
    ilcoord_t	p,*lowseg,*highseg,theseg[4];
    int			i;
    ilbool		inside, low_inside, high_inside;
    ilcoord_t	best[4];

    high = 1.0;
    low = 0.0;
    low_inside = inshape(node,A[0]);
    high_inside = inshape(node,A[n-1]);

    if (low_inside == high_inside)
        return A;

    if (low_inside) {
        highseg = theseg;
        lowseg = NIL(ilcoord_t*);

    } else {
        lowseg = theseg;
        highseg = NIL(ilcoord_t*);
    }

    do {
        t = (high + low) / 2.0;
        p = Bezier(A,n-1,t,lowseg,highseg);
        inside = inshape(node,p);

        if (NOT(inside)) {
            for (i = 0; i < n; i++)
                best[i] = theseg[i];
        }

        if (inside == low_inside)
            low = t;
        else
            high = t;

    } while (high - low > EPSILON);	/* should be adaptive with resolution */

    for (i = 0; i < n; i++)
        out[i] = best[i];

    return out;
}

ilcurve_t *il_clip_endpoints(engview_t *view, ilcurve_t *curve, ILnode_t *tl, ILnode_t *hd) {
    int			i, n, segsz, fst_i, lst_i;
    ilcurve_t	*result;
    ilcoord_t	*section,temp[4];

    result = il_newcurve(agheap(view->model.main),curve->type,curve->n);
    segsz = seg_size(curve);
    n = curve->n;

    /* find terminal sections */

    for (fst_i = 0; fst_i < n; fst_i = fst_i + (segsz - 1))
        if (NOT(inshape(tl,curve->p[fst_i + segsz - 1])))
            break;

    for (lst_i = n - segsz; lst_i >= 0; lst_i = lst_i - (segsz - 1))
        if (NOT(inshape(hd,curve->p[lst_i])))
            break;

    for (i = fst_i; i <= lst_i; i = i + segsz - 1) {
        section = &(curve->p[i]);

        if (i == fst_i)
            section = clip(section,segsz,tl,temp);

        if (i == lst_i)
            section = clip(section,segsz,hd,temp);

        install_seg(section,segsz,result);
    }

    return result;
}

int il_get_seg(ilcurve_t *curve, double y) {
    int             i,i0;
    int				sz;
    double			y0,y1;

    sz = seg_size(curve) - 1;

    for (i = 0; i < curve->n - 1; i += sz) {
        for (i0 = i; i0 < i + sz; i0++) {
            y0 = curve->p[i0].y;
            y1 = curve->p[i0+1].y;

            if (((y0 <= y) && (y <= y1)) || ((y1 <= y) && (y <= y0)))
                return i;
        }
    }

    return -1;
}

ilcoord_t il_intersect_at_y(ilcurve_t *curve, double y) {
    ilcoord_t	rv;

    if (il_test_y_intersection(curve, y, &rv) == FALSE)
        abort();

    return rv;
}

ilbool il_test_y_intersection(ilcurve_t *curve, double y, ilcoord_t *rv) {
    double	high, low, t, abs_d;
    double	d, abs_dd;
    int		i, seg, sz;
    ilcoord_t	p,q;

    if (curve->n <= 0)
        return FALSE;

    for (i = 0; i < curve->n; i += curve->n - 1)
        if (curve->p[i].y == y) {
            *rv = curve->p[i];
            return TRUE;
        }

    if (curve->p[curve->n-1].y == y) {
        *rv = curve->p[0];
        return TRUE;
    }

    sz = seg_size(curve) - 1;
    seg = il_get_seg(curve,y);

    if (seg < 0)
        return FALSE;

    if (curve->p[seg].y < curve->p[seg+sz].y) {
        high = 1.0;
        low = 0.0;

    } else  {
        high = 0.0;
        low = 1.0;
    }

    abs_dd = MAXDOUBLE;

    do {
        t = (high + low) / 2.0;
        p = Bezier(&(curve->p[seg]),sz,t,NIL(ilcoord_t *),NIL(ilcoord_t *));
        d = p.y - y;
        abs_d = fabs(d);

        if (abs_d < abs_dd) {
            q = p;
            abs_dd = abs_d;
        }

        if (d > 0)
            high = t;
        else
            low = t;

    } while (fabs(high - low) > .01); /* should be adaptive */

    *rv = q;

    return TRUE;
}

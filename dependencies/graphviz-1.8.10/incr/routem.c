#include <engine.h>
#include <pathplan.h>
#include <vispath.h>
#include <stdio.h>	/* temporary for debugging */
#include <math.h>

#ifdef DMALLOC
#include "dmalloc.h"
#endif

typedef struct {
    Ppoly_t **list;
    int		n;
}

polylist;

typedef struct {
    Pedge_t	*list;
    int		n;
}

seglist;

static Ppoint_t cvt2Ppoint(ilcoord_t p) {
    Ppoint_t rv;
    rv.x = p.x;
    rv.y = p.y;
    return rv;
}

static ilcurve_t *cvt2ilcurve(Ppolyline_t spline) {
    ilcurve_t	*rv;
    int			i;

    rv = il_newcurve(vmregion(spline.ps), IL_SPLINE, spline.pn);

    for (i = 0; i < spline.pn; i++) {
        rv->p[i].x = spline.ps[i].x;
        rv->p[i].y = spline.ps[i].y;
    }

    rv->n = spline.pn;
    return rv;
}

static polylist buildpolylist(ILview_t *view) {
    Ppoly_t 	*obs,**lst;
    int			i, pi, nnodes, sides;
    ILnode_t	*nspec;
    ilshape_t	*shape;
    polylist	rv;
    Ppoint_t	cntr;
    double		adj;

    nnodes = agnnodes(ilmodel(view));
    obs = malloc(nnodes * sizeof(obs[0]));
    lst = malloc(nnodes * sizeof(lst[0]));
    i = 0;
    adj = drand48() * 0.01;
    nspec = NIL(ILnode_t*);

    while ((nspec = ilnextnode(view,nspec))) {
        for (shape = nspec->shape; shape; shape = shape->next) {
            if (shape->type == IL_POLYGON)
                sides = shape->def.curve.n;
            else /* assume ellipse */
                sides = 8; /* ha */

            lst[i] = &(obs[i]);

            obs[i].pn = sides;

            obs[i].ps = vmalloc(agheap(ilmodel(view)),sizeof(Ppoint_t)*obs[i].pn);

            cntr.x = cntr.y = 0.0;

            if (shape->type == IL_POLYGON) {
                for (pi = 0; pi < obs[i].pn; pi++) {
                    cntr.x += shape->def.curve.p[pi].x;
                    cntr.y += shape->def.curve.p[pi].y;
                }

                cntr.x = cntr.x /  obs[i].pn;
                cntr.y = cntr.y /  obs[i].pn;
            }

            for (pi = 0; pi < obs[i].pn; pi++) {
                if (shape->type == IL_POLYGON) {
                    obs[i].ps[pi].x = nspec->pos.x + 1.1 * shape->def.curve.p[pi].x;
                    obs[i].ps[pi].y = nspec->pos.y + 1.1 * shape->def.curve.p[pi].y;

                } else {
                    double  c, s;
                    c = cos(2.0 * M_PI * pi / sides + adj);
                    s = sin(2.0 * M_PI * pi / sides + adj);
                    obs[i].ps[pi].x = nspec->pos.x + c * 1.1 * shape->def.ellipse.radius_a;
                    obs[i].ps[pi].y = nspec->pos.y + c * 1.1 * shape->def.ellipse.radius_b;
                }
            }

            i++;
            break;
        }
    }

    assert(i == nnodes);
    rv.list = lst;
    rv.n = nnodes;
    return rv;
}

static int horrible_find_polyid(ILview_t *view, ILnode_t *target) {
    ILnode_t	*nspec;
    int			i;

    nspec = NIL(ILnode_t*);
    i = 0;

    while ((nspec = ilnextnode(view,nspec))) {
        if (nspec == target)
            return i;

        i++;
    }

    return POLYID_NONE;
}

static void route_edges_of(ILview_t *view, Agraph_t *subg) {
    Agnode_t	*n;
    Agedge_t	*e;
    ILnode_t	*tl, *hd;
    polylist	obs;
    Ppolyline_t	polyline;
    Ppolyline_t	spline;
    ilcurve_t	*unclipped;
    ilcurve_t	*clipped_spline;
    Ppoint_t	p[2];
    Pvector_t	endslope[2];
    ILedge_t 	*espec;
    seglist		barriers;
    vconfig_t	*vc;
    int		id0,id1;

    vc = NIL(vconfig_t*);

    for (n = agfstnode(subg); n; n = agnxtnode(n)) {
        for (e = agfstout(n); e; e = agnxtout(e)) {
            if (vc == NIL(vconfig_t*)) {
                obs = buildpolylist(view);
                vc = Pobsopen(obs.list, obs.n);
                (void) Ppolybarriers(obs.list, obs.n, &barriers.list, &barriers.n);
            }

            espec = il_edge(e);
            tl= ((ILnode_t*)(espec->tail.term));
            hd = ((ILnode_t*)(espec->head.term));
            p[0] = cvt2Ppoint(tl->pos);
            p[1] = cvt2Ppoint(hd->pos);
            endslope[0].x = endslope[0].y = 0.0;
            endslope[1].x = endslope[1].y = 0.0;

            /* when erg fixes Pobspath, this will be unnecessary */
            id0 = horrible_find_polyid(view,tl);
            id1 = horrible_find_polyid(view,hd);
            Pobspath(vc, p[0], id0, p[1], id1, &polyline);
            (void) Proutespline(barriers.list, barriers.n, polyline, endslope, &spline);
            unclipped = cvt2ilcurve(spline);
            clipped_spline = il_clip_endpoints(view->pvt,unclipped,tl,hd);

#if 0

            {int i;
                fprintf(stderr,"0 1 0 setrgbcolor\nnewpath");

                for (i = 0; i < clipped_spline->n; i++) {
                    fprintf(stderr," %.3lf %.3lf",
                            clipped_spline->p[i].x,clipped_spline->p[i].y);

                    if (i == 0)
                        fprintf(stderr," moveto");
                    else if (i % 3 == 0)
                        fprintf(stderr," curveto");
                }

                fprintf(stderr," stroke\n");
            }
#endif

            if (espec->pos)
                il_freeshape(vmregion(espec->pos),espec->pos);

            espec->pos = il_newshape(agheap(ilmodel(view)),clipped_spline,NIL(ilshape_t*));
        }
    }
}

#ifdef TIMING
#include <sys/time.h>
#include <sys/resource.h>
#endif

void ilroutem(ILview_t *view) {

#ifdef TIMING

    struct rusage ru0, ru1;
    getrusage(RUSAGE_SELF, &ru0);
#endif

    route_edges_of(view,view->pvt->model.e[IL_INS]);
    route_edges_of(view,view->pvt->model.e[IL_MOD]);
#ifdef TIMING

    getrusage(RUSAGE_SELF, &ru1);
    fprintf(stderr,"%% timing %d usecs\n",
            ru1.ru_utime.tv_usec - ru0.ru_utime.tv_usec);
#endif
}

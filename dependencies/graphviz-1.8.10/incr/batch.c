#include <engine.h>

#ifdef DMALLOC
#include "dmalloc.h"
#endif

/*
 * maintain buffers of pending requests (in callback subgraphs)
 * note that certain sequences of operations can reduce:
 * ins mod => ins
 * [ins mod] del => (empty)
 */

ilbool il_batch_delete_node(ILview_t *view, ILnode_t *spec);
ilbool il_batch_delete_edge(ILview_t *view, ILedge_t *spec);

ilbool il_batch_insert_node(ILview_t *view, ILnode_t *spec) {
    engview_t	*ev;
    Agnode_t	*n;
    ilbool		rv;

    ev = (engview_t*)(view->pvt);

    if (il_find_node(ev,spec) == NILnode) {
        n = il_open_node(ev,spec);
        agsubnode(ev->model.v[IL_INS],n,TRUE);
        rv = TRUE;

    } else
        rv = FALSE; /* can't insert pre-existing nodes */

    return rv;
}

ilbool il_batch_modify_node(ILview_t *view, ILnode_t *spec) {
    engview_t	*ev;
    Agnode_t	*n, *subnode;
    ilbool		rv;

    ev = (engview_t*)(view->pvt);

    if ((n = il_find_node(ev,spec))) {
        if ((agsubnode(ev->model.v[IL_INS],n,FALSE)) == NILnode) {
            subnode = agsubnode(ev->model.v[IL_MOD],n,TRUE);

            if (spec->update & IL_UPD_SHAPE)
                il_nd(subnode)->bb.valid = FALSE;
        }

        /* else fold  ins mode -> mod */
        rv = TRUE;

    } else
        rv = FALSE; /* can't modify non-existent nodes */

    return rv;
}

ilbool il_batch_delete_node(ILview_t *view, ILnode_t *spec) {
    engview_t	*ev;
    Agnode_t	*n, *subnode;
    Agedge_t	*e;
    ilbool		rv;

    ev = (engview_t*)(view->pvt);

    if ((n = il_find_node(ev,spec))) {
        /* implies deletion of adjacent edges */

        for (e = agfstedge(n); e; e = agnxtedge(e,n))
            il_batch_delete_edge(view, il_edge(e));

        if ((subnode = agsubnode(ev->model.v[IL_INS],n,FALSE))) {
            /* fold  ins [mod] del => (empty), i.e. cancel in buffer */
            il_close_node(ev, spec);

        } else {
            /* fold  mod del => del */

            if ((subnode = agsubnode(ev->model.v[IL_MOD],n,FALSE)))
                agdelnode(subnode);

            /* else no mods are pending */
            agsubnode(ev->model.v[IL_DEL],n,TRUE);
        }

        rv = TRUE;

    } else
        rv = FALSE; /*can't delete non-existent nodes */

    return rv;
}

/* edges are the same.  if we adoped a cleaner/more functional
 * coding style, we could just merge all this code.
 */
ilbool il_batch_insert_edge(ILview_t *view, ILedge_t *spec) {
    engview_t	*ev;
    Agedge_t	*e;
    ilbool		rv;

    ev = (engview_t*)(view->pvt);

    if (il_find_edge(ev,spec) == NILedge) {
        e = il_open_edge(ev,spec);

        if (e) {
            agsubedge(ev->model.e[IL_INS],e,TRUE);
            assert(agnedges(ev->model.e[IL_INS]) > 0);
            rv = TRUE;

        } else
            rv = FALSE;

    } else
        rv = FALSE; /* can't insert pre-existing edges */

    return rv;
}

ilbool il_batch_modify_edge(ILview_t *view, ILedge_t *spec) {
    engview_t	*ev;
    Agedge_t	*e, *subedge;
    ilbool		rv;

    ev = (engview_t*)(view->pvt);

    if ((e = il_find_edge(ev,spec))) {
        if (agsubedge(ev->model.e[IL_INS],e,FALSE) == NILedge)
            subedge = agsubedge(ev->model.e[IL_MOD],e,TRUE);

        /* else fold  ins mod => ins  */
        rv = TRUE;

    } else
        rv = FALSE; /* can't modify non-existent edges */

    return rv;
}

ilbool il_batch_delete_edge(ILview_t *view, ILedge_t *spec) {
    engview_t	*ev;
    Agedge_t	*e, *subedge;
    ilbool		rv;

    ev = (engview_t*)(view->pvt);

    if ((e = il_find_edge(ev,spec))) {
        if ((subedge = agsubedge(ev->model.e[IL_INS],e,FALSE))) {
            /* fold  ins [mod] del => (empty), i.e. cancel in buffer */
            il_close_edge(ev, spec);

        } else {
            /* fold  mod del => del */

            if ((subedge = agsubedge(ev->model.e[IL_MOD],e,FALSE)))
                agdeledge(subedge);

            /* else no mods are pending */
            agsubedge(ev->model.e[IL_DEL],e,TRUE);
        }

        rv = TRUE;

    } else
        rv = FALSE; /* can't delete non-existent edges */

    return rv;
}

static void cbcheck(ILview_t *view) {
    if (view->enable_immediate_callbacks)
        ((engview_t*)(view->pvt))->engine->callback(view);
}

ilbool il_batch_ins(ILview_t *view, ILobj_t *spec) {
    static ILfnlist_t f = {
                              (ILnodefn_t)il_batch_insert_node,
                              (ILedgefn_t)il_batch_insert_edge,
                              NIL(ILhyperfn_t),
                              NIL(ILviewfn_t)
                          };
    ilbool	rv;
    rv = ildispatch(view,spec,&f);
    cbcheck(view);
    return rv;
}

ilbool il_batch_mod(ILview_t *view, ILobj_t *spec) {
    static ILfnlist_t f = {
                              (ILnodefn_t)il_batch_modify_node,
                              (ILedgefn_t)il_batch_modify_edge,
                              NIL(ILhyperfn_t),
                              NIL(ILviewfn_t)
                          };
    ilbool	rv;
    rv = ildispatch(view,spec,&f);
    cbcheck(view);
    return rv;
}

ilbool il_batch_del(ILview_t *view, ILobj_t *spec) {
    static ILfnlist_t f = {
                              (ILnodefn_t)il_batch_delete_node,
                              (ILedgefn_t)il_batch_delete_edge,
                              NIL(ILhyperfn_t),
                              NIL(ILviewfn_t)
                          };
    ilbool	rv;
    rv = ildispatch(view,spec,&f);
    cbcheck(view);
    return rv;
}

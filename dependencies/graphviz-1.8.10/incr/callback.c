/* Common callback management for incremental layout engines. */

#include <engine.h>
#include <math.h>

#ifdef DMALLOC
#include "dmalloc.h"
#endif

static ilbool do_node_cb(engview_t *view, Agraph_t *set
                         , ILevf_t fn) {
    Agnode_t	*n;
    ILnode_t	*nd;
    ilbool		rv = FALSE;

    if (fn)
        for (n = agfstnode(set
                          ); n; n = agnxtnode(n)) {
            rv = TRUE;
            nd = il_node(n);
            nd->update = 0;
            fn(view->client,ilmdlobj_to_spec(view->client,(Agobj_t*)n));
            /* if fn() was a delete then the application may
               have free'd n. So don't attempt anything with n
               from here on. */
        }

    return rv;
}

static ilbool do_edge_cb(engview_t *view, Agraph_t *set
                         , ILevf_t fn) {
    Agnode_t	*n;
    Agedge_t	*e;
    ILedge_t	*ed;
    ilbool		rv = FALSE;

    if (fn)
        for (n = agfstnode(set
                          ); n; n = agnxtnode(n)) {
            for (e = agfstout(n); e; e = agnxtout(e)) {
                rv = TRUE;
                ed = il_edge(e);
                ed->update = 0;
                fn(view->client,ilmdlobj_to_spec(view->client,(Agobj_t*)e));
                /* if fn() was a delete then the application may
                  have free'd e. So don't attempt anything with e
                  from here on. */
            }
        }

    return rv;
}

ilbool il_issue_callbacks(engview_t *view) {
    ilbool		rv;

    rv = FALSE;
    rv |= do_node_cb(view,view->model.v[IL_INS],view->client->ins);
    rv |= do_edge_cb(view,view->model.e[IL_INS],view->client->ins);
    rv |= do_node_cb(view,view->model.v[IL_MOD],view->client->mod);
    rv |= do_edge_cb(view,view->model.e[IL_MOD],view->client->mod);
    /*
      Edge deletes must be done before node deletes so that the client of
      the callbacks doesn't transiently see a state of dangling edges.
     */
    rv |= do_edge_cb(view,view->model.e[IL_DEL],view->client->del);
    rv |= do_node_cb(view,view->model.v[IL_DEL],view->client->del);
    return rv;
}

static void empty(Agraph_t *g) {
    Agnode_t	*u, *v;
    Agedge_t	*e;

    for (u = agfstnode(g); u; u = v) {
        v = agnxtnode(u);
        /* clear the update request flags */
        il_node(u)->update = 0;

        for (e = agfstout(u); e; e = agnxtout(e))
            il_edge(e)->update = 0;

        agdelete(g,u);
    }
}

static void nuke_nodes(engview_t *view, Agraph_t *g) {
    Agnode_t	*u, *v;

    for (u = agfstnode(g); u; u = v) {
        v = agnxtnode(u);
        il_close_node(view,il_node(u));
    }
}

static void nuke_edges(engview_t *view, Agraph_t *g) {
    Agnode_t	*u;
    Agedge_t	*e, *f;

    for (u = agfstnode(g); u; u = agnxtnode(u)) {
        for (e = agfstedge(u); e; e = f) {
            f = agnxtedge(e,u);
            il_close_edge(view,il_edge(e));
        }
    }
}

void il_clear_callbacks(engview_t *view) {
    int		i;

    for (i = 0; i < IL_N_OPS; i++) {
        if (i != IL_DEL) {
            empty(view->model.e[i]);
            empty(view->model.v[i]);

        } else {
            nuke_edges(view,view->model.e[i]);
            nuke_nodes(view,view->model.v[i]);
        }
    }
}

# Microsoft Developer Studio Generated NMAKE File, Based on libcdt.dsp
!IF "$(CFG)" == ""
CFG=libcdt - Win32 Debug
!MESSAGE No configuration specified. Defaulting to libcdt - Win32 Debug.
!ENDIF 

!IF "$(CFG)" != "libcdt - Win32 Release" && "$(CFG)" != "libcdt - Win32 Debug"
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "libcdt.mak" CFG="libcdt - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "libcdt - Win32 Release" (based on "Win32 (x86) Static Library")
!MESSAGE "libcdt - Win32 Debug" (based on "Win32 (x86) Static Library")
!MESSAGE 
!ERROR An invalid configuration is specified.
!ENDIF 

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE 
NULL=nul
!ENDIF 

CPP=cl.exe
RSC=rc.exe

!IF  "$(CFG)" == "libcdt - Win32 Release"

OUTDIR=.\Release
INTDIR=.\Release

ALL : "..\lib\Release\libcdt.lib"


CLEAN :
	-@erase "$(INTDIR)\dtclose.obj"
	-@erase "$(INTDIR)\dtdisc.obj"
	-@erase "$(INTDIR)\dtextract.obj"
	-@erase "$(INTDIR)\dtflatten.obj"
	-@erase "$(INTDIR)\dthash.obj"
	-@erase "$(INTDIR)\dtlist.obj"
	-@erase "$(INTDIR)\dtmethod.obj"
	-@erase "$(INTDIR)\dtopen.obj"
	-@erase "$(INTDIR)\dtrenew.obj"
	-@erase "$(INTDIR)\dtrestore.obj"
	-@erase "$(INTDIR)\dtsize.obj"
	-@erase "$(INTDIR)\dtstat.obj"
	-@erase "$(INTDIR)\dtstrhash.obj"
	-@erase "$(INTDIR)\dttree.obj"
	-@erase "$(INTDIR)\dtview.obj"
	-@erase "$(INTDIR)\dtwalk.obj"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "..\lib\Release\libcdt.lib"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP_PROJ=/nologo /MT /W3 /GX /O1 /D "NDEBUG" /D "HAVE_CONFIG_H" /D "WIN32" /D "_MBCS" /D "_LIB" /D "MSWIN32" /Fp"$(INTDIR)\libcdt.pch" /YX /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /c 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\libcdt.bsc" 
BSC32_SBRS= \
	
LIB32=link.exe -lib
LIB32_FLAGS=/nologo /out:"..\lib\Release\libcdt.lib" 
LIB32_OBJS= \
	"$(INTDIR)\dtclose.obj" \
	"$(INTDIR)\dtdisc.obj" \
	"$(INTDIR)\dtextract.obj" \
	"$(INTDIR)\dtflatten.obj" \
	"$(INTDIR)\dthash.obj" \
	"$(INTDIR)\dtlist.obj" \
	"$(INTDIR)\dtmethod.obj" \
	"$(INTDIR)\dtopen.obj" \
	"$(INTDIR)\dtrenew.obj" \
	"$(INTDIR)\dtrestore.obj" \
	"$(INTDIR)\dtsize.obj" \
	"$(INTDIR)\dtstat.obj" \
	"$(INTDIR)\dtstrhash.obj" \
	"$(INTDIR)\dttree.obj" \
	"$(INTDIR)\dtview.obj" \
	"$(INTDIR)\dtwalk.obj"

"..\lib\Release\libcdt.lib" : "$(OUTDIR)" $(DEF_FILE) $(LIB32_OBJS)
    $(LIB32) @<<
  $(LIB32_FLAGS) $(DEF_FLAGS) $(LIB32_OBJS)
<<

!ELSEIF  "$(CFG)" == "libcdt - Win32 Debug"

OUTDIR=.\Debug
INTDIR=.\Debug

ALL : "..\lib\Debug\libcdt.lib"


CLEAN :
	-@erase "$(INTDIR)\dtclose.obj"
	-@erase "$(INTDIR)\dtdisc.obj"
	-@erase "$(INTDIR)\dtextract.obj"
	-@erase "$(INTDIR)\dtflatten.obj"
	-@erase "$(INTDIR)\dthash.obj"
	-@erase "$(INTDIR)\dtlist.obj"
	-@erase "$(INTDIR)\dtmethod.obj"
	-@erase "$(INTDIR)\dtopen.obj"
	-@erase "$(INTDIR)\dtrenew.obj"
	-@erase "$(INTDIR)\dtrestore.obj"
	-@erase "$(INTDIR)\dtsize.obj"
	-@erase "$(INTDIR)\dtstat.obj"
	-@erase "$(INTDIR)\dtstrhash.obj"
	-@erase "$(INTDIR)\dttree.obj"
	-@erase "$(INTDIR)\dtview.obj"
	-@erase "$(INTDIR)\dtwalk.obj"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(INTDIR)\vc60.pdb"
	-@erase "..\lib\Debug\libcdt.lib"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP_PROJ=/nologo /MTd /W3 /Gm /GX /ZI /Od /D "_DEBUG" /D "HAVE_CONFIG_H" /D "WIN32" /D "_MBCS" /D "_LIB" /D "MSWIN32" /Fp"$(INTDIR)\libcdt.pch" /YX /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /GZ /c 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\libcdt.bsc" 
BSC32_SBRS= \
	
LIB32=link.exe -lib
LIB32_FLAGS=/nologo /out:"..\lib\Debug\libcdt.lib" 
LIB32_OBJS= \
	"$(INTDIR)\dtclose.obj" \
	"$(INTDIR)\dtdisc.obj" \
	"$(INTDIR)\dtextract.obj" \
	"$(INTDIR)\dtflatten.obj" \
	"$(INTDIR)\dthash.obj" \
	"$(INTDIR)\dtlist.obj" \
	"$(INTDIR)\dtmethod.obj" \
	"$(INTDIR)\dtopen.obj" \
	"$(INTDIR)\dtrenew.obj" \
	"$(INTDIR)\dtrestore.obj" \
	"$(INTDIR)\dtsize.obj" \
	"$(INTDIR)\dtstat.obj" \
	"$(INTDIR)\dtstrhash.obj" \
	"$(INTDIR)\dttree.obj" \
	"$(INTDIR)\dtview.obj" \
	"$(INTDIR)\dtwalk.obj"

"..\lib\Debug\libcdt.lib" : "$(OUTDIR)" $(DEF_FILE) $(LIB32_OBJS)
    $(LIB32) @<<
  $(LIB32_FLAGS) $(DEF_FLAGS) $(LIB32_OBJS)
<<

!ENDIF 

.c{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<


!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("libcdt.dep")
!INCLUDE "libcdt.dep"
!ELSE 
!MESSAGE Warning: cannot find "libcdt.dep"
!ENDIF 
!ENDIF 


!IF "$(CFG)" == "libcdt - Win32 Release" || "$(CFG)" == "libcdt - Win32 Debug"
SOURCE=.\dtclose.c

"$(INTDIR)\dtclose.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\dtdisc.c

"$(INTDIR)\dtdisc.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\dtextract.c

"$(INTDIR)\dtextract.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\dtflatten.c

"$(INTDIR)\dtflatten.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\dthash.c

"$(INTDIR)\dthash.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\dtlist.c

"$(INTDIR)\dtlist.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\dtmethod.c

"$(INTDIR)\dtmethod.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\dtopen.c

"$(INTDIR)\dtopen.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\dtrenew.c

"$(INTDIR)\dtrenew.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\dtrestore.c

"$(INTDIR)\dtrestore.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\dtsize.c

"$(INTDIR)\dtsize.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\dtstat.c

"$(INTDIR)\dtstat.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\dtstrhash.c

"$(INTDIR)\dtstrhash.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\dttree.c

"$(INTDIR)\dttree.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\dtview.c

"$(INTDIR)\dtview.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\dtwalk.c

"$(INTDIR)\dtwalk.obj" : $(SOURCE) "$(INTDIR)"



!ENDIF 


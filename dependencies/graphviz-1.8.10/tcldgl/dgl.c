/*
 * Tcl extension for Dynamic Graphs by John Ellson (ellson@lucent.com)
 * 
 * Builds on libincr + libgraph by Stephen North (north@research.att.com)
 */

#include "dgl.h"

#ifdef DMALLOC
#include "dmalloc.h"
#endif

/* FIXME globally accessable structure.  not thread safe */
/* should be malloc'ed in Init and reference through ClientData */
dglInterp_t dglInterp;

/* available engines from dynagraph */
extern ILengine_t OrthoGrid;
extern ILengine_t DynaDag;
extern ILengine_t GeoGraph;
extern ILengine_t FDPGraph;

static int
dglayoutprops(dgLayout_t *v, int argc, char **argv) {
    /*
      processes any combination of the following configuration switches:
    	-engine			(enum:dynadag,geograph,orthogrid) (default geograph)
    	-orientation	(int)	(default 2)
    	-resolution		(float)	(default 1)
    	-xseparation    (float) (default 10)
    	-yseparation    (float) (default 40)
    	-ticks    		(float) (default 1)
    */

    int			res=TCL_OK, i=0, length;
    double		dtemp;
    char		c;
    Tcl_Interp	*interp;

    interp = v->dglInterp->interp;

    if (argc % 2) {
        Tcl_AppendResult(interp,
                         "invalid number of args, must be even",
                         (char *) NULL);
        return TCL_ERROR;
    }

    while (i < argc) {
        if (argv[i][0] != '-') {
            Tcl_AppendResult(interp,
                             "invalid switch: \"", argv[i], (char *) NULL);
            return TCL_ERROR;
        }

        length = strlen(argv[i]);

        if (strncmp(argv[i], "-engine", length) == 0) {
            ++i;
            res = TCL_OK;
            length = strlen(argv[i]);
            c = argv[i][0];

            if (c == 'd' && strncasecmp(argv[i], "dynadag", length) == 0)
                v->engine = &DynaDag;
            else if (c == 'f' && strncasecmp(argv[i], "fdpgraph", length) == 0)
                v->engine = &FDPGraph;
            else if (c == 'g' && strncasecmp(argv[i], "geograph", length) == 0)
                v->engine = &GeoGraph;
            else if (c == 'o' && strncasecmp(argv[i], "orthogrid", length) == 0)
                v->engine = &OrthoGrid;
            else
                res = TCL_ERROR;

        } else if (strncmp(argv[i], "-orientation", length) == 0)
            res = Tcl_GetInt(interp, argv[++i], &v->orientation);
        else if (strncmp(argv[i], "-resolution", length) == 0) {
            res = Tcl_GetDouble(interp, argv[++i], &dtemp);

            if (res == TCL_OK)
                ((ILview_t *)v)->resolution = (float)dtemp;

        } else if (strncmp(argv[i], "-xseparation", length) == 0)
            res = Tcl_GetDouble(interp, argv[++i],
                                &((ILview_t *)v)->separation.x);
        else if (strncmp(argv[i], "-yseparation", length) == 0)
            res = Tcl_GetDouble(interp, argv[++i],
                                &((ILview_t *)v)->separation.y);
        else if (strncmp(argv[i], "-ticks", length) == 0) {
            res = Tcl_GetDouble(interp, argv[++i], &dtemp);

            if (res == TCL_OK)
                ((ILview_t *)v)->ticks = (float)dtemp;

        } else {
            Tcl_AppendResult(interp,
                             "unknown switch: \"", argv[i], (char *) NULL);
            return TCL_ERROR;
        }

        if (res != TCL_OK) {
            Tcl_AppendResult(interp, "invalid switch value: \"",
                             argv[i-1], " ", argv[i], (char *) NULL);
            return TCL_ERROR;
        }

        i++;
    }

    return TCL_OK;
}

static int
dglayout(ClientData clientData, Tcl_Interp *interp, int argc, char *argv[]) {
    dglInterp_t *dg = (dglInterp_t *)clientData;
    dgLayout_t	   *v, **tv;
    char			vbuf[16];
    unsigned long		id;

    tv = (dgLayout_t **) tclhandleAlloc(dg->layoutTable, (char *)NULL, &id);
    *tv = v = (dgLayout_t *) Tcl_Alloc(sizeof(dgLayout_t));
    v->id = id;
    layout_to_handle(v, vbuf);
    v->dglInterp = dg;

    Tcl_InitHashTable(&(v->edgetable), TCL_STRING_KEYS);
    Tcl_InitHashTable(&(v->nodetable), TCL_STRING_KEYS);

    ((ILview_t *)v)->base.tag = ILVIEW;
    ((ILview_t *)v)->ins = (ILevf_t)insert_obj;
    ((ILview_t *)v)->mod = (ILevf_t)modify_obj;
    ((ILview_t *)v)->del = (ILevf_t)delete_obj;

    v->batch_cmd = (char *) NULL;
    v->insert_node_cmd = (char *) NULL;
    v->insert_edge_cmd = (char *) NULL;
    v->modify_node_cmd = (char *) NULL;
    v->modify_edge_cmd = (char *) NULL;
    v->delete_node_cmd = (char *) NULL;
    v->delete_edge_cmd = (char *) NULL;

    /* callbacks are controlled by the "batch" command */
    /* default is "batch 0"  i.e. callbacks enabled */
    ((ILview_t *)v)->enable_immediate_callbacks = 1;

    /* TODO the following should be configure options with cget support */
    ((ILview_t *)v)->resolution = 1;
    ((ILview_t *)v)->separation.x = 10;
    ((ILview_t *)v)->separation.y = 40;
    ((ILview_t *)v)->ticks = 1;
    ((ILview_t *)v)->max_bb.x = 50;
    ((ILview_t *)v)->max_bb.y = 50;
    v->orientation = 0;
    v->engine = &GeoGraph;

    if (dglayoutprops(v, argc-1, &argv[1]) != TCL_OK) {
        tclhandleFreeIndex(dg->layoutTable, id);
        return TCL_ERROR;
    }

    /* create node and edge defaults */
    v->defaultedge = (ILedge_t *)Tcl_Alloc(sizeof(ILedge_t));

    assert (v->defaultedge);

    initedgespec(v->defaultedge);

    setedgeshape(v->defaultedge, IL_SPLINE);

    v->defaultnode = (ILnode_t *)Tcl_Alloc(sizeof(ILnode_t));

    assert (v->defaultnode);

    initnodespec(v->defaultnode);

    setnodeshape(v->defaultnode, IL_ELLIPSE);

    ilopen(v->engine, (ILview_t *)v);

    Tcl_CreateCommand(interp, vbuf, layoutcmd, clientData,
                      (Tcl_CmdDeleteProc *) NULL);

    Tcl_AppendResult(interp, vbuf, (char *) NULL);

    return TCL_OK;
}

int
Tcldgl_Init(Tcl_Interp * interp) {
#ifdef USE_TCL_STUBS

    if (Tcl_InitStubs(interp, TCL_VERSION, 0) == NULL) {
        return TCL_ERROR;
    }

#else
    if (Tcl_PkgRequire(interp, "Tcl", TCL_VERSION, 0) == NULL) {
        return TCL_ERROR;
    }

#endif
    if (Tcl_PkgRequire(interp, "Tcldgr", VERSION, 0) == NULL) {
        return TCL_ERROR;
    }

    if (Tcl_PkgProvide(interp, "Tcldgl", VERSION) != TCL_OK) {
        return TCL_ERROR;
    }

    dglInterp.interp = interp;
    dglInterp.layoutTable = tclhandleInit("dgL",sizeof(dgLayout_t), 10);

    Tcl_CreateCommand(interp, "dglayout", dglayout, &dglInterp,
                      (Tcl_CmdDeleteProc *) NULL);

    return TCL_OK;
}

int
Tcldgl_SafeInit(Tcl_Interp * interp) {
    return Tcldgl_Init(interp);
}

void __eprintf(const char *a, const char *b, unsigned c, const char *d) {}

/*
 * Tcl extension for Dynamic Graphs by John Ellson (ellson@lucent.com)
 * 
 * Builds on libincr + libgraph by Stephen North (north@research.att.com)
 */

#include "dgl.h"

#ifdef DMALLOC
#include "dmalloc.h"
#endif

void
setnodeshapefromdefault(dgLayout_t * v, ILnode_t *spec) {
    int i;

    spec->shape = (ilshape_t *)Tcl_Alloc(sizeof(ilshape_t));
    assert(spec->shape);
    spec->shape->next = (ilshape_t *) NULL;
    spec->shape->type = ((ILnode_t *)v->defaultnode)->shape->type;

    if (spec->shape->type == IL_POLYGON) {
        i = spec->shape->def.curve.n = ((ILnode_t *)v->defaultnode)->shape->def.curve.n;

        if (i) {
            spec->shape->def.curve.p =
                (ilcoord_t *) Tcl_Alloc (i * sizeof(ilcoord_t));
            assert(spec->shape->def.curve.p);

            while(i) {
                --i;
                spec->shape->def.curve.p[i].x =
                    ((ILnode_t *)v->defaultnode)->shape->def.curve.p[i].x;
                spec->shape->def.curve.p[i].y =
                    ((ILnode_t *)v->defaultnode)->shape->def.curve.p[i].y;
            }
        }

    } else if (spec->shape->type == IL_ELLIPSE) {
        spec->shape->def.ellipse.radius_a =
            ((ILnode_t *)v->defaultnode)->shape->def.ellipse.radius_a;
        spec->shape->def.ellipse.radius_b =
            ((ILnode_t *)v->defaultnode)->shape->def.ellipse.radius_b;
    }
}

void
setnodeshape(ILnode_t *spec, ilshapetype_t type) {
    if (! spec->shape) {
        spec->shape = (ilshape_t *)Tcl_Alloc(sizeof(ilshape_t));
        assert(spec->shape);
        spec->shape->next = (ilshape_t *) NULL;
        spec->shape->type = IL_NOSHAPE;
    }

    if (type == IL_POLYGON && spec->shape->type != IL_POLYGON) {
        spec->shape->type = IL_POLYGON;
        spec->shape->def.curve.n = 0;
        spec->shape->def.curve.p = (ilcoord_t *) NULL;
        spec->update |= IL_UPD_SHAPE;
    }

    if (type == IL_ELLIPSE && spec->shape->type != IL_ELLIPSE) {
        if (spec->shape->type == IL_POLYGON && spec->shape->def.curve.p)
            Tcl_Free((char *)spec->shape->def.curve.p);

        spec->shape->type = IL_ELLIPSE;

        spec->shape->def.ellipse.radius_a = 8.;

        spec->shape->def.ellipse.radius_b = 8.;

        spec->update |= IL_UPD_SHAPE;
    }
}

void
setedgeshapefromdefault(dgLayout_t * v, ILedge_t *spec) {
    int i;

    spec->pos = (ilshape_t *)Tcl_Alloc(sizeof(ilshape_t));
    assert(spec->pos);
    spec->pos->next = (ilshape_t *) NULL;
    spec->pos->type = ((ILedge_t *)v->defaultedge)->pos->type;
    spec->pos->def.curve.type = ((ILedge_t *)v->defaultedge)->pos->def.curve.type;
    i = spec->pos->def.curve.n = ((ILedge_t *)v->defaultedge)->pos->def.curve.n;

    if (i) {
        spec->pos->def.curve.p =
            (ilcoord_t *) Tcl_Alloc (i * sizeof(ilcoord_t));
        assert(spec->pos->def.curve.p);

        while(i) {
            --i;
            spec->pos->def.curve.p[i] =
                ((ILedge_t *)v->defaultedge)->pos->def.curve.p[i];
        }

    } else {
        spec->pos->def.curve.p = (ilcoord_t *) NULL;
    }
}

void
setedgeshape(ILedge_t *spec, ilshapetype_t type) {
    if (! spec->pos) {
        spec->pos = (ilshape_t *)Tcl_Alloc(sizeof(ilshape_t));
        assert(spec->pos);
        spec->pos->next = (ilshape_t *) NULL;
        spec->pos->type = IL_SPLINEGON;
        spec->pos->def.curve.type = IL_SPLINE;
        spec->pos->def.curve.n = 0;
        spec->pos->def.curve.p = (ilcoord_t *)NULL;
    }

    if (spec->pos->def.curve.type != type) {
        spec->pos->def.curve.type = type;
        spec->update |= IL_UPD_SHAPE;
    }
}

void
freeshapes(ilshape_t *shape) {
    ilshape_t *nextshape;

    while(shape) {
        if ((shape->type == IL_POLYGON || shape->type == IL_SPLINEGON)
                && (shape->def.curve.p))
            Tcl_Free((char *)shape->def.curve.p);

        nextshape = shape->next;

        Tcl_Free((char *)shape);

        shape = nextshape;
    }
}


int
dgedgeshape(dgLayout_t *vp, int argc, char **argv, ILedge_t *spec) {
    /*
      processes any combination of the following switches:
    	-shape			line|spline  (shape type - default spline)
    	-coordinates	coordinates	 (list of x y coordinates of the edge)
    	-constraint		true|false (constrain edge to horizontal - default false)
    	-width			floatingpointvalue  (default 0.0)
    	-length_hint	floatingpointvalue  (default 1.0)
    	-cost			floatingpointvalue  (default 0.0)
    	-				(indicate continued existence with no shape change)
    */

#define MAX_PTS 50
    char	  **point_array, c;
    ilcoord_t  *coords, ptarray[MAX_PTS];
    int			arg=0, length, temp, max_pts, min_pts, num_pts, i, j;
    int			curveIsNew;
    double		ftemp;
    Tcl_Interp  *interp = vp->dglInterp->interp;

    spec->update = 0;

    if (! spec->pos)
        setedgeshapefromdefault(vp, spec);

    while (arg < argc) {
        c = argv[arg][1];
        length = strlen(argv[arg]);

        if (length == 1 && argv[arg][0] == '-') {
            arg++;
            /* null. indicates contnued existance only */

        } else if ((c == 's') && (strncmp(argv[arg], "-shape", length) == 0)) {
            arg++;

            if (arg > argc-1) {
                Tcl_AppendResult(interp,
                                 "-shape requires one argument: line|spline",
                                 (char *) NULL);
                return TCL_ERROR;
            }

            if (strcmp(argv[arg], "line") == 0)
                setedgeshape(spec, IL_POLYLINE);
            else if (strcmp(argv[arg], "spline") == 0)
                setedgeshape(spec, IL_SPLINE);
            else {
                Tcl_AppendResult(interp, "-shape \"", argv[arg],
                                 "\" must be either line or spline.",
                                 (char *) NULL);
                return TCL_ERROR;
            }

            arg++;

        } else if ((c == 'c') && (strncmp(argv[arg], "-coordinates", length) == 0)) {
            arg++;

            if (arg > argc-1) {
                Tcl_AppendResult(interp,
                                 "-coordinates requires a list of x y coordinate pairs",
                                 (char *) NULL);
                return TCL_ERROR;
            }

            if (! spec->pos) {
                Tcl_AppendResult(interp,
                                 "a shape is required before coordinates can be specified",
                                 (char *) NULL);
                return TCL_ERROR;
            }

            if (spec->pos->def.curve.type == IL_POLYLINE) {
                max_pts = MAX_PTS;
                min_pts = 2;

            } else if (spec->pos->def.curve.type == IL_SPLINE) {
                max_pts = MAX_PTS;
                min_pts = 4;

            } else {
                Tcl_AppendResult(interp,
                                 "unknown shape - must be either IL_POLYLINE or IL_SPLINE.",
                                 "(internal error)", (char *) NULL);
                return TCL_ERROR;
            }

            if (Tcl_SplitList(interp, argv[arg],
                              &num_pts, (CONST84 char ***) &point_array) != TCL_OK) {
                Tcl_AppendResult(interp,
                                 "invalid list of points",
                                 (char *) NULL);
                return TCL_ERROR;
            }

            if (num_pts % 2) {
                Tcl_AppendResult(interp,
                                 "odd number of coords",
                                 (char *) NULL);
                Tcl_Free((char *)point_array);
                return TCL_ERROR;
            }

            num_pts /= 2;  /* convert to number of x,y points */

            if (num_pts < min_pts) {
                Tcl_AppendResult(interp,
                                 "not enough points",
                                 (char *) NULL);
                Tcl_Free((char *)point_array);
                return TCL_ERROR;
            }

            if (num_pts > max_pts) {
                Tcl_AppendResult(interp,
                                 "too many points",
                                 (char *) NULL);
                Tcl_Free((char *)point_array);
                return TCL_ERROR;
            }

            if ((spec->pos->def.curve.type == IL_SPLINE) && (num_pts%3 != 1)) {
                Tcl_AppendResult(interp,
                                 "invalid number of points for a spline, should be 3N+1",
                                 (char *) NULL);
                Tcl_Free((char *)point_array);
                return TCL_ERROR;
            }

            curveIsNew = 0;

            if (spec->pos->def.curve.p
                    && spec->pos->def.curve.n < num_pts) {
                Tcl_Free((char *)spec->pos->def.curve.p);
                spec->pos->def.curve.p = (ilcoord_t *)NULL;
            }

            if (! spec->pos->def.curve.p) {
                spec->pos->def.curve.p =
                    (ilcoord_t *)Tcl_Alloc(num_pts*sizeof(ilcoord_t));
                assert(spec->pos->def.curve.p);
                curveIsNew = 1;
            }

            coords = spec->pos->def.curve.p;

            for (i = 0, j = 0; i < num_pts; i++, j += 2) {
                if (dgscanxy(vp, &point_array[j], &ptarray[i]) != TCL_OK) {
                    Tcl_AppendResult(interp, "coordinate syntax error: ",
                                     point_array[j], " ", point_array[j+1], (char *) NULL);
                    Tcl_Free((char *)point_array);
                    return TCL_ERROR;
                }
            }

            Tcl_Free((char *)point_array);

            for (i = 0; i < num_pts; i++) {
                if (curveIsNew
                        || coords[i].x != ptarray[i].x
                        || coords[i].y != ptarray[i].y) {
                    coords[i].x = ptarray[i].x;
                    coords[i].y = ptarray[i].y;
                    spec->update |= IL_UPD_SHAPE;
                }
            }

            spec->pos->def.curve.n = num_pts;
            arg++;

        } else if ((c == 'c') && (strncmp(argv[arg], "-constraint", length) == 0)) {
            arg++;

            if (arg > argc-1) {
                Tcl_AppendResult(interp,
                                 "-constraint requires one argument: true|false",
                                 (char *) NULL);
                return TCL_ERROR;
            }

            if (Tcl_GetBoolean(interp, argv[arg], &temp) != TCL_OK) {
                Tcl_AppendResult(interp,
                                 "invalid boolean value for -constraint",
                                 (char *) NULL);
                return TCL_ERROR;
            }

            ((ILedge_t *)spec)->constraint = (ilbool)temp;
            arg++;

        } else if ((c == 'l') && (strncmp(argv[arg], "-length_hint", length) == 0)) {
            arg++;

            if (arg > argc-1) {
                Tcl_AppendResult(interp,
                                 "-length_hint requires one floatingpoint value",
                                 (char *) NULL);
                return TCL_ERROR;
            }

            if (Tcl_GetDouble(interp, argv[arg], &ftemp) != TCL_OK) {
                Tcl_AppendResult(interp,
                                 "invalid floatingpoint value for -length_hint",
                                 (char *) NULL);
                return TCL_ERROR;
            }

            ((ILedge_t *)spec)->length_hint = (float)ftemp;
            arg++;

        } else if ((c == 'w') && (strncmp(argv[arg], "-width", length) == 0)) {
            arg++;

            if (arg > argc-1) {
                Tcl_AppendResult(interp,
                                 "-width requires one floatingpoint value",
                                 (char *) NULL);
                return TCL_ERROR;
            }

            if (Tcl_GetDouble(interp, argv[arg], &ftemp) != TCL_OK) {
                Tcl_AppendResult(interp,
                                 "invalid floatingpoint value for -width",
                                 (char *) NULL);
                return TCL_ERROR;
            }

            ((ILedge_t *)spec)->width = (float)ftemp;
            arg++;

        } else if ((c == 'c') && (strncmp(argv[arg], "-cost", length) == 0)) {
            arg++;

            if (arg > argc-1) {
                Tcl_AppendResult(interp,
                                 "-cost requires one floatingpoint value",
                                 (char *) NULL);
                return TCL_ERROR;
            }

            if (Tcl_GetDouble(interp, argv[arg], &ftemp) != TCL_OK) {
                Tcl_AppendResult(interp,
                                 "invalid floatingpoint value for -cost",
                                 (char *) NULL);
                return TCL_ERROR;
            }

            ((ILedge_t *)spec)->cost = (float)ftemp;
            arg++;

        } else {
            Tcl_AppendResult(interp, "unknown shape option: ",
                             argv[arg], (char *) NULL);
            return TCL_ERROR;
        }
    }

    return TCL_OK;
}

int
dgnodeshape(dgLayout_t *vp, int argc, char **argv, ILnode_t *spec) {
    /*
      processes any combination of the following switches:
    	-shape		polygon|oval (shape type)
    	-boundary	boundary	 (list of x y coordinates of the shape boundary)
    	-at			x y			 (absolute position)
    	-by			dx dy		 (relative position)
    	-						 (indicate continued existence with no shape change)
    */

#define MAX_PTS 50
    char	  **point_array, c;
    ilcoord_t  *coords, ptarray[MAX_PTS], prev;
    int			arg=0, i, j, max_pts, min_pts, num_pts, length, curveIsNew;
    float		max_x=0, min_x=0, max_y=0, min_y=0, ctr_x=0, ctr_y=0;
    Tcl_Interp  *interp = vp->dglInterp->interp;

    spec->update = 0;

    if (! spec->shape)
        setnodeshapefromdefault(vp, spec);

    while (arg < argc) {
        c = argv[arg][1];
        length = strlen(argv[arg]);

        if (length == 1 && argv[arg][0] == '-') {
            arg++;
            /* null. indicates continued existance only */

        } else if ((c == 's') && (strncmp(argv[arg], "-shape", length) == 0)) {
            arg++;

            if (arg > argc-1) {
                Tcl_AppendResult(interp,
                                 "-shape requires one argument",
                                 (char *) NULL);
                return TCL_ERROR;
            }

            if (strcmp(argv[arg], "polygon") == 0)
                setnodeshape(spec, IL_POLYGON);
            else if (strcmp(argv[arg], "oval") == 0)
                setnodeshape(spec, IL_ELLIPSE);
            else {
                Tcl_AppendResult(interp, "-shape \"", argv[arg],
                                 "\" must be either polygon or oval.",
                                 (char *) NULL);
                return TCL_ERROR;
            }

            arg++;

        } else if ((c == 'b') && (strncmp(argv[arg], "-boundary", length) == 0)) {
            arg++;

            if (arg > argc-1) {
                Tcl_AppendResult(interp,
                                 "-boundary requires a list of x y coordinate pairs",
                                 (char *) NULL);
                return TCL_ERROR;
            }

            if (! spec->shape) {
                Tcl_AppendResult(interp,
                                 "a shape is required before coords can be specified",
                                 (char *) NULL);
                return TCL_ERROR;
            }

            if (spec->shape->type == IL_POLYGON) {
                max_pts = MAX_PTS;
                min_pts = 3;

            } else if (spec->shape->type == IL_ELLIPSE) {
                max_pts = min_pts = 2;

            } else {
                Tcl_AppendResult(interp,
                                 "unknown shape - must be either IL_POLYGON or IL_ELLIPSE.",
                                 "(internal error)", (char *) NULL);
                return TCL_ERROR;
            }

            if (Tcl_SplitList(interp, argv[arg],
                              &num_pts, (CONST84 char ***) &point_array) != TCL_OK) {
                Tcl_AppendResult(interp,
                                 "invalid list of points",
                                 (char *) NULL);
                return TCL_ERROR;
            }

            if (num_pts % 2) {
                Tcl_AppendResult(interp,
                                 "odd number of coords",
                                 (char *) NULL);
                Tcl_Free((char *)point_array);
                return TCL_ERROR;
            }

            num_pts /= 2;  /* convert to number of x,y points */

            if (num_pts < min_pts) {
                Tcl_AppendResult(interp,
                                 "not enough points",
                                 (char *) NULL);
                Tcl_Free((char *)point_array);
                return TCL_ERROR;
            }

            if (num_pts > max_pts) {
                Tcl_AppendResult(interp,
                                 "too many points",
                                 (char *) NULL);
                Tcl_Free((char *)point_array);
                return TCL_ERROR;
            }

            curveIsNew = 0;

            if (spec->shape->type == IL_POLYGON) {
                if (spec->shape->def.curve.p
                        && spec->shape->def.curve.n < num_pts) {
                    Tcl_Free((char *)spec->shape->def.curve.p);
                    spec->shape->def.curve.p = (ilcoord_t *)NULL;
                }

                if (! spec->shape->def.curve.p) {
                    spec->shape->def.curve.p =
                        (ilcoord_t *)Tcl_Alloc(num_pts*sizeof(ilcoord_t));
                    assert(spec->shape->def.curve.p);
                    spec->shape->def.curve.type = IL_POLYLINE;
                    curveIsNew = 1;
                }
            }

            coords = spec->shape->def.curve.p;

            for (i = 0, j = 0; i < num_pts; i++, j += 2) {
                if (dgscanxy(vp, &point_array[j], &ptarray[i]) != TCL_OK) {
                    Tcl_AppendResult(interp, "coordinate syntax error: ",
                                     point_array[j], " ", point_array[j+1], (char *) NULL);
                    Tcl_Free((char *)point_array);
                    return TCL_ERROR;
                }

                if (i == 0) {
                    max_x = min_x = ptarray[i].x;
                    max_y = min_y = ptarray[i].y;

                } else {
                    if (ptarray[i].x > max_x)
                        max_x = ptarray[i].x;
                    else if (ptarray[i].x < min_x)
                        min_x = ptarray[i].x;

                    if (ptarray[i].y > max_y)
                        max_y = ptarray[i].y;
                    else if (ptarray[i].y < min_y)
                        min_y = ptarray[i].y;
                }
            }

            Tcl_Free((char *)point_array);

            ctr_x = (max_x + min_x) / 2.0;
            ctr_y = (max_y + min_y) / 2.0;

            if (spec->shape->type == IL_POLYGON) {
                for (i = 0; i < num_pts; i++) {
                    ptarray[i].x -= ctr_x;
                    ptarray[i].y -= ctr_y;

                    if (curveIsNew
                            || coords[i].x != ptarray[i].x
                            || coords[i].y != ptarray[i].y) {
                        coords[i].x = ptarray[i].x;
                        coords[i].y = ptarray[i].y;
                        spec->update |= IL_UPD_SHAPE;
                    }
                }

                spec->shape->def.curve.n = num_pts;

            } else if (spec->shape->type == IL_ELLIPSE) {
                ptarray[0].x = max_x - ctr_x;
                ptarray[0].y = max_y - ctr_y;

                if ((spec->shape->def.ellipse.radius_a != ptarray[0].x)
                        || (spec->shape->def.ellipse.radius_b != ptarray[0].y)) {
                    spec->shape->def.ellipse.radius_a = ptarray[0].x;
                    spec->shape->def.ellipse.radius_b = ptarray[0].y;
                    spec->update |= IL_UPD_SHAPE;
                }
            }

            arg++;

        } else if ((c == 'a') && (strncmp(argv[arg], "-at", length) == 0)) {
            arg++;

            if (arg > argc-2) {
                Tcl_AppendResult(interp,
                                 "-at requires: x y",
                                 (char *) NULL);
                return TCL_ERROR;
            }

            if (dgscanxy(vp, &argv[arg], &ptarray[0]) != TCL_OK) {
                Tcl_AppendResult(interp, "coordinate syntax error: ",
                                 argv[arg], " ", argv[arg+1], (char *) NULL);
                return TCL_ERROR;
            }

            prev.x = spec->pos.x;
            prev.y = spec->pos.y;
            spec->pos.x = ptarray[0].x;
            spec->pos.y = ptarray[0].y;

            if (spec->pos_valid == FALSE
                    || prev.x != spec->pos.x || prev.y != spec->pos.y)
                spec->update |= IL_UPD_MOVE;

            spec->pos_valid = TRUE;

            arg+=2;

        } else if ((c == 'b') && (strncmp(argv[arg], "-by", length) == 0)) {
            arg++;

            if (arg > argc-2) {
                Tcl_AppendResult(interp,
                                 "-by requires: dx dy",
                                 (char *) NULL);
                return TCL_ERROR;
            }

            if (dgscanxy(vp, &argv[arg], &ptarray[0]) != TCL_OK) {
                Tcl_AppendResult(interp, "coordinate syntax error: ",
                                 argv[arg], " ", argv[arg+1], (char *) NULL);
                return TCL_ERROR;
            }

            prev.x = spec->pos.x;
            prev.y = spec->pos.y;
            spec->pos.x += ptarray[0].x;
            spec->pos.y += ptarray[0].y;

            if (spec->pos_valid == FALSE
                    || prev.x != spec->pos.x || prev.y != spec->pos.y)
                spec->update |= IL_UPD_MOVE;

            spec->pos_valid = TRUE;

            arg+=2;

        } else {
            Tcl_AppendResult(interp, "unknown shape option: ",
                             argv[arg], (char *) NULL);
            return TCL_ERROR;
        }
    }

    return TCL_OK;
}

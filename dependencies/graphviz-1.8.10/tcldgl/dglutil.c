/*
 * Tcl extension for Dynamic Graphs by John Ellson (ellson@lucent.com)
 * 
 * Builds on libincr + libgraph by Stephen North (north@research.att.com)
 */

#include "dgl.h"

#ifdef DMALLOC
#include "dmalloc.h"
#endif

char *
layout_to_handle(dgLayout_t *v, char *buf) {
    sprintf(buf, "dgL%lu", v->id);
    return buf;
}

dgLayout_t *
handle_to_layout(char *s) {
    unsigned long  i;
    dgLayout_t	*v;

    if ((sscanf(s, "dgL%lu", &i)) != 1)
        return NULL;

    if (!(v = *(dgLayout_t **)tclhandleXlateIndex(dglInterp.layoutTable, i)))
        return NULL;

    return v;
}

void
initedgespec(ILedge_t *spec) {
    spec->base.tag = ILEDGE;
    spec->tail.term = (ILobj_t *)NULL;
    spec->tail.port.x = 0;
    spec->tail.port.y = 0;
    spec->tail.clipped = TRUE;
    spec->head.term = (ILobj_t *)NULL;
    spec->head.port.x = 0;
    spec->head.port.y = 0;
    spec->head.clipped = TRUE;
    spec->width = 0;
    spec->length_hint = 1;
    spec->cost = 0;
    spec->pos = (ilshape_t *) NULL;
    spec->constraint = TRUE;
    spec->update = 0;
}

int
getedgespec (dgLayout_t *v, char *e, dgLayoutEdge_t **vep, int create_p) {
    int				new, in_layout = 1;
    dgLayoutEdge_t	*spec = NULL;

    Tcl_HashEntry
    *obj;

    if (create_p) {

        obj = Tcl_CreateHashEntry
              (&(v->edgetable), e, &new);

        assert (obj);

        if (new) {
            in_layout = 0;
            spec = (dgLayoutEdge_t *)Tcl_Alloc(sizeof(dgLayoutEdge_t));
            assert (spec);

            Tcl_SetHashValue(obj, spec);
            spec->e = obj;

            initedgespec((ILedge_t *)spec);
            setedgeshapefromdefault(v, (ILedge_t *)spec);
        }

    } else {

        obj = Tcl_FindHashEntry
              (&(v->edgetable), e);

        if (!obj) {
            in_layout = 0;
        }
    }

    if (in_layout) {
        spec = (dgLayoutEdge_t *)Tcl_GetHashValue(obj);
    }

    *vep = spec;
    return in_layout;
}

void
initnodespec(ILnode_t *spec) {
    spec->base.tag = ILNODE;
    spec->update = 0;
    spec->pos_valid = FALSE;
    spec->pos.x = 0;
    spec->pos.y = 0;
    spec->shape = (ilshape_t *)NULL;
}

int
getnodespec (dgLayout_t *v, char *n, dgLayoutNode_t **vnp, int create_p) {
    int				new, in_layout = 1;
    dgLayoutNode_t	*spec = NULL;

    Tcl_HashEntry
    *obj;

    if (create_p) {

        obj = Tcl_CreateHashEntry
              (&(v->nodetable), n, &new);

        assert (obj);

        if (new) {
            in_layout = 0;
            spec = (dgLayoutNode_t *)Tcl_Alloc(sizeof(dgLayoutNode_t));
            assert (spec);

            Tcl_SetHashValue(obj, spec);
            spec->n = obj;

            initnodespec((ILnode_t *)spec);
            setnodeshapefromdefault(v, (ILnode_t *)spec);
        }

    } else {

        obj = Tcl_FindHashEntry
              (&(v->nodetable), n);

        if (!obj) {
            in_layout = 0;
        }
    }

    if (in_layout) {
        spec = (dgLayoutNode_t *)Tcl_GetHashValue(obj);
    }

    *vnp = spec;
    return in_layout;
}

void
dgldelete(dgLayout_t *v) {
    char buf[16];

    /* ilclose provides callbacks for node and edge deletes */
    ilclose((ILview_t *)v);

    freeshapes(((ILnode_t *)(v->defaultedge))->shape);
    freeshapes(((ILnode_t *)(v->defaultnode))->shape);
    Tcl_Free((char *)v->defaultedge);
    Tcl_Free((char *)v->defaultnode);

    Tcl_DeleteHashTable(&v->edgetable);
    Tcl_DeleteHashTable(&v->nodetable);
    Tcl_DeleteCommand(v->dglInterp->interp, layout_to_handle(v, buf));
    tclhandleFreeIndex(dglInterp.layoutTable, v->id);
    Tcl_Free((char *)v);
}

void
dgsprintxy(dgLayout_t *v, Tcl_DString *result, int npts, ilcoord_t *p) {
    int i, flip, invert;
    ilcoord_t t;
    char buf[20];

    flip = v->orientation & 1;

    if (v->orientation & 2)
        invert = -1;
    else
        invert = 1;

    for (i = 0; i < npts; i++) {
        if (flip) {
            t.x = p[i].y * invert;
            t.y = p[i].x * invert;

        } else {
            t.x = p[i].x * invert;
            t.y = p[i].y * invert;
        }

        sprintf(buf, "%.1f", t.x);
        Tcl_DStringAppendElement(result, buf);
        sprintf(buf, "%.1f", t.y);
        Tcl_DStringAppendElement(result, buf);
    }
}

int
dgscanxy(dgLayout_t *v, char **argv, ilcoord_t *p) {
    ilcoord_t t;
    int invert;

    if ((sscanf(argv[0], "%lf", &t.x) != 1) || (sscanf(argv[1], "%lf", &t.y) != 1))
        return TCL_ERROR;

    if (v->orientation & 2)
        invert = -1;
    else
        invert = 1;

    if (v->orientation & 1) {
        p->x = t.y * invert;
        p->y = t.x * invert;

    } else {
        p->x = t.x * invert;
        p->y = t.y * invert;
    }

    return TCL_OK;
}

/********************************************************************/
/* event batching                                                   */

int dglCallbacks (dgLayout_t *v, int flag) {
    char vbuf[16];

    if (((ILview_t *)v)->enable_immediate_callbacks) {
        if (! flag) {
            ((ILview_t *)v)->enable_immediate_callbacks = FALSE;
        }

        return TRUE;
    }

    if (flag) { /* && NOT ((ILview_t *)v)->enable_immediate_callbacks */

        if (v->batch_cmd) {
            dglExpandPercentsEval(v->dglInterp->interp, v->batch_cmd,
                                  layout_to_handle(v, vbuf), "", "", "", "1",
                                  (dgLayout_t *) NULL, 0, (ilcoord_t *) NULL);
        }

        ilcallback((ILview_t *)v);

        if (v->batch_cmd) {
            dglExpandPercentsEval(v->dglInterp->interp, v->batch_cmd,
                                  layout_to_handle(v, vbuf), "", "", "", "0",
                                  (dgLayout_t *) NULL, 0, (ilcoord_t *) NULL);
        }

        ((ILview_t *)v)->enable_immediate_callbacks = TRUE;
    }

    return FALSE;
}

/********************************************************************/
/* evaluate bindings after % substitutions						    */

char *
dglExpandPercentsEval(
    Tcl_Interp * interp,	 /* interpreter context */
    char *command,   /* Command with percent expressions */
    char *l,	 /* layoutHandle string to substitute for "%l" */
    char *n,	 /* nodeHandle string to substitute for "%n"
        					or tailnode in *_edge_cmd */
    char *e,	 /* edgeHandle string to substitute for "%e" */
    char *A,	 /* headnode in *_edge_cmd */
    char *a,	 /* node/edge coords */
    dgLayout_t *vp, /* layout context */
    int npts,	/* number of coordinates */
    ilcoord_t * ppos	 /* Coordinates to substitute for %a */
) {
    register char  *string, *before=command;
    Tcl_DString	 scripts;

    Tcl_DStringInit(&scripts);

    while (1) {
        /*
        	* Find everything up to the next % character and append it to the
        	* result string.
        	*/

        for (string = before; (*string != 0) && (*string != '%'); string++) {
            /* Empty loop body. */
        }

        if (string != before) {
            Tcl_DStringAppend(&scripts, before, string - before);
            before = string;
        }

        if (*before == 0) {
            break;
        }

        /*
        	* There's a percent sequence here.  Process it.
        	*/

        switch (before[1]) {

                case 'l':
                Tcl_DStringAppend(&scripts, l, strlen(l));	/* layoutHandle */
                break;

                case 'n':

                case 't':
                Tcl_DStringAppend(&scripts, n, strlen(n));	/* nodeHandle */
                break;

                case 'e':
                Tcl_DStringAppend(&scripts, e, strlen(e));	/* edgeHandle */
                break;

                case 'A':

                case 'h':

                case 'p':
                Tcl_DStringAppend(&scripts, A, strlen(A));	/* attributeName */
                break;

                case 'a':

                case 'P':

                if (npts)
                    dgsprintxy(vp, &scripts, npts, ppos);
                else
                    Tcl_DStringAppend(&scripts, a, strlen(a)); /* attributeValue */

                break;

                default:
                Tcl_DStringAppend(&scripts, before+1, 1);

                break;
        }

        before += 2;
    }

    if (Tcl_GlobalEval(interp, Tcl_DStringValue(&scripts)) != TCL_OK)
        fprintf(stderr, "%s while in binding: %s\n\n",
                interp->result, Tcl_DStringValue(&scripts));

    Tcl_DStringFree(&scripts);

    return interp->result;
}

void
insert_obj(dgLayout_t *v, ILobj_t *spec) {
    char		vbuf[16];
    char		*obj, *tobj, *hobj;

    layout_to_handle(v, vbuf);

    if (spec->tag == ILNODE) {
        if (v->insert_node_cmd) {
            obj = Tcl_GetHashKey(&(v->nodetable),
                                 ((dgLayoutNode_t *)spec)->n);
            dglExpandPercentsEval(v->dglInterp->interp,
                                  v->insert_node_cmd, vbuf, obj, "", "",
                                  "", v, 1, &((ILnode_t *)spec)->pos);
        }

    } else if (spec->tag == ILEDGE) {
        if (v->insert_edge_cmd) {
            obj = Tcl_GetHashKey(&(v->edgetable),
                                 ((dgLayoutEdge_t *)spec)->e);
            tobj = Tcl_GetHashKey(&(v->nodetable),
                                  ((dgLayoutNode_t *)((ILedge_t *)spec)->tail.term)->n);
            hobj = Tcl_GetHashKey(&(v->nodetable),
                                  ((dgLayoutNode_t *)((ILedge_t *)spec)->head.term)->n);
            dglExpandPercentsEval(v->dglInterp->interp,
                                  v->insert_edge_cmd, vbuf, tobj, obj, hobj,
                                  "{0 0 0 0 0 0 0 0}", v,
                                  ((ILedge_t *)spec)->pos->def.curve.n,
                                  ((ILedge_t *)spec)->pos->def.curve.p);
        }
    }
}

void
modify_obj(dgLayout_t *v, ILobj_t *spec) {
    char		vbuf[16];
    char		*obj;

    layout_to_handle(v, vbuf);

    if (spec->tag == ILNODE) {
        if (v->modify_node_cmd) {
            obj = Tcl_GetHashKey(&(v->nodetable),
                                 ((dgLayoutNode_t *)spec)->n);
            dglExpandPercentsEval(v->dglInterp->interp,
                                  v->modify_node_cmd, vbuf, obj, "",
                                  "", "", v, 1, &((ILnode_t *)spec)->pos);
        }

    } else if (spec->tag == ILEDGE) {
        if (v->modify_edge_cmd) {
            obj = Tcl_GetHashKey(&(v->edgetable),
                                 ((dgLayoutEdge_t *)spec)->e);
            dglExpandPercentsEval(v->dglInterp->interp,
                                  v->modify_edge_cmd, vbuf, "", obj, "",
                                  "{0 0 0 0 0 0 0 0}", v,
                                  ((ILedge_t *)spec)->pos->def.curve.n,
                                  ((ILedge_t *)spec)->pos->def.curve.p);
        }
    }
}

void
delete_obj(dgLayout_t *v, ILobj_t *spec) {
    char		vbuf[16];
    char		*obj;

    layout_to_handle(v, vbuf);

    if (spec->tag == ILNODE) {
        if (v->delete_node_cmd) {
            obj = Tcl_GetHashKey(&(v->edgetable),
                                 ((dgLayoutNode_t *)spec)->n);
            dglExpandPercentsEval(v->dglInterp->interp,
                                  v->delete_node_cmd, vbuf, (char *)obj, "",
                                  "", "", v, 0, (ilcoord_t *) NULL);
        }

        freeshapes(((ILnode_t *)spec)->shape);

        Tcl_DeleteHashEntry
        (((dgLayoutNode_t *)spec)->n);

        Tcl_Free((char *)spec);

    } else if (spec->tag == ILEDGE) {
        if (v->delete_edge_cmd) {
            obj = Tcl_GetHashKey(&(v->edgetable),
                                 ((dgLayoutEdge_t *)spec)->e);
            dglExpandPercentsEval(v->dglInterp->interp,
                                  v->delete_edge_cmd, vbuf, "", (char *)obj, "",
                                  "", v, 0, (ilcoord_t *) NULL);
        }

        freeshapes(((ILedge_t *)spec)->pos);

        Tcl_DeleteHashEntry
        (((dgLayoutEdge_t *)spec)->e);

        Tcl_Free((char *)spec);
    }
}

char *
buildBindings(char *s1, char *s2)
/*
 * previous binding in s1 binding to be added in s2 result in s3
 *
 * if s2 begins with + then append (separated by \n) else s2 replaces if
 * resultant string is null then bindings are deleted
 */
{
    char		   *s3;
    int			 l;

    if (s2[0] == '+') {
        if (s1) {
            l = strlen(s2) - 1;

            if (l) {
                s3 = Tcl_Alloc(strlen(s1) + l + 2);
                assert (s3);
                strcpy(s3, s1);
                strcat(s3, "\n");
                strcat(s3, s2 + 1);
                Tcl_Free(s1);

            } else {
                s3 = s1;
            }

        } else {
            l = strlen(s2) - 1;

            if (l) {
                s3 = Tcl_Alloc(l + 2);
                assert (s3);
                strcpy(s3, s2 + 1);

            } else {
                s3 = (char *) NULL;
            }
        }

    } else {
        if (s1)
            Tcl_Free(s1);

        l = strlen(s2);

        if (l) {
            s3 = Tcl_Alloc(l + 2);
            assert (s3);
            strcpy(s3, s2);

        } else {
            s3 = (char *) NULL;
        }
    }

    return s3;
}

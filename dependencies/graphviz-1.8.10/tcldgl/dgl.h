#include <assert.h>
#include <tcl.h>
#include <agraph.h>
#include <tclhandle.h>
#include <incr.h>
#include <shape.h>

#ifdef HAVE_CONFIG_H
#include "gvconfig.h"
#endif

#ifndef CONST84
#define CONST84
#endif

typedef struct dgLayout_s dgLayout_t;

typedef struct dgLayoutNode_s dgLayoutNode_t;

typedef struct dgLayoutEdge_s dgLayoutEdge_t;

typedef enum dgdir_e {BOTH, FORWARD, BACKWARD, NOREVERSE} dgdir_t;

typedef struct dglInterp_s {
    Tcl_Interp	   *interp;
    tblHeader_pt	layoutTable;
}

dglInterp_t;

struct dgLayoutNode_s {
    ILnode_t		node; /* must be first */
    Tcl_HashEntry   *n;	/* pointer to base object */
};

struct dgLayoutEdge_s {
    ILedge_t		edge; /* must be first */
    Tcl_HashEntry   *e;	/* pointer to base object */
};

struct dgLayout_s {
    ILview_t		v;	   /* must be first */
    unsigned long		id;
    dglInterp_t	   *dglInterp;
    ILengine_t	   *engine;
    char		   *batch_cmd;
    char		   *insert_node_cmd;
    char		   *insert_edge_cmd;
    char		   *modify_node_cmd;
    char		   *modify_edge_cmd;
    char		   *delete_node_cmd;
    char		   *delete_edge_cmd;
    ILedge_t	   *defaultedge;	/* a place to keep default shape info */
    ILnode_t	   *defaultnode;
    int				orientation;
    Tcl_HashTable   edgetable;
    Tcl_HashTable   nodetable;
};

/* functions from dglayout.c */
int layoutcmd(ClientData clientData, Tcl_Interp * interp, int argc, char *argv[]);

/* functions from dgshape.c */
void setnodeshapefromdefault(dgLayout_t * v, ILnode_t *spec);
void setedgeshapefromdefault(dgLayout_t * v, ILedge_t *spec);
void setnodeshape(ILnode_t *spec, ilshapetype_t type);
void setedgeshape(ILedge_t *spec, ilshapetype_t type);
int dgedgeshape(dgLayout_t *vp, int argc, char **argv, ILedge_t *spec);
int dgnodeshape(dgLayout_t *vp, int argc, char **argv, ILnode_t *spec);
void freeshapes(ilshape_t *shape);

/* functions from dgutil.c */
void initedgespec(ILedge_t *spec);
void initnodespec(ILnode_t *spec);
int getedgespec (dgLayout_t *v, char *e, dgLayoutEdge_t **vep, int create_p);
int getnodespec (dgLayout_t *v, char *n, dgLayoutNode_t **vnp, int create_p);
void dgldelete (dgLayout_t *v);

dgLayoutEdge_t * edge_to_spec(dgLayout_t *v, Tcl_HashEntry *e);
dgLayoutNode_t * node_to_spec(dgLayout_t *v, Tcl_HashEntry *n);

ilshape_t *init_edge_pos();
void insert_obj(dgLayout_t *v, ILobj_t *spec);
void modify_obj(dgLayout_t *v, ILobj_t *spec);
void delete_obj(dgLayout_t *v, ILobj_t *spec);
int shape_obj(dgLayout_t *v, ILobj_t *spec);

int dgscanxy(dgLayout_t *vp, char **argv, ilcoord_t *p);
void dgsprintxy(dgLayout_t *vp, Tcl_DString *result, int npts, ilcoord_t *p);
int dgboundary(dgLayout_t *vp, int argc, char **argv, ILnode_t *spec);

char *buildBindings(char *s1, char *s2);
char *dglExpandPercentsEval(
    Tcl_Interp * interp,     /* interpreter context */
    register char *before,   /* Command with percent expressions */
    char *l,     /* layoutHandle string to substitute for "%l" */
    char *n,     /* nodeHandle string to substitute for "%n"
                        or tailnode in *_edge_cmd */
    char *e,     /* edgeHandle string to substitute for "%e" */
    char *A,     /* headnode in *_edge_cmd */
    char *a,     /* node/edge coords */
    dgLayout_t *vp, /* layout context */
    int npts,   /* number of coordinates */
    ilcoord_t * ppos     /* Coordinates to substitute for %a */
);

char * layout_to_handle(dgLayout_t *v, char *buf);
dgLayout_t * handle_to_layout(char *s);
int dglCallbacks (dgLayout_t *v, int flag);

extern dglInterp_t dglInterp;

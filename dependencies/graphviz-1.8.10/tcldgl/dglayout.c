/*
 * Tcl extension for Dynamic Graphs by John Ellson (ellson@lucent.com)
 * 
 * Builds on libincr + libgraph by Stephen North (north@research.att.com)
 */

#include "dgl.h"

#ifdef DMALLOC
#include "dmalloc.h"
#endif

int
layoutcmd(ClientData clientData, Tcl_Interp * interp, int argc, char *argv[]) {
    dgLayout_t		*v;
    dgLayoutEdge_t	*ve;
    dgLayoutNode_t	*vn;
    char			c, *s;
    int				length;
    ilcoord_t		twocoords[2];
    Tcl_DString		result;
    int				i, rc, save;

    if (argc < 2) {
        Tcl_AppendResult(interp, "wrong # args: should be \"",
                         argv[0], " option ?arg arg ...?\"", (char *) NULL);
        return TCL_ERROR;
    }

    if (!(v = handle_to_layout(argv[0]))) {
        Tcl_AppendResult(interp, "Invalid handle: \"", argv[0],
                         "\"", (char *) NULL);
        return TCL_ERROR;
    }

    c = argv[1][0];
    length = strlen(argv[1]);

    if ((c == 'b') && (strncmp(argv[1], "batch", length) == 0)) {
        if (argc < 3) {
            Tcl_AppendResult(interp, "wrong # args: should be \"",
                             argv[0], " batch <boolean>\"",
                             (char *) NULL);
            return TCL_ERROR;
        }

        if ((Tcl_GetBoolean(interp, argv[2], &i)) != TCL_OK) {
            return TCL_ERROR;
        }

        dglCallbacks (v, !i);
        return TCL_OK;

    } else if ((c == 'b') && (strncmp(argv[1], "bind", length) == 0)) {
        if (argc > 4) {
            Tcl_AppendResult(interp, "wrong # args: should be \"",
                             argv[0], " bind event ?command?\"",
                             "\nwhere \"event\" is one of:",
                             "\n  batch,",
                             "\n  insert_node, insert_edge,",
                             "\n  modify_node, modify_edge,",
                             "\n  delete_node, delete_edge,",
                             (char *) NULL);
            return TCL_ERROR;
        }

        if (argc == 2) {
            Tcl_AppendElement(interp, "batch");
            Tcl_AppendElement(interp, "insert_node");
            Tcl_AppendElement(interp, "insert_edge");
            Tcl_AppendElement(interp, "modify_node");
            Tcl_AppendElement(interp, "modify_edge");
            Tcl_AppendElement(interp, "delete_node");
            Tcl_AppendElement(interp, "delete_edge");
            return TCL_OK;
        }

        length = strlen(argv[2]);

        if (strncmp(argv[2], "batch", length) == 0) {
            s = v->batch_cmd;

            if (argc == 4)
                v->batch_cmd = s = buildBindings(s, argv[3]);

        } else if (strncmp(argv[2], "insert_node", length) == 0) {
            s = v->insert_node_cmd;

            if (argc == 4)
                v->insert_node_cmd = s = buildBindings(s, argv[3]);

        } else if (strncmp(argv[2], "insert_edge", length) == 0) {
            s = v->insert_edge_cmd;

            if (argc == 4)
                v->insert_edge_cmd = s = buildBindings(s, argv[3]);

        } else if (strncmp(argv[2], "modify_node", length) == 0) {
            s = v->modify_node_cmd;

            if (argc == 4)
                v->modify_node_cmd = s = buildBindings(s, argv[3]);

        } else if (strncmp(argv[2], "modify_edge", length) == 0) {
            s = v->modify_edge_cmd;

            if (argc == 4)
                v->modify_edge_cmd = s = buildBindings(s, argv[3]);

        } else if (strncmp(argv[2], "delete_node", length) == 0) {
            s = v->delete_node_cmd;

            if (argc == 4)
                v->delete_node_cmd = s = buildBindings(s, argv[3]);

        } else if (strncmp(argv[2], "delete_edge", length) == 0) {
            s = v->delete_edge_cmd;

            if (argc == 4)
                v->delete_edge_cmd = s = buildBindings(s, argv[3]);

        } else {
            Tcl_AppendResult(interp, "unknown event \"", argv[2],
                             "\": must be one of:",
                             " insert_node, insert_edge,",
                             " modify_node, modify_edge,",
                             " delete_node, delete_edge.",
                             (char *) NULL);
            return TCL_ERROR;
        }

        if (argc == 3)
            Tcl_AppendResult(interp, s, (char *) NULL);

        return TCL_OK;

    } else if ((c == 'd') && (strncmp(argv[1], "delete", length) == 0)) {
        dgldelete(v);
        return TCL_OK;

        /* note short form "d" is reserved for delete, so these come after */

    } else if ((c == 'd') && (strncmp(argv[1], "delete_edge", length) == 0)) {
        if (argc < 3) {
            Tcl_AppendResult(interp, "wrong # args: should be: ",
                             "\"delete_edge edge\"", (char *) NULL);
            return TCL_ERROR;
        }

        if (!(getedgespec(v, argv[2], &ve, FALSE))) {
            Tcl_AppendResult(interp, "edge \"",
                             argv[2], "\" is not in layout.", (char *) NULL);
            return TCL_ERROR;
        }

        save = dglCallbacks(v, FALSE);
        ildelete((ILview_t *)v, (ILobj_t *)ve);
        dglCallbacks(v, save);
        return TCL_OK;

    } else if ((c == 'd') && (strncmp(argv[1], "delete_node", length) == 0)) {
        if (argc < 3) {
            Tcl_AppendResult(interp, "wrong # args: should be: ",
                             "\"delete_node node\"", (char *) NULL);
            return TCL_ERROR;
        }

        if (!(getnodespec(v, argv[2], &vn, FALSE))) {
            Tcl_AppendResult(interp, "node \"",
                             argv[2], "\" is not in layout.", (char *) NULL);
            return TCL_ERROR;
        }

        save = dglCallbacks(v, FALSE);
        ildelete((ILview_t *)v, (ILobj_t *)vn);
        dglCallbacks(v, save);
        return TCL_OK;

    } else if ((c == 'd') && (strncmp(argv[1], "defaultedge", length) == 0)) {
        return (dgedgeshape(v, argc-2, &argv[2], (ILedge_t *)(v->defaultedge)));

    } else if ((c == 'd') && (strncmp(argv[1], "defaultnode", length) == 0)) {
        return (dgnodeshape(v, argc-2, &argv[2], (ILnode_t *)(v->defaultnode)));

    } else if ((c == 'v') && (strncmp(argv[1], "visible", length) == 0)) {
        if (argc != 6) {
            Tcl_AppendResult(interp, "wrong # args: should be \"",
                             argv[0], " visible x1 y2 x2 y2", (char *)NULL);
            return TCL_ERROR;
        }

        dgscanxy (v, &argv[2], &twocoords[0]);
        dgscanxy (v, &argv[4], &twocoords[1]);

        ((ILview_t *)v)->max_bb.x = abs (twocoords[1].x - twocoords[0].x);
        ((ILview_t *)v)->max_bb.y = abs (twocoords[1].y - twocoords[0].y);

        return TCL_OK;

    } else if ((c == 'g') && (strncmp(argv[1], "get_edge", length) == 0)) {
        if (argc > 2) {
            if (!(getedgespec(v, argv[2], &ve, FALSE))) {
                Tcl_AppendResult(interp, "No spec found for \"",
                                 argv[2], "\"", (char *) NULL);
                return TCL_ERROR;
            }

        } else {
            ve = (dgLayoutEdge_t *)(v->defaultedge);
        }

        Tcl_DStringInit(&result);
        /* shape */

        if (((ILedge_t *)ve)->pos->def.curve.type == IL_SPLINE)
            Tcl_DStringAppendElement(&result, "spline");
        else
            Tcl_DStringAppendElement(&result, "line");

        /* points */
        if (((ILedge_t *)ve)->pos->def.curve.n) {
            dgsprintxy(v, &result,
                       ((ILedge_t *)ve)->pos->def.curve.n,
                       ((ILedge_t *)ve)->pos->def.curve.p);

        } else {
            Tcl_DStringStartSublist(&result);

            for (i=0; i<8; i++)
                Tcl_DStringAppendElement(&result, "0");

            Tcl_DStringEndSublist(&result);
        }

        Tcl_DStringResult(interp, &result);
        return TCL_OK;

    } else if ((c == 'g') && (strncmp(argv[1], "get_node", length) == 0)) {
        if (argc > 2) {
            if (!(getnodespec(v, argv[2], &vn, FALSE))) {
                Tcl_AppendResult(interp, "No spec found for \"",
                                 argv[2], "\"", (char *) NULL);
                return TCL_ERROR;
            }

        } else {
            vn = (dgLayoutNode_t *)(v->defaultnode);
        }

        Tcl_DStringInit(&result);
        /* x,y */
        dgsprintxy(v, &result, 1, &((ILnode_t *)vn)->pos);
        /* shape, boundary */

        if (((ILnode_t *)vn)->shape->type == IL_POLYGON
                || ((ILnode_t *)vn)->shape->type == IL_SPLINEGON) {
            Tcl_DStringAppendElement(&result, "polygon");
            dgsprintxy(v, &result,
                       ((ILnode_t *)vn)->shape->def.curve.n,
                       ((ILnode_t *)vn)->shape->def.curve.p);

        } else if (((ILnode_t *)vn)->shape->type == IL_ELLIPSE) {
            Tcl_DStringAppendElement(&result, "oval");
            twocoords[0].x = 0 - ((ILnode_t *)vn)->shape->def.ellipse.radius_a;
            twocoords[0].y = 0 - ((ILnode_t *)vn)->shape->def.ellipse.radius_b;
            twocoords[1].x = ((ILnode_t *)vn)->shape->def.ellipse.radius_a;
            twocoords[1].y = ((ILnode_t *)vn)->shape->def.ellipse.radius_b;
            dgsprintxy(v, &result, 2, twocoords);

        } else {
            Tcl_AppendResult(interp, "unknown shape", (char *) NULL);
            return TCL_ERROR;
        }

        Tcl_DStringResult(interp, &result);
        return TCL_OK;

    } else if ((c == 'i') && (strncmp(argv[1], "insert_edge", length) == 0)) {
        if (argc < 5) {
            Tcl_AppendResult(interp, "wrong # args: should be: ",
                             "\"insert_edge edge tail head ?-shapeswitch ...?\"",
                             (char *) NULL);
            return TCL_ERROR;
        }

        if ((getedgespec(v, argv[2], &ve, TRUE))) {
            Tcl_AppendResult(interp, "\"",
                             argv[2], "\" is already in layout.", (char *) NULL);
            return TCL_ERROR;
        }

        if (!(getnodespec(v, argv[3], &vn, FALSE))) {
            Tcl_AppendResult(interp, "tail node \"",
                             argv[3], "\" is not in layout.", (char *) NULL);
            return TCL_ERROR;
        }

        ((ILedge_t *)ve)->tail.term = (ILobj_t *)vn;

        if (!(getnodespec(v, argv[4], &vn, FALSE))) {
            Tcl_AppendResult(interp, "head node \"",
                             argv[4], "\" is not in layout.", (char *) NULL);
            return TCL_ERROR;
        }

        ((ILedge_t *)ve)->head.term = (ILobj_t *)vn;

        if (argc >= 5) {
            if ((dgedgeshape(v, argc-5, &argv[5], (ILedge_t *)ve)) != TCL_OK) {
                return TCL_ERROR;
            }
        }

        save = dglCallbacks(v, FALSE);
        rc = ilinsert((ILview_t *)v, (ILobj_t *)ve);
        dglCallbacks(v, save);

        if (!rc) {
            Tcl_AppendResult(interp,
                             "edge modification rejected by layout engine",
                             (char *) NULL);
            return TCL_ERROR;
        }

        return TCL_OK;

    } else if ((c == 'i') && (strncmp(argv[1], "insert_node", length) == 0)) {
        if (argc < 3) {
            Tcl_AppendResult(interp, "wrong # args: should be: ",
                             "\"insert_node node ?-shapeswitch ...?\"",
                             (char *) NULL);
            return TCL_ERROR;
        }

        if ((getnodespec(v, argv[2], &vn, TRUE))) {
            Tcl_AppendResult(interp, "\"",
                             argv[2], "\" is already in layout.", (char *) NULL);
            return TCL_ERROR;
        }

        if (argc >= 3) {
            if ((dgnodeshape(v, argc-3, &argv[3], (ILnode_t *)vn)) != TCL_OK) {
                return TCL_ERROR;
            }
        }

        save = dglCallbacks(v, FALSE);
        rc = ilinsert((ILview_t *)v, (ILobj_t *)vn);
        dglCallbacks(v, save);

        if (!rc) {
            Tcl_AppendResult(interp,
                             "node modification rejected by layout engine",
                             (char *) NULL);
            return TCL_ERROR;
        }

        return TCL_OK;

    } else if ((c == 'm') && (strncmp(argv[1], "modify_edge", length) == 0)) {
        if (argc < 4) {
            Tcl_AppendResult(interp, "wrong # args: should be: ",
                             "\"modify_edge edge -shapeswitch ?-shapeswitch ...?\"",
                             (char *) NULL);
            return TCL_ERROR;
        }

        if (!(getedgespec(v, argv[2], &ve, FALSE))) {
            Tcl_AppendResult(interp, "\"",
                             argv[2], "\" not found in layout.", (char *) NULL);
            return TCL_ERROR;
        }

        if ((dgedgeshape(v, argc-3, &argv[3], (ILedge_t *)ve)) != TCL_OK) {
            return TCL_ERROR;
        }

        save = dglCallbacks(v, FALSE);
        rc = ilmodify((ILview_t *)v, (ILobj_t *)ve);
        dglCallbacks(v, save);

        if (!rc) {
            Tcl_AppendResult(interp,
                             "edge modification rejected by layout engine",
                             (char *) NULL);
            return TCL_ERROR;
        }

        return TCL_OK;

    } else if ((c == 'm') && (strncmp(argv[1], "modify_node", length) == 0)) {
        if (argc < 4) {
            Tcl_AppendResult(interp, "wrong # args: should be: ",
                             "\"modify_node node -shapeswitch ?-shapeswitch ...?\"",
                             (char *) NULL);
            return TCL_ERROR;
        }

        if (!(getnodespec(v, argv[2], &vn, FALSE))) {
            Tcl_AppendResult(interp, "\"",
                             argv[2], "\" not found in layout.", (char *) NULL);
            return TCL_ERROR;
        }

        if ((dgnodeshape(v, argc-3, &argv[3], (ILnode_t *)vn)) != TCL_OK) {
            return TCL_ERROR;
        }

        save = dglCallbacks(v, FALSE);
        rc = ilmodify((ILview_t *)v, (ILobj_t *)vn);
        dglCallbacks(v, save);

        if (!rc) {
            Tcl_AppendResult(interp,
                             "node modification rejected by layout engine",
                             (char *) NULL);
            return TCL_ERROR;
        }

        return TCL_OK;

    } else {
        Tcl_AppendResult(interp, "bad option \"", argv[1], "\" must be one of:",
                         "\n\tbatch, bind, defaultnode, defaultedge,",
                         "\n\tget_edge, get_node,",
                         "\n\tinsert_edge, insert_node,",
                         "\n\tmodify_edge, modify_node,",
                         "\n\tdelete_edge, delete_node.",
                         (char *) NULL);
    }

    return TCL_ERROR;
}

#include <engine.h>

#ifdef DMALLOC
#include "dmalloc.h"
#endif

/* this illustrates how one might employ inheritance */

typedef struct geoview_s {
    engview_t	base;
    int			ctr;	/* count number of layout updates */
}

geoview_t;

static ilbool GeoOpen(ILview_t *client_view) {
    engview_t	*rv;
    extern ILengine_t GeoGraph;	/* forward reference */

    unsigned int S[] = {
                           sizeof(geoview_t),
                           sizeof(engnode_t),
                           sizeof(engedge_t)
                       };

    rv = il_open_view(&GeoGraph, client_view, Agdirected, S);
    return TRUE;
}

static void GeoClose(ILview_t *client_view) {
    il_close_view(client_view->pvt);
}

static ilcoord_t choose_random_pos(engview_t *view) {
    ilcoord_t	rv;
    double		f;

    f = il_nodesep(view).x;
    rv.x = f * agnnodes(view->model.main) * drand48();
    rv.y = f * agnnodes(view->model.main) * drand48();
    return rv;
}

static void place_new_nodes(ILview_t *view) {
    Agraph_t	*new_node_graph;
    Agnode_t	*n;
    ILnode_t	*spec;
    engview_t	*ev;

    new_node_graph = view->pvt->model.v[IL_INS];
    ev = view->pvt;

    for (n = agfstnode(new_node_graph); n; n = agnxtnode(n)) {
        spec = il_node(n);

        if (spec->pos_valid == FALSE)	/* need auto-placement */
            il_set_pos(n,choose_random_pos(ev));
    }
}

static void move_old_nodes(ILview_t *view) {
    Agraph_t	*moved_node_graph;
    Agnode_t	*n0, *n;
    Agedge_t	*e;
    ILnode_t	*spec;

    moved_node_graph = view->pvt->model.v[IL_MOD];

    for (n0 = agfstnode(moved_node_graph); n0; n0 = agnxtnode(n0)) {
        n = agsubnode(view->pvt->model.main,n0,FALSE);
        spec = il_node(n);

        if ((spec->update & IL_UPD_MOVE) == FALSE)
            continue;

        if (spec->pos_valid == FALSE)	/* need auto-placement */
            il_set_pos(n,choose_random_pos(view->pvt));

        /* mark its edges as needing auto-placement */
        for (e = agfstedge(n); e; e = agnxtedge(e,n)) {
            il_register_edge_callback(view->pvt,il_edge(e),IL_MOD);
        }
    }
}

#if 0 /* not used */
static void layout_edge(ILview_t *view, Agedge_t *model_e) {
    Agnode_t	*u, *v;
    ILedge_t	*spec;
    engview_t	*ev;
    ilcurve_t	temp, *curve;
    ilcoord_t	A[2];

    ev = view->pvt;
    spec = il_edge(model_e);
    u = il_find_node(ev,(ILnode_t*)(spec->tail.term));
    v = il_find_node(ev,(ILnode_t*)(spec->head.term));

    /* note: if the user provided an edge route, we're about to lose it.
     * if we wanted to do this cleanly, we would need a test to determine
     * if the user's edge spline or polyline was consistent with the
     * endpoints and perhaps the rest of the layout.  (this is just demo code.)
     */

    if (spec->pos)
        il_freeshape(vmregion(spec->pos),spec->pos);

    /* all this just to draw a line! */
    A[0] = il_pos(u);

    A[1] = il_pos(v);

    temp.p = A;

    temp.n = 2;

    temp.type = IL_POLYLINE;

    curve = il_clip_endpoints(ev, &temp, il_node(u), il_node(v));

    spec->pos = il_newshape(agheap(ilmodel(view)), curve, NIL(ilshape_t*));
}

#endif

#if 0  /* not used */
static void adjust_edges_of(ILview_t *view, Agraph_t *g) {
    Agnode_t	*n;
    Agedge_t	*e;

    for (n = agfstnode(g); n; n = agnxtnode(n))
        for (e = agfstout(n); e; e = agnxtout(e))
            layout_edge(view,e);
}

#endif

static void adjust_edges(ILview_t *view) {
#ifndef EDGEROUTER
    adjust_edges_of(view,view->pvt->model.e[IL_INS]);
    adjust_edges_of(view,view->pvt->model.e[IL_MOD]);
#else
    ilroutem(view);
#endif
}

static ilbool GeoWork(ILview_t *view) {
    geoview_t	*gv;
    engview_t	*ev;
    ilbool		rv;

    ev = view->pvt;
    gv = (geoview_t*)ev;

    place_new_nodes(view);
    move_old_nodes(view);
    adjust_edges(view);
    rv = il_issue_callbacks(ev);
    il_clear_callbacks(ev);
    gv->ctr++;
    return rv;
}

ILengine_t GeoGraph = {
                          GeoOpen,
                          GeoClose,
                          il_batch_ins,
                          il_batch_mod,
                          il_batch_del,
                          GeoWork,
                          ilmdlobj_to_spec,
                          ilspec_to_mdlobj
                      };

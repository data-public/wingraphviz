
/*  A Bison parser, made from parser.y
    by GNU Bison version 1.28  */

#define YYBISON 1  /* Identify Bison output.  */

#define	T_graph	257
#define	T_digraph	258
#define	T_strict	259
#define	T_node	260
#define	T_edge	261
#define	T_edgeop	262
#define	T_symbol	263
#define	T_subgraph	264


/*
    This software may only be used by you under license from AT&T Corp.
    ("AT&T").  A copy of AT&T's Source Code Agreement is available at
    AT&T's Internet website having the URL:
    <http://www.research.att.com/sw/tools/graphviz/license/source.html>
    If you received this software without first entering into a license
    with AT&T, you have an infringing copy of this software and cannot use
    it without violating AT&T's intellectual property rights.
*/


#include	"libgraph.h"

#ifdef DMALLOC
#include "dmalloc.h"
#endif

static char		Port[SMALLBUF],*Symbol;
static char		In_decl,In_edge_stmt;

static int		Current_class,Agraph_type;
static Agraph_t		*G;
static Agnode_t		*N;
static Agedge_t		*E;
static objstack_t	*SP;
static Agraph_t		*Gstack[32];
static int			GSP;
static int			override;

/* agoverride:
 * If override == 1, initial attr_stmt is ignored if the attribute is
 *                   already defined.
 * If override > 2,  any attr_stmt is ignored if the attribute is
 *                   already defined with non-trivial default.
 */
void agoverride(int ov) {
    override = (ov > 0 ? ov : 0);
}

static void push_subg(Agraph_t *g) {
    G = Gstack[GSP++] = g;
}

static Agraph_t *pop_subg(void) {
    Agraph_t		*g;

    if (GSP == 0) {
        errorPrintf(stderr,"Gstack underflow in graph parser\n");
        exit(1);
    }

    g = Gstack[--GSP];					/* graph being popped off */

    if (GSP > 0)
        G = Gstack[GSP - 1];	/* current graph */
    else
        G = 0;

    return g;
}

static objport_t pop_gobj(void) {
    objport_t	rv;
    rv.obj = pop_subg();
    rv.port = NULL;
    return rv;
}

static void begin_graph(char *name) {
    Agraph_t		*g;
    g = AG.parsed_g = agopen(name,Agraph_type);
    push_subg(g);
    In_decl = TRUE;
}

static void end_graph(void) {
    pop_subg();
}

static Agnode_t *bind_node(char *name) {
    Agnode_t	*n = agnode(G,name);
    In_decl = FALSE;
    return n;
}

static void anonsubg(void) {
    static int		anon_id = 0;
    char			buf[SMALLBUF];
    Agraph_t			*subg;

    In_decl = FALSE;
    sprintf(buf,"_anonymous_%d",anon_id++);
    subg = agsubg(G,buf);
    push_subg(subg);
}

static void begin_edgestmt(objport_t objp) {

    struct objstack_t	*new_sp;

    new_sp = NEW(objstack_t);
    new_sp->link = SP;
    SP = new_sp;
    SP->list = SP->last = NEW(objlist_t);
    SP->list->data  = objp;
    SP->list->link = NULL;
    SP->in_edge_stmt = In_edge_stmt;
    SP->subg = G;
    agpushproto(G);
    In_edge_stmt = TRUE;
}

static void mid_edgestmt(objport_t objp) {
    SP->last->link = NEW(objlist_t);
    SP->last = SP->last->link;
    SP->last->data = objp;
    SP->last->link = NULL;
}

static void end_edgestmt(void) {
    objstack_t	*old_SP;
    objlist_t	*tailptr,*headptr,*freeptr;
    Agraph_t		*t_graph,*h_graph;
    Agnode_t	*t_node,*h_node,*t_first,*h_first;
    Agedge_t	*e;
    char		*tport,*hport;

    for (tailptr = SP->list; tailptr->link; tailptr = tailptr->link) {
        headptr = tailptr->link;
        tport = tailptr->data.port;
        hport = headptr->data.port;

        if (TAG_OF(tailptr->data.obj) == TAG_NODE) {
            t_graph = NULL;
            t_first = (Agnode_t*)(tailptr->data.obj);

        } else {
            t_graph = (Agraph_t*)(tailptr->data.obj);
            t_first = agfstnode(t_graph);
        }

        if (TAG_OF(headptr->data.obj) == TAG_NODE) {
            h_graph = NULL;
            h_first = (Agnode_t*)(headptr->data.obj);

        } else {
            h_graph = (Agraph_t*)(headptr->data.obj);
            h_first = agfstnode(h_graph);
        }

        for (t_node = t_first; t_node; t_node = t_graph ?
                                                agnxtnode(t_graph,t_node) : NULL) {
            for (h_node = h_first; h_node; h_node = h_graph ?
                                                    agnxtnode(h_graph,h_node) : NULL ) {
                e = agedge(G,t_node,h_node);

                if (e) {
                    char	*tp = tport;
                    char 	*hp = hport;

                    if ((e->tail != e->head) && (e->head == t_node)) {
                        /* could happen with an undirected edge */
                        char 	*temp;
                        temp = tp;
                        tp = hp;
                        hp = temp;
                    }

                    if (tp && tp[0])
                        agxset(e,TAILX,tp);

                    if (hp && hp[0])
                        agxset(e,HEADX,hp);
                }
            }
        }
    }

    tailptr = SP->list;

    while (tailptr) {
        freeptr = tailptr;
        tailptr = tailptr->link;

        if (TAG_OF(freeptr->data.obj) == TAG_NODE)
            free(freeptr->data.port);

        free(freeptr);
    }

    if (G != SP->subg)
        abort();

    agpopproto(G);

    In_edge_stmt = SP->in_edge_stmt;

    old_SP = SP;

    SP = SP->link;

    In_decl = FALSE;

    free(old_SP);

    Current_class = TAG_GRAPH;
}

static void attr_set(char *name, char *value) {
    Agsym_t		*ap = NULL;
    char		*defval = "";

    if (In_decl && (G->root == G))
        defval = value;

    switch (Current_class) {

            case TAG_NODE:
            ap = agfindattr(G->proto->n,name);

            if (ap == NULL)
                ap = agnodeattr(AG.parsed_g,name,defval);
            else if (override &&
                     (In_decl ||
                      ((override > 1) && (N == G->proto->n) && *(ap->value))))
                return;

            agxset(N,ap->index,value);

            break;

            case TAG_EDGE:
            ap = agfindattr(G->proto->e,name);

            if (ap == NULL)
                ap = agedgeattr(AG.parsed_g,name,defval);
            else if (override &&
                     (In_decl ||
                      ((override > 1) && (E == G->proto->e) && *(ap->value))))
                return;

            agxset(E,ap->index,value);

            break;

            case 0:		/* default */

            case TAG_GRAPH:
            ap = agfindattr(G,name);

            if (ap == NULL)
                ap = agraphattr(AG.parsed_g,name,defval);
            else if (override && (In_decl || ((override > 1) && *(ap->value))))
                return;

            agxset(G,ap->index,value);

            break;
    }
}


typedef union	{
    int					i;
    char				*str;

    struct objport_t	obj;

    struct Agnode_t		*n;
} YYSTYPE;
#include <stdio.h>

#ifndef __cplusplus
#ifndef __STDC__
#define const
#endif
#endif



#define	YYFINAL		91
#define	YYFLAG		-32768
#define	YYNTBASE	22

#define YYTRANSLATE(x) ((unsigned)(x) <= 264 ? agtranslate[x] : 58)

static const char agtranslate[] = {
                                      0,
                                      2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
                                      2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
                                      2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
                                      2,     2,     2,     2,     2,     2,     2,     2,     2,    19,
                                      20,     2,     2,    13,     2,     2,     2,     2,     2,     2,
                                      2,     2,     2,     2,     2,     2,     2,    18,    17,     2,
                                      16,     2,     2,    21,     2,     2,     2,     2,     2,     2,
                                      2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
                                      2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
                                      14,     2,    15,     2,     2,     2,     2,     2,     2,     2,
                                      2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
                                      2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
                                      2,     2,    11,     2,    12,     2,     2,     2,     2,     2,
                                      2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
                                      2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
                                      2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
                                      2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
                                      2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
                                      2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
                                      2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
                                      2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
                                      2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
                                      2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
                                      2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
                                      2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
                                      2,     2,     2,     2,     2,     1,     3,     4,     5,     6,
                                      7,     8,     9,    10
                                  };




#if YYDEBUG != 0 || defined (YYERROR_VERBOSE)

static const char * const agtname[] = {   "$","error","$undefined.","T_graph",
                                      "T_digraph","T_strict","T_node","T_edge","T_edgeop","T_symbol","T_subgraph",
                                      "'{'","'}'","','","'['","']'","'='","';'","':'","'('","')'","'@'","file","@1",
                                      "graph_type","attr_class","inside_attr_list","optcomma","attr_list","rec_attr_list",
                                      "opt_attr_list","attr_set","iattr_set","stmt_list","stmt_list1","stmt","stmt1",
                                      "attr_stmt","node_id","node_name","node_port","port_location","@2","port_angle",
                                      "node_stmt","@3","edge_stmt","@4","@5","@6","@7","edgeRHS","@8","@9","subg_stmt",
                                      "@10","subg_hdr","symbol", NULL
                                      };

#endif

static const short agr1[] = {
                                0,
                                23,    22,    22,    22,    24,    24,    24,    24,    25,    25,
                                25,    26,    26,    27,    27,    28,    29,    29,    30,    31,
                                32,    32,    33,    33,    34,    34,    35,    35,    35,    36,
                                36,    36,    36,    37,    37,    38,    39,    40,    40,    40,
                                40,    40,    41,    42,    41,    43,    45,    44,    47,    48,
                                46,    49,    50,    46,    51,    52,    51,    51,    53,    51,
                                54,    55,    54,    54,    56,    57
                            };

static const short agr2[] = {
                                0,
                                0,     6,     1,     0,     1,     2,     1,     2,     1,     1,
                                1,     3,     0,     0,     1,     3,     2,     0,     1,     3,
                                1,     1,     1,     0,     1,     2,     1,     2,     1,     1,
                                1,     1,     1,     2,     1,     2,     1,     0,     1,     1,
                                2,     2,     2,     0,     7,     2,     0,     3,     0,     0,
                                5,     0,     0,     5,     2,     0,     4,     2,     0,     4,
                                4,     0,     4,     1,     2,     1
                            };

static const short agdefact[] = {
                                    0,
                                    3,     5,     7,     0,     0,     6,     8,    66,     1,     0,
                                    0,    29,     9,    10,    11,     0,    62,     0,    35,     0,
                                    0,    25,    27,    32,    47,    38,    30,    31,    33,    64,
                                    37,    65,     0,    13,    34,     2,    26,    28,    18,     0,
                                    0,     0,    36,    39,    40,     0,     0,     0,     0,     0,
                                    21,    14,    22,    19,    48,     0,    50,     0,    43,    46,
                                    42,    41,    53,     0,    20,    63,    16,    15,    13,    17,
                                    55,    58,    37,    18,    44,    18,    61,    12,     0,     0,
                                    51,     0,    54,    57,    60,     0,     0,    45,     0,     0,
                                    0
                                };

static const short agdefgoto[] = {
                                     89,
                                     10,     5,    18,    50,    69,    35,    54,    55,    19,    52,
                                     20,    21,    22,    23,    24,    25,    26,    43,    44,    82,
                                     45,    27,    39,    28,    40,    74,    46,    76,    57,    79,
                                     80,    29,    33,    30,    31
                                 };

static const short agpact[] = {
                                  5,
                                  -32768,-32768,-32768,    51,    -2,-32768,-32768,-32768,-32768,     6,
                                  13,-32768,-32768,-32768,-32768,    -2,-32768,     7,-32768,    14,
                                  38,-32768,    11,-32768,    32,     9,-32768,-32768,    34,    35,
                                  36,-32768,    13,    -2,-32768,-32768,-32768,-32768,-32768,    48,
                                  -7,    -2,-32768,    37,    39,    48,    13,    -2,    47,    45,
                                  -32768,    49,    36,     7,-32768,    22,-32768,    -2,-32768,-32768,
                                  -32768,-32768,-32768,    53,-32768,-32768,-32768,-32768,    -2,-32768,
                                  55,    58,-32768,-32768,-32768,-32768,-32768,-32768,    48,    48,
                                  -32768,    54,-32768,-32768,-32768,    -2,    41,-32768,    68,    69,
                                  -32768
                              };

static const short agpgoto[] = {
                                   -32768,
                                   -32768,-32768,-32768,     1,-32768,    17,-32768,   -61,   -31,-32768,
                                   -29,-32768,    52,-32768,-32768,    16,-32768,-32768,    29,-32768,
                                   31,-32768,-32768,-32768,-32768,-32768,-32768,-32768,   -45,-32768,
                                   -32768,    20,-32768,-32768,    -5
                               };


#define	YYLAST		81


static const short agtable[] = {
                                   9,
                                   63,     8,    51,    49,    -4,     1,     8,     2,     3,     4,
                                   32,    58,    81,    12,    83,    13,    11,    64,    14,    15,
                                   34,     8,    16,    17,   -24,    36,    41,    38,    53,    42,
                                   8,    16,    17,    84,    85,    59,    60,    51,    12,   -49,
                                   13,   -52,    65,    14,    15,    47,     8,    16,    17,   -23,
                                   73,    48,    75,     6,     7,    56,    41,    42,    66,    67,
                                   88,    68,   -56,    53,    77,   -59,    86,    90,    91,    78,
                                   70,    71,    37,    62,    61,    72,     0,     0,     0,     0,
                                   87
                               };

static const short agcheck[] = {
                                   5,
                                   46,     9,    34,    33,     0,     1,     9,     3,     4,     5,
                                   16,    19,    74,     1,    76,     3,    11,    47,     6,     7,
                                   14,     9,    10,    11,    12,    12,    18,    17,    34,    21,
                                   9,    10,    11,    79,    80,    41,    42,    69,     1,     8,
                                   3,     8,    48,     6,     7,    11,     9,    10,    11,    12,
                                   56,    16,    58,     3,     4,     8,    18,    21,    12,    15,
                                   20,    13,     8,    69,    12,     8,    13,     0,     0,    69,
                                   54,    56,    21,    45,    44,    56,    -1,    -1,    -1,    -1,
                                   86
                               };

/* -*-C-*-  Note some compilers choke on comments on `#line' lines.  */
/* This file comes from bison-1.28.  */

/* Skeleton output parser for bison,
   Copyright (C) 1984, 1989, 1990 Free Software Foundation, Inc.
 
   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.
 
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
 
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.  */

/* As a special exception, when this file is copied by Bison into a
   Bison output file, you may use that output file without restriction.
   This special exception was added by the Free Software Foundation
   in version 1.24 of Bison.  */

/* This is the parser code that is written into each bison parser
  when the %semantic_parser declaration is not specified in the grammar.
  It was written by Richard Stallman by simplifying the hairy parser
  used when %semantic_parser is specified.  */

#ifndef YYSTACK_USE_ALLOCA
#ifdef alloca
#define YYSTACK_USE_ALLOCA
#else /* alloca not defined */
#ifdef __GNUC__
#define YYSTACK_USE_ALLOCA
#define alloca __builtin_alloca
#else /* not GNU C.  */
#if (!defined (__STDC__) && defined (sparc)) || defined (__sparc__) || defined (__sparc) || defined (__sgi) || (defined (__sun) && defined (__i386))
#define YYSTACK_USE_ALLOCA
#include <alloca.h>
#else /* not sparc */
/* We think this test detects Watcom and Microsoft C.  */
/* This used to test MSDOS, but that is a bad idea
since that symbol is in the user namespace.  */
#if (defined (_MSDOS) || defined (_MSDOS_)) && !defined (__TURBOC__)
#if 0 /* No need for malloc.h, which pollutes the namespace;
instead, just don't use alloca.  */
#include <malloc.h>
#endif
#else /* not MSDOS, or __TURBOC__ */
#if defined(_AIX)

/* I don't know what this was needed for, but it pollutes the namespace.
So I turned it off.   rms, 2 May 1997.  */
/* #include <malloc.h>  */
#pragma alloca
#define YYSTACK_USE_ALLOCA
#else /* not MSDOS, or __TURBOC__, or _AIX */
#if 0
#ifdef __hpux /* haible@ilog.fr says this works for HPUX 9.05 and up,
and on HPUX 10.  Eventually we can turn this on.  */
#define YYSTACK_USE_ALLOCA
#define alloca __builtin_alloca
#endif /* __hpux */
#endif
#endif /* not _AIX */
#endif /* not MSDOS, or __TURBOC__ */
#endif /* not sparc */
#endif /* not GNU C */
#endif /* alloca not defined */
#endif /* YYSTACK_USE_ALLOCA not defined */

#ifdef YYSTACK_USE_ALLOCA
#define YYSTACK_ALLOC alloca
#else
#define YYSTACK_ALLOC malloc
#endif

/* Note: there must be only one dollar sign in this file.
   It is replaced by the list of actions, each action
   as one case of the switch.  */

#define agerrok		(agerrstatus = 0)
#define agclearin	(agchar = YYEMPTY)
#define YYEMPTY		-2
#define YYEOF		0
#define YYACCEPT	goto agacceptlab
#define YYABORT 	goto agabortlab
#define YYERROR		goto agerrlab1
/* Like YYERROR except do call agerror.
   This remains here temporarily to ease the
   transition to the new meaning of YYERROR, for GCC.
   Once GCC version 2 has supplanted version 1, this can go.  */
#define YYFAIL		goto agerrlab
#define YYRECOVERING()  (!!agerrstatus)
#define YYBACKUP(token, value) \
do								\
if (agchar == YYEMPTY && aglen == 1)				\
{ agchar = (token), aglval = (value);			\
agchar1 = YYTRANSLATE (agchar);				\
YYPOPSTACK;						\
goto agbackup;						\
}								\
else								\
{ agerror ("syntax error: cannot back up"); YYERROR; }	\
while (0)

#define YYTERROR	1
#define YYERRCODE	256

#ifndef YYPURE
#define YYLEX		aglex()
#endif

#ifdef YYPURE
#ifdef YYLSP_NEEDED
#ifdef YYLEX_PARAM
#define YYLEX		aglex(&aglval, &aglloc, YYLEX_PARAM)
#else
#define YYLEX		aglex(&aglval, &aglloc)
#endif
#else /* not YYLSP_NEEDED */
#ifdef YYLEX_PARAM
#define YYLEX		aglex(&aglval, YYLEX_PARAM)
#else
#define YYLEX		aglex(&aglval)
#endif
#endif /* not YYLSP_NEEDED */
#endif

/* If nonreentrant, generate the variables here */

#ifndef YYPURE

int	agchar;			/*  the lookahead symbol		*/

YYSTYPE	aglval;			/*  the semantic value of the		*/

/*  lookahead symbol			*/


int agnerrs;			/*  number of parse errors so far       */

#endif  /* not YYPURE */

#if YYDEBUG != 0
int agdebug;			/*  nonzero means print parse trace	*/

/* Since this is uninitialized, it does not stop multiple parsers
   from coexisting.  */
#endif

/*  YYINITDEPTH indicates the initial size of the parser's stacks	*/

#ifndef	YYINITDEPTH
#define YYINITDEPTH 200
#endif

/*  YYMAXDEPTH is the maximum size the stacks can grow to
    (effective only if the built-in stack extension method is used).  */

#if YYMAXDEPTH == 0
#undef YYMAXDEPTH
#endif

#ifndef YYMAXDEPTH
#define YYMAXDEPTH 10000
#endif

/* Define __ag_memcpy.  Note that the size argument
   should be passed with type unsigned int, because that is what the non-GCC
   definitions require.  With GCC, __builtin_memcpy takes an arg
   of type size_t, but it can handle unsigned int.  */

#if __GNUC__ > 1		/* GNU C and GNU C++ define this.  */
#define __ag_memcpy(TO,FROM,COUNT)	__builtin_memcpy(TO,FROM,COUNT)
#else				/* not GNU C or C++ */
#ifndef __cplusplus

/* This is the most reliable way to avoid incompatibilities
in available built-in functions on various systems.  */
static void
__ag_memcpy (to, from, count)
char *to;

char *from;

unsigned int count;

{
    register char *f = from;
    register char *t = to;
    register int i = count;

    while (i-- > 0)
        *t++ = *f++;
}

#else /* __cplusplus */

/* This is the most reliable way to avoid incompatibilities
in available built-in functions on various systems.  */
static void
__ag_memcpy (char *to, char *from, unsigned int count) {
    register char *t = to;
    register char *f = from;
    register int i = count;

    while (i-- > 0)
        *t++ = *f++;
}

#endif
#endif


/* The user can define YYPARSE_PARAM as the name of an argument to be passed
   into agparse.  The argument should have type void *.
   It should actually point to an object.
   Grammar actions can access the variable by casting it
   to the proper pointer type.  */

#ifdef YYPARSE_PARAM
#ifdef __cplusplus
#define YYPARSE_PARAM_ARG void *YYPARSE_PARAM
#define YYPARSE_PARAM_DECL
#else /* not __cplusplus */
#define YYPARSE_PARAM_ARG YYPARSE_PARAM
#define YYPARSE_PARAM_DECL void *YYPARSE_PARAM;
#endif /* not __cplusplus */
#else /* not YYPARSE_PARAM */
#define YYPARSE_PARAM_ARG
#define YYPARSE_PARAM_DECL
#endif /* not YYPARSE_PARAM */

/* Prevent warning if -Wstrict-prototypes.  */
#ifdef __GNUC__
#ifdef YYPARSE_PARAM
int agparse (void *);

#else
int agparse (void);

#endif
#endif

int
agparse(YYPARSE_PARAM_ARG)
YYPARSE_PARAM_DECL
{
    register int agstate;
    register int agn;
    register short *agssp;
    register YYSTYPE *agvsp;
    int agerrstatus;	/*  number of tokens to shift before error messages enabled */
    int agchar1 = 0;		/*  lookahead token as an internal (translated) token number */

    short	agssa[YYINITDEPTH];	/*  the state stack			*/
    YYSTYPE agvsa[YYINITDEPTH];	/*  the semantic value stack		*/

    short *agss = agssa;		/*  refer to the stacks thru separate pointers */
    YYSTYPE *agvs = agvsa;	/*  to allow agoverflow to reallocate them elsewhere */

#define YYPOPSTACK   (agvsp--, agssp--)

    int agstacksize = YYINITDEPTH;
    int agfree_stacks = 0;

#ifdef YYPURE

    int agchar;
    YYSTYPE aglval;
    int agnerrs;

#endif

    YYSTYPE agval;		/*  the variable used to return		*/
    /*  semantic values from the action	*/
    /*  routines				*/

    int aglen;


    agstate = 0;

    agerrstatus = 0;

    agnerrs = 0;

    agchar = YYEMPTY;		/* Cause a token to be read.  */

    /* Initialize stack pointers.
       Waste one element of value and location stack
       so that they stay on the same level as the state stack.
       The wasted elements are never initialized.  */

    agssp = agss - 1;

    agvsp = agvs;



    /* Push a new state, which is found in  agstate  .  */
    /* In all cases, when you get here, the value and location stacks
       have just been pushed. so pushing a state here evens the stacks.  */

    agnewstate:

    *++agssp = agstate;

    if (agssp >= agss + agstacksize - 1) {
    /* Give user a chance to reallocate the stack */
    /* Use copies of these so that the &'s don't force the real ones into memory. */
    YYSTYPE *agvs1 = agvs;
    short *agss1 = agss;


    /* Get the current used size of the three stacks, in elements.  */
    size_t size = agssp - agss + 1;

#ifdef agoverflow
    /* Each stack pointer address is followed by the size of
    the data in use in that stack, in bytes.  */

    agoverflow("parser stack overflow",
               &agss1, size * sizeof (*agssp),
               &agvs1, size * sizeof (*agvsp),
               &agstacksize);


        agss = agss1;
        agvs = agvs1;

#else /* no agoverflow */
    /* Extend the stack our own way.  */

    if (agstacksize >= YYMAXDEPTH) {
            agerror("parser stack overflow");

            if (agfree_stacks) {
                free (agss);
                free (agvs);

            }

            return 2;
        }

        agstacksize *= 2;

        if (agstacksize > YYMAXDEPTH)
            agstacksize = YYMAXDEPTH;

#ifndef YYSTACK_USE_ALLOCA

        agfree_stacks = 1;

#endif

        agss = (short *) YYSTACK_ALLOC (agstacksize * sizeof (*agssp));

        __ag_memcpy ((char *)agss, (char *)agss1,
                     size * (unsigned int) sizeof (*agssp));

        agvs = (YYSTYPE *) YYSTACK_ALLOC (agstacksize * sizeof (*agvsp));

        __ag_memcpy ((char *)agvs, (char *)agvs1,
                     size * (unsigned int) sizeof (*agvsp));


#endif /* no agoverflow */

        agssp = agss + size - 1;

        agvsp = agvs + size - 1;




        if (agssp >= agss + agstacksize - 1)
            YYABORT;
    }


    goto agbackup;

    agbackup:

    /* Do appropriate processing given the current state.  */
    /* Read a lookahead token if we need one and don't already have one.  */
    /* agresume: */

    /* First try to decide what to do without reference to lookahead token.  */

    agn = agpact[agstate];

    if (agn == YYFLAG)
    goto agdefault;

    /* Not known => get a lookahead token if don't already have one.  */

    /* agchar is either YYEMPTY or YYEOF
       or a valid token in external form.  */

    if (agchar == YYEMPTY) {

        agchar = YYLEX;
    }

    /* Convert token to internal form (in agchar1) for indexing tables with */

    if (agchar <= 0)		/* This means end of input. */
    {
    agchar1 = 0;
    agchar = YYEOF;		/* Don't call YYLEX any more */


    } else {
    agchar1 = YYTRANSLATE(agchar);


    }

    agn += agchar1;

    if (agn < 0 || agn > YYLAST || agcheck[agn] != agchar1)
    goto agdefault;

    agn = agtable[agn];

    /* agn is what to do for this token type in this state.
       Negative => reduce, -agn is rule number.
       Positive => shift, agn is new state.
         New state is final state => don't bother to shift,
         just return success.
       0, or most negative number => error.  */

    if (agn < 0) {
        if (agn == YYFLAG)
                goto agerrlab;

            agn = -agn;

            goto agreduce;

        } else if (agn == 0)
        goto agerrlab;

        if (agn == YYFINAL)
            YYACCEPT;

            /* Shift the lookahead token.  */


            /* Discard the token being shifted unless it is eof.  */
            if (agchar != YYEOF)
                agchar = YYEMPTY;

                *++agvsp = aglval;



                /* count tokens shifted since error; after three, turn off error status.  */
                if (agerrstatus)
                    agerrstatus--;

                    agstate = agn;

                    goto agnewstate;

                    /* Do the default action for the current state.  */

    agdefault:

                    agn = agdefact[agstate];

                    if (agn == 0)
                        goto agerrlab;

                        /* Do a reduction.  agn is the number of a rule to reduce with.  */

    agreduce:
                        aglen = agr2[agn];

                        if (aglen > 0)
                            agval = agvsp[1-aglen]; /* implement default value of the action */




                            switch (agn) {

                                    case 1: {
                                        begin_graph(agvsp[0].str);
                                            agstrfree(agvsp[0].str);
                                            ;
                                            break;
                                        }

                                        case 2: {
                                            AG.accepting_state = TRUE;
                                            end_graph();
                                            ;
                                            break;
                                        }

                                        case 3: {
                                            if (AG.parsed_g)
                                                agclose(AG.parsed_g);

                                            AG.parsed_g = NULL;

                                            /*exit(1);*/

                                            ;
                                            break;
                                        }

                                        case 4: {
                                            AG.parsed_g = NULL;
                                            ;
                                            break;
                                        }

                                        case 5: {
                                            Agraph_type = AGRAPH;
                                            AG.edge_op = "--";
                                            ;
                                            break;
                                        }

                                        case 6: {
                                            Agraph_type = AGRAPHSTRICT;
                                            AG.edge_op = "--";
                                            ;
                                            break;
                                        }

                                        case 7: {
                                            Agraph_type = AGDIGRAPH;
                                            AG.edge_op = "->";
                                            ;
                                            break;
                                        }

                                        case 8: {
                                            Agraph_type = AGDIGRAPHSTRICT;
                                            AG.edge_op = "->";
                                            ;
                                            break;
                                        }

                                        case 9: {

                                            Current_class = TAG_GRAPH;
                                            ;
                                            break;
                                        }

                                        case 10: {

                                            Current_class = TAG_NODE;
                                            N = G->proto->n;
                                            ;
                                            break;
                                        }

                                        case 11: {

                                            Current_class = TAG_EDGE;
                                            E = G->proto->e;
                                            ;
                                            break;
                                        }

                                        case 20: {
                                            attr_set(agvsp[-2].str,agvsp[0].str);
                                            agstrfree(agvsp[-2].str);
                                            agstrfree(agvsp[0].str);
                                            ;
                                            break;
                                        }

                                        case 22: {
                                            attr_set(agvsp[0].str,"true");
                                            agstrfree(agvsp[0].str);
                                            ;
                                            break;
                                        }

                                        case 29: {
                                            agerror("syntax error, statement skipped");
                                            ;
                                            break;
                                        }

                                        case 33: {
                                            ;
                                            break;
                                        }

                                        case 34: {

                                            Current_class = TAG_GRAPH; /* reset */
                                            ;
                                            break;
                                        }

                                        case 35: {

                                            Current_class = TAG_GRAPH;
                                            ;
                                            break;
                                        }

                                        case 36: {
                                            objport_t		rv;
                                            rv.obj = agvsp[-1].n;
                                            rv.port = _strdup(Port);
                                            Port[0] = '\0';
                                            agval.obj = rv;
                                            ;
                                            break;
                                        }

                                        case 37: {
                                            agval.n = bind_node(agvsp[0].str);
                                            agstrfree(agvsp[0].str);
                                            ;
                                            break;
                                        }

                                        case 43: {
                                            strcat(Port,":");
                                            strcat(Port,agvsp[0].str);
                                            ;
                                            break;
                                        }

                                        case 44: {
                                            Symbol = _strdup(agvsp[0].str);
                                            ;
                                            break;
                                        }

                                        case 45: {
                                            char buf[SMALLBUF];
                                            sprintf(buf,":(%s,%s)",Symbol,agvsp[-1].str);
                                            strcat(Port,buf);
                                            free(Symbol);
                                            ;
                                            break;
                                        }

                                        case 46: {
                                            char buf[SMALLBUF];
                                            sprintf(buf,"@%s",agvsp[0].str);
                                            strcat(Port,buf);
                                            ;
                                            break;
                                        }

                                        case 47: {

                                            Current_class = TAG_NODE;
                                            N = (Agnode_t*)(agvsp[0].obj.obj);
                                            ;
                                            break;
                                        }

                                        case 48: {

                                            Current_class = TAG_GRAPH; /* reset */
                                            ;
                                            break;
                                        }

                                        case 49: {
                                            begin_edgestmt(agvsp[0].obj);
                                            ;
                                            break;
                                        }

                                        case 50: {
                                            E = SP->subg->proto->e;

                                            Current_class = TAG_EDGE;
                                            ;
                                            break;
                                        }

                                        case 51: {
                                            end_edgestmt();
                                            ;
                                            break;
                                        }

                                        case 52: {
                                            begin_edgestmt(agvsp[0].obj);
                                            ;
                                            break;
                                        }

                                        case 53: {
                                            E = SP->subg->proto->e;

                                            Current_class = TAG_EDGE;
                                            ;
                                            break;
                                        }

                                        case 54: {
                                            end_edgestmt();
                                            ;
                                            break;
                                        }

                                        case 55: {
                                            mid_edgestmt(agvsp[0].obj);
                                            ;
                                            break;
                                        }

                                        case 56: {
                                            mid_edgestmt(agvsp[0].obj);
                                            ;
                                            break;
                                        }

                                        case 58: {
                                            mid_edgestmt(agvsp[0].obj);
                                            ;
                                            break;
                                        }

                                        case 59: {
                                            mid_edgestmt(agvsp[0].obj);
                                            ;
                                            break;
                                        }

                                        case 61: {
                                            agval.obj = pop_gobj();
                                            ;
                                            break;
                                        }

                                        case 62: {
                                            anonsubg();
                                            ;
                                            break;
                                        }

                                        case 63: {
                                            agval.obj = pop_gobj();
                                            ;
                                            break;
                                        }

                                        case 64: {
                                            agval.obj = pop_gobj();
                                            ;
                                            break;
                                        }

                                        case 65: {
                                            Agraph_t	 *subg;

                                            if ((subg = agfindsubg(AG.parsed_g,agvsp[0].str)))
                                                aginsert(G,subg);
                                            else
                                                subg = agsubg(G,agvsp[0].str);

                                            push_subg(subg);

                                            In_decl = FALSE;

                                            agstrfree(agvsp[0].str);

                                            ;
                                            break;
                                        }

                                        case 66: {
                                            agval.str = agstrdup(agvsp[0].str);
                                            ;
                                            break;
                                        }
                                }

    /* the action file gets copied in in place of this dollarsign */
    
    agvsp -= aglen;

    agssp -= aglen;





    *++agvsp = agval;


    /* Now "shift" the result of the reduction.
       Determine what state that goes to,
       based on the state we popped back to
       and the rule number reduced by.  */

    agn = agr1[agn];

    agstate = agpgoto[agn - YYNTBASE] + *agssp;

    if (agstate >= 0 && agstate <= YYLAST && agcheck[agstate] == *agssp)
    agstate = agtable[agstate];
                            else
                                agstate = agdefgoto[agn - YYNTBASE];

                                goto agnewstate;

    agerrlab:   /* here on detecting error */

                                if (! agerrstatus)
                                    /* If not already recovering from an error, report this error.  */
                                {
                                    ++agnerrs;


                                    agerror("parse error");
                                    }

    goto agerrlab1;

    agerrlab1:   /* here on error raised explicitly by an action */

    if (agerrstatus == 3) {
    /* if just tried and failed to reuse lookahead token after an error, discard it.  */

    /* return failure if at end of input */

    if (agchar == YYEOF)
            YYABORT;

        agchar = YYEMPTY;
    }

    /* Else will try to reuse lookahead token
       after shifting the error token.  */

    agerrstatus = 3;		/* Each real token shifted decrements this */

    goto agerrhandle;

    agerrdefault:  /* current state does not do anything special for the error token. */

    agerrpop:   /* pop the current state because it cannot handle the error token */

    if (agssp == agss)
    YYABORT;

    agvsp--;

    agstate = *--agssp;

    agerrhandle:

    agn = agpact[agstate];

    if (agn == YYFLAG)
        goto agerrdefault;

        agn += YYTERROR;

        if (agn < 0 || agn > YYLAST || agcheck[agn] != YYTERROR)
            goto agerrdefault;

            agn = agtable[agn];

            if (agn < 0) {
                if (agn == YYFLAG)
                        goto agerrpop;

                    agn = -agn;

                    goto agreduce;

                } else if (agn == 0)
                goto agerrpop;

                if (agn == YYFINAL)
                    YYACCEPT;


                    *++agvsp = aglval;



                    agstate = agn;

                    goto agnewstate;

    agacceptlab:
                    /* YYACCEPT comes here.  */
                    if (agfree_stacks) {
                        free (agss);
                            free (agvs);

                        }

    return 0;

    agabortlab:
    /* YYABORT comes here.  */

    if (agfree_stacks) {
    free (agss);
        free (agvs);

    }

    return 1;
}


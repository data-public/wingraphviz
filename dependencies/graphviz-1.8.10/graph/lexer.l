%{
#include	"libgraph.h"
#include	"parser.h"

static char *lex_gets(void);
static void my_yyinput(char* buf, yy_size_t* result, yy_size_t max_size);
#define YY_INPUT(buf,result,max_size) my_yyinput(buf, &result, max_size);


static int line_num = 1;
static int html_nest = 0;  /* nesting level for html strings */
static int graphType;

void agreadline(int n) { line_num = n; }

static char	*Sbuf,*Sptr,*Send;
static void beginstr(void) {
	if (Sbuf == NIL(char*)) {
		Sbuf = malloc(BUFSIZ);
		Send = Sbuf + BUFSIZ;
	}
	Sptr = Sbuf;
	*Sptr = 0;
}

static void addstr(char *src) {
	char	c;
	if (Sptr > Sbuf) Sptr--;
	do {
		do {c = *Sptr++ = *src++;} while (c && (Sptr < Send));
		if (c) {
			long	sz = Send - Sbuf;
			long	off = Sptr - Sbuf;
			sz *= 2;
			Sbuf = (char*)realloc(Sbuf,sz);
			Send = Sbuf + sz;
			Sptr = Sbuf + off;
		}
	} while (c);
}

static void endstr(void) {
	aglval.str = Sbuf;
//	*Sbuf = 0;
}

/* twoDots:
 * Return true if token has more than one '.';
 * we know the last character is a '.'.
 */
static int twoDots(void)
{
    int i;
    for (i = yyleng-2; i >= 0; i--) {
	if (((unsigned char)yytext[i]) == '.')
	    return 1;
    }
    return 0;
}

/* chkNum:
 * The regexp for NUMBER allows a terminating letter or '.'.
 * This way we can catch a number immediately followed by a name
 * or something like 123.456.78, and report this to the user.
 */
static int chkNum(void) {
    unsigned char c = (unsigned char)yytext[yyleng-1];   /* last character */
    if ((!isdigit(c) && (c != '.')) || ((c == '.') && twoDots())) {  /* c is letter */
	/*
		unsigned char xbuf[BUFSIZ];
		char buf[BUFSIZ];
		agxbuf  xb;
		char* fname;

		if (InputFile)
			fname = InputFile;
		else
			fname = "input";

		agxbinit(&xb, BUFSIZ, xbuf);

		agxbput(&xb,"syntax ambiguity - badly delimited number '");
		agxbput(&xb,yytext);
		sprintf(buf,"' in line %d of ", line_num);
		agxbput(&xb,buf);
		agxbput(&xb,fname);
		agxbput(&xb, " splits into two tokens\n");
		agerr(AGWARN, "%s", agxbuse(&xb));

		agxbfree(&xb);*/
		return 1;
    }
    else return 0;
}


#define YY_NO_UNISTD_H
%}

%option noyywrap

GRAPH_EOF_TOKEN				[@]	
LETTER [A-Za-z_\200-\377]
DIGIT	[0-9]
NAME	{LETTER}({LETTER}|{DIGIT})*
NUMBER	[-]?(({DIGIT}+(\.{DIGIT}*)?)|(\.{DIGIT}+))(\.|{LETTER})?
ID		({NAME}|{NUMBER})
%x comment
%x qstring
%x hstring
%%
{GRAPH_EOF_TOKEN}		return(EOF);
<INITIAL,comment,qstring>\n	line_num++;
"/*"					BEGIN(comment);
<comment>[^*\n]*		/* eat anything not a '*' */
<comment>"*"+[^*/\n]*	/* eat up '*'s not followed by '/'s */
<comment>"*"+"/"		BEGIN(INITIAL);
"//".*					/* ignore C++-style comments */
"#".*					/* ignore shell-like comments */
[ \t\r]					/* ignore whitespace */
"\xEF\xBB\xBF"				/* ignore BOM */
"node"					return(T_node);			/* see tokens in agcanonstr */
"edge"					return(T_edge);
"graph"					if (!graphType) graphType = T_graph; return(T_graph);
"digraph"				if (!graphType) graphType = T_digraph; return(T_digraph);
"strict"				return(T_strict);
"subgraph"				return(T_subgraph);
"->"				if (graphType == T_digraph) return(T_edgeop); else return('-');
"--"				if (graphType == T_graph) return(T_edgeop); else return('-');
{NAME}					{ aglval.str = yytext; return(T_symbol); }
{NUMBER}				{ if (chkNum()) yyless(yyleng-1); aglval.str = yytext; return(T_symbol); }
["]						BEGIN(qstring); beginstr();
<qstring>["]			BEGIN(INITIAL); endstr(); return (T_symbol);
<qstring>[\\]["]		addstr ("\"");
<qstring>[\\][\\]		addstr ("\\\\");
<qstring>[\\][\n]		line_num++; /* ignore escaped newlines */
<qstring>([^"\\]*|[\\])		addstr(yytext);
[<]						BEGIN(hstring); html_nest = 1; beginstr();
<hstring>[>]			html_nest--; if (html_nest) addstr(yytext); else {BEGIN(INITIAL); endstr(); return (T_symbol);}
<hstring>[<]			html_nest++; addstr(yytext);
<hstring>[\n]			addstr(yytext); line_num++; /* add newlines */
<hstring>([^><\n]*)		addstr(yytext);
.						return (yytext[0]);
%%
 
void agerror(const char *str)
{
}

static int		Line_number;
static gets_f	Lexer_gets;
static FILE		*Lexer_fp;
static char		*LexPtr,*TokenBuf;
static int		LineBufSize;

#if defined _DEBUG
extern int agdebug;
#endif

void aglexinit(FILE* fp, gets_f mygets) {
    Lexer_fp = fp;
    Lexer_gets = mygets;
    LexPtr = NULL;

    if (AG.linebuf == NULL) {
        LineBufSize = BUFSIZ;
        AG.linebuf = N_NEW(LineBufSize,char);
        TokenBuf = N_NEW(LineBufSize,char);
    }

    (Lexer_gets)(AG.linebuf,0,fp);  /* reset mygets */
#if defined _DEBUG
	agdebug = 1;
#endif
}

static char *lex_gets(void) {
    char	*clp;
    int		len,curlen;

    len = curlen = 0;

    do {
        /* make sure there is room for at least another SMALLBUF worth */

        if (curlen + SMALLBUF >= LineBufSize) {
            LineBufSize += BUFSIZ;
            AG.linebuf = realloc(AG.linebuf,LineBufSize);
            TokenBuf = realloc(TokenBuf,LineBufSize);
        }

        /* off by one so we can back up in LineBuf */
        clp = (Lexer_gets)(AG.linebuf + curlen + 1, LineBufSize-curlen-1 , Lexer_fp);

        if (clp == NULL)
            break;


        len = strlen(clp);        /* since clp != NULL, len > 0 */

        if (clp[len-1] == '\n') { /* have physical line */

            if ((clp[0] == '#') && (curlen == 0)) {
                /* comment line or cpp line sync */

                if (sscanf(clp+1,"%d",&Line_number) == 0)
                    Line_number++;

                clp[0] = 0;

                len = 1;  /* this will make the while test below succeed */

                continue;
            }

            Line_number++;

            if ((len > 1) && (clp[len-2] == '\\')) { /* escaped newline */
                len = len - 2;
                clp[len] = '\0';
            }
        }

        curlen += len;

    } while (clp[len-1] != '\n');

    if (curlen > 0)
        return AG.linebuf + 1;
    else
        return NULL;
}

/* implement own buffer over lex_gets() */
static char* my_yyinput_old = 0;
static void my_yyinput(char* buf, yy_size_t* result, yy_size_t max_size)
{
	char * s = 0;
	if (my_yyinput_old) {
		s = my_yyinput_old;
		my_yyinput_old = 0;
	}
	else {
		s = lex_gets();
	}
	if (s == 0) {
		*result = 0;
		return;
	}
	int l = strlen(s);
	if (l <= max_size) {
		memcpy(buf, s, l);
		*result = l;
	}
	else {
		memcpy(buf, s, max_size);
		*result = max_size;
		my_yyinput_old = s + max_size;
	}
}
@rem win_bison parser.y
@rem copy /Y parser.tab.c parser.c

win_bison -pag -o parser.c --defines=parser.h parser.y
win_flex -Pag -o lexer.c lexer.l

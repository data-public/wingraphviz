# Microsoft Developer Studio Generated NMAKE File, Based on libgraph.dsp
!IF "$(CFG)" == ""
CFG=libgraph - Win32 Debug
!MESSAGE No configuration specified. Defaulting to libgraph - Win32 Debug.
!ENDIF 

!IF "$(CFG)" != "libgraph - Win32 Release" && "$(CFG)" != "libgraph - Win32 Debug"
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "libgraph.mak" CFG="libgraph - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "libgraph - Win32 Release" (based on "Win32 (x86) Static Library")
!MESSAGE "libgraph - Win32 Debug" (based on "Win32 (x86) Static Library")
!MESSAGE 
!ERROR An invalid configuration is specified.
!ENDIF 

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE 
NULL=nul
!ENDIF 

CPP=cl.exe
RSC=rc.exe

!IF  "$(CFG)" == "libgraph - Win32 Release"

OUTDIR=.\Release
INTDIR=.\Release

ALL : "..\lib\Release\libgraph.lib"


CLEAN :
	-@erase "$(INTDIR)\attribs.obj"
	-@erase "$(INTDIR)\edge.obj"
	-@erase "$(INTDIR)\graph.obj"
	-@erase "$(INTDIR)\graphio.obj"
	-@erase "$(INTDIR)\lexer.obj"
	-@erase "$(INTDIR)\node.obj"
	-@erase "$(INTDIR)\parser.obj"
	-@erase "$(INTDIR)\refstr.obj"
	-@erase "$(INTDIR)\trie.obj"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "..\lib\Release\libgraph.lib"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP_PROJ=/nologo /MT /W3 /GX /O1 /I "..\cdt" /I "..\tools\error" /D "NDEBUG" /D "WIN32" /D "_MBCS" /D "_LIB" /D "HAVE_CONFIG_H" /D "MSWIN32" /Fp"$(INTDIR)\libgraph.pch" /YX /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /c 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\libgraph.bsc" 
BSC32_SBRS= \
	
LIB32=link.exe -lib
LIB32_FLAGS=/nologo /out:"..\lib\Release\libgraph.lib" 
LIB32_OBJS= \
	"$(INTDIR)\attribs.obj" \
	"$(INTDIR)\edge.obj" \
	"$(INTDIR)\graph.obj" \
	"$(INTDIR)\graphio.obj" \
	"$(INTDIR)\lexer.obj" \
	"$(INTDIR)\node.obj" \
	"$(INTDIR)\parser.obj" \
	"$(INTDIR)\refstr.obj" \
	"$(INTDIR)\trie.obj"

"..\lib\Release\libgraph.lib" : "$(OUTDIR)" $(DEF_FILE) $(LIB32_OBJS)
    $(LIB32) @<<
  $(LIB32_FLAGS) $(DEF_FLAGS) $(LIB32_OBJS)
<<

!ELSEIF  "$(CFG)" == "libgraph - Win32 Debug"

OUTDIR=.\Debug
INTDIR=.\Debug

ALL : "..\lib\Debug\libgraph.lib"


CLEAN :
	-@erase "$(INTDIR)\attribs.obj"
	-@erase "$(INTDIR)\edge.obj"
	-@erase "$(INTDIR)\graph.obj"
	-@erase "$(INTDIR)\graphio.obj"
	-@erase "$(INTDIR)\lexer.obj"
	-@erase "$(INTDIR)\node.obj"
	-@erase "$(INTDIR)\parser.obj"
	-@erase "$(INTDIR)\refstr.obj"
	-@erase "$(INTDIR)\trie.obj"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(INTDIR)\vc60.pdb"
	-@erase "..\lib\Debug\libgraph.lib"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP_PROJ=/nologo /MTd /W3 /Gm /GX /ZI /Od /I "..\cdt" /I "..\tools\error" /D "_DEBUG" /D "HAVE_CONFIG_H" /D "MSWIN32" /D "WIN32" /D "_MBCS" /D "_LIB" /Fp"$(INTDIR)\libgraph.pch" /YX /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /GZ /c 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\libgraph.bsc" 
BSC32_SBRS= \
	
LIB32=link.exe -lib
LIB32_FLAGS=/nologo /out:"..\lib\Debug\libgraph.lib" 
LIB32_OBJS= \
	"$(INTDIR)\attribs.obj" \
	"$(INTDIR)\edge.obj" \
	"$(INTDIR)\graph.obj" \
	"$(INTDIR)\graphio.obj" \
	"$(INTDIR)\lexer.obj" \
	"$(INTDIR)\node.obj" \
	"$(INTDIR)\parser.obj" \
	"$(INTDIR)\refstr.obj" \
	"$(INTDIR)\trie.obj"

"..\lib\Debug\libgraph.lib" : "$(OUTDIR)" $(DEF_FILE) $(LIB32_OBJS)
    $(LIB32) @<<
  $(LIB32_FLAGS) $(DEF_FLAGS) $(LIB32_OBJS)
<<

!ENDIF 

.c{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<


!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("libgraph.dep")
!INCLUDE "libgraph.dep"
!ELSE 
!MESSAGE Warning: cannot find "libgraph.dep"
!ENDIF 
!ENDIF 


!IF "$(CFG)" == "libgraph - Win32 Release" || "$(CFG)" == "libgraph - Win32 Debug"
SOURCE=.\attribs.c

"$(INTDIR)\attribs.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\edge.c

"$(INTDIR)\edge.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\graph.c

"$(INTDIR)\graph.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\graphio.c

"$(INTDIR)\graphio.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\lexer.c

"$(INTDIR)\lexer.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\node.c

"$(INTDIR)\node.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\parser.c

"$(INTDIR)\parser.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\refstr.c

"$(INTDIR)\refstr.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\trie.c

"$(INTDIR)\trie.obj" : $(SOURCE) "$(INTDIR)"



!ENDIF 


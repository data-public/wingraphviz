/*
    This software may only be used by you under license from AT&T Corp.
    ("AT&T").  A copy of AT&T's Source Code Agreement is available at
    AT&T's Internet website having the URL:
    <http://www.research.att.com/sw/tools/graphviz/license/source.html>
    If you received this software without first entering into a license
    with AT&T, you have an infringing copy of this software and cannot use
    it without violating AT&T's intellectual property rights.
*/

#include "error.h"
#include <stdarg.h>
#include "libgraph.h"
#include "parser.h"
#include "triefa.cP"
#include "windows.h"

#ifdef DMALLOC
#include "dmalloc.h"
#endif

#include "unicodeconst.h"

extern unsigned int DOT_CODEPAGE;

static FILE		*Lexer_fp;
static char		*LexPtr,*TokenBuf;
static int		LineBufSize;
static uchar	In_comment;
static uchar	Comment_start;
static int		Line_number;
static gets_f	Lexer_gets;

void aglexinit(FILE* fp, gets_f mygets) {
    Lexer_fp = fp;
    Lexer_gets = mygets;
    LexPtr = NULL;

    if (AG.linebuf == NULL) {
        LineBufSize = BUFSIZ;
        AG.linebuf = N_NEW(LineBufSize,char);
        TokenBuf = N_NEW(LineBufSize,char);
    }

    (Lexer_gets)(AG.linebuf,0,fp);  /* reset mygets */
}

/* skip leading white space and comments in a string p */
static char *
skip_wscomments(char* p) {
    unsigned char p1;


    do {
        if(DOT_CODEPAGE==CP_JAPANESE_SHIFT_JIS) {

            p1 = *p;



            while ((p1 != 0x83) && isspace(*p)  ) {
                p++;

                p1 = *p;

                if(p1 == 0x83)
                    break;
            }

        } else {
            while (isspace(*p))
                p++;
        }

        while (In_comment && p[0]) {
            while (p[0] && (p[0] != '*'))
                p++;

            if (p[0]) {
                if (p[1] == '/') {
                    In_comment = FALSE;
                    p += 2;
                    break;

                } else
                    p++;
            }
        }

        if (p[0] == '/') {
            if (p[1] == '/')
                while (*p)
                    p++;	/* skip to end of line */
            else {
                if (p[1] == '*') {
                    In_comment = TRUE;
                    Comment_start = Line_number;
                    p += 2;
                    continue;

                } else
                    break;	/* return a slash */
            }

        } else {
            if(DOT_CODEPAGE==CP_JAPANESE_SHIFT_JIS) {
                p1 = *p;


                if(p1 == 0x83) {
                    break;

                } else {
                    if (!isspace(*p))
                        break;
                }

            } else {
                if (!isspace(*p))
                    break;
            }
        }

    } while (p[0]);

    return p;
}

/* scan an unquoted token and return the position after its terminator */
static char *
scan_token(char* p, char* token) {
    char 	*q;
    unsigned char p1;


    q = token;

    if (p == '\0')
        return NULL;

    p1 = *p;


    while ((DOT_CODEPAGE==CP_JAPANESE_SHIFT_JIS && p1 ==0x83) || ISALNUM(*p) ) {
        if((DOT_CODEPAGE==CP_JAPANESE_SHIFT_JIS && p1 ==0x83)) {
            *q++ = *p++;
            *q++ = *p++;

        } else {
            *q++ = *p++;
        }


        p1 = *p;
    }

    *q = '\0';
    return p;
}

static char *
scan_num(char* p, char* token) {
    char 	*q,*z;
    int		saw_rp = FALSE;
    int		saw_digit = FALSE;

    z = p;
    q = token;

    if (*z == '-')
        *q++ = *z++;

    if (*z == '.') {
        saw_rp = TRUE;
        *q++ = *z++;
    }

    if( *z != -125) {
        while (isdigit(*z)) {
            saw_digit = TRUE;
            *q++ = *z++;
        }
    }

    if ((*z == '.') && (saw_rp == FALSE)) {
        saw_rp = TRUE;
        *q++ = *z++;

        while (isdigit(*z)) {
            saw_digit = TRUE;
            *q++ = *z++;
        }
    }

    *q = '\0';

    if (saw_digit && *z && ((isalpha(*z)) || (*z == '_'))) {
        agerror("badly formed number %s",p);
        errorPrintf (stderr, "Splits into two name tokens\n");
    }

    if (saw_digit == FALSE)
        z = NULL;

    return z;
}

/* scan a quoted string and return the position after its terminator */

static char *
quoted_string(char* CurChar, char* token) {
	char		QUOTE, *OutString, *NextChar, *ThirdChar;
    size_t i ;
    QUOTE = *CurChar++;
    OutString = token;

    while ((*CurChar) && (*CurChar != QUOTE) ) {
        NextChar = CharNextExA(DOT_CODEPAGE,CurChar,0);
        ThirdChar = CharNextExA(DOT_CODEPAGE,NextChar,0);
        i = NextChar - CurChar;
        //過濾掉控制位元

        if (*CurChar == '\\' && i == 1) {

            switch(*(CurChar + 1)) {

                    case 'a':

                    case 'n':

                    case 'r':

                    case 'v':
                    *OutString++ = *CurChar ;
                    break;


            }

            *OutString++ = *(CurChar + 1);

            if(NextChar)
                CurChar = CurChar + 2 ;
            else
                CurChar = CurChar + 1;

        } else {
            while(CurChar != NextChar) {
                *OutString++ = *CurChar++;

            }

        }


    }

    NextChar = CharNextExA(DOT_CODEPAGE,CurChar,0);

    if (*CurChar == '\0')
        agerror("string ran past end of line","");
    else
        CurChar =NextChar ;

    //字串結束
    *OutString = 0;

    return CurChar;
}

/*
static char *
quoted_string(char* p, char* token)
{
	char		quote,*q;
 
	quote = *p++;
	q = token;
	while ((*p) && (*p != quote)) {
		if (*p == '\\') {
			if (*(p+1) == quote) p++;
			else {if (*(p+1) == '\\') *q++ = *p++;}
		}
		*q++ = *p++;
	}
 
	if (*p == '\0') 
		agerror("string ran past end of line","");
	else 
		p++;
 
	*q = 0;
	return p;
}
*/
int myaglex(void) {		/* for debugging */
    int rv = aglex();
    errorPrintf(stderr,"returning %d\n",rv);

    if (rv == T_symbol)
        errorPrintf(stderr,"string val is %s\n",aglval.str);

    return rv;
}

/*
 * Return a logical line in AG.linebuf.
 * In particular, the buffer will contain a '\n' as the last non-null char.
 * Ignore lines beginning with '#'; update cpp line number if applicable.
 * Fold long lines, i.e., ignore escaped newlines.
 * Assume the Lexer_gets function reads upto newline or buffer length
 * like fgets.
 * Need to be careful that Lexer_gets might not return full physical line
 * because buffer is too small to hold it.
 */
static char *lex_gets(void) {
    char	*clp;
    int		len,curlen;

    len = curlen = 0;

    do {
        /* make sure there is room for at least another SMALLBUF worth */

        if (curlen + SMALLBUF >= LineBufSize) {
            LineBufSize += BUFSIZ;
            AG.linebuf = realloc(AG.linebuf,LineBufSize);
            TokenBuf = realloc(TokenBuf,LineBufSize);
        }

        /* off by one so we can back up in LineBuf */
        clp = (Lexer_gets)(AG.linebuf + curlen + 1, LineBufSize-curlen-1 , Lexer_fp);

        if (clp == NULL)
            break;


        len = strlen(clp);        /* since clp != NULL, len > 0 */

        if (clp[len-1] == '\n') { /* have physical line */

            if ((clp[0] == '#') && (curlen == 0)) {
                /* comment line or cpp line sync */

                if (sscanf(clp+1,"%d",&Line_number) == 0)
                    Line_number++;

                clp[0] = 0;

                len = 1;  /* this will make the while test below succeed */

                continue;
            }

            Line_number++;

            if ((len > 1) && (clp[len-2] == '\\')) { /* escaped newline */
                len = len - 2;
                clp[len] = '\0';
            }
        }

        curlen += len;

    } while (clp[len-1] != '\n');

    if (curlen > 0)
        return AG.linebuf + 1;
    else
        return NULL;
}

int agtoken(char* p) {
    TFA_Init();


    while (*p) {
        //TFA_Advance(*p++);
        isupper(*p++);
    }


    return TFA_Definition();
}

int aglex(void) {
    int		token;
    char	*tbuf,*p;

    /* if the parser has accepted a graph, reset and return EOF */

    if (AG.accepting_state) {
        AG.accepting_state = FALSE;
        return EOF;
    }

    /* get a nonempty lex buffer */
    do {
        if ((LexPtr == NULL) || (LexPtr[0] == '\0'))
            if ((LexPtr = lex_gets()) == NULL) {
                if (In_comment)
                    errorPrintf(stderr,"warning, nonterminated comment in line %d\n",Comment_start);

                return EOF;
            }

        LexPtr = skip_wscomments(LexPtr);

    } while (LexPtr[0] == '\0');

    tbuf = TokenBuf;

    /* scan quoted strings */
    if (LexPtr[0] == '\"') {
        LexPtr = quoted_string(LexPtr,tbuf);
        aglval.str = tbuf;
        return T_symbol;
    }

    /* scan edge operator */
    if (AG.edge_op && (strncmp(LexPtr,AG.edge_op,strlen(AG.edge_op)) == 0)) {
        LexPtr += strlen(AG.edge_op);
        return T_edgeop;
    }

    /* scan numbers */
    if ((p = scan_num(LexPtr,tbuf))) {
        LexPtr = p;
        aglval.str = tbuf;
        return T_symbol;

    } else {

        if (ispunct(LexPtr[0]) && (LexPtr[0] != '_'))
            return *LexPtr++;
        else
            LexPtr = scan_token(LexPtr,tbuf);


    }

    /* scan other tokens */
    token = agtoken(tbuf);

    if (token == -1) {
        aglval.str = tbuf;
        token = T_symbol;
    }

    return token;
}

static void error_context(void) {
    char *p,*q;

    if (LexPtr == NULL)
        return;

    errorPrintf(stderr,"context: ");

    for (p = LexPtr - 1; (p > AG.linebuf) && (isspace(*p) == FALSE); p--)

        ;
    for (q = AG.linebuf; q < p; q++)
        ErrPutc(*q,stderr);

    ErrPuts(" >>> ",stderr);

    for (; q < LexPtr; q++)
        ErrPutc(*q,stderr);

    ErrPuts(" <<< ",stderr);

    ErrPuts(LexPtr,stderr);
}

extern char * getErrMsg();

/* #if __STD_C */
void agerror(char *fmt, ...)
/* #else */
/* void agerror(va_alist) */
/* va_dcl */
/* #endif */
{
    va_list	args;
    char	*s;

    /* #if __STD_C */
    va_start(args, fmt);
    /* #else */
    /* char	*fmt; */
    /* va_start(args); */
    /* fmt = va_arg(args,char*); */
    /* #endif */
    s = va_arg(args,char*);

    if (AG.syntax_errors++)
        return;

    errorPrintf(stderr, "graph parser: ");

    errorPrintf(stderr, fmt, s);

    errorPrintf(stderr, " near line %d\n",Line_number);

    error_context();

    va_end(args);

	fprintf(stderr, "%s\n", getErrMsg());
}

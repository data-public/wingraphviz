/*   Copyright (c) AT&T Corp.  All rights reserved.
   
This software may only be used by you under license from 
AT&T Corp. ("AT&T").  A copy of AT&T's Source Code Agreement 
is available at AT&T's Internet website having the URL 
 
http://www.research.att.com/sw/tools/graphviz/license/
 
If you received this software without first entering into a license 
with AT&T, you have an infringing copy of this software and cannot 
use it without violating AT&T's intellectual property rights. */

#ifndef PARSESTR_H
#define PARSESTR_H

// overload >> DString for better-delimited tokens
// returns one token or one delimeter
inline istream &operator>>(istream &in,DString &s) {
    static const string delim = "=,[]; \t\n\r";
    string temp;
    in >> ws;

    while(!in.eof()) {
        char c = in.get();

        if(c<0)
            break; // why does eof() delay?

        unsigned i;

        for(i=0; i<delim.length(); ++i)
            if(delim[i]==c) {
                if(temp.empty())
                    temp += c;
                else
                    in.putback(c);

                break;
            }

        if(i<delim.length())
            break;

        temp += c;
    }

    s = temp.c_str();
    return in;
}

struct ParseEOF : DGException {
    ParseEOF() : DGException("the parser encountered an end-of-file") {}

}

;

struct ParseExpected : DGException {
    DString expect,got;
    ParseExpected(DString expect,DString got) : expect(expect),got(got),DGException("the string parser didn't get what it expected") {}

    ParseExpected(char e,char g) : DGException("the string parser didn't get char it expected") {
        char buf[2];
        buf[1] = 0;
        buf[0] = e;
        expect = buf;
        buf[0] = g;
        got = buf;
    }
};

struct must {
    DString s;
    must(DString s) : s(s) {}

}

;
inline istream &operator>>(istream &in,const must &m) {
    DString s;
    in >> s;

    if(s!=m.s)
        throw ParseExpected(m.s,s);

    return in;
}

struct quote {
    const string &s;
    quote(const string &s) : s(s) {}

}

;
inline ostream &operator<<(ostream &out,quote &o) {
    out << '"';

    if(!o.s.empty())
        for(unsigned i = 0; i<o.s.length(); ++i)
            if(o.s[i]=='"') // " => \"
                out << "\\\"";
            else if(o.s[i]=='\\') // \ => \\       .
                out << "\\\\";
            else
                out << o.s[i];

    out << '"';

    return out;
}

struct unquote {
    string &s;
    unquote(string &s) : s(s) {}

}

;
inline istream &operator>>(istream &in,unquote &o) {
    char c;
    in >> ws;
    in.get(c);

    if(c!='"')
        throw ParseExpected('"',c);

    o.s.erase();

    while(1) {
        in.get(c);

        if(c=='\\')
            in.get(c);
        else if(c=='"')
            break;

        o.s += c;
    }

    return in;
}

template
<typename C>
inline bool needsQuotes(const C *s) {
    static const char g_badChars[] = "\" ,[]-+";
    static const int g_nBads = sizeof(g_badChars);

    if(isdigit(s[0]))
        return true;

    // didn't work in gcc: find_first_of(s.begin(),s.end(),g_badChars,g_badChars+g_nBads)!=s.end()
    for(const C *i=s; *i; ++i)
        for(int i2 = 0; i2<g_nBads; ++i2)
            if(*i==g_badChars[i2])
                return true;

    return false;
}

bool needsQuotes(const DString &s);

struct mquote { // quote if needed
    const DString &s;
    mquote(const DString &s) : s(s) {}

}

;
ostream &operator<<(ostream &,const mquote &);
// match e.g. braces.  throws outers away (this isn't general yet)

struct match {
    char open,close;
    string &s;
    match(char open,char close,string &s) : open(open),close(close),s(s) {}

}

;
inline istream &operator>>(istream &in,match &m) {
    in >> ws;
    char c;
    in.get(c);

    if(c!=m.open)
        throw ParseExpected(m.open,c);

    int count = 1;

    while(!in.eof()) {
        in.get(c);

        if(c==m.open)
            ++count;
        else if(c==m.close)
            if(!--count)
                return in;

        m.s += c;
    }

    throw ParseEOF();
    return in;
}

#endif // PARSESTR_H

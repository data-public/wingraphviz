/*   Copyright (c) AT&T Corp.  All rights reserved.
   
This software may only be used by you under license from 
AT&T Corp. ("AT&T").  A copy of AT&T's Source Code Agreement 
is available at AT&T's Internet website having the URL 
 
http://www.research.att.com/sw/tools/graphviz/license/
 
If you received this software without first entering into a license 
with AT&T, you have an infringing copy of this software and cannot 
use it without violating AT&T's intellectual property rights. */

#include "common/Dynagraph.h"
#include "common/applyAttrs.h"
#include "common/genpoly.h"
#include <sstream>
bool attrChanged(const StrAttrs &oldAttrs,const StrAttrs &newAttrs, const DString &name) {
    StrAttrs::const_iterator ai1,ai2;

    if((ai2 = newAttrs.find(name))==newAttrs.end())
        return false;

    if((ai1 = oldAttrs.find(name))==oldAttrs.end())
        return true;

    return ai1->second!=ai2->second;
}

bool transformShape(Transform *trans,Line &shape) {
    bool nonzero = false;

    for(Line::iterator pi = shape.begin(); pi!=shape.end(); ++pi) {
        if(pi->x!=0 || pi->y!=0)
            nonzero = true;

        *pi = trans->in(*pi);
    }

    return nonzero;
}

Update applyAttrs(Transform *trans,Layout *l,const StrAttrs &attrs) {
    for(StrAttrs::const_iterator ai = attrs.begin(); ai!=attrs.end(); ++ai) {
        if(ai->first=="resolution") {
            istringstream s(ai->second);
            s >> gd<GraphGeom>(l).resolution;

        } else if(ai->first=="ticks") {
            istringstream s(ai->second);
            s >> gd<GraphGeom>(l).timeLimit;

        } else if(ai->first=="separation") {
            istringstream s(ai->second);
            s >> gd<GraphGeom>(l).separation;
        }

        gd<StrAttrs2>(l).put(ai->first,ai->second);
    }

    return 0;
}

// if any of these attrs get changed it's time to recalc shape
const DString g_shapeAttrs[] = {
                                   "boundary",
                                   "shape",
                                   "sides",
                                   "peripheries",
                                   "perispacing",
                                   "rotation",
                                   "regular",
                                   "skew",
                                   "distortion",
                                   "textsize"
                               };

const int g_numShapeAttrs = sizeof(g_shapeAttrs)/sizeof(DString);
bool shapeChanged(const StrAttrs &oldAttrs,const StrAttrs &newAttrs) {
    for(int i = 0; i<g_numShapeAttrs; ++i)
        if(attrChanged(oldAttrs,newAttrs,g_shapeAttrs[i]))
            return true;

    return false;
}

const Coord house_coords[6] = {
                                  Coord(1,-1),Coord(1,1),Coord(0,2),Coord(-1,1),Coord(-1,-1),Coord(1,-1)
                              };

const Coord trapezium_coords[5] = {
                                      Coord(2,-1),Coord(1,1),Coord(-1,1),Coord(-2,-1),Coord(2,-1)
                                  };

PolyDef readPolyDef(Transform *trans,const StrAttrs &attrs) {
    PolyDef ret;
    StrAttrs::const_iterator ai;

    if((ai = attrs.find("shape"))!=attrs.end()) {
        if(ai->second=="ellipse")
            ret.isEllipse = true;
        else if(ai->second=="hexagon") {
            ret.regular = true;
            ret.sides = 6;

        } else if(ai->second=="box")

            ; // default
        else if(ai->second=="circle") {
            ret.regular = true;
            ret.isEllipse = true;

        } else if(ai->second=="diamond") {
            ret.rotation = M_PI/4;

        } else if(ai->second=="doublecircle") {
            ret.isEllipse = true;
            ret.regular = true;
            ret.peripheries = 1;

        } else if(ai->second=="doubleoctagon") {
            ret.sides = 8;
            ret.peripheries = 1;

        } else if(ai->second=="egg") {
            ret.isEllipse = true;
            ret.distortion = 1.3;
            ret.rotation = M_PI_2;

        } else if(ai->second=="hexagon") {
            ret.sides = 6;

        } else if(ai->second=="house") {
            ret.input.degree = 1;
            ret.input.insert(ret.input.begin(),house_coords,house_coords+6);

        } else if(ai->second=="invhouse") {
            ret.input.degree = 1;
            ret.input.insert(ret.input.begin(),house_coords,house_coords+6);
            ret.rotation = M_PI;

        } else if(ai->second=="invtrapezium") {
            ret.input.degree = 1;
            ret.input.insert(ret.input.begin(),trapezium_coords,trapezium_coords+5);
            ret.rotation = M_PI;

        } else if(ai->second=="invtriangle") {
            ret.sides = 3;
            ret.rotation = M_PI;

        } else if(ai->second=="octagon") {
            ret.sides = 8;

        } else if(ai->second=="parallelogram") {
            ret.sides = 4;
            ret.skew = 0.5;

        } else if(ai->second=="trapezium") {
            ret.input.degree = 1;
            ret.input.insert(ret.input.begin(),trapezium_coords,trapezium_coords+5);

        } else if(ai->second=="triangle") {
            ret.sides = 3;

        } else if(ai->second=="tripleoctagon") {
            ret.sides = 8;
            ret.peripheries = 2;
        }
    }

    if((ai = attrs.find("regular"))!=attrs.end())
        ret.regular = ai->second=="true";

    if((ai = attrs.find("sides"))!=attrs.end()) {
        istringstream stream(ai->second);
        stream >> ret.sides;
    }

    if((ai = attrs.find("peripheries"))!=attrs.end()) {
        istringstream stream(ai->second);
        stream >> ret.peripheries;
    }

    if((ai = attrs.find("perispacing"))!=attrs.end()) {
        istringstream stream(ai->second);
        stream >> ret.perispacing;
    }

    if((ai = attrs.find("rotation"))!=attrs.end()) {
        istringstream stream(ai->second);
        stream >> ret.rotation;
    }

    if((ai = attrs.find("skew"))!=attrs.end()) {
        istringstream stream(ai->second);
        stream >> ret.skew;
    }

    if((ai = attrs.find("distortion"))!=attrs.end()) {
        istringstream stream(ai->second);
        stream >> ret.distortion;
    }

    if((ai = attrs.find("textsize"))!=attrs.end()) {
        istringstream stream(ai->second);
        stream >> ret.interior_box;
    }

    if((ai = attrs.find("width"))!=attrs.end()) {
        istringstream stream(ai->second);
        stream >> ret.exterior_box.x;
    }

    if((ai = attrs.find("height"))!=attrs.end()) {
        istringstream stream(ai->second);
        stream >> ret.exterior_box.y;
    }

    ret.interior_box = trans->inSize(ret.interior_box);
    ret.exterior_box = trans->inSize(ret.exterior_box);
    return ret;
}

Update assureAttrs(Transform *trans,Layout::Node *n) {
    Update ret;
    StrAttrs &att = gd<StrAttrs>(n);
    StrAttrChanges &cha = gd<StrAttrChanges>(n);

    if(att.find("shape")==att.end()) {
        att["shape"] = "ellipse";
        cha.insert("shape");
        ret.flags |= DG_UPD_REGION;
    }

    StrAttrs::iterator ai;
    Coord size(0,0);

    if((ai=att.find("width"))!=att.end())
        sscanf(ai->second.c_str(),"%lf",&size.x);

    if((ai=att.find("height"))!=att.end())
        sscanf(ai->second.c_str(),"%lf",&size.y);

    if(size.x&&size.y)
        return ret;

    if(size.x||size.y) {
        if(!size.y)
            size.y = size.x;

        if(!size.x)
            size.x = size.y;

    } else
        size = trans->outSize(gd<GraphGeom>(n->g).defaultSize);
    char buf[20];
    sprintf(buf,"%f",size.x);
    att["width"] = buf;
    sprintf(buf,"%f",size.y);
    att["height"] = buf;
    cha.insert("width");
    cha.insert("height");
    ret.flags |= DG_UPD_REGION;
    return ret;
}

Update applyAttrs(Transform *trans,Layout::Node *n,const StrAttrs &attrs) {
    Update ret;
    NodeGeom &ng = gd<NodeGeom>(n);
    StrAttrs &att = gd<StrAttrs>(n);
    StrAttrs::const_iterator ai=attrs.end();
    bool chshape = shapeChanged(att,attrs);

    for(ai = attrs.begin(); ai!=attrs.end(); ++ai) {
        if(ai->first=="pos") {
            ng.pos.valid = true;
            istringstream stream(ai->second);
            stream >> ng.pos;
            ng.pos = trans->in(ng.pos);
            ret.flags |= DG_UPD_MOVE;
        }

        gd<StrAttrs2>(n).put(ai->first,ai->second);
    }

    ret.flags |= assureAttrs(trans,n).flags;

    if(chshape || ret.flags&DG_UPD_REGION) {
        if((ai = att.find("boundary"))!=att.end()) {
            const DString &s = ai->second;
            istringstream stream(s);
            ng.region.shape.Clear();
            stream >> ng.region.shape;
            ng.region.updateBounds();
            ret.flags |= DG_UPD_REGION;

        } else if((ai = att.find("shape"))!=att.end() && ai->second=="plaintext") {
            Coord size;
            ai = att.find("textsize");

            if(ai != att.end()) {
                istringstream stream(ai->second);
                stream >> size;
            }

            size = trans->inSize(size);
            ng.region.shape.Clear();
            ng.region.boundary = Rect(-size.x/2,-size.y/2,size.x/2,size.y/2);

        } else {
            gd<PolyDef>(n) = readPolyDef(trans,att);
            ret.flags |= DG_UPD_POLYDEF;
        }
    }

    return ret.flags;
}

Update applyAttrs(Transform *trans,Layout::Edge *e,const StrAttrs &attrs) {
    Update ret;
    EdgeGeom &eg = gd<EdgeGeom>(e);

    for(StrAttrs::const_iterator ai = attrs.begin(); ai!=attrs.end(); ++ai) {
        bool skip = false;

        if(ai->first=="pos") {
            const DString &s = ai->second;
            const char *val = ai->second.c_str();
            DString::size_type begin = 0,end=s.size();

            if(!strncmp(val,"e,",2))
                begin = 2;

            DString::size_type i = s.find(';',begin);

            if(i!=DString::npos)
                end = i;

            Line newline;

            istringstream stream(string(val+begin,val+end));

            stream >> newline;

            if(transformShape(trans,newline)) {
                eg.pos = newline;
                ret.flags |= DG_UPD_MOVE;

            } else
                skip = true;
        }

        if(!skip)
            gd<StrAttrs2>(e).put(ai->first,ai->second);
    }

    return ret;
}

void applyStrGraph(Transform *trans,StrGraph *g,Layout *out, Layout *subg) {
    gd<Name>(out) = gd<Name>(g);
    map<DString,Layout::Node*> renode;

    for(StrGraph::node_iter ni = g->nodes().begin(); ni!=g->nodes().end(); ++ni) {
        Layout::Node *n = out->create_node();
        subg->insert(n);
        renode[gd<Name>(n) = gd<Name>(*ni)] = n;
        StrAttrs &attrs = gd<StrAttrs>(*ni);
        applyAttrs(trans,n,attrs);
    }

    for(StrGraph::graphedge_iter ei = g->edges().begin(); ei!=g->edges().end(); ++ei) {
        Layout::Edge *e = out->create_edge(renode[gd<Name>((*ei)->tail)],
                                           renode[gd<Name>((*ei)->head)]).first;
        gd<Name>(e) = gd<Name>(*ei);
        subg->insert(e);
        StrAttrs &attrs = gd<StrAttrs>(*ei);
        applyAttrs(trans,e,attrs);
    }
}

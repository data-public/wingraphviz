inline void breakList(DString list,vector<DString> &out) {
    for(DString::size_type i=0,com = 0;i<list.size() && com<list.size();i = com+1) {
        while(isspace(list[i]))
            ++i;

        com = list.find(',',i);

        out.push_back(list.substr(i,com-i));
    }
}

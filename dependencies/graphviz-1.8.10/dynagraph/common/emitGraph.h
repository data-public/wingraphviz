/*   Copyright (c) AT&T Corp.  All rights reserved.
   
This software may only be used by you under license from 
AT&T Corp. ("AT&T").  A copy of AT&T's Source Code Agreement 
is available at AT&T's Internet website having the URL 
 
http://www.research.att.com/sw/tools/graphviz/license/
 
If you received this software without first entering into a license 
with AT&T, you have an infringing copy of this software and cannot 
use it without violating AT&T's intellectual property rights. */

#include "common/Dynagraph.h"
#include "common/applyAttrs.h"
#include "common/parsestr.h"

void emitAttrs(ostream &os,const StrAttrs &attrs,const DString &id=DString());

template
<typename G>
void emitGraph(ostream &os,G *g) {
    os << "digraph " << mquote(gd<Name>(g));
    os << " {" << endl << "\tgraph ";
    emitAttrs(os,gd<StrAttrs>(g));

    for(typename G::node_iter ni = g->nodes().begin(); ni!=g->nodes().end(); ++ni) {
        os << '\t' << mquote(gd<Name>(*ni)) << ' ';
        emitAttrs(os,gd<StrAttrs>(*ni));
    }

    for(typename G::graphedge_iter ei = g->edges().begin(); ei!=g->edges().end(); ++ei) {
        os << '\t' << mquote(gd<Name>((*ei)->tail)) << " -> " << mquote(gd<Name>((*ei)->head));
        os << ' ';
        emitAttrs(os,gd<StrAttrs>(*ei),gd<Name>(*ei));
    }

    os << "}\n";
}

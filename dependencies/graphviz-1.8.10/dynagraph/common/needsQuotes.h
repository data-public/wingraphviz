/*   Copyright (c) AT&T Corp.  All rights reserved.
   
This software may only be used by you under license from 
AT&T Corp. ("AT&T").  A copy of AT&T's Source Code Agreement 
is available at AT&T's Internet website having the URL 
 
http://www.research.att.com/sw/tools/graphviz/license/
 
If you received this software without first entering into a license 
with AT&T, you have an infringing copy of this software and cannot 
use it without violating AT&T's intellectual property rights. */

template
<typename C>
inline bool needsQuotes(const C *s) {
    static const char g_badChars[] = "\" ,[]-+";
    static const int g_nBads = sizeof(g_badChars);

    if(isdigit(s[0]))
        return true;

    // didn't work in gcc: find_first_of(s.begin(),s.end(),g_badChars,g_badChars+g_nBads)!=s.end()
    for(const C *i=s; *i; ++i)
        for(int i2 = 0; i2<g_nBads; ++i2)
            if(*i==g_badChars[i2])
                return true;

    return false;
}

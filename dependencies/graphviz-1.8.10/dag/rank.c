#include <dag.h>
#include <ns.h>

#ifdef DMALLOC
#include "dmalloc.h"
#endif

/* work functions for level assignment */

Agnode_t *dd_getvar(ddview_t *view, Agnode_t *ln, int coordsys) {
    Agraph_t	*cg;
    Agnode_t	*rv;
    ddnode_t	*nd;
    int			initial_rank;

    cg = view->con[coordsys].g;
    nd = dd_node(ln);

    if ((rv = nd->con[coordsys].n) == NILnode) {
        rv = nd->con[coordsys].n = agnode(cg,NILstr,TRUE);

        if (coordsys == XCON)
            initial_rank = dd_pos(ln).x * (double)(view->xscale);
        else
            initial_rank = dd_rank(ln);

        ns_setrank(rv,initial_rank + ns_getrank(view->con[coordsys].anchor));
    }

    return rv;
}

edgepair_t dd_getedgepair(Agraph_t *cg, Agnode_t *anchor, Agnode_t *tvar, Agnode_t *hvar) {
    edgepair_t	rv;

    rv.e[0] = agedge(anchor,tvar,NILstr,TRUE);
    rv.e[1] = agedge(anchor,hvar,NILstr,TRUE);
    return rv;
}

static int ranklength(ILedge_t *espec) {
    return MAX(0, ROUND(espec->length_hint) );
}

static void make_hard_constraint(ddview_t *view, ddpath_t *path) {
    Agraph_t	*cg;			/* constraint graph */
    Agnode_t	*tvar, *hvar;	/* NS variables */
    Agedge_t	*constr;		/* constraint edge */
    ILedge_t	*espec;			/* client edge specification */
    int			minlen;			/* adjusted value */

    assert(path->strong == NILedge);
    espec = dd_pathspec(path);
    espec->constraint = TRUE;

    cg = view->con[YCON].g;
    tvar = dd_getvar(view,dd_rep(agtail(path->model)),YCON);
    hvar = dd_getvar(view,dd_rep(aghead(path->model)),YCON);

    constr = agedge(tvar,hvar,NIL(char*),TRUE);
    path->strong  = constr;
    minlen = ranklength(espec);
    ns_setminlength(constr,minlen);
}

static void make_weak_constraint(ddview_t *view, ddpath_t *path) {
    Agraph_t	*cg;				/* constraint graph */
    Agnode_t	*tvar, *hvar;		/* NS variables */
    Agnode_t	*avar;				/* anchor for this weak constraint */
    edgepair_t	ep;					/* constraint edge pair */
    ILedge_t	*espec;				/* client edge specification */
    int			minlen;				/* adjusted value */

    assert(path->weak == NILnode);
    espec = dd_pathspec(path);

    cg = view->con[YCON].g;
    tvar = dd_getvar(view,dd_rep(agtail(path->model)),YCON);
    hvar = dd_getvar(view,dd_rep(aghead(path->model)),YCON);
    avar = path->weak = agnode(cg,NILstr,TRUE);
    ep = dd_getedgepair(cg,avar,tvar,hvar);
    minlen = ranklength(espec);
    ns_setminlength(ep.e[0],0);
    ns_setweight(ep.e[0],BACKEDGE_PENALTY);
    ns_setminlength(ep.e[1],minlen);
}

static void remove_edge_constraint(ddview_t *view, ddpath_t *path) {
    Agraph_t	*cg;

    cg = view->con[YCON].g;

    if (path->weak)
        agdelete(cg,path->weak);

    if (path->strong)
        agdelete(cg,path->strong);

    dd_pathspec(path)->constraint = FALSE;
}

/* should be subsumed by dd_unstabilize */
static void remove_node_constraint(ddview_t *view, Agnode_t *ln) {
    ddnode_t	*nd;
    Agraph_t	*cg;

    nd = dd_node(ln);
    cg = view->con[YCON].g;

    if (nd->con[YCON].n) {
        agdelete(cg,nd->con[YCON].n);
        nd->con[YCON].n = NILnode;
    }

    if (nd->con[YCON].stab) {
        agdelete(cg,nd->con[YCON].stab);
        nd->con[YCON].stab = NILnode;
    }
}

static void execute_deletions(ddview_t *view) {
    Agnode_t	*mn;	/* model node */
    Agedge_t	*me;	/* model edge */
    Agnode_t	*ln;	/* layout proxy */
    ddpath_t	*path;

    for (mn = agfstnode(BASE(view)->model.e[IL_DEL]); mn; mn = agnxtnode(mn)) {
        for (me = agfstout(mn); me; me = agnxtout(me)) {
            if ((path = dd_pathrep(me)))
                remove_edge_constraint(view,path);
        }
    }

    for (mn = agfstnode(BASE(view)->model.v[IL_DEL]); mn; mn = agnxtnode(mn)) {
        if ((ln = dd_rep(mn)))
            remove_node_constraint(view,ln);
    }
}

static void weaken_edge_constraints(ddview_t *view, Agnode_t *mn) {
    Agraph_t	*cg;
    Agnode_t	*mmn;		/* model node in main graph */
    Agedge_t	*me;		/* model edge */
    Agnode_t	*anchor;
    ddpath_t	*path;

    cg = view->con[YCON].g;
    mmn = agsubnode(BASE(view)->model.main, mn, FALSE);
    anchor = view->con[YCON].anchor;

    for (me = agfstedge(mmn); me; me = agnxtedge(me,mmn)) {
        path = dd_pathrep(me);

        if (path->strong) {
            agdelete(cg,path->strong);
            path->strong = NILedge;
            make_weak_constraint(view,path);
        }
    }
}

void dd_stabilize(ddview_t *view, Agnode_t *ln, int csys, int newrank, int weight) {
    ddnode_t	*nd;
    Agnode_t	*slack, *anchor, *var;
    edgepair_t	ep;
    int			len0, len1;

    nd = dd_node(ln);
    slack = nd->con[csys].stab;

    if (slack == NILnode)
        slack = nd->con[csys].stab = agnode(view->con[csys].g,NILstr,TRUE);

    anchor = view->con[csys].anchor;

    var = dd_getvar(view,ln,csys);

    if (newrank >= 0) {
        len0 = 0;
        len1 = newrank;

    } else {
        len0 = -newrank;
        len1 = 0;
    }

    ep = dd_getedgepair(view->con[csys].g,slack,anchor,var);
    ns_setminlength(ep.e[0],len0);
    ns_setminlength(ep.e[1],len1);
    ns_setweight(ep.e[0],weight);
    ns_setweight(ep.e[1],weight);
}


void dd_unstabilize(ddview_t *view, Agnode_t *ln, int csys) {
    Agraph_t	*cg;	/* constraint graph */
    ddnode_t	*nd;

    cg = view->con[csys].g;
    nd = dd_node(ln);

    if (nd->con[csys].stab) {
        agdelete(cg,nd->con[csys].stab);
        nd->con[csys].stab = NILnode;
    }

    /*if (nd->con[csys].n)
    	agdelete(cg,nd->con[csys].n); */ 	/* incorrect? */
}

ilbool node_being_moved(ILnode_t *nspec) {
    return (nspec->update & (IL_UPD_NAIL | IL_UPD_NAILX | IL_UPD_NAILY | IL_UPD_MOVE))?TRUE:FALSE;
}

ilbool strong_move(ILnode_t *nspec) {
    return (nspec->update & (IL_UPD_NAIL | IL_UPD_NAILY))?TRUE:FALSE;
}

static void move_old_nodes(ddview_t *view) {
    Agraph_t	*cg;	/* constraint graph */
    Agnode_t	*mn;	/* model node */
    Agnode_t	*ln;	/* layout proxy */
    ILnode_t	*nspec;
    ddnode_t	*nd;
    int			newrank;

    cg = view->con[YCON].g;

    for (mn = agfstnode(BASE(view)->model.v[IL_MOD]); mn; mn = agnxtnode(mn)) {
        ln = dd_rep(mn);
        nspec = dd_nspec(ln);
        nd = dd_node(ln);

        if (node_being_moved(nspec)) {
            newrank = dd_map_coord_to_rank(view,nspec->pos.y);

            if (strong_move(nspec)) {
                /*remove_node_constraint(view,ln);  wrongo */
                weaken_edge_constraints(view,mn);
                dd_set_newrank(ln,newrank);
                nd->rank_fixed = TRUE;
            }

            dd_stabilize(view,ln,YCON,newrank,STABILITY_FACTOR);
        }
    }
}

static void insert_new_nodes(ddview_t *view) {
    Agnode_t	*ln, *mn;	/* layout and model nodes */
    ILnode_t	*nspec;
    int			newrank;
    int			stability_weight;

    for (mn = agfstnode(BASE(view)->model.v[IL_INS]); mn; mn = agnxtnode(mn)) {
        ln = dd_open_node(view,mn);
        nspec = ddm_nspec(mn);
#ifndef NEW_BAD

        if (nspec->pos_valid) {
            newrank = dd_map_coord_to_rank(view,nspec->pos.y);
            stability_weight = STABILITY_FACTOR;

        } else {
            newrank = view->config->low;
            stability_weight = 0;
        }

        dd_stabilize(view,ln,YCON,newrank,stability_weight);
#else

        if (nspec->pos_valid) {
            newrank = dd_map_coord_to_rank(view,nspec->pos.y);
            dd_stabilize(view,ln,YCON,newrank);
        }

#endif

    }
}

/* is there a path from head(e) to tail(e)? next two fns. */
static ilbool dfs(Agnode_t *src, Agnode_t *dest, uchar mark) {
    Agedge_t    *e;
    Agnode_t	*rep;

    if (src == dest)
        return TRUE;

    rep = dd_rep(src);

    if (dd_dfsmark(rep) == mark)
        return FALSE;

    dd_set_dfsmark(rep,mark);

    for (e = agfstout(src); e; e = agnxtout(e)) {
        if (NOT(ddm_espec(e)->constraint))
            continue;

        if (dfs(e->node,dest,mark))
            return TRUE;
    }

    return FALSE;
}

static ilbool path_exists(ddview_t *view, Agnode_t *src, Agnode_t *dest) {
    static uchar uniqmark;

    return dfs( agsubnode(BASE(view)->model.main,src,FALSE),
                agsubnode(BASE(view)->model.main,dest,FALSE),++uniqmark);
}



static void insert_new_edges(ddview_t *view) {
    Agnode_t	*mn;		/* model node */
    Agedge_t	*me;		/* model edge */
    Agnode_t	*tl, *hd;	/* layout nodes */
    ddpath_t	*path;

    for (mn = agfstnode(BASE(view)->model.e[IL_INS]); mn; mn = agnxtnode(mn)) {
        for (me = agfstout(mn); me; me = agnxtout(me)) {
            tl = dd_rep(agtail(me));
            hd = dd_rep(aghead(me));
            path = dd_open_path(view,ddm_espec(me));

            if (hd == tl)
                continue;

            if (path_exists(view,aghead(me),agtail(me)) ||
                    ((dd_node(tl)->rank_fixed) || (dd_node(hd)->rank_fixed)))
                make_weak_constraint(view,path);
            else
                make_hard_constraint(view,path);
        }
    }
}

/* special-case when it is easy */
static ilbool simple_case(ddview_t *view) {
    Agraph_t	*new_nodes;
    Agnode_t	*n, *u, *v;
    Agnode_t	*possible_leaf, *other;
    Agedge_t	*e;

    /* there must be no deletions */

    if (agnnodes(BASE(view)->model.v[IL_DEL]) > 0)
        return FALSE;

    if (agnnodes(BASE(view)->model.e[IL_DEL]) > 0)
        return FALSE;

    /* there must be no mods */
    if (agnnodes(BASE(view)->model.v[IL_MOD]) > 0)
        return FALSE;

    if (agnnodes(BASE(view)->model.e[IL_MOD]) > 0)
        return FALSE;

    /* there can only be singleton or leaf node insertions */
    new_nodes = BASE(view)->model.v[IL_INS];

    for (n = agfstnode(BASE(view)->model.e[IL_INS]); n; n = agnxtnode(n)) {
        for (e = agfstout(n); e; e = agnxtout(e)) {
            u = agtail(e);
            v = aghead(e);

            if (agsubnode(new_nodes,u,FALSE)) {
                possible_leaf = u;
                other = v;

            } else if (agsubnode(new_nodes,v,FALSE)) {
                possible_leaf = v;
                other = u;

            } else
                return FALSE;	/* edge connects old nodes */

            /* test if edge connects two new nodes */
            if (agsubnode(new_nodes, other, FALSE))
                return FALSE;

            /* test if new node is not a leaf */
            if (agdegree(possible_leaf,TRUE,TRUE) > 1)
                return FALSE;
        }
    }

    /* do the insertions */
    for (n = agfstnode(BASE(view)->model.v[IL_INS]); n; n = agnxtnode(n)) {
        int			newrank, r;
        ILnode_t	*nspec;
        ILedge_t	*espec;

        nspec = ddm_nspec(n);

        if (nspec->pos_valid)
            newrank = dd_map_coord_to_rank(view,nspec->pos.y);
        else
            newrank = view->config->low;

        if ((u = agsubnode(BASE(view)->model.e[IL_INS],n,FALSE))) {
            if ((e = agfstin(u))) {
                espec = ddm_espec(e);

                if ((nspec->pos_valid == FALSE) || (espec->constraint)) {
                    r = dd_rank(dd_rep(e->node)) + ranklength(espec);

                    if ((nspec->pos_valid == FALSE) || (r > newrank))
                        newrank = r;
                }

            } else if ((e = agfstout(u))) {
                espec = ddm_espec(e);

                if ((nspec->pos_valid == FALSE) || (espec->constraint)) {
                    r = dd_rank(dd_rep(e->node)) - ranklength(espec);

                    if ((nspec->pos_valid == FALSE) || (r < newrank))
                        newrank = r;
                }

            } else
                abort();	/* where is the edge? */

        } else { /* singleton */
        }

        dd_set_newrank(dd_rep(n),newrank);
    }

    return TRUE;
}

static void recompute_ranks(ddview_t *view) {
    Agnode_t	*mn;
    Agnode_t	*ln;
    Agedge_t	*me;
    int			oldrank,newrank;
    int			anchor_rank;

    if (simple_case(view))
        return;

    ns_solve(view->con[YCON].g,NS_NORMALIZE|NS_DEBUG|NS_VALIDATE|NS_ATTACHATTRS);

    anchor_rank = ns_getrank(view->con[YCON].anchor);

    for (mn = agfstnode(BASE(view)->model.main); mn; mn = agnxtnode(mn)) {
        if (agsubnode(BASE(view)->model.v[IL_DEL],mn,FALSE))
            continue;

        ln = dd_rep(mn);

        oldrank = dd_rank(ln);

        newrank = ns_getrank(dd_node(ln)->con[YCON].n) - anchor_rank;

        if (newrank != oldrank) {
            dd_set_newrank(ln,newrank);
            il_register_node_callback(BASE(view),ddm_nspec(mn),IL_MOD);

            for (me = agfstedge(mn); me; me = agnxtedge(me,mn))
                il_register_edge_callback(BASE(view),ddm_espec(me),IL_MOD);
        }
    }
}

void dd_rerank_nodes(ddview_t *view) {
    execute_deletions(view);
    move_old_nodes(view);
    insert_new_nodes(view);
    insert_new_edges(view);
    recompute_ranks(view);
}

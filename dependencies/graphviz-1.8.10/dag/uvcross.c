#include <dag.h>

#ifdef DMALLOC
#include "dmalloc.h"
#endif

int dd_uvcross(Agnode_t *v, Agnode_t *w, int use_in, int use_out) {
    Agedge_t	*e, *f;
    int			v_inv,w_inv;
    int			cross = 0;

    if (use_in)
        for (e = agfstin(w); e; e = agnxtin(e)) {
            w_inv = dd_order(e->node);

            for (f = agfstin(v); f; f = agnxtin(f)) {
                v_inv = dd_order(f->node);

                if (v_inv > w_inv)
                    cross++;
            }
        }

    if (use_out)
        for (e = agfstout(w); e; e = agnxtout(e)) {
            w_inv = dd_order(e->node);

            for (f = agfstout(v); f; f = agnxtout(f)) {
                v_inv = dd_order(f->node);

                if (v_inv > w_inv)
                    cross++;
            }
        }

    return cross;
}

#define dd_xpenalty(e) 1		/* edge crossing coefficient of e */

int dd_ncross(ddview_t *view) {
    int		i, k, r, cross, max, *count;
    Agnode_t	*v;
    Agedge_t	*e;
    rank_t		*rd;

    cross = 0;

    for (r = view->config->low; r < view->config->high; r++) {
        rd = dd_rankd(view,r);

        k = dd_rankd(view,r+1)->n + 1;
        count = malloc(k * sizeof(count[0]));

        for (i = 0; i < k; i++)
            count[i] = 0;

        max = 0;

        for (i = 0; i < rd->n; i++) {
            v = rd->v[i];

            if (max > 0) {
                for (e = agfstout(v); e; e = agnxtout(e)) {
                    for (k = dd_order(e->node) + 1; k <= max; k++)
                        cross += count[k] * dd_xpenalty(e);
                }
            }

            for (e = agfstout(v); e; e = agnxtout(e)) {
                int    inv = dd_order(e->node);

                if (inv > max)
                    max = inv;

                count[inv] += dd_xpenalty(e);
            }
        }

        free(count);
    }

    return cross;
}

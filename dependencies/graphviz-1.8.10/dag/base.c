#include <dag.h>

#ifdef DMALLOC
#include "dmalloc.h"
#endif

/* data access - avoid direct access */

ddnode_t *dd_node(Agnode_t *n) {
    return (ddnode_t*)(AGDATA(n));
}

ddedge_t *dd_edge(Agedge_t *e) {
    return (ddedge_t*)(AGDATA(e));
}

ILnode_t *dd_nspec(Agnode_t *n) {
    return il_node(dd_node(n)->model);
}

ILnode_t *ddm_nspec(Agnode_t *mn) {
    return ((ddmdlnode_t*)(AGDATA(mn)))->base.client;
}

ILedge_t *ddm_espec(Agedge_t *me) {
    return ((ddmdledge_t*)(AGDATA(me)))->base.client;
}

ILedge_t *dd_espec(Agedge_t *e) {
    return il_edge(dd_edge(e)->path->model);
}

ILedge_t *dd_pathspec(ddpath_t *path) {
    return path->base.client;
}

Agnode_t *dd_rep(Agnode_t *mn) {
    return ((ddmdlnode_t*)(AGDATA(mn)))->rep;
}

ddpath_t *dd_pathrep(Agedge_t *me) {
    return (ddpath_t*)(AGDATA(me));
}

int dd_rank(Agnode_t *n) {
    return dd_node(n)->rank;
}

int dd_order(Agnode_t *n) {
    return dd_node(n)->order;
}

int dd_newrank(Agnode_t *n) {
    return dd_node(n)->newrank;
}

int dd_oldrank(Agnode_t *n) {
    return dd_node(n)->oldrank;
}

void dd_set_oldrank(Agnode_t *n) {
    dd_node(n)->oldrank = dd_node(n)->rank;
}

void dd_set_newrank(Agnode_t *n, int r) {
    dd_node(n)->newrank = r;
}

void dd_fix_coord(Agnode_t *n, ilbool val) {
    dd_node(n)->coord_fixed = val;
}

ilbool dd_coord_fixed(Agnode_t *n) {
    return dd_node(n)->coord_fixed;
}

void dd_fix_order(Agnode_t *n, ilbool val) {
    dd_node(n)->order_fixed = val;
}

ilbool dd_order_fixed(Agnode_t *n) {
    return dd_node(n)->order_fixed;
}

void dd_set_old_coord(Agnode_t *n) {
    ddnode_t	*spec;

    spec = dd_node(n);
    spec->prev = spec->cur;
}

void dd_set_ycoord(ddview_t *view, Agnode_t *n) {
    double	y;

    y = dd_rankd(view,dd_rank(n))->y_base;
    dd_set_y(n,y);
}

double dd_ranksep(ddview_t *view) {
    return il_nodesep(BASE(view)).y;
}

double dd_minlen(Agedge_t *e) {
    return 1;
    return dd_espec(e)->length_hint;
}

double dd_efactor(Agedge_t *e) {
    return dd_espec(e)->cost;
}

unsigned char dd_dfsmark(Agnode_t *n) {
    return dd_node(n)->dfsmark;
}

void dd_set_dfsmark(Agnode_t *n, unsigned char val) {
    dd_node(n)->dfsmark = val;
}

double dd_uv_sep(ddview_t *view, Agnode_t *left, Agnode_t *right) {
    double   s0,s1,rv;
    s0 = dd_nodesize(view,left).x;
    s1 = dd_nodesize(view,right).x;
    rv = (s0 + s1) / 2.0 + dd_nodesep(view).x;
    return rv;
}

ddpath_t *dd_path(Agedge_t *e) {
    return (ddpath_t*)AGDATA(dd_edge(e)->path->model);
}

Agnode_t *dd_pathhead(Agedge_t *e) {
    return aghead(dd_path(e)->last);
}

Agnode_t *dd_pathtail(Agedge_t *e) {
    return agtail(dd_path(e)->first);
}

Agedge_t *dd_first_elt(Agedge_t *e) {
    return dd_path(e)->first;
}

Agedge_t *dd_last_elt(Agedge_t *e) {
    return dd_path(e)->last;
}

void dd_set_first(ddpath_t *p, Agedge_t *first) {
    p->first = first;
}

void dd_set_last(ddpath_t *p, Agedge_t *last) {
    p->last = last;
}

void dd_set_firstlast(ddpath_t *p, Agedge_t *first, Agedge_t *last) {
    p->first = first;
    p->last = last;
}


ilcoord_t dd_pos(Agnode_t *n) {
    return dd_node(n)->cur.pos;
}

int dd_is_a_vnode(Agnode_t *n) {
    return (dd_node(n)->model == NILnode);
}

ilcoord_t dd_nodesize(ddview_t *view, Agnode_t *node) {
    ilcoord_t	rv;

    if (node == NILnode)
        rv.x = rv.y = 0;
    else if (dd_is_a_vnode(node)) {	/* is a virtual node */
        rv.y = EPSILON;
        rv.x = 2.0 * il_nodesep(BASE(view)).x;

    } else {
        rv = il_nodesize(BASE(view),dd_node(node)->model);

        if (rv.x <= 0)
            rv.x = EPSILON;

        if (rv.y <= 0)
            rv.y = EPSILON;
    }

    return rv;
}

void dd_set_x(Agnode_t *n, double x) {
    ddnode_t	*nd;
    ILnode_t	*spec;

    nd = dd_node(n);
    nd->cur.pos.x = x;
    nd->cur.valid = TRUE;	/* see below, sort of a cheat */

    if (nd->model) {
        spec = il_node(nd->model);
        spec->pos.x = x;
        spec->pos_valid = TRUE;
    }
}

void dd_set_y(Agnode_t *n, double y) {
    ddnode_t	*nd;
    ILnode_t	*spec;

    nd = dd_node(n);
    nd->cur.pos.y = y;
    nd->cur.valid = TRUE;	/* see above, still sort of a cheat */

    if (nd->model) {
        spec = il_node(nd->model);
        spec->pos.y = y;
        spec->pos_valid = TRUE;
    }
}

static char DDrecname[] = "_dag";

Agedge_t *dd_open_edge(ddview_t *view, Agnode_t *u, Agnode_t *v, ddpath_t *path) {
    Agedge_t	*rv;
    ddedge_t	*ed;

    rv = agidedge(u,v,AGID(path->model),TRUE);
    ed = agbindrec(rv,DDrecname,sizeof(ddedge_t),TRUE);
    ed->path = path;
    return rv;
}

void dd_close_edge(ddview_t *view, Agedge_t *e) {
    ddedge_t	*ed;

    ed = dd_edge(e);

    if (ed->cn)
        agdelete(view->con[XCON].g,ed->cn);

    agdelete(view->layout,e);
}

Agnode_t *dd_open_node(ddview_t *view, Agnode_t *model_n) {
    Agnode_t	*n;
    ddnode_t	*nd;
    unsigned long	id;
    static unsigned long	anon_id = 1;

    if (model_n)
        id = (unsigned long)(model_n->base.tag.id);
    else {
        id = anon_id;
        anon_id += 2;
    }

    n = agidnode(view->layout,id,TRUE);
    nd = agbindrec(n,DDrecname,sizeof(ddnode_t),TRUE);

    if (model_n) {
        nd->model = agsubnode(BASE(view)->model.main,model_n,FALSE);
        ((ddmdlnode_t*)(AGDATA(model_n)))->rep = n;
    }

    return n;
}

Agnode_t *dd_find_node(ddview_t *view, ILnode_t *spec) {
    return agidnode(view->layout,(unsigned long)spec,FALSE);
}

void dd_close_node(ddview_t *view, Agnode_t *n) {
    /* must already be isolated in graph */
    assert(agfstedge(n) == NIL(Agedge_t*));

    if (dd_node_in_config(n))
        dd_rank_delete(view,n);

    agdelete(view->layout,n);
}

ddpath_t *dd_open_path(ddview_t *view, ILedge_t *spec) {
    Agedge_t	*model_edge;
    ddpath_t	*path;

    model_edge = il_find_edge(BASE(view),spec);
    assert(il_find_edge(BASE(view),spec));
    path = (ddpath_t*)AGDATA(model_edge);
    path->model = model_edge;
    return path;
}

ddpath_t *dd_find_path(ddview_t *view, ILedge_t *spec) {
    Agedge_t	*model_e;

    model_e = il_find_edge(BASE(view), spec);

    if (model_e)
        return (ddpath_t*)AGDATA(model_e);
    else
        return NIL(ddpath_t*);
}

void dd_close_path(ddview_t *view, ddpath_t *path) {
    Agnode_t	*vn,*nv;
    Agedge_t	*ve;

    if (path->first) {
        if (path->first == path->last) {
            dd_invalidate_mval(dd_pathtail(path->first),DOWN);
            dd_invalidate_mval(dd_pathhead(path->last),UP);
            dd_close_edge(view, path->first);

        } else

            for (vn = path->first->node; dd_is_a_vnode(vn); vn = nv) {
                nv = agfstout(vn)->node;

                for (ve = agfstedge(vn); ve; ve = agnxtedge(ve,vn))
                    dd_close_edge(view, ve);

                dd_close_node(view, vn);
            }

    } else { } /* no work for flat or self edge */

    if (path->unclipped_path)
        il_freecurve(vmregion(path->unclipped_path), path->unclipped_path);

    path->unclipped_path = NIL(ilcurve_t*);

    path->first = path->last = NILedge;

    /* note, do not close model edge until callback is cleared ! */
}

ilcoord_t dd_nodesep(ddview_t *view) {
    return il_nodesep((engview_t*)view);
}

ilbool dd_constraint(Agedge_t *e) {
    return dd_espec(e)->constraint;
}

#include <dag.h>
#include <pathplan.h>

#ifdef DMALLOC
#include "dmalloc.h"
#endif

/*
 * return polygon for a given route 
 */

#ifndef DONOTDEBUGANYMORE
void printregion( Ppoly_t			*region) {
    int	i;

    fprintf(stderr,"%%!PS\nnewpath\n");
    fprintf(stderr,"100 100 translate .5 .5 scale\n");

    for (i = 0; i < region->pn; i++)
        fprintf(stderr,"%f %f %s\n",region->ps[i].x,region->ps[i].y,
                (i ==0? "moveto" : "lineto"));

    fprintf(stderr,"closepath stroke showpage\n");
}

#endif

typedef struct tmproute_s {
    ilcurve_t bound[2];	/* left and right sides of region */
}

tmproute_t;

#define LEFT 0
#define RIGHT 1

static ilcoord_t c(double x, double y) {
    ilcoord_t rv;
    rv.x = x;
    rv.y = y;
    return rv;
}

static ilcurve_t *get_unclipped_curve(Agedge_t *e) {
    return dd_path(e)->unclipped_path;
}

static Ppoly_t *allocpoly(Agraph_t *g, int npts) {
    Ppoly_t	*rv;

    rv = agalloc(g,sizeof(Ppoly_t));
    rv->ps = agalloc(g,sizeof(Ppoint_t)*npts);
    return rv;
}

static void freepoly(Agraph_t *g, Ppoly_t *poly) {
    agfree(g,poly->ps);
    agfree(g,poly);
}

/* get the spacing box below rank r */
static double y_below(ddview_t *view, int r, double fract) /* from top */
{
    rank_t		*rd;
    double		val;

    rd = dd_rankd(view,r);
    val = rd->y_base + rd->delta_below + fract * rd->space_below;
    return val;
}

static ilbool local_crossing(Agnode_t *v, Agnode_t *w) {
    int			vw_order,vvww_order,i,pass,pathlen;
    Agnode_t	*vv,*ww;
    Agedge_t	*(*f)(Agnode_t *);

    pathlen = 2;
    vw_order = dd_order(w) - dd_order(v);
    ww = w;
    vv = v;

    for (pass = 0; pass < 2; pass++) {
        f = (pass == 0? agfstin: agfstout);

        for (i = 0; i < pathlen; i++) {
            if (NOT(dd_is_a_vnode(vv)))
                break;

            if (NOT(dd_is_a_vnode(ww)))
                break;

            vv = f(vv)->node;

            ww = f(ww)->node;

            vvww_order = dd_order(ww) - dd_order(vv);

            if (vw_order * vvww_order < 0)
                return TRUE;
        }
    }

    return FALSE;
}

static Agnode_t *bounding_node(ddview_t *view, Agnode_t *inp, int dir) {
    Agnode_t	*v,*w;

    v = inp;

    while ((w = dd_relnode(view,v,dir))) {
        if (NOT(dd_is_a_vnode(w)))
            break;

        /* just did this for testing */
        break;

        if (NOT(local_crossing(v,w)))
            break;

        v = w;
    }

    return w;
}

static double dd_bound(ddview_t *view, Agnode_t *u, int side) {
    double		rv;
    double		sep;
    Agnode_t	*w;

    assert(BASE(view)->bb.valid);
    sep = il_nodesep(BASE(view)).x/2.0;

    switch(side) {

            case RIGHT:

            if ((w = bounding_node(view,u,1)))
                rv = dd_pos(w).x - dd_nodesize(view,w).x/2.0 - sep;
            else
                rv = BASE(view)->bb.size.ur.x;

            break;

            case LEFT:
            if ((w = bounding_node(view,u,-1)))
                rv = dd_pos(w).x + dd_nodesize(view,w).x/2.0 + sep;
            else
                rv = BASE(view)->bb.size.ll.x;

            break;

            default:
            rv = 0.0;

            abort();
    }

    return rv;
}

static void append_vnode(tmproute_t *route, ddview_t *view, Agnode_t *virt);
#if 0 /* not used */
static void append_curve(tmproute_t *, int side, ilcurve_t *curve, double y0, double y1);
#endif
static void append_side(tmproute_t *, int side, double x, double y0, double y1);
static void append_point(tmproute_t *, int side, double x, double y);
static void append_box(tmproute_t *rp, ddview_t *view, int toprank, double f0, double f1);
static Agedge_t *neighboring_edge(ddview_t *view, Agnode_t *n, Agedge_t *e, int side);
static void term_route(tmproute_t *rv, ddview_t *view, Agnode_t *n,
                       Agedge_t *e, ilcoord_t port, double fract);
static Ppoly_t *define_boundary_poly(Agraph_t *g, tmproute_t *tmp);

static tmproute_t *tmproute(Agraph_t *g, int npts) {
    tmproute_t	*rv;
    int			i;

    rv = agalloc(g,sizeof(tmproute_t));

    for (i = 0; i < 2; i++) {
        rv->bound[i].n = 0;
        rv->bound[i].p = agalloc(g,npts * sizeof(ilcoord_t));
    }

    return rv;
}

static void term_route(tmproute_t *rv, ddview_t *view, Agnode_t *n, Agedge_t *e, ilcoord_t port, double fract) {
    int			r,rb;
    double		y_cept;
    double		leftx_port, leftx_cept, rightx_port, rightx_cept;
    Agedge_t	*ne;
    ilcurve_t	*curve;

    r = dd_rank(n);

    if (n == agtail(e)) {
        rb = r;
        e = AGMKOUT(e);

    } else {
        rb = r - 1;
        e = AGMKIN(e);
    }

    y_cept = y_below(view,rb,fract);

    if ((ne = neighboring_edge(view,n,e,LEFT)) && (curve = get_unclipped_curve(ne))) {
        leftx_port = il_intersect_at_y(curve,port.y).x;
        leftx_cept = il_intersect_at_y(curve,y_cept).x;

    } else {
        leftx_port = leftx_cept = dd_bound(view,n,LEFT);
    }

    if ((ne = neighboring_edge(view,n,e,RIGHT)) && (curve = get_unclipped_curve(ne))) {
        rightx_port = il_intersect_at_y(curve,port.y).x;
        rightx_cept = il_intersect_at_y(curve,y_cept).x;

    } else {
        rightx_port = rightx_cept = dd_bound(view,n,RIGHT);
    }

    if (leftx_port > rightx_port) {
        double t = leftx_port;
        leftx_port = rightx_port;
        rightx_port = t;
    }

    if (leftx_cept > rightx_cept) {
        double t = leftx_cept;
        leftx_cept = rightx_cept;
        rightx_cept = t;
    }

    if (port.y < y_cept) {
        append_point(rv, LEFT, leftx_port, port.y);
        append_point(rv, LEFT, leftx_cept, y_cept);
        append_point(rv, RIGHT, rightx_port, port.y);
        append_point(rv, RIGHT, rightx_cept, y_cept);

    } else {
        append_point(rv, LEFT, leftx_cept, y_cept);
        append_point(rv, LEFT, leftx_port, port.y);
        append_point(rv, RIGHT, rightx_cept, y_cept);
        append_point(rv, RIGHT, rightx_port, port.y);
    }
}

static Agedge_t *neighboring_edge(ddview_t *view, Agnode_t *n, Agedge_t *e, int side) {
    Agedge_t	*f,*rv;
#ifdef NEIGHBORING_EDGE

    int			delta,delta0, dir;
#endif

    if (AGTYPE(e) == AGINEDGE)
        f = agfstin(aghead(e));
    else
        f = agfstout(agtail(e));

    rv = NILedge;

#ifdef NEIGHBORING_EDGE

    if (side == LEFT)
        dir = -1;
    else
        dir = 1;

    delta0 = 0;

    while (f) {
        if (f != e) {
            if (dd_espec(f)->pos) {
                delta = dd_order(f->node) - dd_order(e->node);

                if (delta * dir > 0) {
                    delta = abs(delta);

                    if ((rv == NILedge) || (delta < delta0)) {
                        rv = f;
                        delta0 = delta;
                    }
                }
            }
        }

        if (AGTYPE(e) == AGINEDGE)
            f = agnxtin(f);
        else
            f = agnxtout(f);
    }

#endif
    return rv;
}

#if 0 /* not used */
static void append_curve(tmproute_t *rp, int side, ilcurve_t *curve, double y0, double y1) {
    int		i;

    if (y0 > y1) {
        double temp = y0;
        y0 = y1;
        y1 = temp;
    }

    i = rp->bound[side].n;
    rp->bound[side].n += 2;
    rp->bound[side].p[i++] = il_intersect_at_y(curve,y0);
    rp->bound[side].p[i++] = il_intersect_at_y(curve,y1);
}

#endif

static void add_extrema(tmproute_t *rp, ddview_t *view, double y) {
    int		i;
    i = rp->bound[LEFT].n++;
    rp->bound[LEFT].p[i] = c(BASE(view)->bb.size.ll.x,y);
    i = rp->bound[RIGHT].n++;
    rp->bound[RIGHT].p[i] = c(BASE(view)->bb.size.ur.x,y);
}

static void append_box(tmproute_t *rp, ddview_t *view, int toprank, double f0, double f1) {
    double		y;

    y = y_below(view, toprank, f0);
    add_extrema(rp,view,y);
    y = y_below(view, toprank, f1);
    add_extrema(rp,view,y);
}

static void append_side(tmproute_t *rp, int side, double x, double arg_y0, double arg_y1) {
    int		i;
    double	y0,y1;

    if (arg_y0 < arg_y1) {
        y0 = arg_y0;
        y1 = arg_y1;

    } else {
        y0 = arg_y1;
        y1 = arg_y0;
    }

    i = rp->bound[side].n;
    rp->bound[side].n += 2;
    rp->bound[side].p[i++] = c(x,y0);
    rp->bound[side].p[i++] = c(x,y1);
}

static void append_point(tmproute_t *rp, int side, double x, double y) {
    int		i;

    i = rp->bound[side].n++;
    rp->bound[side].p[i].x = x;
    rp->bound[side].p[i].y = y;
}

/* this appends the part of the path needed to route through a vnode box */
static void append_vnode(tmproute_t *route, ddview_t *view, Agnode_t *virt) {
    int		r;
    double	top_y, bot_y, left_x, right_x;

    r = dd_rank(virt);
    top_y = y_below(view,r-1,1.0);
    bot_y = y_below(view,r,0.0);
    left_x = dd_bound(view, virt, LEFT);
    right_x = dd_bound(view, virt, RIGHT);
    assert(left_x < right_x);
    append_side(route,LEFT,left_x,top_y, bot_y);
    append_side(route,RIGHT,right_x,top_y, bot_y);
}

static Ppoint_t pppoint(double x, double y) {
    Ppoint_t rv;
    rv.x = x;
    rv.y = y;
    return rv;
}

static Ppoint_t cvtpt(ilcoord_t p) {
    Ppoint_t rv;
    rv.x = (double)p.x;
    rv.y = (double)p.y;
    return rv;
}

static ilcoord_t uncvtpt(Ppoint_t p) {
    ilcoord_t rv;
    rv.x = p.x;
    rv.y = p.y;
    return rv;
}

static ilcoord_t addpt(ilcoord_t p, ilcoord_t q) {
    ilcoord_t rv;
    rv.x = p.x + q.x;
    rv.y = p.y + q.y;
    return rv;
}

static ilbool pt_eq(Ppoint_t p, Ppoint_t q) {
    return ((p.x == q.x) && (p.y == q.y));
}

/* destructive conversion */
static Ppoly_t *define_boundary_poly(Agraph_t *g, tmproute_t *tmp) {
    Ppoly_t		*rv;
    int			i,j,max_npts;

    max_npts = tmp->bound[0].n + tmp->bound[1].n;
    rv = allocpoly(g,max_npts);
    j = 0;

    for (i = 0; i < tmp->bound[0].n; i++)
        if ((j == 0) || !pt_eq(rv->ps[j-1],cvtpt(tmp->bound[0].p[i])))
            rv->ps[j++] = cvtpt(tmp->bound[0].p[i]);

    for (i = tmp->bound[1].n - 1; i >= 0; i--)
        if ((j == 0) || !pt_eq(rv->ps[j-1],cvtpt(tmp->bound[1].p[i])))
            rv->ps[j++] = cvtpt(tmp->bound[1].p[i]);

    rv->pn = j;

    agfree(g,tmp->bound[0].p);

    agfree(g,tmp->bound[1].p);

    agfree(g,tmp);

    return rv;
}

Ppoly_t *dd_forward_edge_region(ddview_t *view, Agnode_t *tl, Agnode_t *hd,Agedge_t **inp, ilcoord_t tp, ilcoord_t hp) {
    int			i,r;
    tmproute_t	*route;
    double		prev_fract;
    Ppoly_t		*rv;

    r = dd_rank(tl);
    /* 2 points for each vnode side, 2 points for each inter-rank box side,
       plus 5 for head and 5 for tail termination */
    route = tmproute(view->layout, (dd_rank(hd) - r)*4 + 10);

    prev_fract = .3;
    term_route(route,view,tl,inp[0],tp,prev_fract);

    for (i = 0; inp[i+1]; i++) {
        append_box(route,view,r++,prev_fract,1.0);
        append_vnode(route,view,inp[i]->node);
        prev_fract = 0.0;
    }

    append_box(route,view,r++,prev_fract,.7);
    term_route(route,view,hd,inp[i],hp,.7);

    rv = define_boundary_poly(view->layout,route);
    return rv;
}

Ppoly_t *dd_flat_edge_region(ddview_t *view, Agnode_t *tl, Agnode_t *hd, ilcoord_t tp, ilcoord_t hp) {
    int			i, ix, n_obstacles;
    double		xleft, xright, y;
    Agnode_t	*left, *right, *obstacle;
    ilcoord_t	lp,rp,size;
    rank_t		*rd;
    Ppoly_t		*rv;

    if (dd_order(tl) < dd_order(hd)) {
        left = tl;
        right = hd;
        lp = tp;
        rp = hp;

    } else {
        left = hd;
        right = tl;
        lp = hp;
        rp = tp;
    }

    n_obstacles = dd_order(right) - dd_order(left) - 1;
    rv = allocpoly(view->layout,4 + n_obstacles * 2);
    rd = dd_rankd(view,dd_rank(tl));

    /* walk along bottom */
    ix = 0;

    if (n_obstacles == 0) {
        y = rd->y_base + rd->delta_below;
        rv->ps[ix++] = pppoint(lp.x,y);
        rv->ps[ix++] = pppoint(rp.x,y);

    } else {
        rv->ps[ix++] = cvtpt(lp);
        obstacle = left;

        for (i = 0; i < n_obstacles; i++) {
            obstacle = dd_right(view,obstacle);
            size = dd_nodesize(view,obstacle);
            y = rd->y_base - size.y / 2.0;
            xleft = dd_pos(obstacle).x - size.x / 2.0;
            xright = xleft + size.x;
            rv->ps[ix++] = pppoint(xleft,y);
            rv->ps[ix++] = pppoint(xright,y);
        }

        rv->ps[ix++] = cvtpt(rp);
    }

    /* add the top */
    y = rd->y_base - rd->delta_above - dd_ranksep(view) / 2.0;

    ;
    rv->ps[ix++] = pppoint(rp.x,y);

    rv->ps[ix++] = pppoint(lp.x,y);

    rv->pn = ix;

    return rv;
}

static void get_vnode_path(ddview_t *view, Agedge_t *view_edge, Agedge_t **result) {
    Agedge_t	*e,*f;
    int			i;

    i = 0;
    e = dd_first_elt(view_edge);
    f = dd_last_elt(view_edge);

    while (1) {
        result[i++] = e;

        if (e == f)
            break;

        e = agfstout(e->node);
    }

    result[i++] = NILedge;
}

static void adjust_vpath(ddview_t *view, Agedge_t **vpath, ilcurve_t *curve) {
    Agedge_t	*e;
    Agnode_t	*vnode;
    int			i;
    double		x,y;
    double		lb,rb,halfwidth,least_move;

    for (i = 0; (e = vpath[i]); i++) {
        vnode = e->node;

        if (dd_is_a_vnode(vnode)) {
            Agnode_t *left = dd_left(view, vnode);
            Agnode_t *right = dd_right(view, vnode);

            y = dd_pos(vnode).y;
            x = il_intersect_at_y(curve, y).x;
            halfwidth = dd_nodesize(view,vnode).x / 2.0;
            lb = dd_bound(view,vnode,LEFT) + halfwidth;

            if (x < lb)
                x = lb;

            rb = dd_bound(view,vnode,RIGHT) - halfwidth;

            if (x > rb)
                x = rb;

            /* don't move if it's too small */
            least_move = BASE(view)->client->separation.x;

            if ((x < rb) && (x > lb) && fabs(x - dd_pos(vnode).x) >= least_move)
                dd_set_x(vnode,x);

            /* it's possible that local_crossing() swaps vnode order */
            while ((left = dd_left(view, vnode))) {
                /* for testing - should never happen right now! */

                if (dd_pos(left).x > x) {
                    abort();
                    dd_exchange(view,left,vnode);

                } else
                    break;
            }

            while ((right = dd_right(view, vnode))) {
                if (dd_pos(right).x < x) {
                    abort();
                    dd_exchange(view,right,vnode);

                } else
                    break;
            }
        }
    }
}

static void set_actual_x(ddview_t *view, ddpath_t *path) {
    Agnode_t		*vn;
    Agedge_t		*ve;
    ilcurve_t		*curve;
    ddnode_t		*nd;

    curve = &(path->base.client->pos->def.curve);

    for (ve = path->first; dd_is_a_vnode(ve->node); ve = ve->node->out) {
        vn = ve->node;
        nd = dd_node(vn);
        nd->actual_x = il_intersect_at_y(curve,nd->cur.pos.y).x;
        nd->actual_x_valid = TRUE;
    }
}

void dd_make_edge_spline(ddview_t *view, ddpath_t *path) {
    int				i;
    Agraph_t		*layout;
    Agedge_t		*view_edge, **vpath;
    Agnode_t		*tl,*hd;
    ilcoord_t		tailpt, headpt;
    ilcurve_t		*unclipped,*clipped;
    ilbool			reversed;
    Ppoly_t			*region;
    Ppoint_t		endpoints[2];
    Pvector_t		endslopes[2];
    Ppolyline_t		polyline_route, spline_route;
    Ppoly_t			*polys[1];
    Pedge_t			*barriers;
    int				n_barriers;

    assert(path->unclipped_path == NILcurve);

    layout = view->layout;
    tl = dd_rep(agtail(path->model));
    hd = dd_rep(aghead(path->model));

    reversed = (dd_rank(tl) > dd_rank(hd));

    if (reversed) {
        Agnode_t *temp;
        temp = tl;
        tl = hd;
        hd = temp;
    }

    tailpt = addpt(path->base.client->tail.port,dd_pos(tl));
    headpt = addpt(path->base.client->head.port,dd_pos(hd));

    if (tl == hd) {	/* self arc */
        ilcoord_t	farpt,sep;
        double		rb;

        sep = view->base.client->separation;
        unclipped = il_newcurve(agheap(layout),IL_SPLINE,100);
        farpt.x = dd_pos(tl).x + dd_nodesize(view,tl).x / 2.0 + 2.0 * sep.x;
        rb = dd_bound(view, tl, RIGHT);

        if (dd_right(view,tl) == NILnode)
            rb += 2.0 * sep.x; /* ok to grow */

        if (farpt.x > rb)
            farpt.x = rb;

        farpt.y = (tailpt.y + headpt.y) / 2.0;

        unclipped->p[0] = tailpt;

        unclipped->p[1] = ilcoord(tailpt.x,tailpt.y + sep.y / 2.0);

        unclipped->p[2] = ilcoord(farpt.x,farpt.y + sep.y / 2.0);

        unclipped->p[3] = farpt;

        unclipped->p[4] = ilcoord(farpt.x,farpt.y - sep.y / 2.0);

        unclipped->p[5] = ilcoord(headpt.x,headpt.y - sep.y / 2.0);

        unclipped->p[6] = headpt;

        unclipped->n = 7;

        vpath = NIL(Agedge_t**);

    } else {

        if (dd_rank(tl) == dd_rank(hd)) {
            region = dd_flat_edge_region(view,tl,hd,tailpt,headpt);
            vpath = NIL(Agedge_t**);

        } else {
            view_edge = path->first;
            vpath = agalloc(layout,sizeof(Agedge_t*)*(view->config->high - view->config->low + 2));
            get_vnode_path(view, view_edge, vpath);
            dd_check_vnode_path(view, vpath);
            region = dd_forward_edge_region(view,tl,hd,vpath,tailpt,headpt);
        }

        endpoints[0] = cvtpt(tailpt);
        endpoints[1] = cvtpt(headpt);
        endslopes[0].x = endslopes[0].y = 0.0;
        endslopes[1].x = endslopes[1].y = 0.0;
        Pshortestpath(region, endpoints, &polyline_route);

        polys[0] = region;
        Ppolybarriers(polys, 1, &barriers, &n_barriers);

        assert(!Proutespline (barriers, n_barriers, polyline_route,
                              endslopes, &spline_route));


        unclipped = il_newcurve(agheap(layout),IL_SPLINE,spline_route.pn);

        for (i = 0; i < spline_route.pn; i++)
            unclipped->p[i] = uncvtpt(spline_route.ps[i]);

        unclipped->n = spline_route.pn;

        freepoly(layout,region);

        free(barriers);

        /* do not free these because they are reused by path/shortest.c */
        /* free(polyline_route.ps); free(spline_route.ps); */
    }

    path->unclipped_path = unclipped;
    clipped = il_clip_endpoints(BASE(view),unclipped, dd_nspec(tl), dd_nspec(hd));

    if (reversed) {
        for (i = 0; i < clipped->n / 2; i++) {
            ilcoord_t t = clipped->p[i];
            clipped->p[i] = clipped->p[clipped->n - i - 1];
            clipped->p[clipped->n - i - 1] = t;
        }
    }

    il_freeshape(agheap(DDmodel(BASE(view)->client)),path->base.client->pos);
    path->base.client->pos =
        il_newshape(agheap(DDmodel(BASE(view)->client)),clipped,NILshape);

    if (vpath) {
        adjust_vpath(view, vpath, unclipped);
        agfree(layout,vpath);
        set_actual_x(view, path);
    }
}

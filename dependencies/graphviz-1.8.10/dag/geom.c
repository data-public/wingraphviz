/*
 *	compute node and edge coordinates 
 */

#include <dag.h>

#ifdef DMALLOC
#include "dmalloc.h"
#endif

static void update_bb(ddview_t *view) {
    Agnode_t	*left, *right;
    int			r;
    double		lb,glb,rb,grb;	/* left and right bounds: local, global */
    ilcoord_t	sz;
    rank_t		*rd;

    glb = MAXDOUBLE;
    grb = -MAXDOUBLE;

    for (r = view->config->low; r <= view->config->high; r++) {
        if ((left = dd_leftmost(view,r))) {
            sz = dd_nodesize(view,left);
            lb = dd_pos(left).x - sz.x / 2.0;

            if (glb > lb)
                glb = lb;

            right = dd_rightmost(view,r);

            sz = dd_nodesize(view,right);

            rb = dd_pos(right).x + sz.x / 2.0;

            if (grb < rb)
                grb = rb;
        }
    }

    view->base.client->actual_bb.ll.x = glb;
    rd = dd_rankd(view,view->config->low);

    if (rd)
        view->base.client->actual_bb.ll.y = rd->y_base - rd->delta_above;

    view->base.client->actual_bb.ur.x = grb;

    rd = dd_rankd(view,view->config->high);

    if (rd)
        view->base.client->actual_bb.ur.y = rd->y_base + rd->delta_below;

    /*BASE(view)->bb.size.ll.x = glb;
    BASE(view)->bb.size.ur.x = grb;*/
    BASE(view)->bb.size = view->base.client->actual_bb;

    BASE(view)->bb.valid = TRUE;
}

/* (re)spline edges */

static ilbool node_moved(Agnode_t *n) {
    ddnode_t	*dspec;

    dspec = dd_node(n);

    if (dspec->prev.valid &&
            (dspec->cur.pos.x == dspec->prev.pos.x) &&
            (dspec->cur.pos.y == dspec->prev.pos.y))
        return FALSE;

    return TRUE;
}

static ilbool edge_needs_redraw(ddview_t *view, ddpath_t *path) {
    Agedge_t	*ve;
    Agnode_t	*n, *left, *right;
    double		x;
    double		sep;
    ddnode_t	*nd;

    if (path->unclipped_path == NILcurve)
        return TRUE; /* new edge */

    /* check for endpoint resize */
    if (il_node(agtail(path->model))->update & IL_UPD_SHAPE)
        return TRUE;

    if (il_node(aghead(path->model))->update & IL_UPD_SHAPE)
        return TRUE;

    ve = path->first;

    if (ve == NILedge) {	/* flat */

        if (node_moved(dd_rep(agtail(path->model))))
            return TRUE;

        if (node_moved(dd_rep(aghead(path->model))))
            return TRUE;

        return FALSE;
    }

    sep = view->base.client->separation.x;

    if (node_moved(agtail(ve)))
        return TRUE;

    while (dd_is_a_vnode((n = ve->node))) {
        if(NOT(dd_node(n)->actual_x_valid))
            return TRUE;

        x = D(dd_node(n)->actual_x);

        if ((left = dd_left(view, n))) {
            nd = dd_node(left);

            if (dd_is_a_vnode(left)) {
                if (nd->actual_x_valid && D(nd->actual_x) + sep > x)
                    return TRUE;

            } else {
                if (nd->cur.pos.x + il_nodesize(BASE(view),nd->model).x / 2.0 + sep > x)
                    return TRUE;
            }
        }

        if ((right = dd_right(view, n))) {
            nd = dd_node(right);

            if (dd_is_a_vnode(right)) {
                if (nd->actual_x_valid && D(nd->actual_x) - sep < x)
                    return TRUE;

            } else {
                if (nd->cur.pos.x - il_nodesize(BASE(view),nd->model).x / 2.0 - sep < x)
                    return TRUE;
            }
        }

        ve = ve->node->out;
    }

    if (node_moved(aghead(ve)))
        return TRUE;

    return FALSE;
}

static void update_edges(ddview_t *view) {
    Agnode_t	*mn;
    Agedge_t	*me;
    ddpath_t	*path;
    ILedge_t	*espec;

    for (mn = agfstnode(BASE(view)->model.main); mn; mn = agnxtnode(mn)) {
        for (me = agfstout(mn); me; me = agnxtout(me)) {
            if (agsubedge(BASE(view)->model.e[IL_DEL],me,FALSE))
                continue;

            if ((path = dd_pathrep(me))) {
                espec = ddm_espec(me);

                if (edge_needs_redraw(view,path)) {
                    if (path->unclipped_path)
                        il_freecurve(vmregion(path->unclipped_path),
                                     path->unclipped_path);

                    path->unclipped_path = NILcurve;

                    il_register_edge_callback(BASE(view), espec, IL_MOD);
                }
            }
        }
    }

    for (mn = agfstnode(BASE(view)->model.main); mn; mn = agnxtnode(mn)) {
        for (me = agfstout(mn); me; me = agnxtout(me)) {
            if (agsubedge(BASE(view)->model.e[IL_DEL],me,FALSE))
                continue;

            if ((path = dd_pathrep(me)) && (path->unclipped_path == NILcurve))
                dd_make_edge_spline(view,path);
        }
    }
}

/* recompute coordinates */
static int			Giter;

void dd_update_geometry
(ddview_t *view) {
    Giter++;

    dd_update_X(view);
    dd_update_Y(view);
    update_bb(view);
    dd_check_all(view);
    update_edges(view);
}

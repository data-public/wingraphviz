#include <dag.h>

#ifdef DMALLOC
#include "dmalloc.h"
#endif

/* maintains ranked graph configurations */

rank_t *dd_rankd(ddview_t *view, int absrank) {
    if ((absrank < view->config->low) || (absrank > view->config->high))
        return NILrank;

    return view->config->r[absrank - view->config->low];
}

Agnode_t *dd_relnode(ddview_t *view, Agnode_t *n, int offset) {
    rank_t		*rd;
    int			r,pos;

    r = dd_rank(n);
    pos = dd_order(n) + offset;

    if (pos < 0)
        return NILnode;

    rd = dd_rankd(view,r);

    if (pos >= rd->n)
        return NILnode;

    return rd->v[pos];
}

Agnode_t *dd_right(ddview_t *view, Agnode_t *n) {
    return dd_relnode(view,n,1);
}

Agnode_t *dd_left(ddview_t *view, Agnode_t *n) {
    return dd_relnode(view,n,-1);
}

Agnode_t *dd_rightmost(ddview_t *view, int r) {
    rank_t		*rd;

    rd = dd_rankd(view,r);

    if ((rd == NILrank) || (rd->n == 0))
        return NILnode;

    return rd->v[rd->n - 1];
}

Agnode_t *dd_leftmost(ddview_t *view, int r) {
    rank_t		*rd;

    rd = dd_rankd(view,r);

    if ((rd == NILrank) || (rd->n == 0))
        return NILnode;

    return rd->v[0];
}

static void extend_config(ddview_t *view, int low, int high) {
    int			i,osize,nsize,o_nbytes,n_nbytes;
    config_t	*config;
    rank_t		**list;

    config = view->config;

    if (config->r) {
        osize = (config->high - config->low + 1);
        o_nbytes = osize * sizeof(config->r[0]);
        nsize = (high - low + 1);
        n_nbytes = nsize * sizeof(config->r[0]);

        list = config->r = agrealloc(view->layout,config->r,o_nbytes,n_nbytes);

        if (low < config->low) {
            for (i = osize - 1; i >= 0; i--)
                list[i + (nsize - osize)] = list[i];

            for (i = 0; i < (nsize - osize); i++)
                list[i] = agalloc(view->layout,sizeof(rank_t));

        } else {
            for (i = osize; i < nsize; i++)
                list[i] = agalloc(view->layout,sizeof(rank_t));
        }

    } else {
        nsize = high - low + 1;
        n_nbytes = nsize * sizeof(config->r[0]);
        config->r = agalloc(view->layout,n_nbytes);

        for (i = 0; i < nsize; i++)
            config->r[i] = agalloc(view->layout,sizeof(rank_t));
    }

    config->low = low;
    config->high = high;
}

rank_t *dd_extendrank(ddview_t *view, int r) {
    rank_t		*rd;

    if ((r < view->config->low) || (r > view->config->high)) {
        if (view->config->low <= view->config->high) {
            extend_config(view,MIN(r,view->config->low),MAX(r,view->config->high));

        } else {
            extend_config(view,r,r);
        }
    }

    rd = dd_rankd(view,r);
    rd->v = agrealloc(view->layout,rd->v,(rd->n + 1) * sizeof(rd->v[0]),
                      (rd->n + 2) * sizeof(rd->v[0]));
    return rd;
}

void dd_install_at_right(ddview_t *view, Agnode_t *n, int r) {
    Agnode_t	*right;
    rank_t		*rd;
    double		x;
    int			slot;

    right = dd_rightmost(view,r);

    if (right)
        x = dd_pos(right).x + dd_uv_sep(view,right,n);
    else
        x = 0.0;

    rd = dd_extendrank(view,r);

    slot = rd->n++;

    rd->v[slot] = n;

    dd_node(n)->rank = r;

    dd_node(n)->order = slot;

    dd_set_x(n,x);

    dd_set_config_flag(n,TRUE);

    dd_set_ycoord(view,n);
}

void dd_install_at_pos(ddview_t *view, Agnode_t *n, int r, double x) {
    Agnode_t	*ln,*rn,*mv;
    rank_t		*rd;
    int			i,slot;

    dd_set_x(n,x);

    /* find where incoming node belongs */
    ln = NILnode;

    for (rn = dd_leftmost(view,r); rn; rn = dd_right(view,rn)) {
        if (x <= dd_pos(rn).x)
            break;
        else
            ln = rn;
    }

    if (ln)
        slot = dd_order(ln) + 1;
    else
        slot = 0;

    rd = dd_extendrank(view,r);

    for (i = rd->n - 1; i >= slot; i--) {
        mv = rd->v[i+1] = rd->v[i];
        dd_node(mv)->order = i+1;
        dd_invalidate_adj_mvals(mv);
    }

    rd->v[slot] = n;
    rd->n++;
    dd_node(n)->order = slot;
    dd_node(n)->rank = r;
    dd_invalidate_adj_mvals(n);
    dd_set_config_flag(n,TRUE);
    dd_set_ycoord(view,n);
}

void dd_exchange(ddview_t *view, Agnode_t *u, Agnode_t *v) {
    rank_t		*rd;
    int			upos,vpos;

    Agnode_t	*cn_u, *cn_v;	/* constraint vars */
    Agedge_t	*ce;			/* possible LR constraint edge */

    assert(dd_node_in_config(u));
    assert(dd_node_in_config(v));
    assert(dd_rank(u) == dd_rank(v));

    rd = dd_rankd(view,dd_rank(u));
    upos = dd_order(u);
    vpos = dd_order(v);

    /* delete any LR constraint between these nodes */
    cn_u =  dd_node(u)->con[XCON].n;
    cn_v =  dd_node(v)->con[XCON].n;

    if (cn_u && cn_v && ((ce = agedge(cn_u,cn_v,NILstr,FALSE))))
        agdelete(view->con[XCON].g,ce);

    rd->v[vpos] = u;

    dd_node(u)->order = vpos;

    rd->v[upos] = v;

    dd_node(v)->order = upos;

    dd_invalidate_adj_mvals(u);

    dd_invalidate_adj_mvals(v);

    (void)agsubnode(view->dirty,u,TRUE);

    (void)agsubnode(view->dirty,v,TRUE);
}

void dd_delete_constraint(ddview_t *view, Agnode_t *ln, int sys) {
    ddnode_t	*nd;
    Agnode_t	*c_var;

    nd = dd_node(ln);

    if ((c_var = nd->con[sys].n)) {
        agdelete(view->con[sys].g,c_var);
        nd->con[sys].n = NILnode;
    }

    if ((c_var = nd->con[sys].stab)) {
        agdelete(view->con[sys].g,c_var);
        nd->con[sys].stab = NILnode;
    }
}


void dd_rank_delete(ddview_t *view, Agnode_t *n) {
    rank_t		*rd;
    Agnode_t	*mv, *cn;
    Agedge_t	*e;
    int			pos,i;

    for (e = agfstedge(n); e; e = agnxtedge(e,n)) {
        cn = dd_edge(e)->cn;

        if (cn) {
            agdelete(view->con[XCON].g, cn);
            dd_edge(e)->cn = NILnode;
        }
    }

    dd_delete_constraint(view, n, XCON);
    dd_invalidate_adj_mvals(n);
    rd = dd_rankd(view,dd_rank(n));
    pos = dd_order(n);
    assert(rd->v[pos] == n);

    for (i = pos; i < rd->n - 1; i++) {
        mv = rd->v[i] = rd->v[i+1];
        dd_node(mv)->order = i;

        if (i == pos)
            agsubnode(view->dirty,mv,TRUE);
    }

    rd->v[i] = NILnode;
    rd->n--;

    dd_set_config_flag(n,FALSE);
    dd_node(n)->rank = -MAXINT;
    dd_fix_coord(n,FALSE);
    dd_fix_order(n,FALSE);
}

void    dd_set_config_flag(Agnode_t *n, ilbool val) {
    dd_node(n)->in_config = val;
}

ilbool	dd_node_in_config(Agnode_t *n) {
    return dd_node(n)->in_config;
}

void dd_open_config(ddview_t *view) {
    config_t	*config;

    config = agalloc(view->layout,sizeof(config_t));
    config->low = 0;
    config->high = -1;
    view->config = config;
}

void dd_close_config(ddview_t *view) {
    int			i;
    config_t	*config;

    config = view->config;

    for (i = 0; i <= config->high - config->low; i++) {
        agfree(view->layout, config->r[i]->v);
        agfree(view->layout, config->r[i]);
    }

    agfree(view->layout, config->r);
    agfree(view->layout, config);
}

Agnode_t *dd_vnode(ddview_t *view, int rank, double xpos) {
    Agnode_t	*rv;

    rv = dd_open_node(view, NILnode);
    dd_install_at_pos(view,rv,rank,xpos);
    return rv;
}

#include <dag.h>
#include <sys/signal.h>

/* sys/time.h needed for struct timeval which is used from sys/resource.h */
#if TIME_WITH_SYS_TIME
#  include <sys/time.h>
#  include <time.h>
#else
#  if HAVE_SYS_TIME_H
#     include <sys/time.h>
#  else
#     include <time.h>
#  endif
#endif

#include <sys/resource.h>
#include <limits.h>

#if HAVE_UNISTD_H
#include <sys/types.h>
#include <unistd.h>
#endif

#ifndef CLK_TCK
# if HAVE_SYSCONF
#   define CLK_TCK sysconf(_SC_CLK_TCK)
# elif HAVE__SYSCONF
#   define CLK_TCK _sysconf(_SC_CLK_TCK)
# else
#   define CLK_TCK 60
# endif
#endif

static clock_t Clock;
static int Nreq;
static FILE *Log;

#if HAVE_GETRUSAGE
static double getClock() {

    struct rusage ru;
    (void) getrusage(RUSAGE_SELF,&ru);
    return ru.ru_utime.tv_usec + 1e+6 * ru.ru_utime.tv_sec;
}

#else
#include <sys/times.h>
static double getClock() {

    struct tms t;
    times(&t);
    /* times() returns times in units of clock ticks, but
    * rusage() returns time in units of seconds.  Convert
    * ticks to seconds to make things consistent.
    */
    return (double)(t.tms_utime / CLK_TCK);
}

#endif

#ifdef DMALLOC
#include "dmalloc.h"
#endif

static int count_mods(ddview_t *view) {
    return agnnodes(BASE(view)->model.v[IL_INS])
           + agnnodes(BASE(view)->model.v[IL_MOD])
           + agnnodes(BASE(view)->model.v[IL_DEL])
           + agnedges(BASE(view)->model.e[IL_INS])
           + agnedges(BASE(view)->model.e[IL_MOD])
           + agnedges(BASE(view)->model.e[IL_DEL]);
}

static void count_inv(ddview_t *view, int *inv, int *rinv) {
    /* have oldpos, but not oldorder */
    int		r, i, j;
    rank_t		*rd;
    Agnode_t	*u,*v;

    *inv = *rinv = 0;

    for (r = view->config->low; r <= view->config->high; r++) {
        rd = dd_rankd(view,r);

        for (i = 0; i < rd->n-1; i++) {
            u = rd->v[i];

            if (dd_oldrank(u) != dd_rank(u))
                continue;

            if (!dd_node(u)->prev.valid)
                continue;

            for (j = i + 1; j < rd->n; j++) {
                v = rd->v[j];

                if (!dd_node(v)->prev.valid)
                    continue;

                if (dd_oldrank(v) != dd_rank(v))
                    continue;

                if (dd_node(u)->prev.pos.x > dd_node(v)->prev.pos.x) {
                    *inv = *inv + 1;

                    if (!dd_is_a_vnode(u) && !dd_is_a_vnode(v))
                        *rinv = *rinv + 1;
                }
            }
        }
    }
}

static double get_area(ddview_t *view) {
    ilrect_t	r = view->base.client->actual_bb;

    return  (r.ur.x - r.ll.x) * (r.ur.y - r.ll.y);
}

static double dist(ilcoord_t p, ilcoord_t q) {
    double a = (q.x - p.x);
    double b = (q.y - p.y);
    return sqrt(a*a + b*b);
}

static double get_delta(ddview_t *view, double *min_delta, double *t_delta, double* max_delta) {
    Agraph_t	*g;
    Agnode_t	*n;
    double		d = 0.0;
    ddnode_t	*spec;
    int		first = 1;

    g = view->layout;
    *t_delta = 0.0;

    for (n = agfstnode(g); n; n = agnxtnode(n)) {
        spec = dd_node(n);

        if (spec->cur.valid && spec->prev.valid)
            d = dist(spec->cur.pos,spec->prev.pos);

        *t_delta += d;

        if (first) {
            first = 0;
            *min_delta =  *max_delta = d;

        } else {
            if (*min_delta > d)
                *min_delta = d;

            if (*max_delta < d)
                *max_delta = d;
        }
    }

    return d;
}

static double get_edgelen(ddview_t *view) {
    Agnode_t	*v;
    Agedge_t	*e;
    ilshape_t	*pos;
    double		len = 0.0;
    int		i;

    for (v = agfstnode(view->base.model.main); v; v = agnxtnode(v)) {
        for (e = agfstout(v); e; e = agnxtout(e)) {
            pos = il_edge(e)->pos;

            for (i = 1; i < pos->def.curve.n; i++)
                len += dist(pos->def.curve.p[i],pos->def.curve.p[i-1]);
        }
    }

    return len;
}

static void cleanup() {
    if(Log)
        fflush(Log);
}

void dd_preprocess(ddview_t *view) {
    static int onetime = 1;
    char	*name,buf[128];

    if (onetime) {
        onetime = 0;

        if ((Log == NIL(FILE*)) && (name = getenv("DYNALOG"))) {
            Log = fopen(name,"w");

            if (Log == NIL(FILE*)) {
                sprintf(buf,"dyna.log.%d",(int)(getpid()));
                name = buf;
                Log = fopen(name,"w");
            }

            fprintf(Log,"#time nreq nmod nobj area tlen minmov tmov maxmov cross inv realinv\n");
            signal(SIGINT,cleanup);
        }
    }

    if (Log) {
        Clock = getClock();
        Nreq = count_mods(view);
    }
}

void dd_postprocess(ddview_t *view) {
    Agraph_t	*g;
    Agnode_t	*n;

    if (Log) {
        double	time, area, elen, min_delta, t_delta, max_delta;
        int	mods, ncross, ninv, rinv;

        time = (getClock() - Clock) / CLK_TCK;
        mods = count_mods(view);
        area = get_area(view);
        elen = get_edgelen(view);
        get_delta(view,&min_delta, &t_delta, &max_delta);
        ncross = dd_ncross(view);
        count_inv(view, &ninv, &rinv);

        /* #time nreq nmod nobj area tlen minmov tmov maxmov cross inv realinv */
        fprintf(Log,"%.3f %d %d %d %.2f %.2f %.2f %.2f %.2f %d %d %d\n",
                time, Nreq, mods,
                agnnodes(view->base.model.main)+agnedges(view->base.model.main),
                area, elen, min_delta, t_delta, max_delta, ncross, ninv, rinv);
    }

    g = view->layout;

    for (n = agfstnode(g); n; n = agnxtnode(n)) {
        dd_fix_order(n,TRUE);
        dd_fix_coord(n,TRUE);
        dd_set_old_coord(n);
        dd_set_oldrank(n);
    }
}

static int DD_work_cnt;

ilbool dd_work(ddview_t *view) {
    ilbool		rv;

    /* preprocess requests */
    dd_preprocess(view);

    /* compute new ranks */
    dd_rerank_nodes(view);

    /* install new nodes and edges; might reroute some edges too */
    dd_adjust_config(view);

    /* find new coordinates */

    dd_update_geometry
    (view);

    /* issue callbacks and clean up */
    rv = il_issue_callbacks(BASE(view));

    dd_postprocess(view);

    il_clear_callbacks(BASE(view));

    DD_work_cnt++;

    return rv;
}

#ifndef DYNADAG_H
#define DYNADAG_H

/* dynadag internal definitions */

#ifdef HAVE_CONFIG_H
#include "gvconfig.h"
#endif

#include <math.h>

#ifdef HAVE_VALUES_H
#include <values.h>
#else
#include <limits.h>
#ifndef MAXINT
#define MAXINT INT_MAX
#endif
#include <float.h>
#ifndef MAXDOUBLE
#define MAXDOUBLE DBL_MAX
#endif
#ifndef MAXFLOAT
#define MAXFLOAT FLT_MAX
#endif
#endif

#include <engine.h>
#include <stdio.h>

#ifdef assert
#undef assert
#endif
#define assert(x) do {if (!(x)) abort();} while (0)

#ifndef uchar
typedef unsigned char uchar;
#endif

#define EPSILON			(1.0e-36)
#define INITIAL_SIZE	8		/* for dynamic allocation */
#define MAX(x,y)		(((x)>=(y))?(x):(y))
#define MIN(x,y)		(((x)<=(y))?(x):(y))
#define MINCROSS_PASSES 12

#define BASE(obj)		(&((obj)->base))

#define ROUND(f)	((f>=0)?(int)(f + .5):(int)(f - .5))
#define D(f)	 	((double)f)

typedef struct intrange_s {
    int low,high;
}

intrange_t;

typedef struct rank_s {
    int			n;
    Agnode_t	**v;
    double		y_base;	/* absolute */
    double		delta_above, delta_below, space_below;
}

rank_t;

#define NILrank	NIL(rank_t*)
#define NILcurve NIL(ilcurve_t*)
#define NILshape NIL(ilshape_t*)

typedef struct config_s {
    int			low, high;
    rank_t		**r;
}

config_t;

typedef struct cg_s {	 /* constraint graph */
    Agraph_t	*g;
    Agnode_t	*anchor;		/* stability constraints */
    ilbool		inconsistent;	/* if re-solve needed */
}

cg_t;

typedef struct cn_s {
    Agnode_t	*n;				/* the variable */
    Agnode_t	*stab;			/* for stability constraints */
}

cn_t;

typedef struct twoedges_s {
    Agedge_t	*first, *second;
}

twoedges_t;

typedef struct ddview_s {
    engview_t	base;		/* must be first */
    Agraph_t	*layout;	/* contains real nodes, vnodes, vedges, etc. */
    Agraph_t	*dirty;		/* subgraph of above - nodes to the right of a change */
    config_t	*config;	/* indexes layout nodes by rank and order */
    cg_t		con[2];		/* constraint systems */
    int			xscale;		/* 1/Resolution coefficient for x coord ranks */
    int			prev_low_rank;
}

ddview_t ;

typedef struct median_s {
    double		val;		/* value */
    ilbool		exists;		/* if defined */
    ilbool		cached;		/* if definition is current */
}

median_t;

typedef struct {
    Agedge_t *e[2];
}

edgepair_t;

#define	UP		0	/* node median array */
#define DOWN	1
#define XCON	0	/* constraint systems */
#define YCON	1
#define	BACKEDGE_PENALTY	10
#define	STABILITY_FACTOR	100

typedef struct ddnode_s {
    Agrec_t		h;
    Agnode_t	*model;		/* if real (not virtual) */
    cn_t		con[2];		/* constraint variables XCON, YCON */
    median_t	med[2];		/* UP, DOWN */
    int			rank;
    int			order;
    int			newrank;	/* destination rank assignment */
    int			oldrank;	/* previous rank assignment */

    struct {
        ilcoord_t	pos;
        ilbool		valid;
    }

    cur, prev;

    double		actual_x;	/* spline intercept */
    ilbool		actual_x_valid;
    ilbool		in_config;
    ilbool		rank_fixed;
    ilbool		order_fixed;
    ilbool		coord_fixed;
    uchar 		dfsmark;	/* mark for depth-first search */
}

ddnode_t ;

typedef struct ddmdledge_s ddpath_t;

typedef struct ddedge_s {
    Agrec_t		h;
    Agnode_t	*cn;		/* constraint node */
    ddpath_t	*path;
}

ddedge_t;

typedef struct ddmdlnode_s {	/* extend model nodes */
    engnode_t	base;
    Agnode_t	*rep;			/* point to layout representative */
}

ddmdlnode_t;

typedef struct ddmdledge_s {
    engedge_t	base;
    Agedge_t	*first, *last;	/* points to virtual edge chain */
    ilcurve_t	*unclipped_path;
    Agedge_t	*model;
    Agnode_t	*weak;
    Agedge_t	*strong;
}

ddmdledge_t;

/* these apply to layout objects */
ddnode_t	*dd_node(Agnode_t *ln);
ddedge_t	*dd_edge(Agedge_t *le);
ddpath_t	*dd_path(Agedge_t *le);
ILnode_t	*dd_nspec(Agnode_t *ln);
ILedge_t	*dd_espec(Agedge_t *le);
ILedge_t	*dd_pathspec(ddpath_t *le);
Agnode_t	*dd_rep(Agnode_t *mn);		/* maps to model object */
ddpath_t	*dd_pathrep(Agedge_t *me);	/* "" */

/* these apply to model objects */
ILnode_t	*ddm_nspec(Agnode_t *n);
ILedge_t	*ddm_espec(Agedge_t *e);

/* layout graph operations */
Agnode_t	*dd_find_node(ddview_t *view, ILnode_t *spec);
Agnode_t	*dd_open_node(ddview_t *view, Agnode_t *model_n);
void		dd_close_node(ddview_t *view, Agnode_t *n);

ddpath_t	*dd_find_path(ddview_t *view, ILedge_t *spec);
ddpath_t	*dd_open_path(ddview_t *view, ILedge_t *spec);
void		dd_close_path(ddview_t *view, ddpath_t *path);

void		dd_open_config(ddview_t *view);
void		dd_close_config(ddview_t *view);

Agedge_t	*dd_open_edge(ddview_t *view, Agnode_t *t, Agnode_t *h, ddpath_t *path);
void		dd_close_edge(ddview_t *view, Agedge_t *e);

/* callbacks */
void		dd_register_node_callback(ddview_t *view, ILnode_t *spec, int kind);
void		dd_register_edge_callback(ddview_t *view, ILedge_t *spec, int kind);

/* rank representation */
rank_t		*dd_rankd(ddview_t *view, int r);
int			dd_rank(Agnode_t*);
int			dd_newrank(Agnode_t*);
int			dd_oldrank(Agnode_t*);
void		dd_set_newrank(Agnode_t *n, int r);
void		dd_set_oldrank(Agnode_t *n);
int			dd_map_coord_to_rank(ddview_t *view, double y);
int			dd_order(Agnode_t *n);
void		dd_fix_coord(Agnode_t *n, ilbool val);
ilbool		dd_coord_fixed(Agnode_t *);
void		dd_fix_order(Agnode_t *n, ilbool val);
ilbool		dd_order_fixed(Agnode_t *n);
void		dd_set_old_coord(Agnode_t *n);
ilcoord_t	dd_pos(Agnode_t *);
ilcoord_t	dd_nodesize(ddview_t *view, Agnode_t*);

Agnode_t	*dd_relnode(ddview_t *view, Agnode_t *n, int offset);
Agnode_t	*dd_left(ddview_t *view, Agnode_t *);
Agnode_t	*dd_right(ddview_t *view, Agnode_t *);
Agnode_t	*dd_leftmost(ddview_t *view, int r);
Agnode_t	*dd_rightmost(ddview_t *view, int r);
rank_t		*dd_extendrank(ddview_t *view, int r);
void		dd_install_at_right(ddview_t *view, Agnode_t *n, int r);
void		dd_install_at_pos(ddview_t *view, Agnode_t *n, int r, double x);
void        dd_fix_order(Agnode_t *n, ilbool val);
ilbool      dd_is_order_fixed(Agnode_t *n);
void		dd_exchange(ddview_t *view, Agnode_t *u, Agnode_t *v);
int			dd_uvcross(Agnode_t *left, Agnode_t *right, int use_in, int use_out);
int			dd_ncross(ddview_t *view);
int			dd_cross(ddview_t *view);
void		dd_rank_delete(ddview_t *view, Agnode_t *n);
void		dd_edge_blast(ddview_t *view, ddpath_t *path);
Agedge_t	*dd_first_elt(Agedge_t *e);
Agedge_t	*dd_last_elt(Agedge_t *e);
Agnode_t	*dd_pathtail(Agedge_t*);
Agnode_t	*dd_pathhead(Agedge_t*);
void		dd_set_first(ddpath_t *, Agedge_t *first);
void		dd_set_last(ddpath_t *, Agedge_t *last);
void		dd_set_firstlast(ddpath_t *, Agedge_t *first, Agedge_t *last);
int			dd_is_a_vnode(Agnode_t *n);
Agnode_t	*dd_vnode(ddview_t *view, int rank, double xpos);
Agedge_t	*dd_vedge(ddview_t *view, Agnode_t *u, Agnode_t *v, ddpath_t *e);

/* internal incremental layout primitives */
void		dd_stabilize(ddview_t *view, Agnode_t *ln, int csys, int newrank, int weight);
void		dd_unstabilize(ddview_t *view, Agnode_t *ln, int csys);
void		dd_make_neighborhood_movable(Agnode_t *n);
ddpath_t	*dd_route_edge(ddview_t *view, ILedge_t *spec);
ddpath_t	*dd_route_user_edge(ddview_t *view, ILedge_t *spec);
void		dd_adjust_edge(ddview_t *view, Agedge_t *e);
Agnode_t 	*dd_percolate(ddview_t *view, Agnode_t *n, int newrank, ilbool copy);
double		dd_place_and_reopt(ddview_t *view, Agnode_t *n, int r, double x);
void		dd_opt_path(ddview_t *view, ddpath_t *path);
void		dd_opt_elt(ddview_t *view, Agnode_t *elt, int dir, int eq_pass);
void		dd_make_edge_spline(ddview_t *view, ddpath_t *path);

ilbool		dd_work(ddview_t *view);
void		dd_rerank_nodes(ddview_t *view);
void		dd_adjust_config(ddview_t *view);

void		dd_update_geometry
(ddview_t *view);

/* network-simplex coord solver */
Agnode_t	*dd_getvar(ddview_t *view, Agnode_t *ln, int coordsys);

edgepair_t dd_getedgepair(Agraph_t *cg, Agnode_t *anchor, Agnode_t *tvar, Agnode_t *hvar);

void		dd_constrain_prevpos(ddview_t *view, Agnode_t *n, int sys);

void		dd_delete_constraint(ddview_t *view, Agnode_t *ln, int sys);

void		dd_update_X(ddview_t *view);

void		dd_update_Y(ddview_t *view);

/* internal consistency checks */
void		dd_check_all(ddview_t *view);

void		dd_check_rank(ddview_t *view, int r);

void		dd_check_edges(Agraph_t *g);

void		dd_check_containment(ddview_t *view, int r, Agnode_t *n, int must_be_in);

void		dd_check_first(ddview_t *view);

void		dd_check_vnode_path(ddview_t *view, Agedge_t **vpath);

void		dd_check_elts(ddview_t *view, Agnode_t *n);

void		dd_check_newranks(Agraph_t *g);

void		dd_check_vnodes(ddview_t *view);

/* attributes */
uchar 		dd_dfsmark(Agnode_t *n);

void		dd_set_dfsmark(Agnode_t *n, uchar mark);

double		dd_coord_between(ddview_t *view, Agnode_t *left, Agnode_t *right);

void        dd_set_ycoord(ddview_t *view, Agnode_t *n);

double		dd_ranksep(ddview_t *view);

ilcoord_t	dd_nodesep(ddview_t *view);

double		dd_uv_sep(ddview_t *view, Agnode_t *left, Agnode_t *right);

double		dd_minlen(Agedge_t *e), dd_efactor(Agedge_t *e);

ilbool		dd_constraint(Agedge_t *e);

double		dd_mval(Agnode_t *n, int dir);

void		dd_set_mval(Agnode_t *n, double d, int dir);

void		dd_auto_mval(Agnode_t *n, int dir);

ilbool		dd_mval_exists(Agnode_t *n, int dir);

void		dd_invalidate_mval(Agnode_t *n, int dir);

void		dd_invalidate_adj_mvals(Agnode_t *n);

void		dd_set_curve_from_path(ddview_t *view, Agedge_t *view_edge);

ilbool		dd_path_pos_changed(Agedge_t *e);

ilbool		dd_node_in_config(Agnode_t*);

void		dd_set_config_flag(Agnode_t*, ilbool flag);

void		dd_set_x(Agnode_t *n, double x);

void		dd_set_y(Agnode_t *n, double y);

/* external entry points */
ilbool		DDopen(ILview_t *view);

void		DDclose(ILview_t *view);

void		DDins(ILview_t *argview, ILobj_t *argspec);

void		DDmod(ILview_t *argview, ILobj_t *argspec);

void		DDdel(ILview_t *argview, ILobj_t *argspec);

ilbool		DDcallback(ILview_t *view);

Agraph_t	*DDmodel(ILview_t *view);

ILnode_t	*DDnodespec(ILview_t *view, Agnode_t *client_node);

ILedge_t	*DDedgespec(ILview_t *view, Agedge_t *client_edge);

extern ILengine_t DynaDag;

#endif

#include <dag.h>

#ifdef DMALLOC
#include "dmalloc.h"
#endif

static int median_cmpf(const void *p0, const void *p1) {
    double		d;
    d = ( (*(double *) p0) - (*(double *) p1));

    if (d < 0.0)
        return -1;

    if (d > 0.0)
        return 1;

    return 0;
}

static double median_of_list(double *list, int n) {
    int			lm,rm;
    double		m,w,lspan,rspan;


    switch (n) {

            case 0:
            m = 0.0;
            abort();
            break;

            case 1:
            m = list[0];
            break;

            case 2:
            m = (list[0] + list[1]) / 2.0;
            break;

            default:	/* weighted median */
            qsort(list,n,sizeof(list[0]),median_cmpf);

            if (n % 2)
                m = list[n / 2];
            else {
                rm = n / 2;
                lm = rm - 1;
                rspan = list[n - 1] - list[rm];
                lspan = list[lm] - list[0];

                if (lspan == rspan)
                    m = (list[lm] + list[rm]) / 2.0;
                else {
                    w = list[lm]*rspan + list[rm]*lspan;
                    m = w / (lspan + rspan);
                }
            }

            break;
    }

    return m;
}

void dd_auto_mval(Agnode_t *n, int dir) {
    Agedge_t	*e;
    int			d;
    double		med;

    static int	extent;
    static double *list;

    if (extent == 0) {
        extent = INITIAL_SIZE;
        list = malloc(sizeof(list[0])*extent);
    }

    d = 0;

    if (dir == UP)
        e = agfstin(n);
    else
        e = agfstout(n);

    while (e) {
        list[d++] = dd_order(e->node);

        if (d >= extent) {
            extent = extent * 2;
            list = realloc(list, sizeof(list[0]) * extent);
        }

        if (dir == UP)
            e = agnxtin(e);
        else
            e = agnxtout(e);
    }

    if (d == 0)
        dd_node(n)->med[dir].exists = FALSE;
    else {
        med = median_of_list(list,d);
        dd_node(n)->med[dir].exists = TRUE;
        dd_node(n)->med[dir].val = med;
    }

    dd_node(n)->med[dir].cached = TRUE;
}

void dd_invalidate_mval(Agnode_t *n, int dir) {
    dd_node(n)->med[dir].cached = FALSE;
}

ilbool dd_mval_exists(Agnode_t *n, int dir) {
    ddnode_t	*d;

    d = dd_node(n);

    if (d->med[dir].cached == FALSE)
        dd_auto_mval(n,dir);

    return d->med[dir].exists;
}

double dd_mval(Agnode_t *n, int dir) {
    ddnode_t	*d;

    d = dd_node(n);

    if (d->med[dir].cached == FALSE)
        dd_auto_mval(n,dir);

    return d->med[dir].val;
}

void dd_set_mval(Agnode_t *n, double val, int dir) {
    ddnode_t	*d;

    d = dd_node(n);
    d->med[dir].exists = TRUE;
    d->med[dir].cached = TRUE;
    d->med[dir].val = val;
}

/* this could be a lot better.  forces a lot of needless
 * recomputation.  could cache node pointers from which median
 * was computed, etc.  just live with this for now.
 */
void dd_invalidate_adj_mvals(Agnode_t *n) {
    Agedge_t	*e;

    for (e = agfstin(n); e; e = agnxtin(e))
        dd_node(e->node)->med[UP].cached = FALSE;

    for (e = agfstout(n); e; e = agnxtout(e))
        dd_node(e->node)->med[DOWN].cached = FALSE;
}

#include <dag.h>

#ifdef DMALLOC
#include "dmalloc.h"
#endif

#ifndef offsetof
#define offsetof(typ,fld)  ((int)(&(((typ*)0)->fld)))
#endif

cg_t dd_open_constraint(void) {
    cg_t	rv;

    rv.g = agopen("nsg",Agstrictdirected, NIL(Agdisc_t*));
    rv.anchor = agnode(rv.g,"_anchor",TRUE);
    rv.inconsistent = TRUE;	/* onetime initialization to set anchor */
    return rv;
}

/*
 * creates and initializes a new graph to contain the layout set,
 * and creates a pointer from the original graph back to this layout.
 */
ilbool DDopen(ILview_t *client) {
    static unsigned int D[] = {
                                  sizeof(ddview_t), sizeof(ddmdlnode_t), sizeof(ddmdledge_t)
                              };
    ddview_t		*rv;

    rv = (ddview_t*)il_open_view(&DynaDag, client, Agdirected, D);
    rv->layout = agopen("dd_layout",Agdirected,&IL_graph_id_disc);
    rv->dirty = agsubg(rv->layout,"updated nodes",TRUE);
    rv->con[XCON] = dd_open_constraint();
    rv->con[YCON] = dd_open_constraint();
    rv->xscale = ROUND(1.0 / client->resolution);
    rv->prev_low_rank = MAXINT;
    dd_open_config(rv);
    return TRUE;
}

void DDclose(ILview_t *argview) {
    Agraph_t	*model;
    Agnode_t	*n, *ln;
    Agedge_t	*e;
    ddpath_t	*path;
    ddview_t	*view;

    view = (ddview_t*)(argview->pvt);
    model = DDmodel(argview);

    for (n = agfstnode(model); n; n = agnxtnode(n)) {
        for (e = agfstedge(n); e; e = agnxtedge(e,n)) {
            path = (ddmdledge_t*)(AGDATA(e));
            dd_close_path(view,path);
        }

        if ((ln = dd_rep(n)))
            dd_close_node(view,ln);
    }

    agclose(view->con[XCON].g);
    agclose(view->con[YCON].g);
    dd_close_config(view);
    agclose(view->layout);
    il_close_view((engview_t*)view);
}

Agraph_t *DDmodel(ILview_t *view) {
    return ((engview_t*)(view->pvt))->model.main;
}

ILnode_t *DDnodespec(ILview_t *argview, Agnode_t *model_node) {
    return (ILnode_t*)(((engview_t*)(argview->pvt))->engine->mdlobj_to_spec(argview,(Agobj_t*)model_node));
}

ILedge_t *DDedgespec(ILview_t *argview, Agedge_t *model_edge) {
    return (ILedge_t*)(((engview_t*)(argview->pvt))->engine->mdlobj_to_spec(argview,(Agobj_t*)model_edge));
}

ilbool DDcallback(ILview_t *view) {
    return dd_work((ddview_t*)(view->pvt));
}

ILengine_t DynaDag = {
                         DDopen,
                         DDclose,
                         il_batch_ins,
                         il_batch_mod,
                         il_batch_del,
                         DDcallback,
                         ilmdlobj_to_spec,
                         ilspec_to_mdlobj
                     };

#include <stdio.h>
void ddwrite(Agraph_t *g) {
    agwrite(g,stderr);
}

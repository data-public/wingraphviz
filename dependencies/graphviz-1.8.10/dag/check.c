/* Various runtime sanity checks.  These should be disabled in
 * a production version of the code.
 */

#include <dag.h>

#ifdef DMALLOC
#include "dmalloc.h"
#endif

/* Test global consistency of the view. */
void dd_check_all(ddview_t *view) {
    int		r;

    for (r = view->config->low; r <= view->config->high; r++)
        dd_check_rank(view,r);

    dd_check_edges(view->layout);
}

/* Check consistency of the nodes within one rank. */
void dd_check_rank(ddview_t *view, int r) {
    Agnode_t	*ln,*rn,**list;
    int			i;
    rank_t		*rd;

    rd = dd_rankd(view,r);
    list = rd->v;
    i = 0;
    ln = NILnode;

    for (rn = dd_leftmost(view,r); rn; rn = dd_right(view,rn)) {
        assert(list[i] == rn);
        i++;
        assert(dd_node_in_config(rn));
        assert(dd_rank(rn) == r);
        dd_check_elts(view,rn);

        if (ln) {
            assert(dd_order(ln) + 1 == dd_order(rn));
            assert(dd_pos(ln).x + BASE(view)->client->separation.x <= dd_pos(rn).x);
        }

        ln = rn;
    }

    assert (i == rd->n);
}

/* Check membership of a node w.r.t. graph configuration. */
void dd_check_containment(ddview_t *view, int r, Agnode_t *n, int must_be_in) {
    Agnode_t	*rn;

    for (rn = dd_leftmost(view,r); rn; rn = dd_right(view,rn)) {
        if (must_be_in) {
            if (n == rn)
                break;

        } else
            assert (n != rn);
    }

    if (must_be_in)
        assert (n == rn);
}

/* all edges should be unit-length */
void dd_check_edges(Agraph_t *g) {
    Agnode_t	*u,*v;
    Agedge_t	*e;
    ddpath_t	*p;

    for (u = agfstnode(g); u; u = agnxtnode(u)) {
        if (NOT(dd_node_in_config(u)))
            continue;

        for (e = agfstout(u); e; e = agnxtout(e)) {
            p = dd_path(e);
            assert(aghead(p->last));
            v = aghead(e);

            if (NOT(dd_node_in_config(v)))
                continue;

            if (dd_first_elt(e) == NILedge)
                continue;

            assert(dd_rank(u) + 1 == dd_rank(v));
        }
    }
}

ilbool dd_check_pathnode(ddview_t *view, Agnode_t *n) {
    rank_t		*rd;
    int			i,r;

    i = dd_order(n);
    r = dd_rank(n);
    rd = dd_rankd(view,r);
    assert(rd->v[i] == n);
    return FALSE;
}

void dd_check_vnode_path(ddview_t *view, Agedge_t **vpath) {
    int			i;
    Agedge_t	*e,*f;

    f = NILedge;

    for (i = 0; (e = vpath[i]); i++) {
        dd_check_pathnode(view,agtail(e));

        if (i > 0)
            assert(dd_is_a_vnode(agtail(e)));

        f = e;
    }

    dd_check_pathnode(view,aghead(f));
}

void dd_check_elts(ddview_t *view, Agnode_t *n) {
    Agedge_t	*e,*f,*fst,*lst;

    if (dd_is_a_vnode(n))
        return;

    for (e = agfstout(n); e; e = agnxtout(e)) {
        fst = dd_first_elt(e);
        lst = dd_last_elt(e);

        for (f = fst; f; f = agfstout(aghead(f))) {
            dd_check_pathnode(view,aghead(f));

            if (f == lst)
                break;
        }
    }
}

void dd_check_newranks(Agraph_t *g) {
    Agnode_t	*n;
    Agedge_t	*e;

    for (n = agfstnode(g); n; n = agnxtnode(n)) {
        if (dd_is_a_vnode(n))
            continue;

        for (e = agfstout(n); e; e = agnxtout(e)) {
            if (NOT(dd_constraint(e)))
                continue;

            assert (dd_newrank(dd_pathhead(e)) - dd_newrank(dd_pathtail(e)) >= 1);
        }
    }
}

static void check_mg(Agraph_t *g, Agraph_t *root) {
    Agnode_t	*mn;
    Agedge_t	*me;

    for (mn = agfstnode(g); mn; mn = agnxtnode(mn)) {
        assert(mn->base.data);
        assert(agsubnode(root,mn,FALSE));

        for (me = agfstout(mn); me; me = agnxtout(me)) {
            assert(me->base.data);
            assert(agsubedge(root,me,FALSE));
        }
    }
}

void dd_check_model(ddview_t *view) {
    Agraph_t	*root;

    root = BASE(view)->model.main;
    check_mg(root,root);
    check_mg(BASE(view)->model.v[IL_INS],root);
    check_mg(BASE(view)->model.e[IL_INS],root);
    check_mg(BASE(view)->model.v[IL_MOD],root);
    check_mg(BASE(view)->model.e[IL_MOD],root);
    check_mg(BASE(view)->model.v[IL_DEL],root);
    check_mg(BASE(view)->model.e[IL_DEL],root);
}

void dd_check_really_gone(Agraph_t *g, Agnode_t *n, unsigned long id) {
    Agnode_t	*u;
    Agedge_t	*e;

    assert (agidnode(g,id,FALSE) == NILnode);

    for (u = agfstnode(g); u; u = agnxtnode(u)) {
        assert(u != n);

        for (e = agfstedge(u); e; e = agnxtedge(e,u))
            assert(e->node != n);
    }
}

void dd_check_vnodes(ddview_t *view) {
    Agnode_t	*n;
    Agedge_t	*e;

    for (n = agfstnode(view->layout); n; n = agnxtnode(n)) {
        if (NOT(dd_is_a_vnode(n)))
            continue;

        e = agfstin(n);

        if (e == NILedge)
            abort();

        e = agfstout(n);

        if (e == NILedge)
            abort();
    }
}

static int CLcnt = 0;
void dd_check_links(ddview_t *view) {
    Agraph_t	*model, *layout;
    Agnode_t	*mn, *ln;
    Agedge_t	*me, *le, *mme;
    ddpath_t	*path;

    dd_check_model(view);
    model = BASE(view)->model.main;
    layout = view->layout;

    /* check model graph links to layout graph */

    for (mn = agfstnode(model); mn; mn = agnxtnode(mn)) {
        ln = dd_rep(mn);

        if (ln == NILnode)
            continue;

        assert(dd_node(ln)->model == mn);

        for (me = agfstedge(mn); me; me = agnxtedge(me,mn)) {
            path = dd_pathrep(me);
            mme = path->model;

            if (mme == NILedge)
                continue;

            assert((mme == me) || (mme == AGOPP(me)));
        }
    }

    /* check layout object links to model objects */
    for (ln = agfstnode(layout); ln; ln = agnxtnode(ln)) {
        if (dd_is_a_vnode(ln) == FALSE) {
            mn = dd_node(ln)->model;
            assert(mn);
            assert(agsubnode(model,mn,FALSE) == mn);
            assert(ln == dd_rep(mn));

            for (le = agfstedge(ln); le; le = agnxtedge(le,ln)) {
                path = dd_edge(le)->path;
                me = path->model;
                assert(agsubedge(model,me,FALSE) == me);
            }

        } else {
            assert(agfstin(ln) != NILedge);
            assert(agfstout(ln) != NILedge);
        }
    }

    CLcnt++;
}

/*
 *	compute node and edge coordinates 
 */

#include <dag.h>
#include <ns.h>

#ifdef DMALLOC
#include "dmalloc.h"
#endif

static void fix_separation(ddview_t *view, Agnode_t *ln) {
    Agnode_t	*left, *right;	/* ln's left and right neighbors */
    Agnode_t	*var, *left_var, *right_var; /* constraint variables */
    Agedge_t	*ce;			/* constraint edge */
    double		sep, width, left_width, right_width;
    int			scaled_len;

    sep = dd_nodesep(view).x;
    width = dd_nodesize(view,ln).x / 2.0;
    var = dd_getvar(view, ln, XCON);

    if ((left = dd_left(view,ln))) {
        left_var = dd_getvar(view, left, XCON);
        left_width = dd_nodesize(view,left).x / 2.0;

    } else {
        left_var =  view->con[XCON].anchor;
        left_width = -ns_getrank(left_var);
    }

    ce = agedge(left_var,var,NILstr,TRUE);
    scaled_len = ROUND(view->xscale * (sep + left_width + width));
    ns_setminlength(ce,scaled_len);
    ns_setweight(ce,0);

    if (left && (ns_getrank(var) - ns_getrank(left_var) < scaled_len))
        view->con[XCON].inconsistent = TRUE;

    if ((right = dd_right(view,ln))) {
        right_var = dd_getvar(view, right, XCON);
        right_width = dd_nodesize(view,right).x / 2.0;
        ce = agedge(var,right_var,NILstr,TRUE);
        scaled_len = ROUND(view->xscale * (sep + right_width + width));
        ns_setminlength(ce,scaled_len);
        ns_setweight(ce,0);

        if (ns_getrank(right_var) - ns_getrank(var) < scaled_len)
            view->con[XCON].inconsistent = TRUE;
    }
}

static void do_nodesep(ddview_t *view, Agraph_t *subg) {
    Agnode_t	*n;

    for (n = agfstnode(subg); n; n = agnxtnode(n))
        fix_separation(view, dd_rep(n));
}

static void do_edgesep(ddview_t *view, Agraph_t *subg) {
    Agnode_t	*n;
    Agedge_t	*e;
    Agedge_t	*le;
    ddpath_t	*path;

    for (n = agfstnode(subg); n; n = agnxtnode(n)) {
        for (e = agfstout(n); e; e = agnxtout(e))  {
            if ((path = dd_pathrep(e)) && ((le = path->first))) {
                while (le != path->last) {
                    fix_separation(view,le->node);
                    le = agfstout(le->node);
                }

            } else {	/* flat or self edge */

                if (e->node != n) { 	/* flat */
                    Agnode_t	*u, *v;
                    Agnode_t	*uvar, *vvar;
                    int			ux, vx;
                    Agedge_t	*ce;
                    double		sep;

                    u = dd_rep(agtail(e));
                    v = dd_rep(aghead(e));
                    ux = dd_order(u);
                    vx = dd_order(v);

                    if (abs(ux - vx) == 1) {
                        if (ux > vx) {
                            Agnode_t *t;
                            t = u;
                            u = v;
                            v = t;
                        }

                        uvar = dd_node(u)->con[XCON].n;
                        vvar = dd_node(v)->con[XCON].n;
                        ce = agedge(uvar,vvar,NILstr,FALSE);
                        sep = (dd_nodesize(view,u).x + dd_nodesize(view,v).x) / 2.0 + 3.0 * dd_nodesep(view).x;
                        ns_setminlength(ce,ROUND(view->xscale * sep));
                    }

                } else {} /* self */

            }

        }

    }

}

static void restore_nodesep(ddview_t *view) {
    Agnode_t	*ln;

    do_nodesep(view, BASE(view)->model.v[IL_INS]);
    do_nodesep(view, BASE(view)->model.v[IL_MOD]);
    do_edgesep(view, BASE(view)->model.e[IL_INS]);
    do_edgesep(view, BASE(view)->model.e[IL_MOD]);

    for (ln = agfstnode(view->dirty); ln; ln = agnxtnode(ln))
        fix_separation(view,ln);

    while ((ln = agfstnode(view->dirty)))
        agdelete(view->dirty,ln);
}

static void fix_edgecost(ddview_t *view, Agedge_t *le) {
    ddedge_t	*ed;
    Agraph_t	*cg;
    edgepair_t	ep;

    ed = dd_edge(le);
    cg = view->con[XCON].g;

    if (ed->cn == NILnode) {
        ed->cn = agnode(cg,NILstr,TRUE);
        ep = dd_getedgepair(cg,ed->cn,dd_getvar(view,agtail(le),XCON),dd_getvar(view,aghead(le),XCON));
        ns_setweight(ep.e[0],1000);
        ns_setweight(ep.e[1],1000);
    }
}

static void do_edgecost(ddview_t *view, Agraph_t *subg) {
    Agnode_t	*n;
    Agedge_t	*e, *le;
    ddpath_t	*path;

    for (n = agfstnode(subg); n; n = agnxtnode(n)) {
        for (e = agfstout(n); e; e = agnxtout(e)) {
            if ((path = dd_pathrep(e)) && ((le = path->first))) {
                while (1) {
                    fix_edgecost(view,le);

                    if (le == path->last)
                        break;

                    le = agfstout(le->node);
                }
            }
        }
    }
}

static void restore_edgecost(ddview_t *view) {
    do_edgecost(view, BASE(view)->model.e[IL_INS]);
    do_edgecost(view, BASE(view)->model.e[IL_MOD]);
}

static void readout_coords(ddview_t *view) {
    Agnode_t	*ln;
    Agnode_t	*cn;
    double		oldpos,newpos;
    ILnode_t	*spec;
    ddnode_t	*dn;
    int			anchor_rank;

    anchor_rank = ns_getrank(view->con[XCON].anchor);

    for (ln = agfstnode(view->layout); ln; ln = agnxtnode(ln)) {
        dn = dd_node(ln);

        if ((cn = dn->con[XCON].n)) {
            oldpos = dn->prev.pos.x;
            newpos = (ns_getrank(cn) - anchor_rank) / view->xscale;
            dd_set_x(ln,newpos);

            if (dd_is_a_vnode(ln))
                continue;

            spec = dd_nspec(ln);

            if (newpos != oldpos) {
                il_register_node_callback(BASE(view),spec,IL_MOD);
            }
        }
    }
}

static void stabilize_nodes(ddview_t *view) {
    Agnode_t	*mn;
    Agnode_t	*ln;

    Agraph_t	*modnodes[2], *modedges[2];
    int			i;

    modnodes[0] = BASE(view)->model.v[IL_INS];
    modnodes[1] = BASE(view)->model.v[IL_MOD];
    modedges[0] = BASE(view)->model.e[IL_INS];
    modedges[1] = BASE(view)->model.e[IL_MOD];
    /* what about  e[IL_DEL]? make sure endpoint isn't deleted too. */

    for (i = 0; i < 2; i++) {
        for (mn = agfstnode(modnodes[i]); mn; mn = agnxtnode(mn)) {
            ln = dd_rep(mn);

            if (dd_coord_fixed(ln))
                dd_stabilize(view, ln, XCON, ROUND(view->xscale * il_pos(mn).x),
                             STABILITY_FACTOR);
        }
    }

    for (i = 0; i < 2; i++) {
        for (mn = agfstnode(modedges[i]); mn; mn = agnxtnode(mn)) {
            ln = dd_rep(mn);

            if (NOT(dd_coord_fixed(ln)))
                dd_unstabilize(view, ln, XCON);
        }
    }
}

void dd_update_X(ddview_t *view) {
    restore_nodesep(view);
    restore_edgecost(view);
    stabilize_nodes(view);

    if (view->con[XCON].inconsistent || 1) {
        ns_solve(view->con[XCON].g,NS_DEBUG|NS_VALIDATE|NS_NORMALIZE);
        readout_coords(view);
        view->con[XCON].inconsistent = FALSE;
    }
}

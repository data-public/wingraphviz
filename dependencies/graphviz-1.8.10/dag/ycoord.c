/*
 *	y coordinates
 */

#include <dag.h>

#ifdef DMALLOC
#include "dmalloc.h"
#endif

int dd_map_coord_to_rank(ddview_t *view, double y) {
    int			r, minr, maxr, rv;
    double		baseline, bestrank, bestdist, d, rankht, ranksep;
    rank_t		*rd;


    maxr = view->config->high;
    minr = view->config->low;
    ranksep = dd_ranksep(view);

    if (minr > maxr) {		/* no config exists yet */
        bestrank = y / ranksep;
        rv = ROUND(bestrank);
        rd = dd_extendrank(view,rv);	/* make this the first rank */
        rd->y_base = y;
        view->prev_low_rank = rv;
        return rv;
    }

    rd = dd_rankd(view,minr);

    if (y < rd->y_base - rd->delta_above - ranksep) {
        /* above min rank */
        rankht = ranksep + rd->delta_above + rd->delta_below;
        bestrank = minr - (rd->y_base - y) / rankht;
        rv = ROUND(bestrank);
        return rv;
    }

    rd = dd_rankd(view,maxr);

    if (y > rd->y_base + rd->delta_below + ranksep) {
        /* below max rank */
        rankht = ranksep + rd->delta_above + rd->delta_below;
        bestrank = maxr + (y - rd->y_base) / rankht;
        rv = ROUND(bestrank);
        return rv;
    }

    /* somewhere in between */
    bestrank = minr;

    bestdist = MAXDOUBLE;

    for (r = minr; r <= maxr; r++) {
        baseline = dd_rankd(view,r)->y_base;

        if ((d = fabs(y - baseline)) < bestdist) {
            bestdist = d;
            bestrank = r;
        }
    }

    rv = ROUND(bestrank);
    return rv;
}

static void reset_rank_box(ddview_t *view, int r) {
    int			i;
    rank_t		*rd;
    Agnode_t	*n,**list;
    double		nht, max_node_ht;

    max_node_ht = BASE(view)->client->separation.y / 10.0;

    rd  = dd_rankd(view,r);
    list = rd->v;

    for (i = 0; i < rd->n; i++) {
        n = list[i];

        if (dd_is_a_vnode(n))
            continue;

        nht = dd_nodesize(view,n).y;

        if (max_node_ht < nht)
            max_node_ht = nht;
    }

    rd->delta_above = rd->delta_below = max_node_ht/2.0;
    rd->space_below = dd_ranksep(view);
}

static void reset_baselines(ddview_t *view) {
    int		r,base;
    rank_t	*rd,*rd_prev,*rd_base;

    base = view->prev_low_rank;

    if (base == MAXINT)
        base = view->config->low;

    rd_base = dd_rankd(view,base);

    /* work upward from base rank */
    rd_prev = rd_base;

    for (r = base - 1; r >= view->config->low; r--) {
        rd = dd_rankd(view,r);
        rd->y_base = rd_prev->y_base - rd_prev->delta_above
                     - rd->space_below - rd->delta_above;
        rd_prev = rd;
    }

    rd_prev = rd_base;

    for (r = base + 1; r <= view->config->high; r++) {
        rd = dd_rankd(view,r);
        rd->y_base = rd_prev->y_base + rd_prev->delta_below
                     + rd_prev->space_below + rd->delta_above;
        rd_prev = rd;
    }

    view->prev_low_rank = view->config->low;
}

void dd_update_Y(ddview_t *view) {
    int			i,r;
    rank_t		*rd;
    double		baseline,old_pos;
    Agnode_t	*n,**list;

    for (r = view->config->low; r <= view->config->high; r++)
        reset_rank_box(view, r);

    reset_baselines(view);

    for (r = view->config->low; r <= view->config->high; r++) {
        rd = dd_rankd(view,r);

        if (rd->n <= 0)
            continue;

        baseline = rd->y_base;

        list = rd->v;

        for (i = 0; i < rd->n; i++) {
            n = list[i];
            old_pos = dd_pos(n).y;
            dd_set_y(n,baseline);

            if ((old_pos != baseline) && NOT(dd_is_a_vnode(n))) {
                il_register_node_callback(BASE(view),dd_nspec(n),IL_MOD);
            }
        }
    }
}

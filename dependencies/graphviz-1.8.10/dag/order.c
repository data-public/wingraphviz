#include <dag.h>

#ifdef DMALLOC
#include "dmalloc.h"
#endif

static int nodemovecmp(const void *p0, const void *p1) {
    Agnode_t	*n0, *n1;

    n0 = *(Agnode_t**)p0;
    n1 = *(Agnode_t**)p1;
    return (dd_oldrank(n0) - dd_oldrank(n1));
}

static Agnode_t **choose_nodemove_order(ddview_t *view) {
    Agnode_t	**rv, *mn;
    int			nn, i;

    nn = agnnodes(BASE(view)->model.v[IL_MOD]);
    rv = agalloc(view->layout, sizeof(Agnode_t *) * (nn + 1));
    i = 0;

    for (mn = agfstnode(BASE(view)->model.v[IL_MOD]); mn; mn = agnxtnode(mn))
        rv[i++] = dd_rep(mn);

    qsort(rv, nn, sizeof(Agnode_t*), nodemovecmp);

#ifdef DEBUGPRINT

    fprintf(stderr,"move order:\n");

    for (i = 0; i < nn; i++)
        fprintf(stderr,"\tL%d,%d\n",dd_oldrank(rv[i]),dd_order(rv[i]));

    fprintf(stderr,"\n");

#endif

    return rv;
}

static void invalidate_path_constraints(ddview_t *view, ddpath_t *path) {
    Agedge_t	*e;
    Agnode_t	*cn;

    for (e = path->first; e; e = agfstout(e->node)) {

        cn = dd_edge(e)->cn;

        if (cn) {
            agdelete(view->con[XCON].g, cn);
            dd_edge(e)->cn = NILnode;
        }

        if (e == path->last)
            break;
    }
}

/* work function for updating configuration */

static void execute_deletions(ddview_t *view) {
    Agraph_t	*root;
    Agnode_t	*mn;
    Agedge_t	*me;
    Agnode_t	*ln;
    ddpath_t	*path;

    root = BASE(view)->model.main;

    /* close down edges being deleted */

    for (mn = agfstnode(BASE(view)->model.e[IL_DEL]); mn; mn = agnxtnode(mn)) {
        for (me = agfstout(mn); me; me = agnxtout(me)) {
            if ((path = dd_pathrep(me)))
                dd_close_path(view, path);
        }
    }

    /* close down nodes being deleted */
    for (mn = agfstnode(BASE(view)->model.v[IL_DEL]); mn; mn = agnxtnode(mn)) {
        if ((ln = dd_rep(mn))) {
            if (agfstedge(ln))
                abort();

            dd_close_node(view,ln);
        }
    }
}

static double x_avg_of_neighbors(ddview_t *view, Agnode_t *mn) {
    int			cnt;
    double		sum;
    Agedge_t	*e;
    ILnode_t	*spec;

    mn = agsubnode(BASE(view)->model.main,mn,FALSE);
    sum = 0.0;
    cnt = 0;

    for (e = agfstedge(mn); e; e = agnxtedge(e,mn)) {
        spec = ddm_nspec(e->node);

        if (spec->pos_valid) {
            sum = sum + spec->pos.x;
            cnt++;
        }
    }

    if (cnt > 0)
        return sum / cnt;
    else
        return 0.0;
}

static void insert_node(ddview_t *view, Agnode_t *mn) {
    Agnode_t	*ln;
    ILnode_t	*nspec;
    double		xpos;

    ln = dd_rep(mn);
    nspec = ddm_nspec(mn);

    if (nspec->pos_valid) {
        xpos = nspec->pos.x;
        dd_install_at_pos(view,ln,dd_newrank(ln),xpos);

    } else {
        if (agfstedge(agsubnode(BASE(view)->model.main,mn,FALSE))) {
            xpos = x_avg_of_neighbors(view, mn);
            dd_install_at_pos(view,ln,dd_newrank(ln),xpos);

        } else
            dd_install_at_right(view,ln,dd_newrank(ln));
    }

    if (nspec->pos_valid) {
        dd_fix_coord(ln,TRUE);
        dd_fix_order(ln,TRUE);
    }
}

static void insert_new_nodes(ddview_t *view) {
    Agnode_t	*mn;

    for (mn = agfstnode(BASE(view)->model.v[IL_INS]); mn; mn = agnxtnode(mn))
        insert_node(view, mn);
}

/* --- */

static ilcoord_t interpolate(ilcoord_t p0, ilcoord_t p1, double t) {
    ilcoord_t	rv;

    rv.x = p0.x + (p1.x - p0.x) * t;
    rv.y = p0.y + (p1.y - p0.y) * t;
    return rv;
}

/* returns model nodes of edge spec, ordered with tail=low rank */
static void get_layout_endpoints(ddview_t *view, ILedge_t *spec, Agnode_t **p_tl, Agnode_t **p_hd) {
    Agnode_t	*t, *h;
    t = dd_find_node(view,(ILnode_t*)(spec->tail.term));
    h = dd_find_node(view,(ILnode_t*)(spec->head.term));

    if (dd_rank(h) < dd_rank(t)) {
        Agnode_t *tmp;
        tmp = t;
        t = h;
        h = tmp;
    }

    *p_tl = t;
    *p_hd = h;
}

/* get path descriptor (new or pre-existing) */
static ddpath_t *get_clean_path(ddview_t *view, ILedge_t *spec) {
    ddpath_t	*path;

    path = dd_find_path(view,spec);

    if (path)
        dd_close_path(view,path);			/* clean old route */
    else
        path = dd_open_path(view,spec);

    return path;
}

static ddpath_t *build_unit_edge(ddview_t *view, ILedge_t *spec, Agnode_t *t, Agnode_t *h) {
    Agedge_t	*e;
    ddpath_t	*path;

    path = get_clean_path(view,spec);
    e = dd_open_edge(view,t,h,path);
    dd_set_firstlast(path,e,e);
    return path;
}

static ddpath_t *build_path(ddview_t *view, ILedge_t *spec, Agnode_t *t, Agnode_t *h, double *xval) {
    int 			r,r0,r1;
    Agnode_t		*vn, *prev;
    Agedge_t		*first,*last,*ve;
    ddpath_t		*path;

    path = get_clean_path(view,spec);
    r0 = dd_rank(t);
    r1 = dd_rank(h);
    vn = prev = NILnode;
    last = NILedge;

    if (r1 - r0 > 1) {
        prev = h;

        for (r = r1 - 1; r > r0; r--) {
            vn = dd_vnode(view,r,xval[r - r0 - 1]);
            ve = dd_open_edge(view,vn,prev,path);

            if (r == r1 - 1)
                last = ve;

            prev = vn;
        }

        first = ve = dd_open_edge(view,t,vn,path);

    } else
        first = last = dd_open_edge(view,t,h,path);

    dd_set_firstlast(path,first,last);

    return path;
}

static void user_route_edge(ddview_t *view, ILedge_t *espec) {
    Agnode_t	*t, *h;
    double		*route,y;
    int			i,r,tr,pathlen;
    ilcurve_t	*pos;
    ilcoord_t	tp;

    get_layout_endpoints(view,espec,&t,&h);
    pos = &(espec->pos->def.curve);
    tr = dd_rank(t);
    pathlen = dd_rank(h) - tr;

    if (pathlen == 0)
        get_clean_path(view, espec);		/* flat/self */
    else if (pathlen == 1)
        (void) build_unit_edge(view,espec,t,h);
    else if (pathlen > 1) {
        route = agalloc(view->layout, (pathlen - 1) * sizeof(route[0]));
        i = 0;

        for (r = tr + 1; r < tr + pathlen; r++) {
            y = dd_rankd(view,r)->y_base;

            if (il_test_y_intersection(pos,y,&tp)) {
                route[i++] = tp.x;

            } else {
                i++;
                route[i] = interpolate(dd_pos(t),dd_pos(h),i/pathlen).x;
            }
        }

        (void) build_path(view,espec,t,h,route);
        agfree(view->layout, route);
    }
}

static void auto_route_edge(ddview_t *view, ILedge_t *spec) {
    Agnode_t	*t, *h;
    ddpath_t	*path;
    int			pathlen, i;
    double		*xval,initial,terminal;

    get_layout_endpoints(view,spec,&t,&h);
    pathlen = dd_rank(h) - dd_rank(t);

    if (pathlen == 1)
        (void) build_unit_edge(view,spec,t,h);
    else if (pathlen > 1) {
        xval = agalloc(view->layout,(pathlen + 1) * sizeof(xval[0]));
        initial = xval[0] = dd_pos(t).x;
        terminal = xval[pathlen] = dd_pos(h).x;

        for (i = 1; i <  pathlen; i++)
            xval[i] = initial + (terminal - initial) * (double)i / pathlen;

        path = build_path(view,spec,t,h,&xval[1]);	/* note index base */

        dd_opt_path(view, path);

        agfree(view->layout,xval);
    }

    /* else self-edge or flat-edge */
}

static void adjust_head(ddview_t *view, ddpath_t *path, int amt) {
    int			i, r;
    Agnode_t	*endpoint, *v, *nv;
    Agedge_t	*e;

    if (amt == 0)
        return;

    endpoint = aghead(path->last);

    v = agtail(path->last);

    dd_close_edge(view,path->last);

    if (path->first == path->last)
        dd_set_firstlast(path,NILedge,NILedge);
    else
        dd_set_last(path,NILedge);

    if (amt > 0)
        for (i = 0; i < amt; i++) {	/* stretch */
            r = dd_rank(v);
            nv = dd_percolate(view, v, r + 1, TRUE);
            e = dd_open_edge(view, v, nv, path);

            if (path->first == NILedge)
                dd_set_first(path,e);

            v = nv;
        }

    else
        for (i = 0; i > amt; i--) {			/* shrink */
            nv = v->in->node;

            for (e = agfstedge(v); e; e = agnxtedge(e,v))
                dd_close_edge(view,e);

            dd_close_node(view,v);

            v = nv;
        }

    dd_set_last(path, dd_open_edge(view, v, endpoint, path));
}

static void adjust_tail(ddview_t *view, ddpath_t *path, int amt) {
    int			i, r;
    Agnode_t	*endpoint, *v, *nv;
    Agedge_t	*e;

    if (amt == 0)
        return;

    endpoint = agtail(path->first);

    v = aghead(path->first);

    dd_close_edge(view,path->first);

    if (path->first == path->last)
        dd_set_firstlast(path,NILedge,NILedge);
    else
        dd_set_first(path,NILedge);

    if (amt > 0)
        for (i = 0; i < amt; i++) {	/* stretch */
            r = dd_rank(v);
            nv = dd_percolate(view, v, r - 1, TRUE);
            e = dd_open_edge(view, nv, v, path);

            if (path->last == NILedge)
                dd_set_last(path,e);

            v = nv;
        }

    else
        for (i = 0; i > amt; i--) {			/* shrink */
            nv = v->out->node;

            for (e = agfstedge(v); e; e = agnxtedge(e,v))
                dd_close_edge(view,e);

            dd_close_node(view,v);

            v = nv;
        }

    dd_set_first(path, dd_open_edge(view, endpoint, v, path));
}

static int myabs(int i) {
    if (i < 0)
        i = -i;

    return i;
}

/* Adjust a virtual node chain, by shrinking, moving, and/or stretching
 * the path.  The dd_newrank() values determine the new endpoints.
 * Shrinking means cutting off vnodes on the tail side.  Stretching
 * means iteratively copying the last virtual node down to the next rank.
 * The rationale is that the tail moves downward toward the head.
 */
static void auto_adjust_edge(ddview_t *view, ILedge_t *spec) {
    Agnode_t		*t, *h, *v;
    int				i;
    int				otr, ohr, ntr, nhr;
    int				olen, nlen;
    int				t_amt, h_amt;
    ddpath_t		*path;

    path = dd_find_path(view,spec);
    /*dd_close_path(view,path);
    auto_route_edge(view,spec);
    return; */

    get_layout_endpoints(view,spec,&t,&h);
    otr = dd_oldrank(t);
    ntr = dd_newrank(t);
    ohr = dd_oldrank(h);
    nhr = dd_newrank(h);

    if ((otr == ntr) && (ohr == nhr))
        return;

    if (nhr == ntr)
        dd_close_path(view, path);	/* flat edge */
    else if (nhr == ntr + 1)
        build_unit_edge(view, spec, t, h);
    else if (otr >= ohr)
        auto_route_edge(view, spec);
    else {
        /* shrink, same, stretch refer to count of vnodes */
        t_amt = h_amt = 0;
        olen = MAX(0,ohr - otr - 1);
        nlen = MAX(0,nhr - ntr - 1);

        if (myabs(nhr - ohr) > myabs(ntr - otr))
            h_amt = nlen - olen;
        else
            t_amt = nlen - olen;

        /* adjust to new path length */
        adjust_tail(view, path, t_amt);

        adjust_head(view, path, h_amt);

        /* invalidate edge cost constraints so they don't dangle */
        invalidate_path_constraints(view, path);

        /* move entire path */
        v = path->first->node;

        for (i = 0; i < nlen; i++) {
            dd_percolate(view, v, ntr + i + 1, FALSE);
            v = v->out->node;
        }

        /* do not call dd_opt_path(view, path) here!  other
        edges are not yet adjusted, so tangles can result. */
    }
}

#if 0 /* not used */
static void unbind_endpoints(ddview_t *view, ILedge_t *espec) {
    Agnode_t	*t, *h;

    get_layout_endpoints(view,espec,&t,&h);
    dd_fix_coord(t, FALSE);
    dd_fix_coord(h, FALSE);
}

#endif

static void insert_edge(ddview_t *view, Agedge_t *me) {
    ILedge_t	*espec;

    espec = ddm_espec(me);

    if (espec->pos && (espec->pos->def.curve.n > 0))
        user_route_edge(view, espec);
    else
        auto_route_edge(view, espec);

    /*unbind_endpoints(view,espec); */ 	/* i don't know if this is good or bad */
}

static void unfix_old_singletons(ddview_t *view) {
    Agraph_t	*maing, *sub;
    Agnode_t	*n, *mn, *ln;
    Agedge_t	*me;

    /* this heuristic unfixes any node on first edge insertion */
    maing = BASE(view)->model.main;
    sub = BASE(view)->model.e[IL_INS];

    for (n = agfstnode(sub); n; n = agnxtnode(n)) {
        /* only unstick nodes if they changed ranks */
        mn = agsubnode(maing,n,FALSE);
        ln = dd_rep(mn);

        if (dd_node(ln)->prev.valid == FALSE)
            continue;

        if (dd_rank(ln) == dd_oldrank(ln))
            continue;

        for (me = agfstedge(mn); me; me = agnxtedge(me,mn)) {
            if (agsubedge(sub,me,FALSE) == NILedge)
                break;
        }

        if (!me)
            il_node(mn)->pos_valid = FALSE;
    }
}

static void insert_new_edges(ddview_t *view) {
    Agnode_t	*mn;
    Agedge_t	*me;

    for (mn = agfstnode(BASE(view)->model.e[IL_INS]); mn; mn = agnxtnode(mn))
        for (me = agfstout(mn); me; me = agnxtout(me))
            insert_edge(view, me);

    unfix_old_singletons(view);
}

/* --- */

ilbool user_defined_move(ILedge_t *espec) {
    return (espec->update & IL_UPD_MOVE);
}

/* push a node through adjacent ranks.  */

Agnode_t *dd_percolate(ddview_t *view, Agnode_t *n, int destrank, ilbool copy) {
    int			oldrank, r, dir;
    double		x;
    Agnode_t	*rv;

    oldrank = dd_rank(n);
    dir = (oldrank < destrank)? 1 : -1;
    x = dd_pos(n).x;

    if (copy)
        rv = dd_open_node(view,NILnode);
    else
        rv = n;

    for (r = oldrank + dir; r != destrank + dir; r += dir)
        x = dd_place_and_reopt(view,rv,r,x);

    return rv;
}

static void move_old_nodes(ddview_t *view) {
    Agnode_t	*ln;
    int			newrank;
    ILnode_t	*nspec;

    /*Agnode_t	*mn;
    for (mn = agfstnode(BASE(view)->model.v[IL_MOD]); mn; mn = agnxtnode(mn)) {*/
    /*ln = dd_rep(mn);*/
    Agnode_t	**movelist;
    int				i;
    movelist = choose_nodemove_order(view);

    for (i = 0; (ln = movelist[i]); i++) {
        nspec = dd_nspec(ln);
        newrank = dd_newrank(ln);

        if (newrank != dd_rank(ln)) {
            if (nspec->update & (IL_UPD_MOVE | IL_UPD_NAIL | IL_UPD_NAILX)) {
                dd_rank_delete(view,ln);
                dd_install_at_pos(view,ln,newrank,nspec->pos.x);
                dd_fix_coord(ln,TRUE);

            } else
                dd_percolate(view, ln, newrank, FALSE);

        } else {
            Agnode_t	*left, *right;
            left = dd_left(view,ln);
            right = dd_right(view,ln);

            if ((left && (dd_pos(left).x > nspec->pos.x)) ||
                    (right && (dd_pos(right).x < nspec->pos.x))) {
                dd_rank_delete(view,ln);
                dd_install_at_pos(view,ln,newrank,nspec->pos.x);
                dd_fix_coord(ln,TRUE);

            } else
                dd_set_x(ln,nspec->pos.x);
        }
    }

    agfree(view->layout,movelist);
}

static void move_old_edges(ddview_t *view) {
    Agnode_t	*mn;
    Agedge_t	*me;
    ILedge_t	*espec;

    for (mn = agfstnode(BASE(view)->model.e[IL_MOD]); mn; mn = agnxtnode(mn)) {
        for (me = agfstout(mn); me; me = agnxtout(me)) {
            espec = ddm_espec(me);

            if (user_defined_move(espec))
                user_route_edge(view, espec);
            else
                auto_adjust_edge(view, espec);
        }
    }
}

static void reopt_all_edges_touched (ddview_t *view) {
    Agnode_t	*mn;
    Agedge_t	*me;
    ILedge_t	*espec;
    ddpath_t	*path;

    for (mn = agfstnode(BASE(view)->model.e[IL_MOD]); mn; mn = agnxtnode(mn)) {
        for (me = agfstout(mn); me; me = agnxtout(me)) {
            espec = ddm_espec(me);

            if (!user_defined_move(espec)) {
                path = dd_find_path(view,espec);
                dd_opt_path(view, path);
            }
        }
    }
}

void dd_adjust_config(ddview_t *view) {
    execute_deletions(view);
    move_old_nodes(view);
    move_old_edges(view);
    insert_new_nodes(view);
    insert_new_edges(view);
    reopt_all_edges_touched(view);
}

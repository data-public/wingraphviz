#include <dag.h>

#ifdef DMALLOC
#include "dmalloc.h"
#endif

#define EQPASS 1

void dd_opt_path(ddview_t *view, ddpath_t *path) {
    int			pass;
    Agedge_t	*e, *first, *last;

    for (pass = 0; pass < MINCROSS_PASSES; pass++) {
        if (!(path->first) || !(path->last))
            continue;

        if (pass % 2 == 0) {
            first = AGMKOUT(path->first);
            last = AGMKOUT(path->last);

            for (e = first; e != last; e = agfstout(e->node))
                dd_opt_elt(view,e->node,UP,(pass%4)<2);

        } else {
            first = AGMKIN(path->first);
            last = AGMKIN(path->last);

            for (e = last; e != first; e = agfstin(e->node))
                dd_opt_elt(view,e->node,DOWN,(pass%4)<2);
        }
    }
}

static ilbool leftgoing(ddview_t *view, Agnode_t *n, int dir, int eq_pass) {
    Agnode_t	*left;

    left = dd_left(view,n);

    if (left) {
        int	uvx,vux,diff;
        uvx = dd_uvcross(left,n,dir==UP,dir==DOWN);
        vux = dd_uvcross(n,left,dir==UP,dir==DOWN);
        diff = uvx - vux;
#ifdef EQPASS

        if ((diff > 0) || ((diff == 0) && eq_pass))
#else
        if (diff > 0)
#endif

            return TRUE;
    }

    if (NOT(dd_mval_exists(n,dir)))
        return FALSE;

    while (left) {
        if (dd_mval_exists(left,dir)) {
            return (dd_mval(n,dir) < dd_mval(left,dir));
        }

        left = dd_left(view,left);
    }

    return FALSE;
}

static void shift_left(ddview_t *view, Agnode_t *n) {
    dd_exchange(view,dd_left(view,n),n);
}

static ilbool rightgoing(ddview_t *view, Agnode_t *n, int dir, int eq_pass) {
    Agnode_t	*right;

    right = dd_right(view,n);

    if (right) {
        int	uvx,vux,diff;
        uvx = dd_uvcross(n,right,dir==UP,dir==DOWN);
        vux = dd_uvcross(right,n,dir==UP,dir==DOWN);
        diff = uvx - vux;
#ifdef EQPASS

        if ((diff > 0) || ((diff == 0) && eq_pass))
#else

        if (diff > 0)
#endif

            return TRUE;
    }

    if (NOT(dd_mval_exists(n,dir)))
        return FALSE;

    while (right) {
        if (dd_mval_exists(right,dir)) {
            return (dd_mval(n,dir) > dd_mval(right,dir));
        }

        right = dd_right(view,right);
    }

    return FALSE;
}

static void shift_right(ddview_t *view, Agnode_t *n) {
    dd_exchange(view,n,dd_right(view,n));
}

static void reset_coord(ddview_t *view, Agnode_t *n) {
    Agnode_t	*L,*R;
    double		x;

    L = dd_left(view,n);
    R = dd_right(view,n);
    x = dd_coord_between(view,L,R);
    dd_set_x(n,x);
}

double dd_coord_between(ddview_t *view, Agnode_t *L, Agnode_t *R) {
    double		lx, rx, x;

    if (L || R) {
        lx = (L ? dd_pos(L).x : dd_pos(R).x - 2.0 * dd_uv_sep(view,NILnode,R));
        rx = (R ? dd_pos(R).x : dd_pos(L).x + 2.0 * dd_uv_sep(view,L,NILnode));
        x = (lx + rx) / 2.0;

    } else
        x = 0.0;

    return x;
}

void dd_opt_elt(ddview_t *view, Agnode_t *n, int dir, int eq_pass) {
    if (leftgoing(view,n,dir,eq_pass)) {
        do
            shift_left(view,n);
        while (leftgoing(view,n,dir,eq_pass));

    } else if (rightgoing(view,n,dir,eq_pass)) {
        do
            shift_right(view,n);
        while (rightgoing(view,n,dir,eq_pass));
    }

    reset_coord(view,n);
}


/* return new coordinate if node were installed in given rank */
double dd_place_and_reopt(ddview_t *view, Agnode_t *n, int r, double x) {
    Agnode_t	*ln,*rn;
    ilbool		go_left,go_right;
    int			oldrank,dir;

    oldrank = dd_rank(n);

    if (dd_node_in_config(n)) {
        dd_rank_delete(view,n);
    }

    dd_install_at_pos(view,n,r,x);
    dir = (oldrank < r)? UP : DOWN;
    dir = (oldrank < r)? DOWN : UP;

    /* set median of n as if it were a leaf from original place of n */
    dd_set_mval(n,(double)dd_order(n),dir);

    /* go left or right */
    go_left = leftgoing(view,n,dir,FALSE);
    go_right = rightgoing(view,n,dir,FALSE);

    if (go_left && NOT(go_right)) {
        rn = dd_right(view,n);

        for (ln = dd_left(view,n); ln; ln = dd_left(view,ln)) {
            if (dd_mval_exists(ln,dir)) {
                if (dd_mval(ln,dir) <= dd_mval(n,dir))
                    break;

            } else
                rn = ln;
        }

    } else if (go_right && NOT(go_left)) {
        ln = dd_left(view,n);

        for (rn = dd_right(view,n); rn; rn = dd_right(view,rn)) {
            if (dd_mval_exists(rn,dir))  {
                if (dd_mval(rn,dir) >= dd_mval(n,dir))
                    break;

            } else
                ln = rn;
        }

    } else {	/* it's frozen in place */
        ln = dd_left(view,n);
        rn = dd_right(view,n);
    }

    if ((ln && (x <= dd_pos(ln).x)) || (rn && (x >= dd_pos(rn).x)))
        x = dd_coord_between(view,ln,rn);

    return x;
}

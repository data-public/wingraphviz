#ifndef GPRSTATE_H
#define GPRSTATE_H

#include <stdio.h>
#include <agraph.h>
#include <ast.h>
#include <vmalloc.h>

typedef enum {TV_flat, TV_dfs} trav_type;

typedef struct {
    Agraph_t*    curgraph;
    Agraph_t*    target;
    Agraph_t*    outgraph;
    Agobj_t*     curobj;
    Sfio_t*      tmp;
    char*        tgtname;
    FILE*        outFile;
    trav_type    tvt;
    int          name_used;
}

Gpr_t;

extern Gpr_t* openGPRState ();
extern void initGPRState (Gpr_t*, Vmalloc_t*, FILE*);
extern int  validTVT(int);

#endif

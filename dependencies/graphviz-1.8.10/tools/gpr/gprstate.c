#include <gprstate.h>
#include <error.h>
#include <sfstr.h>

int  validTVT(int c) {
    int rv = 0;

    switch (c) {

            case TV_flat :

            case TV_dfs :
            rv = 1;
            break;
    }

    return rv;
}

void
initGPRState (Gpr_t*state, Vmalloc_t* vm, FILE* outfile) {
    state->tgtname = vmstrdup (vm, "gpr_result");
    state->tvt = TV_flat;
    state->outFile = outfile;
}

Gpr_t*
openGPRState () {
    Gpr_t*      state;

    if (!(state = newof(0, Gpr_t, 1, 0)))
        error (3, "Could not create gpr state: out of memory");

    if (!(state->tmp = sfstropen()))
        error (3, "Could not create state");

    return state;
}


#include <actions.h>
#include <error.h>

void
nodeInduce(Agraph_t *selected) {
    Agnode_t*   n;
    Agedge_t*   e;
    Agraph_t*   base;

    if (!selected)
        return;

    base = agroot (selected);

    if (base == selected)
        return;

    for (n = agfstnode(selected); n; n = agnxtnode(n)) {
        for (e = agfstout(agsubnode(base,n,FALSE)); e; e = agnxtout(e)) {
            if (agsubnode(selected,aghead(e),FALSE))
                agsubedge(selected,e,TRUE);
        }
    }
}

/* copyAttr;
 * Copy attributes from src to tgt. Overrides currently
 * defined values.
 */
static void
copyAttr (int kind, Agobj_t* src, Agobj_t* tgt) {
    Agraph_t*  srcg;
    Agraph_t*  tgtg;
    Agsym_t*   sym = 0;
    Agsym_t*   tsym = 0;

    srcg = agraphof(src);
    tgtg = agraphof(tgt);

    while ((sym = agnxtattr(srcg,kind,sym))) {
        tsym = agattrsym (tgt, sym->name);

        if (!tsym)
            tsym = agattr(tgtg,kind,sym->name,"");

        agxset (tgt, tsym, agxget(src,sym));
    }
}

/* clone:
 * Create new object of type AGTYPE(obj) with all of its
 * attributes.
 * If obj is an edge, create end nodes if necessary, but
 * do not copy the attributes.
 * If obj is a graph, if g is null, create a clone top-level
 * graph. Otherwise, create a clone subgraph of g.
 */
Agobj_t* clone(Agraph_t *g, Agobj_t* obj) {
    Agobj_t*   nobj = 0;
    Agedge_t*  e;
    Agnode_t*  h;
    Agnode_t*  t;
    int        kind = AGTYPE(obj);

    if (!obj || ((kind != AGRAPH) && !g))
        return 0;

    switch (kind) {

            case AGNODE :
            nobj = (Agobj_t*)agnode (g, agnameof(obj), 1);

            if (nobj)
                copyAttr (kind,obj,nobj);

            break;

            case AGRAPH :
            error (3, "clone of graph unimplemented");

            break;

            case AGEDGE :
            e = (Agedge_t*)obj;

            t = agnode (g, agnameof(agtail(e)), 1);

            h = agnode (g, agnameof(aghead(e)), 1);

            nobj = (Agobj_t*)agedge (t, h, agnameof(obj), 1);

            if (nobj)
                copyAttr (kind,obj,nobj);

            break;
    }

    return nobj;
}


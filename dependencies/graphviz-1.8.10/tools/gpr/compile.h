#ifndef COMPILE_H
#define COMPILE_H

#include <parse.h>
#include <gprstate.h>
#include <expr.h>

typedef struct {
    Exnode_t*   guard;
    Exnode_t*   action;
}

case_stmt;

#define UDATA "userval"

typedef struct {
    Agrec_t   h;
    Extype_t  xu;
    Extype_t  iu;
}

nval_t;

typedef struct {
    Agrec_t   h;
    Extype_t  xu;
}

uval_t;

typedef nval_t ndata;
typedef uval_t edata;
typedef uval_t gdata;

#define SRCOUT    0x1
#define INDUCE    0x2

typedef struct {
    Expr_t*     prog;
    Exnode_t*   begin_stmt;
    Exnode_t*   begg_stmt;
    int         n_nstmts;
    int         n_estmts;
    case_stmt*  node_stmts;
    case_stmt*  edge_stmts;
    Exnode_t*   endg_stmt;
    Exnode_t*   end_stmt;
}

comp_prog;

extern comp_prog* compileProg (parse_prog*, Gpr_t*, int);
extern int usesGraph (comp_prog*);
extern int walksGraph (comp_prog*);

#endif

#ifndef ACTIONS_H
#define ACTIONS_H

#include <agraph.h>

extern void nodeInduce(Agraph_t *selected);
extern Agobj_t* clone(Agraph_t *g, Agobj_t* obj);

#endif

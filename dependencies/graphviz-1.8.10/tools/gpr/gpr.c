/*
    This software may only be used by you under license from AT&T Corp.
    ("AT&T").  A copy of AT&T's Source Code Agreement is available at
    AT&T's Internet website having the URL:
    <http://www.research.att.com/sw/tools/graphviz/license/source.html>
    If you received this software without first entering into a license
    with AT&T, you have an infringing copy of this software and cannot use
    it without violating AT&T's intellectual property rights.
*/

/*
 * gpr: graph pattern recognizer
 *
 * Written by Emden Gansner
 */

#include <stdio.h>
#ifdef HAVE_UNISTD_H
#include	<unistd.h>
#endif
#include <gprstate.h>
#include <agraph.h>
#include <ingraphs.h>
#include <compile.h>
#include <queue.h>
#include <sfstr.h>
#include <error.h>
#ifdef HAVE_CONFIG_H
#include <gvconfig.h>
#endif
#ifdef HAVE_GETOPT_H
#include <getopt.h>
#else
#include "compat_getopt.h"
#endif

static const char* usage =
    ": gpr [-o <ofile>] ([-f <prog>] | 'prog') [files]\n\
    -c         - use source graph for output\n\
    -f <pfile> - find program in file <pfile>\n\
    -i         - create node induced subgraph\n\
    -o <ofile> - write output to <ofile>; stdout by default\n\
    If no files are specified, stdin is used\n";

struct {
    char*     cmdName;
    FILE*     outFile;        /* output stream; stdout default */
    char*     program;
    int       useFile;        /* true if program comes from a file */
    int       compflags;
    char**    inFiles;
}

options;

static FILE*
openOut (char* name) {
    FILE*   outs;

    outs = fopen(name,"w");

    if (outs == 0) {
        error (3|ERROR_SYSTEM, "could not open %s for writing", name);
    }

    return outs;
}

/* scanArgs:
 * Parse command line options.
 */
static void
scanArgs(int argc,char **argv) {
    int       c;
    char*     outname = 0;

    options.cmdName = argv[0];
    options.outFile = 0;
    options.useFile = 0;
    error_info.id = options.cmdName;

    while ((c = getopt(argc, argv, ":?cif:o:")) != -1) {
        switch (c) {

                case 'c':
                options.compflags |= SRCOUT;
                break;

                case 'f':
                options.useFile = 1;
                options.program = optarg;
                break;

                case 'i':
                options.compflags |= INDUCE;
                break;

                case 'o':
                outname = optarg;
                break;

                case '?':

                if (optopt == '?') {
                    error(ERROR_USAGE, "%s", usage);
                    exit (0);

                } else {
                    error(2, "option -%c unrecognized\n", optopt);
                }

                break;

                case ':':
                error(2, "missing argument for option -%c", optopt);
                break;
        }
    }

    argv += optind;
    argc -= optind;

    /* Handle additional semantics */

    if (options.useFile == 0) {
        if (argc == 0) {
            error(2, "No program supplied via argument or -f option");
            error_info.errors = 1;

        } else {
            options.program = *argv++;
            argc--;
        }
    }

    if (argc == 0)
        options.inFiles = 0;
    else
        options.inFiles = argv;

    if (outname)
        options.outFile = openOut (outname);
    else
        options.outFile = stdout;

    if (error_info.errors)
        error(ERROR_USAGE|4, "%s", usage);
}

static void
evalEdge (Gpr_t* state, comp_prog* xprog, Agedge_t* e) {
    int         i;
    case_stmt*  cs;
    int         okay;

    state->curobj = (Agobj_t*)e;

    for (i = 0; i < xprog->n_estmts; i++) {
        cs = xprog->edge_stmts+i;

        if (cs->guard)
            okay = (exeval (xprog->prog, cs->guard, state)).integer;
        else
            okay = 1;

        if (okay) {
            if (cs->action)
                exeval (xprog->prog, cs->action, state);
            else
                agsubedge (state->target, e, TRUE);
        }
    }
}

static void
evalNode (Gpr_t* state, comp_prog* xprog, Agnode_t* n) {
    int         i;
    case_stmt*  cs;
    int         okay;

    state->curobj = (Agobj_t*)n;

    for (i = 0; i < xprog->n_nstmts; i++) {
        cs = xprog->node_stmts+i;

        if (cs->guard)
            okay = (exeval (xprog->prog, cs->guard, state)).integer;
        else
            okay = 1;

        if (okay) {
            if (cs->action)
                exeval (xprog->prog, cs->action, state);
            else
                agsubnode (state->target, n, TRUE);
        }
    }
}

#define nData(n)    ((ndata*)(aggetrec(n,UDATA,0)))
#define MARKED(x)  (((x)->iu.integer)&1)
#define MARK(x)  (((x)->iu.integer) = 1)
#define ONSTACK(x)  (((x)->iu.integer)&2)
#define PUSH(x)  (((x)->iu.integer)|=2)
#define POP(x)  (((x)->iu.integer)&=(~2))

static void
travDFS (Gpr_t* state, comp_prog* xprog) {
    Agnode_t*     n;
    queue*        stk;
    Agedgepair_t  seed;
    Agnode_t*     curn;
    Agedge_t*     cure;

    Agedge_t*     entry

    ;
    int           more;

    ndata*        nd;

    stk = mkStack ();

    for (n = agfstnode(state->curgraph); n; n = agnxtnode(n)) {
        nd = nData(n);

        if (MARKED(nd))
            continue;

        seed.out.node = n;

        seed.in.node = 0;

        curn = n;

        entry
        = &(seed.out);

        cure = 0;

        MARK(nd);

        PUSH(nd);

        evalNode (state, xprog, n);

        more = 1;

        while (more) {
            if (cure)
                cure = agnxtedge (cure,curn);
            else
                cure = agfstedge (curn);

            if (cure) {
                if (entry
                        == agopp(cure))
                    continue;

                nd = nData(cure->node);

                if (MARKED(nd)) {
                    if (ONSTACK(nd))
                        evalEdge (state, xprog, cure);

                } else {
                    evalEdge (state, xprog, cure);

                    push(stk, entry
                        );

                    entry
                    = cure;

                    curn = cure->node;

                    cure = 0;

                    evalNode (state, xprog, curn);

                    MARK(nd);

                    PUSH(nd);
                }

            } else {
                nd = nData(curn);
                POP(nd);

                cure = entry

                       ;
                entry
                = (Agedge_t*)pull(stk);

                if (entry
                   )
                    curn = entry
                           ->node;
                else
                    more = 0;
            }
        }
    }
}

static void
travFlat (Gpr_t* state, comp_prog* xprog) {
    Agnode_t*   n;
    Agedge_t*   e;

    for (n = agfstnode(state->curgraph); n; n = agnxtnode(n)) {
        evalNode (state, xprog, n);

        if (xprog->n_estmts > 0) {
            for (e = agfstout(n); e; e = agnxtout(e)) {
                evalEdge (state, xprog, e);
            }
        }
    }
}

static void
traverse (Gpr_t* state, comp_prog* xprog) {
    char*       target;

    if (state->name_used) {
        sfprintf (state->tmp, "%s%d", state->tgtname, state->name_used);
        target = sfstruse (state->tmp);

    } else
        target = state->tgtname;

    state->name_used++;

    state->target = agsubg(state->curgraph,target,TRUE);

    state->outgraph = state->target;

    if (state->tvt == TV_dfs)
        travDFS (state, xprog);
    else
        travFlat (state, xprog);
}

static Agraph_t*
readG (FILE* fp) {
    Agraph_t*   g;

    g = agread(fp, 0);

    if (g) {
        aginit(g,AGRAPH,UDATA,sizeof(gdata),0);
        aginit(g,AGNODE,UDATA,sizeof(ndata),0);
        aginit(g,AGEDGE,UDATA,sizeof(edata),0);
    }

    return g;
}

int
main (int argc, char* argv[]) {
    parse_prog*     prog;
    ingraph_state*  ing;
    comp_prog*      xprog;
    Gpr_t*          state;

    scanArgs(argc,argv);

    prog = parseProg (options.program, options.useFile);
    state = openGPRState ();
    xprog = compileProg (prog, state, options.compflags);
    initGPRState (state,xprog->prog->vm,options.outFile);

    /* do begin */

    if (xprog->begin_stmt)
        exeval (xprog->prog, xprog->begin_stmt, state);

    /* if program is not null */
    if (usesGraph(xprog)) {
        ing = newIngraph (0, options.inFiles, readG);

        while ((state->curgraph = nextGraph(ing))) {
            /* begin graph */

            if (xprog->begg_stmt)
                exeval (xprog->prog, xprog->begg_stmt, state);

            /* walk graph */
            if (walksGraph(xprog))
                traverse (state, xprog);

            /* end graph */
            if (xprog->endg_stmt)
                exeval (xprog->prog, xprog->endg_stmt, state);

            /* if $O == $G and $T is empty, delete $T */
            if ((state->outgraph == state->curgraph) &&
                    (state->target) && !agnnodes(state->target))
                agdelete (state->curgraph, state->target);

            /* output graph, if necessary */
            if (state->outgraph && agnnodes(state->outgraph))
                agwrite (state->outgraph, options.outFile);

            agclose (state->curgraph);

            state->target = 0;

            state->outgraph = 0;
        }
    }

    /* do end */
    state->curgraph = 0;

    state->curobj = 0;

    if (xprog->end_stmt)
        exeval (xprog->prog, xprog->end_stmt, state);

    exit(0);
}


.TH GPR 1 "21 March 2001"
.SH NAME
gpr \- graph pattern scanning and processing language
.SH SYNOPSIS
.B gpr
[\fB-ic\fP]
[
.BI -o outfile
]
[
.I 'prog'
|
.BI -f
.I progfile
]
[ 
.I files 
]
.SH DESCRIPTION
.B gpr
is a graph stream editor inspired by \fBawk\fP.
It copies input graphs to its
output, possibly transforming their structure and attributes,
creating new graphs, or printing arbitrary information.
.PP
Basically,
.B gpr
traverses each input graph, denoted by \fB$G\fP, visiting each node and edge,
matching it with the pattern-action rules supplied in the input program.
The patterns are evaluated in order.
For each pattern for which a match is found, the resulting 
action is performed. 
During the traversal, the current node or edge being visited
is denoted by \fB$\fP.
.PP
For each input graph, there is a target graph, denoted by
\fB$T\fP, used to accumulate
chosen entities, and an output graph, \fB$O\fP, used for final processing
and then written to output. 
By default, the output graph is the target graph.
The output graph can be set in the program or, in a limited sense,
on the command line.
.SH OPTIONS
The following options are supported:
.TP
.B \-c
Use source graph for output.
.TP
.B \-i
Derive the node-induced subgraph extension of the output graph in the context 
of its root graph.
.TP
.BI \-o " outfile"
Causes the output to be written to the specified file; by default,
output is written to \fBstdout\fP.
.TP
.BI \-f " progfile"
Use the contents of the specified file as the program to execute
on the input. If
.B \-f
is not given,
.B gpr
will use the first non-option argument as the program.
.TP
.B \-?
Causes the program to print usage information.
.SH OPERANDS
The following operand is supported:
.TP 8
.I files
Names of files containing 1 or more graphs in dot format.
If no
.B -f
option is given, the first name removed from the list and used 
as the input program. If the list of files is empty, the 
standard input will be used.
.SH PROGRAMS
A
.B gpr
program consists of list of pattern-action clauses, having one
of the forms:
.IP
.BI "BEGIN { "  action " }"
.IP
.BI "BEG_G { "  action " }"
.IP
.BI "N [ " pattern " ] { " action " }
.IP
.BI "E [ " pattern " ] { " action " }
.IP
.BI "END_G { "  action " }"
.IP
.BI "END { "  action " }"
.PP
A program can contain at most one of each of the \fBBEGIN\fP, \fBBEG_G\fP,
\fBEND_G\fP and \fBEND\fP clauses. The action is performed at specific
times in \fBgpr\fP:
.TF BEGING
.TP
.B BEGIN
At the beginning before any graphs are read.
.TP
.B BEG_G
After a new root graph has been read, but before traversal.
.TP
.B END_G
After the traversal of a root graph has finished.
.TP
.B END
After all graphs have been processed.
.PP
There can be any number of \fBN\fP and \fBE\fP statements,
the first applied to nodes, the second to edges.
Either the pattern or action may be omitted. 
If there is no pattern with an action, the action is 
performed on every node or edge, as appropriate.
If there is no action,
the matched object is added to the target. 
.PP
Patterns and actions are sequences of statements in the C dialect 
supported by \fBlibexpr\fP. 
The only difference between patterns and actions is that the former
must have a type that may interpreted as either true or false.
Here the usual C convention is followed, in which a non-zero value is
considered true. This would include non-empty strings and non-empty
references to nodes, edges, etc. However, if a string can be 
converted to an integer, this value is used.
.PP
In addition to the usual C base types
(\f5void\fP, \f5int\fP, \f5char\fP,
\f5string\fP, \f5float\fP, \f5long\fP, 
\f5unsigned\fP and \f5double\fP), 
.B gpr
provides the graph-based types \f5node_t\fP, 
\f5edge_t\fP, \f5graph_t\fP and \f5obj_t\fP.
The \f5obj_t\fP can be viewed as a supertype of the other 3 concrete types;
the correct base type is maintained dynamically.
Besides these base types, the only other supported type expressions
are (associative) arrays. 
.PP
A statement can be a declaration of a function, a variable
or an array, or one of the following:
.PP
.EX
.ta \w'\f(CWdelete array[expression]'u
.RS
.nf
{\fR [\fP\fI statement ... \fP\fR] \fP}
\fIexpression\fP	#\fR commonly\fP\fI var = expression\fP
if(\fI expression \fP)\fI statement \fP\fR[ \fPelse\fI statement \fP\fR]\fP
for(\fI expression \fP;\fI expression \fP;\fI expression \fP)\fI statement\fP
for(\fI array \fP[\fI var \fP])\fI statement\fP
while(\fI expression \fP)\fI statement\fP
switch(\fI expression \fP)\fI case statements\fP
break [\fI expression \fP]
continue [\fI expression \fP]
return [\fI expression \fP]
.fi
.RE
.EE
.ST
.PP
Expressions include the usual C expressions. 
In addition, expressions of graphical type (i.e., \f5graph_t, node_t,
edge_t, obj_t\fP) may be followed by a field reference in the
form of \fB.\fP\fIname\fP. The resulting value is the value
of the attribute named \fIname\fP of the given object.
If an undeclared identifier appears in a program, it is taken to be an
attribute name. If \fIname\fP is not used explicitly as a field name, 
it is taken to denote the attribute \fIname\fP of the current node or edge,
i.e, \fB$.\fP\fIname\fP.
.PP
As usual in the dot model, attributes are string-valued.
In addition,
.B gpr
supports certain pseudo-attributes of graph objects, not necessarily
string-valued. These reflect intrinsic properties of a graph
and cannot be assigned to.
.TF outdegree
.TP
.B head
refers to the head of an edge.
.TP
.B tail
refers to the tail of an edge.
.TP
.B name
refers to the name of an edge, node or graph.
.TP
.B indegree
refers to the indegree of a node.
.TP
.B outdegree
refers to the outdegree of a node.
.TP
.B degree
refers to the degree of a node.
.PP
The following expression functions are also supported:
.TP
.BI exit( expr )
causes
.B gpr
to exit with the exit code
.IR expr .
.I expr
defaults to 0 if omitted.
.TP
.BI sprintf( fmt , " expr" , " ...\fB )
returns the string resulting from formatting
.I expr ...
according to the
.IR printf (3)
format
.I fmt
.TP
.BI printf( fmt , " expr" , " ...\fB )
prints the string resulting from formatting
.I expr ...
according to the
.IR printf (3)
format
.IR fmt .
Returns 1 on success; 0, otherwise.
.PP
Constants follow C syntax, but strings may be quoted with either
\fB"..."\fP or \fB'...'\fP.
String comparisons
.RB ( ==
and
.BR != )
treat the right hand operand as a
.BR ksh (1)
file match pattern.
Expressions take on string or numeric values as appropriate.
\fBgpr\fP uses C++ comments.
.PP
Built-in functions include:
.TP
.BI graph( s , " t" )
creates a graph whose name is \fIs\fP and whose type is
specified by the string \fIt\fP. Ignoring case, characters
\f5U, D, S, N\fP have the interpretation undirected, directed,
strict, and non-strict, respectively. If \fIt\fP is empty,
a directed, non-strict graph is generated.
.TP
.BI node( g , " s" )
creates a node in graph \fIg\fP of name \fIs\fP. If such a node
already exists, it is returned.
.TP
.BI isNode( g , " s" )
looks for a node in graph \fIg\fP of name \fIs\fP. If such a node
exists, it is returned; NULL otherwise.
.TP
.BI edge( t , " h" , " s\fB)
creates an edge with tail node \fIt\fP, head node \fIh\fP and
name \fIs\fP. If the graph is undirected, the distinction between
head and tail nodes is unimportant.
If such an edge already exists, it is returned.
.TP
.BI isEdge( t , " h" , " s\fB)
looks for an edge with tail node \fIt\fP, head node \fIh\fP and
name \fIs\fP. If the graph is undirected, the distinction between
head and tail nodes is unimportant.
If such an edge exists, it is returned; NULL otherwise.
.TP
.BI clone( g , " x" )
creates a copy of object \fIx\fP in graph \fIg\fP.
In particular, the new object has the same name/value attributes
as the original object.
If an object with the same key as \fIx\fP already exists, its attributes
are overlaid by those of \fIx\fP and the object is returned.
Note that this is a shallow clone. For example, if an edge is
being cloned, the endpoints are created if necessary, but
the endpoints are not cloned.
Currently not implemented when \fIx\fP is a graph.
.TP
.BI induce( g )
extends \fIg\fP to its node-induced subgraph extension in its root graph.
.TP
.BI write( g )
prints \fIg\fP in dot format on the output stream.
.TP
.BI isDirect( g )
returns true if and only if \fIg\fP is directed.
.TP
.BI isStrict( g )
returns true if and only if \fIg\fP is strict.
.TP
.BI delete( g , " x" )
deletes object \fIx\fP from graph \fIg\fP.
.TP
.BI nNodes( g )
returns the number of nodes in \fIg\fP.
.TP
.BI nEdges( g )
returns the number of edges in \fIg\fP.
.PP
.B gpr
provides certain special, built-in variables.
.TP
.B $
denotes the current node or edge during the traversal of a graph.
It is only available in \fBN\fP or \fBE\fP clauses.
A program cannot assign to it.
.TP
.B $G
denotes the current graph being processed. It is not available
in \fBBEGIN\fP or \fBEND\fP clauses.
A program cannot assign to it.
.TP
.B $T
denotes the current target graph. It is a subgraph of \fB$G\fP
and is available only in \fBN\fP, \fBE\fP and \fBEND_G\fP clauses.
A program cannot assign to it.
.TP
.B $O
denotes the output graph. Before graph traversal, it is initialized
to the target graph. After traversal and any \fBEND_G\fP actions,
if it refers to a non-empty graph, that graph is printed.
It is only valid in \fBN\fP, \fBE\fP and \fBEND_G\fP clauses.
.TP
.B Tname
denotes the name of the target graph. 
By default, it is set to \f5"gpr_result"\fP.
If used multiple times
during the execution of
.BR gpr ,
the name will be appended with an integer. 
.SH EXAMPLES
.EX
gpr -i 'N[color=="blue"]' file.dot
.EE
Generate the node-induced subgraph of all nodes with color blue.
.EX
gpr -c 'N[color=="blue"]{color = "red"}' file.dot
.EE
Make all blue nodes red.
.EX
BEGIN { int n, e; int tot_n = 0; int tot_e = 0; }
BEG_G {
  n = nNodes($G);
  e = nEdges($G);
  printf ("%d nodes %d edges %s\n", n, e, $G.name);
  tot_n += n;
  tot_e += e;
}
END { printf ("%d nodes %d edges total\n", tot_n, tot_e) }
.EE
Version of the program \fBgc\fP.
.EX
gpr -c ""
.EE
Equivalent to \fBnop\fP.
.EX
BEG_G { graph_t g = graph ("merge", "S"); }
E {
  node_t h = clone(g,$.head);
  node_t t = clone(g,$.tail);
  edge_t e = edge(t,h,"");
  e.weight = e.weight + 1;
}
END_G { $O = g; }
.EE
Produces a strict version of the input graph, where the weight attribute
of an edge indicates how many edges from the input graph the edge represents.
.EX
BEGIN {node_t n; int deg[]}
E{deg[head]++; deg[tail]++; }
END_G {
  for (deg[n]) {
    printf ("deg[%s] = %d\n", n.name, deg[n]);
  }
}
.EE
Computes the degrees of nodes with edges.
.SH BUGS
When the program is given as a command line argument, the usual
shell interpretation takes place, which may affect some of the
special names in \fBgpr\fP. To avoid this, it is best to wrap the
program in single quotes.
.PP
There is a single scope and the extent of all variables is
the entire life of the program. It might be preferable for scope
to reflect the natural nesting of the clauses, or for the program
to at least reset locally declared variables.
.PP
The expr library does not support NULL strings. This means we can't 
distinguish between empty and NULL edge keys. For the purposes of looking
up and creating edges, we translate "" to be NULL, since this latter value is
necessary in order to look up any edge with a matching head and tail.
.PP
The language inherits the usual C problems such as dangling references
and the confusion between '=' and '=='.
.SH AUTHOR
Emden R. Gansner <erg@research.att.com>
.SH "SEE ALSO"
.PP
gc(1), colorize(1), dot(1), gpr(1), neato(1), sccmap(1), libexpr(3), libagraph(3)

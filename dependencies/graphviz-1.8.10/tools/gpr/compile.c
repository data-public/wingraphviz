#include <compile.h>
#include <assert.h>
#include <agraph.h>
#include <error.h>
#include <actions.h>
#include <sfstr.h>

#define ISEDGE(e) (AGTYPE(e)&2)

#include <gdefs.h>

#define BITS_PER_BYTE 8

static char*
symName (int op) {
    if (op >= MINNAME && op <= MAXNAME)
        return gprnames[op - MINNAME];
    else
        return "<unknown>";
}

/* xargs:
 * Convert string argument to graph to type of graph desired.
 *   u => undirected
 *   d => directed
 *   s => strict
 *   n => non-strict
 * Case-insensitive
 * By default, the graph is directed, non-strict.
 */
static Agdesc_t
xargs (char* args) {
    Agdesc_t desc = Agdirected;
    char     c;

    while ((c = *args++)) {
        switch (c) {

                case 'u' :

                case 'U' :
                desc.directed = 0;
                break;

                case 'd' :

                case 'D' :
                desc.directed = 1;
                break;

                case 's' :

                case 'S' :
                desc.strict = 1;
                break;

                case 'n' :

                case 'N' :
                desc.directed = 0;
                break;

                default :
                error (1, "unknown graph descriptor '%c' : ignored", c);
                break;
        }
    }

    return desc;
}

/* deparse:
 * Recreate string representation of expression involving
 * a reference and a symbol.
 * The parameter sf must be a string stream.
 */
static char*
deparse (Expr_t* ex, Exnode_t* n, Sfio_t* sf) {
    exdump (ex, n, sf);
    return (sfstruse (sf));
}

/* deref:
 * Evaluate reference to derive desired graph object.
 * A reference is either DI* or II*
 * The parameter objp is the current object.
 * Assume ref is type-correct.
 */
static Agobj_t*
deref (Exnode_t* x, Exref_t* ref, Agobj_t* objp, Gpr_t* state) {
    void* ptr;

    if (ref == 0)
        return objp;
    else if (ref->symbol->lex == DYNAMIC) {
        ptr = (void*)(x->data.variable.dyna->data.variable.dyna->data.constant.value.integer);
        return deref (x, ref->next, (Agobj_t*)ptr, state);

    } else

        switch (ref->symbol->index) {    /* sym->lex == ID */

                case V_outgraph :
                return deref (x, ref->next, (Agobj_t*)state->outgraph, state);
                break;

                case V_this :
                return deref (x, ref->next, state->curobj, state);
                break;

                case V_thisg :
                return deref (x, ref->next, (Agobj_t*)state->curgraph, state);
                break;

                case V_targt :
                return deref (x, ref->next, (Agobj_t*)state->target, state);
                break;

                case M_head :

                if (!objp && !(objp = state->curobj)) {
                    error (1, "Current object $ not defined");
                    return 0;
                }

                if (ISEDGE(objp))
                    return deref (x, ref->next, (Agobj_t*)AGHEAD((Agedge_t*)objp), state);
                else
                    error (3, "head of non-edge");

                break;

                case M_tail :
                if (!objp && !(objp = state->curobj)) {
                    error (3, "Current object $ not defined");
                    return 0;
                }

                if (ISEDGE(objp))
                    return deref (x, ref->next, (Agobj_t*)AGTAIL((Agedge_t*)objp), state);
                else
                    error (3, "tail of non-edge %x", objp);

                break;

                default :
                error (1, "%s : illegal reference", ref->symbol->name);

                return 0;

                break;
        }

    return 0;

}

/* lookup:
 * Apply symbol to get field value of objp
 */
static int
lookup (Agobj_t* objp, Exid_t* sym, Extype_t* v) {
    if (sym->lex == ID) {
        switch (sym->index) {

                case M_head :

                if (ISEDGE(objp))
                    v->integer = (Sflong_t)(AGHEAD((Agedge_t*)objp));
                else {
                    error (1, "head of non-edge");
                    return -1;
                }

                break;

                case M_tail :

                if (ISEDGE(objp))
                    v->integer = (Sflong_t)(AGTAIL((Agedge_t*)objp));
                else {
                    error (1, "tail of non-edge");
                    return -1;
                }

                break;

                case  M_name :
                v->string = agnameof (objp);
                break;

                case  M_indegree :

                if (AGTYPE(objp) == AGNODE)
                    v->integer = agdegree((Agnode_t*)objp,1,0);
                else {
                    error (3, "indegree of non-node");
                    return -1;
                }

                break;

                case  M_outdegree :

                if (AGTYPE(objp) == AGNODE)
                    v->integer = agdegree((Agnode_t*)objp,0,1);
                else {
                    error (3, "outdegree of non-node");
                    return -1;
                }

                break;

                case  M_degree :

                if (AGTYPE(objp) == AGNODE)
                    v->integer = agdegree((Agnode_t*)objp,1,1);
                else {
                    error (3, "degree of non-node");
                    return -1;
                }

                break;

                default :
                error (1, "%s : illegal reference", sym->name);
                return -1;
                break;
        }

    } else
        v->string = agget (objp, sym->name);

    return 0;
}

/* getval:
 * Return value associated with gpr identifier.
 */
Extype_t
getval(Expr_t* pgm, Exnode_t* node, Exid_t* sym, Exref_t* ref,
       void* env, int elt, Exdisc_t* disc) {
    Extype_t        v;
    Gpr_t*          state;
    Extype_t*       args;
    Agobj_t*        objp;
    char*           key;

    assert (sym->lex != CONSTANT);

    if (elt == EX_CALL) {
        args = (Extype_t*)env;
        state = (Gpr_t*)(disc->user);

        switch (sym->index) {

                case F_graph :
                objp = agopen (args[0].string, xargs(args[1].string), &AgDefaultDisc);

                if (objp)
                    agbindrec (objp,UDATA,sizeof(gdata),0);

                v.integer = (Sflong_t)objp;

                break;

                case F_node :
                objp = agnode ((Agraph_t*)args[0].integer, args[1].string, 1);

                if (objp)
                    agbindrec (objp,UDATA,sizeof(ndata),0);

                v.integer = (Sflong_t)objp;

                break;

                case F_isnode :
                v.integer = (Sflong_t)agnode ((Agraph_t*)args[0].integer, args[1].string, 0);

                break;

                case F_edge :
                key = args[2].string;

                if (*key == '\0')
                    key = 0;

                objp = agedge ((Agnode_t*)args[0].integer, (Agnode_t*)args[1].integer,
                               key, 1);

                if (objp)
                    agbindrec (objp,UDATA,sizeof(edata),0);

                v.integer = (Sflong_t)objp;

                break;

                case F_isedge :
                key = args[2].string;

                if (*key == '\0')
                    key = 0;

                v.integer = (Sflong_t)agedge ((Agnode_t*)args[0].integer, (Agnode_t*)args[1].integer,
                                              key, 0);

                break;

                case F_clone :
                v.integer = (Sflong_t)(clone ((Agraph_t*)args[0].integer, (Agobj_t*)args[1].integer));

                break;

                case F_induce :
                nodeInduce ((Agraph_t*)args[0].integer);

                v.integer = 0;

                break;

                case F_write :
                v.integer = agwrite ((Agraph_t*)args[0].integer, state->outFile);

                case F_isdirect :
                v.integer = agisdirected ((Agraph_t*)args[0].integer);

                break;

                case F_isstrict :
                v.integer = agisstrict ((Agraph_t*)args[0].integer);

                break;

                case F_delete :
                if ((Agraph_t*)(args[1].integer) == state->curgraph) {
                    error (1, "cannot delete current graph $G");
                    v.integer = 1;

                } else if ((Agraph_t*)(args[1].integer) == state->target) {
                    error (1, "cannot delete current graph $T");
                    v.integer = 1;

                } else
                    v.integer = agdelete ((Agraph_t*)args[0].integer, (Agobj_t*)(args[1].integer));

                break;

                case F_nnodes :
                v.integer = agnnodes ((Agraph_t*)args[0].integer);

                break;

                case F_nedges :
                v.integer = agnedges ((Agraph_t*)args[0].integer);

                break;

                default :
                error (3, "unknown function call: %s", sym->name);
        }

        return v;
    }

    state = (Gpr_t*)env;

    if (ref) {
        objp = deref (node, ref, 0, state);

        if (!objp)
            error (3, "null reference in expression %s",
                   deparse (pgm, node, state->tmp));

    } else if ((sym->lex == ID) && (sym->index <= LAST_V)) {
        switch (sym->index) {

                case V_this :
                v.integer = (Sflong_t)(state->curobj);
                break;

                case V_thisg :
                v.integer = (Sflong_t)(state->curgraph);
                break;

                case V_targt :
                v.integer = (Sflong_t)(state->target);
                break;

                case V_outgraph :
                v.integer = (Sflong_t)(state->outgraph);
                break;

                case V_tgtname :
                v.string = state->tgtname;
                break;

                case V_travtype :
                v.integer = state->tvt;
                break;
        }

        return v;

    } else {
        objp = state->curobj;

        if (!objp)
            error (3, "current object $ not defined as reference for %s",
                   deparse(pgm, node, state->tmp));
    }

    if (lookup (objp, sym, &v))
        error (3, "in expression %s", deparse (pgm, node, state->tmp));

    return v;
}

/* setval:
 * Set sym to value v.
 * Return -1 if not allowed.
 * Assume already type correct.
 */
int
setval(Expr_t* pgm, Exnode_t* x, Exid_t* sym, Exref_t* ref,
       void* env, int elt, Extype_t v, Exdisc_t* disc) {
    Gpr_t*          state;
    Agobj_t*        objp;
    Agsym_t*        gsym;
    int             iv;
    int             rv = 0;

    state = (Gpr_t*)env;

    if (ref) {
        objp = deref (x, ref, 0, state);

        if (!objp)
            error (3, "in expression %s", deparse (pgm, x, state->tmp));

    } else if ((MINNAME <= sym->index) && (sym->index <= MAXNAME)) {
        switch (sym->index) {

                case V_outgraph :
                state->outgraph = (Agraph_t*)(v.integer);
                break;

                case V_travtype :
                iv = v.integer;

                if (validTVT(v.integer))
                    state->tvt = iv;
                else
                    error(1,"unexpected value %d assigned to %s : ignored",
                          iv, symName(V_travtype));

                break;

                case V_tgtname :
                if (!streq(state->tgtname,v.string)) {
                    vmfree (pgm->vm, state->tgtname);
                    state->tgtname = vmstrdup (pgm->vm, v.string);
                    state->name_used = 0;
                }

                break;

                default :
                rv = -1;
                break;
        }

        return rv;

    } else {
        objp = state->curobj;

        if (!objp)
            error (3, "current object $ undefined in expression %s",
                   deparse(pgm, x, state->tmp));
    }

    gsym = agattrsym (objp, sym->name);

    if (!gsym) {
        gsym = agattr(agroot(agraphof(objp)),AGTYPE(objp),sym->name,"");
    }

    return agxset (objp, gsym, v.string);
}

static int codePhase;

#define haveGraph    ((1 <= codePhase) && (codePhase <= 4))
#define haveTarget   ((2 <= codePhase) && (codePhase <= 4))
#define inWalk       ((2 <= codePhase) && (codePhase <= 3))

/* typeChk:
 * Type check input type against implied type of symbol sym.
 * If okay, return result type; else return 0.
 * For functions, input type set must intersect with function domain.
 * This means type errors may occur, but these will be caught at runtime.
 * For non-functions, input type must be 0.
 */
static tctype
typeChk (tctype intype, Exid_t* sym) {
    tctype dom = 0, rng = 0;

    switch (sym->lex) {

            case DYNAMIC :
            dom = 0;

            switch (sym->type) {

                    case T_obj :
                    rng = YALL;
                    ;
                    break;

                    case T_node :
                    rng = Y(V);
                    break;

                    case T_graph :
                    rng = Y(G);
                    break;

                    case T_edge :
                    rng = Y(E);
                    break;

                    case INTEGER :
                    rng = Y(I);
                    break;

                    case FLOATING :
                    rng = Y(F);
                    break;

                    case STRING :
                    rng = Y(S);
                    break;

                    default :
                    exerror ( "\n -- unknown dynamic type %d of symbol %s", sym->lex, sym->name);
                    break;
            }

            break;

            case ID :

            if (sym->index <= MAXNAME) {
                if ((sym->index == V_this) && !inWalk)
                    exerror ("\n -- keyword %s can only be used in N and E statements",
                             sym->name);
                else if ((sym->index == V_thisg) && !haveGraph)
                    exerror ("\n -- keyword %s cannot be used in BEGIN/END statements",
                             sym->name);
                else if ((sym->index == V_targt) && !haveTarget)
                    exerror ("\n -- keyword %s cannot be used in BEGIN/BEG_G/END statements",
                             sym->name);

                dom = tchk[sym->index][0];

                rng = tchk[sym->index][1];

            } else {
                dom = YALL;
                rng = Y(S);
            }

            break;

            case NAME :

            if (!intype && !inWalk)
                exerror ("\n -- undeclared, unmodified names like \"%s\" can only be\nused in N and E statements",
                         sym->name);

            dom = YALL;

            rng = Y(S);

            break;

            default :
            exerror ("\n -- unexpected symbol in typeChk: name %s, lex %d",
                     sym->name, sym->lex);

            break;
    }

    if (dom) {
        if (!intype)
            intype = Y(E)|Y(V);   /* type of $ */

        if (!(dom & intype))
            rng = 0;

    } else if (intype)
        rng = 0;

    return rng;
}

/* typeChkExp:
 * Type check variable expression.
 */
static tctype
typeChkExp (Exref_t* ref, Exid_t* sym) {
    tctype ty;

    if (ref) {
        ty = typeChk (0, ref->symbol);

        for (ref = ref->next; ty && ref; ref = ref->next)
            ty = typeChk (ty, ref->symbol);

        if (!ty)
            return 0;

    } else
        ty = 0;

    return typeChk (ty, sym);
}

/* refval:
 * Called during compilation for uses of references:   abc.x
 * Also for abc.f(..),  type abc.v, "abc".x and CONSTANTS.
 * The grammar has been  altered to disallow the first 3.
 * Type check expressions; return value unused.
 */
Extype_t
refval(Expr_t* pgm, Exnode_t* node, Exid_t* sym, Exref_t* ref,
       char* str, int elt, Exdisc_t* disc) {
    Extype_t        v;

    if (sym->lex == CONSTANT) {
        switch (sym->index) {

                case C_dfs :
                v.integer = TV_dfs;
                break;

                case C_flat :
                v.integer = TV_flat;
                break;

                default :
                v = exzero(node->type);
                break;
        }

    } else {
        if (!typeChkExp (ref, sym))
            exerror ("\n -- type error in %s", deparse (pgm, node, sfstropen()));

        v = exzero(node->type);
    }

    return v;
}

#include <../expr/exop.h>

#if 0 /* not used */
static char*
typeName (int op) {
    if (op > MINTOKEN && op < MAXTOKEN)
        return (char*)exop[op - MINTOKEN];
    else
        return symName(op);
}

#endif

static void
cvtError (Exid_t* xref, char* msg) {
    if (xref)
        error(1, "%s: %s", xref->name, msg);
    else
        error(1, "%s", msg);
}

/*
 * convert:
 * Convert value x of type x->type to type type.
 * Return -1 if conversion cannot be done, 0 otherwise.
 * If arg is > 0, conversion unnecessary; just report possibility.
 * In particular, assume x != 0 if arg == 0.
 */
int
convert(Expr_t* prog, register Exnode_t* x, int type, register Exid_t* xref, int arg, Exdisc_t* disc) {
    Agobj_t*  objp;
    int       ret = -1;

    /* If both types are built-in, let libexpr handle */

    if ((type >= MINTOKEN) && (x->type >= MINTOKEN))
        return -1;

    if (type == T_obj) {
        if (x->type < MINTOKEN)
            ret = 0;

    } else if (type == INTEGER) {
        ret = 0;

    } else if (x->type == T_obj) {
        /* check dynamic type */
        objp = (Agobj_t*)(x->data.constant.value.integer);

        if (arg)
            ret = 0;
        else {
            if (!objp)
                cvtError (xref, "Uninitialized object");
            else
                switch (type) {

                        case T_graph :

                        if (AGTYPE(objp) == AGRAPH)
                            ret = 0;

                        break;

                        case T_node :
                        if (AGTYPE(objp) == AGNODE)
                            ret = 0;

                        break;

                        case T_edge :
                        if (ISEDGE(objp))
                            ret = 0;

                        break;
                }
        }

    } else if (type == STRING) {
        if (arg)
            ret = 0;
        else {
            objp = (Agobj_t*)(x->data.constant.value.integer);

            if (objp) {
                x->data.constant.value.string = agnameof (objp);
                ret = 0;

            } else
                cvtError (xref, "Uninitialized object");
        }
    }

#ifdef UNIMPLEMENTED
    else if (x->type == STRING) {
        /* lookup? */
    }

#endif
    if (ret == 0)
        x->type = type;

    return ret;
}

int strgrpmatch(const char* b, const char* p, int* sub, int n, int flags);
/* matchval:
 * Pattern match strings.
 */
static int
matchval(Expr_t* pgm, Exnode_t* xstr, const char* str, Exnode_t* xpat,
         const char* pat, void* env, Exdisc_t* disc) {
    return strgrpmatch(str, pat, NiL, 0, STR_MAXIMAL|STR_LEFT|STR_RIGHT);
}

/* a2t:
 * Convert type indices to symbolic name.
 */
static int
a2t[] = { 0, FLOATING, INTEGER, STRING,
          T_node, T_edge, T_graph, T_obj };

/* initDisc:
 * Create and initialize expr discipline.
 */
static Exdisc_t*
initDisc (Gpr_t* state) {
    Exdisc_t* dp;

    dp = newof (0, Exdisc_t, 1, 0);

    if (!dp)
        error (3, "could not create libexp discipline: out of memory");

    dp->version = EX_VERSION;

    dp->flags = EX_CHARSTRING|EX_FATAL|EX_UNDECLARED;

    dp->symbols = symbols;

    dp->convertf = convert;

    dp->errorf = (Exerror_f)errorf;

    dp->getf = getval;

    dp->reff = refval;

    dp->setf = setval;

    dp->matchf = matchval;

    dp->types = a2t;

    dp->user = state;

    return dp;
}

/* compile:
 * Compile given string, then extract and return
 * typed expression.
 */
static Exnode_t*
compile (Expr_t* prog, char* src, char* input, int line, char* lbl,
         char* sfx, int kind) {
    Exnode_t*  e;
    Sfio_t*    sf;
    Sfio_t*    prefix;

    /* create input stream */

    if (sfx) {
        sf = sfopen (0, sfx, "rs");

        if (input) {
            prefix = sfopen (0, input, "rs");
            sfstack (sf, prefix);
        }

    } else
        sf = sfopen (0, input, "rs");

    /*  prefixing label if necessary */
    if (lbl) {
        prefix = sfopen (0, 0, "sr+");
        sfprintf (prefix, "%s:\n", lbl);
        sfseek (prefix, 0,0);
        sfstack (sf, prefix);
        line--;
    }

    /* prog is set to exit on errors */
    excomp (prog, src, line, 0, sf);

    sfclose (sf);

    e = exexpr(prog, lbl, NiL, kind);

    return e;
}

/* mkStmts:
 */
static case_stmt*
mkStmts (Expr_t* prog, char* src, case_info* sp, int cnt, char* lbl) {
    case_stmt*    cs;
    int           i;
    Sfio_t*       tmps = sfstropen();

    cs = newof (0, case_stmt, cnt, 0);

    for (i = 0; i < cnt ; i++) {
        if (sp->guard) {
            sfprintf (tmps, "%s_g%d", lbl, i);
            cs[i].guard = compile (prog, src, sp->guard, sp->gstart,
                                   sfstruse(tmps), 0, INTEGER);
        }

        if (sp->action) {
            sfprintf (tmps, "%s_a%d", lbl, i);
            cs[i].action = compile (prog, src, sp->action, sp->astart,
                                    sfstruse(tmps), 0, INTEGER);
        }

        sp = sp->next;
    }

    sfclose (tmps);
    return cs;
}

/* doFlags:
 * Convert command line flags to actions in END_G.
 */
static char*
doFlags (int flags, Sfio_t* s) {
    sfprintf (s, "\n");

    if (flags & SRCOUT)
        sfprintf (s, "$O = $G;\n");

    if (flags & INDUCE)
        sfprintf (s, "induce($O);\n");

    return sfstruse (s);
}

/* compileProg:
 * Convert gpr sections in libexpr program.
 */
comp_prog*
compileProg (parse_prog* inp, Gpr_t* state, int flags) {
    comp_prog*    p;
    Exdisc_t*     dp;
    Sfio_t*       tmps = sfstropen();
    char*         endg_sfx = 0;

    /* Make sure we have enough bits for types */
    assert (BITS_PER_BYTE*sizeof(tctype) >= (1<<TBITS));

    p = newof (0, comp_prog, 1, 0);

    if (!p)
        error (3, "could not create compiled program: out of memory");

    if (flags) {
        endg_sfx = doFlags (flags, tmps);

        if (*endg_sfx == '\0')
            endg_sfx = 0;
    }

    dp = initDisc (state);
    p->prog = exopen (dp);

    codePhase = 0;

    if (inp->begin_stmt)
        p->begin_stmt = compile (p->prog, inp->source, inp->begin_stmt,
                                 inp->l_begin, 0, 0, VOID);

    codePhase = 1;

    if (inp->begg_stmt)
        p->begg_stmt = compile (p->prog, inp->source, inp->begg_stmt,
                                inp->l_beging, "_begin_g", 0, VOID);

    codePhase = 2;

    if (inp->node_stmts) {
        tchk[V_this][1] = Y(V);
        p->n_nstmts = inp->n_nstmts;
        p->node_stmts = mkStmts (p->prog, inp->source, inp->node_stmts,
                                 inp->n_nstmts, "_nd");
    }

    codePhase = 3;

    if (inp->edge_stmts) {
        tchk[V_this][1] = Y(E);
        p->n_estmts = inp->n_estmts;
        p->edge_stmts = mkStmts (p->prog, inp->source, inp->edge_stmts,
                                 inp->n_estmts, "_eg");
    }

    codePhase = 4;

    if (inp->endg_stmt || endg_sfx)
        p->endg_stmt = compile (p->prog, inp->source, inp->endg_stmt,
                                inp->l_endg, "_end_g", endg_sfx, VOID);

    codePhase = 5;

    if (inp->end_stmt)
        p->end_stmt = compile (p->prog, inp->source, inp->end_stmt,
                               inp->l_end, "_end_", 0, VOID);

    sfclose (tmps);

    return p;
}

/* walksGraph;
 * Returns true if program actually has node or edge statements.
 */
int
walksGraph (comp_prog* p) {
    return (p->n_nstmts || p->n_estmts);
}

/* usesGraph;
 * Returns true if program uses the graph, i.e., has
 * N/E/BEG_G/END_G statments
 */
int
usesGraph (comp_prog* p) {
    return (walksGraph(p) || p->begg_stmt || p->endg_stmt);
}

void
ptchk () {
    int i;

    for (i = 0; i <= LAST_M; i++)
        printf ("%d: %d %d\n", i, tchk[i][0], tchk[i][1]);
}

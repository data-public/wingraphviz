/****************************************************************************
 
    PROGRAM: ErrorImp.h
 
    PURPOSE: Implement error process
 
    FUNCTIONS:
 
		errorPrintf() - throw a error message
		ErrPuts() - no implement
		ErrPutc() - no implement
        
    COMMENTS:
        
 
****************************************************************************/

#ifndef		ERRORIMP_H
#define		ERRORIMP_H

#include    <stdio.h>

#define		ERROR_MSG_SIZE 255;

#if defined __cplusplus
extern " C" {
#endif

	void errorPrintf(FILE *, const char *, ...);

	void ErrPuts(const char *, FILE *);

	int ErrPutc(int c, FILE *stream);

	int isError();

	char * getErrMsg();

	void ResetErr();

#if defined __cplusplus
}
#endif



#endif

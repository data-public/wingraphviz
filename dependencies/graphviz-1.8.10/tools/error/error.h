#ifndef ERROR_H
#define ERROR_H
#include <stdio.h>

struct valist {
    char *a0;       /* pointer to first homed integer argument */
    int offset;     /* byte offset of next parameter */
} ;

void errorPrintf(FILE *, const char *, ...);

void ErrPuts(const char *, FILE *);

int ErrPutc( int c, FILE *stream );

void showErrMsg(char *);

#endif

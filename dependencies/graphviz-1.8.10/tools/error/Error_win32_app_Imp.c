/****************************************************************************
 
    PROGRAM: ErrorImp.cpp
 
    PURPOSE: Implement error process
 
    FUNCTIONS:
 
        errorPrintf() - throw a error message
		ErrPuts() - no implement
		ErrPutc() - no implement
        
    COMMENTS:
        
 
****************************************************************************/
#include <stdio.h>
#include <string.h>
#include <TCHAR.H>
#include <STDARG.H>

#define		ERROR_MSG_SIZE 510;

extern void errorPrintf(FILE *, const char *, ...);

extern void ErrPuts(const char *, FILE *);

extern int ErrPutc( int c, FILE *stream );

extern int isError();

extern char * getErrMsg();

extern void ResetErr();

char ERROR_MSG[510];
/**
 * This function throw a error message.
 * 
 * @name : errorPrintf
 * @author ood Tsen
 * @version 0.1, 11/21/2002
 */
void errorPrintf(FILE * f, const char * buf , ...) {

    va_list marker;
    char* name1 = NULL;
    char buf2[255] = {0};
    unsigned int cRemaining;

    va_start( marker, buf );
    _vstprintf( buf2, buf,marker);
    va_end( marker );

    cRemaining = (sizeof(ERROR_MSG) - strlen(ERROR_MSG)) - 1;

    if(cRemaining > 1)
        strncat(ERROR_MSG,buf2,cRemaining);

}


void ErrPuts(const char * buf, FILE * n) {

    _stprintf(ERROR_MSG,"%s%s",ERROR_MSG,buf);


}

int ErrPutc( int c, FILE *stream ) {

    _stprintf(ERROR_MSG,"%s%c",ERROR_MSG,c);

    return 0;
}

int isError() {
    return 1;

    if(_tcslen(ERROR_MSG) > 2)
        return 1;
    else
        return 0;

}

char * getErrMsg() {

    return ERROR_MSG;
}

void ResetErr() {
    memset(ERROR_MSG,0,255);
}

/*
void showError(){
	LPVOID lpMsgBuf;
	DWORD id;
	id = GetLastError();
	if (!FormatMessage( 
		FORMAT_MESSAGE_ALLOCATE_BUFFER | 
		FORMAT_MESSAGE_FROM_SYSTEM | 
		FORMAT_MESSAGE_IGNORE_INSERTS,
		NULL,
		id,
		MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Default language
		(LPTSTR) &lpMsgBuf,
		0,
		NULL ))
	{
	   // Handle the error.
	   return;
	}
 
	// Process any inserts in lpMsgBuf.
	// ...
 
	// Display the string.
	MessageBox( NULL, (LPCTSTR)lpMsgBuf, "Error", MB_OK | MB_ICONINFORMATION );
 
	// Free the buffer.
	LocalFree( lpMsgBuf );
 
}
*/

#ifndef INGRAPHS_H
#define INGRAPHS_H

/* The ingraphs library works with both libagraph and with
 * libgraph, with all user-supplied data. For this to work,
 * the include file relies upon its context to supply a
 * definition of Agraph_t.
 */

#include <stdio.h>

typedef Agraph_t*  (*opengfn)(FILE*);

typedef struct {
    char**     Files;
    int        ctr;
    FILE*      fp;
    opengfn    openf;
}

ingraph_state;

extern ingraph_state* newIngraph (ingraph_state*, char**, opengfn);
extern void closeIngraph (ingraph_state* sp);
extern Agraph_t* nextGraph (ingraph_state*);
extern char* fileName (ingraph_state*);

#endif

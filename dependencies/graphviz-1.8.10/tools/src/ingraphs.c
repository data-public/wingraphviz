/*
    This software may only be used by you under license from AT&T Corp.
    ("AT&T").  A copy of AT&T's Source Code Agreement is available at
    AT&T's Internet website having the URL:
    <http://www.research.att.com/sw/tools/graphviz/license/source.html>
    If you received this software without first entering into a license
    with AT&T, you have an infringing copy of this software and cannot use
    it without violating AT&T's intellectual property rights.
*/

/*
 * Written by Emden Gansner
 */

#include <stdio.h>
#include <stdlib.h>

typedef struct {
    char* dummy;
}

Agraph_t;

#include <ingraphs.h>

/* nextFile:
 * Set next available file.
 * If Files is NULL, we just read from stdin.
 */
static void
nextFile(ingraph_state* sp) {
    FILE    *rv = NULL;

    if (sp->Files == NULL) {
        if (sp->ctr++ == 0) {
            rv = stdin;
        }

    } else {
        while (sp->Files[sp->ctr]) {
            if ((rv = fopen(sp->Files[sp->ctr++],"r")) != 0)
                break;
            else
                fprintf(stderr,"Can't open %s\n",sp->Files[sp->ctr-1]);
        }
    }

    sp->fp = rv;
}

/* nextGraph:
 * Read and return next graph; return NULL if done.
 * Read graph from currently open file. If none, open next file.
 */
Agraph_t*
nextGraph (ingraph_state* sp) {
    Agraph_t*       g;

    if (sp->fp == NULL)
        nextFile(sp);

    g = NULL;

    while (sp->fp != NULL) {
        if (sp->openf) {
            if ((g = sp->openf(sp->fp)) != 0)
                break;

        } else {
            fprintf (stderr, "ingraphs: NULL graph reader\n");
            return 0;
        }

        if (sp->Files)    /* Only close if not using stdin */
            fclose (sp->fp);

        nextFile(sp);
    }

    return g;
}

/* newIngraph:
 * Create new ingraph state. If sp is non-NULL, we
 * assume user is supplying memory.
 * At present, we require opf to be non-NULL. In
 * theory, we could assume a function agread(FILE*,void*)
 */
ingraph_state*
newIngraph (ingraph_state* sp, char** files, opengfn opf) {
    if (!sp) {
        sp = (ingraph_state*)malloc(sizeof(ingraph_state));

        if (!sp) {
            fprintf (stderr, "ingraphs: out of memory\n");
            return 0;
        }
    }

    sp->Files = files;
    sp->ctr = 0;
    sp->fp = NULL;

    if (opf)
        sp->openf = opf;
    else {
        fprintf (stderr, "ingraphs: NULL graph reader\n");
        return 0;
    }

    return sp;
}

/* closeIngraph:
 * Close any open files. Use must handle freeing of memory.
 */
void
closeIngraph (ingraph_state* sp) {
    if (sp->Files && sp->fp)
        fclose (sp->fp);
}

/* fileName:
 * Return name of current file being processed.
 */
char*
fileName (ingraph_state* sp) {
    if (sp->Files) {
        if (sp->ctr)
            return sp->Files[sp->ctr - 1];
        else
            return "<>";

    } else
        return "<stdin>";
}

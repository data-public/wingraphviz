/****************************************************************************
 
    PROGRAM: Info_win32_Imp.c
 
    PURPOSE: GetSystem Infomation
 
    FUNCTIONS:
 
        getSystemFontPath() - throw a error message		
        
    COMMENTS:
        
 
****************************************************************************/
#include <windows.h>

void getSystemFontPath(char * buf,unsigned int size) {

    //	char buf[64] = {0};
    unsigned int flag = 0;

    if (size < 0)
        return;

    flag = GetWindowsDirectory(buf,size - 7);

    if(flag > 0) {
        strcat(buf,"\\Fonts\n");
    }

    return;


}

#include "memory"

using namespace std;

__sgi_alloc * mempool = 0;

extern "C" void mpool_open() {
    if(! mempool)
        mempool = new __sgi_alloc;

}

extern "C" void mpool_close() {
    if(mempool) {
        delete mempool ;
        mempool = 0;
    }
}

extern "C" void * mpool_new(unsigned int s) {
    mpool_open();

    return mempool->allocate(s);
}

extern "C" void * mpool_calloc(size_t num, size_t size) {
    mpool_open();
    void * tmp;
    tmp =mempool->allocate(num * size);

    memset(tmp,0,num * size);

    return tmp;

}

extern "C" void mpool_delete(void * point,unsigned int size) {
    mpool_open();

    mempool->deallocate(point,size);

}


extern void * mpool_calloc(unsigned int num, unsigned int size);
extern void mpool_open();
extern void mpool_close();
extern void * mpool_new(unsigned int s);
extern void mpool_delete(void * point,unsigned int size);

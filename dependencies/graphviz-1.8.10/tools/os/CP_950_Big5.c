#include "CP_950_Big5.h"

char *
get_fontname_CP_950(char *font) {
    char *fontlist;

    fontlist = font;

    if (strcmp(font,"標楷體")==0) {
        fontlist = "KAIU";

    } else if (strcmp(font,"新細明體")==0) {
        fontlist = "MINGLIU.ttc";

    } else if (strcmp(font,"細明體")==0) {
        fontlist = "MINGLIU.ttc";
    }

    return fontlist;
}

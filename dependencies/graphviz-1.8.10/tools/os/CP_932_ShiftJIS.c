#include "CP_932_ShiftJIS.h"

char *
get_fontname_CP_932(char *font) {
    char *fontlist;

    fontlist = font;

    if (strcmp(font,"�l�r �S�V�b�N")==0) {
        fontlist = "msgothic.ttc";

    } else if (strcmp(font,"�l�r �o�S�V�b�N")==0) {
        fontlist = "msgothic.ttc";

    } else if (strcmp(font,"�l�r �o����")==0) {
        fontlist = "msmincho.ttc";

    } else if (strcmp(font,"�l�r ����")==0) {
        fontlist = "msmincho.ttc";

    } else if (strcasecmp(font,"MS Gothic")==0) {
        fontlist = "msgothic.ttc";

    } else if (strcasecmp(font,"Gothic")==0) {
        fontlist = "msgothic.ttc";

    } else if (strcasecmp(font,"Mincho")==0) {
        fontlist = "msmincho.ttc";
    }

    return fontlist;
}

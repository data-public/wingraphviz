
/*
 * Glenn Fowler
 * AT&T Research
 *
 * expression library
 */

#include <exlib.h>

/*
 * library error handler
 */

void
exerror(const char* format, ...) {
    Sfio_t*	sp;

    if (expr.program->disc->errorf && !expr.program->errors && (sp = sfstropen())) {
        va_list	ap;
        char*	s;
        char	buf[64];

        expr.program->errors = 1;
        excontext(expr.program, buf, sizeof(buf));
        sfputr(sp, buf, -1);
        va_start(ap, format);
        sfvprintf(sp, format, ap);
        va_end(ap);
        s = sfstruse(sp);
        (*expr.program->disc->errorf)(expr.program, expr.program->disc, (expr.program->disc->flags & EX_FATAL) ? 3 : 2, "%s", s);
        sfclose(sp);

    } else if (expr.program->disc->flags & EX_FATAL)
        exit(1);
}

#include <grid.h>

#ifdef DMALLOC
#include "dmalloc.h"
#endif

/* utility fn for cut_tile */
static void ERmovesegs(ERview_t *D, Tile_t *old, Tile_t *new, Dir_t side) {
    Seglist_t	*seglist;
    Seg_t		*seg;
    int			i;

    seglist = old->segs[side];

    for (i = 0; (seg = seglist->list[i]); i++) {
        if (seg->b[0] == old)
            seg->b[0] = new;
        else if (seg->b[1] == old)
            seg->b[1] = new;
        else
            abort();

        ERseglist_append(D,new->segs[side],seg);

        seglist->list[i] = NIL(Seg_t*);
    }

    old->segs[side]->size = 0;
}

/* utility fn for cut_tile */
static void ERsortsegs(ERview_t *D, Tile_t *old, Tile_t *b0, Tile_t *b1, Dir_t side) {
    Tile_t		*nb,*ob;
    Seglist_t	*seglist,*s0,*s1;
    Seg_t		*seg;
    int			i,c;

    seglist = old->segs[side];
    s0 = b0->segs[side];
    s1 = b1->segs[side];

    if ((side == north) || (side == south))
        c = 0;
    else
        c = 1;

    for (i = 0; (seg = seglist->list[i]); i++) {
        if (seg->p[1].c[c] <= b0->UR.c[c]) {
            ERseglist_append(D,s0,seg);
            nb=b0;

        } else if (seg->p[0].c[c] >= b1->LL.c[c]) {
            ERseglist_append(D,s1,seg);
            nb=b1;

        } else {
            assert(NONDECREASING(seg->p[0].c[c],b0->UR.c[c],seg->p[1].c[c]));

            if (seg->b[0] != old)
                ob = seg->b[0];
            else
                ob = seg->b[1];

            ERseglist_delete(ob->segs[opposite(side)],seg);

            /*install_new_seg(seg->p[0],b0->UR,seg->kind,b0,side,ob);
            ERinstall_new_seg(D,b1->LL,seg->p[1],seg->kind,b1,side,ob);*/

            ERinstall_new_seg(D,seg->p[0],ERcombine(seg->p[0],b0->UR,(ilbool)c),
                              seg->kind,b0,side,ob);

            ERinstall_new_seg(D,ERcombine(seg->p[1],b1->LL,(ilbool)c),seg->p[1],
                              seg->kind,b1,side,ob);

            ERfree_seg(D,seg);

            continue;
        }

        if (seg->b[0] == old)
            seg->b[0] = nb;
        else
            seg->b[1] = nb;

        seglist->list[i] = NIL(Seg_t*);
    }

    seglist->size = 0;
}


/* slice a box in two.  destroys old box.  sub-boxes are installed
 * in the diagram with the new segment lists.  try to re-use old segs.
 */
void ERcut_tile(ERview_t *D, Tile_t *b, ilbool horizontal, Pt_t p) {
    Pt_t	p0,p1;
    Tile_t	*b0,*b1;
    Dir_t	lowside;

    if (horizontal) {
        p0 = ERmkpoint(x(b->LL),y(p));
        p1 = ERmkpoint(x(b->UR),y(p));
        lowside = north;

    } else {
        p0 = ERmkpoint(x(p),y(b->LL));
        p1 = ERmkpoint(x(p),y(b->UR));
        lowside = east;
    }

    b0 = ERtile(D,b->LL,p1);
    b1 = ERtile(D,p0,b->UR);
    ERinstall_new_seg(D,p0, p1, s_plain, b0, lowside, b1);
    ERmovesegs(D,b,b0,opposite(lowside));
    ERmovesegs(D,b,b1,lowside);
    ERsortsegs(D,b,b0,b1,(lowside+1)%NSIDES);
    ERsortsegs(D,b,b0,b1,(lowside+3)%NSIDES);
    ERtileset_delete(D->config,b);
    ERtileset_append(D,D->config,b0);
    ERtileset_append(D,D->config,b1);
    ERfree_tile(D,b);
}

static ilbool tile_in_set(Tile_t *b, Tileset_t *set
                         ) {
    int		i;
    Tile_t	*cb;

    for (i = 0; (cb = set
                          ->list[i]); i++)
            if (cb == b)
                return TRUE;

    return FALSE;
}

static ilbool seg_in_list(Seg_t *seg, Seglist_t *set
                         ) {
    int		i;
    Seg_t	*cseg;

    for (i = 0; (cseg = set
                            ->list[i]); i++)
            if (cseg == seg)
                return TRUE;

    return FALSE;
}


static void erchecksegs(ERview_t *d) {
    int		i,j,side;
    Tile_t	*b;
    Seg_t	*seg;

    for (i = 0; (b = d->config->list[i]); i++) {
        for (side = 0; side < NSIDES; side++) {
            for (j = 0; (seg = b->segs[side]->list[j]); j++) {
                if (seg->b[0] == b) {
                    assert(seg->b[1] != b);
                    assert(tile_in_set(seg->b[1],d->config));
                    assert(seg_in_list(seg,seg->b[1]->segs[opposite(side)]));

                } else if (seg->b[1] == b) {
                    assert(seg->b[0] != b);
                    assert(tile_in_set(seg->b[0],d->config));
                    assert(seg_in_list(seg,seg->b[0]->segs[opposite(side)]));

                } else
                    abort();
            }
        }
    }
}

/* split the configuration by a given line segment */
void ERsplit_config(ERview_t *D, Pt_t p, Pt_t q) {
    Tile_t	*this_b, *last_b,  *prev_b, *next_b;
    int		c,d;

    if (pt_eq(p,q))
        return;

    if (y(p) == y(q)) {
        c = 0;
        d = 1;

    } else {
        c = 1;
        d = 0;
    }

    if (p.c[c] > q.c[c]) {
        Pt_t tmp;
        tmp = p;
        p = q;
        q = tmp;
    }

    this_b = ERlocate(D,p);
    last_b = ERlocate(D,q);

    do  {
        next_b = ERneighbor(this_b, q);
        /* need nondegenerate intersection */

        if ((p.c[c] < this_b->UR.c[c]) && (q.c[c] > this_b->LL.c[c])) {
            if ((this_b->LL.c[d] < p.c[d]) && (p.c[d] < this_b->UR.c[d]))
                ERcut_tile(D, this_b, (ilbool)d, p);
        }

        prev_b = this_b;
        this_b = next_b;
        erchecksegs(D);

    } while ((prev_b != last_b) && (prev_b != this_b));
}

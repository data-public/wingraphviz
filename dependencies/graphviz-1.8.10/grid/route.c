/* edge routing - shortest path */

#include <grid.h>

#ifdef DMALLOC
#include "dmalloc.h"
#endif

static double Minsep;

/* eventually we could should the active segment set */
static Seg_t *get_best_seg(ERview_t *D) {
    int		i,j;
    Tile_t	*b;
    Dir_t	side;
    Seg_t	*seg,*best = NIL(Seg_t*);

    for (i = 0; (b = D->config->list[i]); i++) {
        for (side = 0; side < 2; side++) {
            for (j = 0; (seg = b->segs[side]->list[j]); j++) {
                if (seg->sp.finalized)
                    continue;

                if ((best == NIL(Seg_t*)) ||  (best->sp.cost > seg->sp.cost))
                    best = seg;
            }
        }
    }

    assert(best != NIL(Seg_t*));
    best->sp.finalized = TRUE;
    return best;
}

static ilbool collinear(Seg_t *s0, Seg_t *s1) {
    int		d;

    for (d = 0; d < 2; d++) {
        if (s0->p[0].c[d] == s0->p[1].c[d]) {
            return ((s0->p[1].c[d] == s1->p[0].c[d])
                    &&   (s1->p[0].c[d] == s1->p[1].c[d]));
        }
    }

    return FALSE;
}

static ilbool near_obj(Pt_t p, Seglist_t *seglist) {
    int		i;
    double	dist;
    Seg_t	*seg;

    for (i = 0; (seg = seglist->list[i]); i++) {
        if (seg->kind == s_plain)
            continue;

        if (ERhorizontal(seg))
            dist = y(p) - y(seg->p[0]);
        else
            dist = x(p) - x(seg->p[0]);

        if (fabs(dist) < Minsep)
            return TRUE;		/* actually could yield 2*Minsep */
    }

    return FALSE;
}

static void set_window(Seg_t *seg, Pt_t *win) {
    double	adj,ww;
    int		i,j,d;
    double	a[2];
    Tile_t	*b;
    Dir_t	dir[2];

    if (seg->sp.is_terminal == s_node) {
        win[0] = win[1] = seg->sp.loc;
        return;
    }

    if (ERhorizontal(seg))
        d = 0;
    else
        d = 1;

    /* can only get more narrow */
    win[0] = seg->sp.win[0];

    win[1] = seg->sp.win[1];

    adj = Minsep;

    ww = (win[1].c[d] - win[0].c[d]) * .1;

    if (adj > ww)
        adj = ww;

    a[0] = adj;

    a[1] = -adj;

    if (ERhorizontal(seg))
        dir[0] = west;
    else
        dir[0] = south;

    dir[1] = opposite(dir[0]);

    for (i = 0; i < 2; i++) {
        for (j = 0; j < 2; j++) {
            b = seg->b[j];

            if (near_obj(win[i],b->segs[dir[i]])) {
                win[i].c[d] += a[i];
                break;
            }
        }
    }
}

static double costfun(double dist, int bends, Segkind_t seg) {
    double	rv;

    if (seg == s_forbidden)
        return MAXFLOAT;

    rv = dist * K_DIST + bends * K_BEND + K_CROSSING * (seg == s_edge);

    return rv;
}

static void gv_restrict (Pt_t *win, Pt_t *gate, int d) {
    if (win[0].c[d] < gate[0].c[d])
        win[0].c[d] = gate[0].c[d];

    if (win[1].c[d] > gate[1].c[d])
        win[1].c[d] = gate[1].c[d];
}

static Pt_t u_turn_delta(Seg_t *dst, Seg_t *src, Tile_t *via) {
    double	dx,dy,boxfraction;

    if (ERhorizontal(dst)) {
        dx = 0.0;
        boxfraction = (y(via->UR) - y(via->LL)) / 10.0;
        dy = MIN(Minsep,boxfraction);

        if (y(dst->sp.loc) == y(via->UR))
            dy = -dy;

    } else {
        dy = 0.0;
        boxfraction = (x(via->UR) - x(via->LL)) / 10.0;
        dx = MIN(Minsep,boxfraction);

        if (x(dst->sp.loc) == x(via->UR))
            dx = -dx;
    }

    return ERmkpoint(dx,dy);
}

static Label_t route_opposing_segs(Seg_t *seg, Seg_t *from, Tile_t *via) {
    Label_t		rv;
    int			d,e,bends;
    double		d0,d1,d2,dist;

    rv.finalized = seg->sp.finalized;
    rv.is_terminal = seg->sp.is_terminal;

    if (ERhorizontal(seg))
        d = 0;
    else
        d = 1;

    e = NOT(d);

    set_window(seg,rv.win);

    d2 = fabs(from->sp.win[0].c[e] - rv.win[0].c[e]);

    /* need a jog? */
    if ((from->sp.win[0].c[d] > rv.win[1].c[d]) ||
            (from->sp.win[1].c[d] < rv.win[0].c[d])) {
        bends = 2;
        d0 = fabs(from->sp.loc.c[d] - rv.win[0].c[d]);
        d1 = fabs(from->sp.loc.c[d] - rv.win[1].c[d]);

        if (d0 < d1) {
            dist = d0;
            rv.loc.c[d] = rv.win[0].c[d];

        } else		 {
            dist = d1;
            rv.loc.c[d] = rv.win[1].c[d];
        }

        rv.loc.c[e] = rv.win[0].c[e];
        rv.trans = t_jog;

    } else {	/* straight, but may make window more narrow */
        gv_restrict(rv.win,from->sp.win,d);
        bends = 0;	/* jog was already made somewhere else */

        if (NONDECREASING(rv.win[0].c[d],from->sp.loc.c[d],rv.win[1].c[d])) {
            dist = 0;
            rv.loc.c[d] = from->sp.loc.c[d];

        } else {
            d0 = fabs(from->sp.loc.c[d] - rv.win[0].c[d]);
            d1 = fabs(from->sp.loc.c[d] - rv.win[1].c[d]);

            if (d0 < d1) {
                dist = d0;
                rv.loc.c[d] = rv.win[0].c[d];

            } else		 {
                dist = d1;
                rv.loc.c[d] = rv.win[1].c[d];
            }
        }

        rv.loc.c[e] = rv.win[0].c[e];
        rv.trans = t_straight;
    }

    dist = dist + d2;
    rv.cost = costfun(dist,bends,seg->kind) + from->sp.cost;
    rv.prev_seg = from;
    rv.tile = via;
    return rv;
}

static Label_t route_collinear_segs(Seg_t *seg, Seg_t *from, Tile_t *via) {
    Label_t		rv;
    double		d0,d1,dd,dist;
    Pt_t		ut_delta;
    int			d,e;

    rv.finalized = seg->sp.finalized;
    rv.is_terminal = seg->sp.is_terminal;

    if (ERhorizontal(seg))
        d = 0;
    else
        d = 1;

    e = NOT(d);

    set_window(seg,rv.win);

    d0 = fabs(from->sp.loc.c[d] - rv.win[0].c[d]);

    d1 = fabs(from->sp.loc.c[d] - rv.win[1].c[d]);

    ut_delta = u_turn_delta(seg,from,via);

    dd = 2 * fabs(ut_delta.c[e]);

    if (d0 < d1) {
        dist = dd + d0;
        rv.loc = rv.win[0];

    } else {
        dist = dd + d1;
        rv.loc = rv.win[1];
    }

    rv.cost = costfun(dist,2,seg->kind) + from->sp.cost;
    rv.prev_seg = from;
    rv.tile = via;
    rv.trans = t_uturn;
    return rv;
}

static Label_t route_orthogonal_segs(Seg_t *seg, Seg_t *from, Tile_t *via) {
    Label_t		rv;
    double		d0,d1,d2,dist;
    int			d,e;

    rv.finalized = seg->sp.finalized;
    rv.is_terminal = seg->sp.is_terminal;

    if (ERhorizontal(seg)) {
        d = 0;
        e = 1;

    } else {
        d = 1;
        e = 0;
    }

    set_window(seg,rv.win);
    d0 = fabs(rv.win[0].c[e] - from->sp.loc.c[e]);
    d1 = fabs(rv.win[0].c[d] - from->sp.loc.c[d]);
    d2 = fabs(rv.win[1].c[d] - from->sp.loc.c[d]);

    if (d1 < d2) {
        dist = d0 + d1;
        rv.loc = rv.win[0];

    } else {
        dist = d0 + d2;
        rv.loc = rv.win[1];
    }

    rv.cost = costfun(dist,1,seg->kind) + from->sp.cost;
    rv.prev_seg = from;
    rv.tile = via;
    rv.trans = t_bend;
    return rv;
}

static void relabel_seg(Seg_t *seg, Seg_t *fromseg, Tile_t *via) {
    Label_t		srch;

    if (ERhorizontal(seg) != ERhorizontal(fromseg))
        srch = route_orthogonal_segs(seg,fromseg,via);
    else if (collinear(seg,fromseg))
        srch = route_collinear_segs(seg,fromseg,via);
    else
        srch = route_opposing_segs(seg,fromseg,via);

    if (srch.cost < seg->sp.cost)
        seg->sp = srch;
}

/* update costs of the neighbors of a segment.
 * each segment separates two or more boxes. */
static void relabel_neighbors(Seg_t *seg) {
    int			i,j;
    Tile_t		*b;
    Dir_t		side;
    Seglist_t	*seglist;
    Seg_t		*nseg;

    for (i = 0; i < 2; i++) {
        b = seg->b[i];

        for (side = 0; side < NSIDES; side++) {
            seglist = b->segs[side];

            for (j = 0; (nseg = seglist->list[j]); j++) {
                if ((nseg->kind == s_node) && nseg->sp.is_terminal == FALSE)
                    continue;

                if (nseg->sp.cost > seg->sp.cost)
                    relabel_seg(nseg,seg,b);
            }
        }
    }
}

/* make 'loc' values consistent in consecutive segment windows.
 * this works by tracing backward, because segment windows can
 * only become more restricted, never wider
 */
static void fix_windows(Seg_t *end) {
    Seg_t	*prev, *seg, *next;
    Pt_t	p;
    int		d;
    double	p0,p1,p2;

    /* switch notation of prev and next because scanning backward */
    prev = NIL(Seg_t*);
    seg = end;

    while (seg) {
        next = seg->sp.prev_seg;
        p = seg->sp.loc;

        if (seg->sp.trans == t_straight) {
            if (ERhorizontal(next))
                x(next->sp.loc) = x(p);
            else
                y(next->sp.loc) = y(p);

        } else {
            /* may be able to balance jogs in windows */

            if (prev && next && (prev->sp.trans != t_straight)) {
                if (ERhorizontal(seg))
                    d = 0;
                else
                    d = 1;

                p0 = prev->sp.loc.c[d];

                p1 = seg->sp.loc.c[d];

                p2 = next->sp.loc.c[d];

                if ( ((p0 <= p1) && (p1 <= p2)) || ((p0 >= p1) && (p1 >= p2)) )
                    seg->sp.loc.c[d] =
                        (seg->sp.win[0].c[d] + seg->sp.win[1].c[d]) / 2;
            }
        }

        prev = seg;
        seg = next;
    }
}

static void chk(Pt_t *p, int i) {
    i--;

    if (i > 0)
        assert((x(p[i]) == x(p[i-1])) || (y(p[i]) == y(p[i-1])));
}

/* return list of routing points for the shortest path that was found */
static ilshape_t *segroute(ERview_t *D, Seg_t *end) {
    int			i;
    Tile_t		*b;
    Seg_t		*seg,*prev;
    Pt_t		*p,q,fst,lst,dd;
    ilbool		horz;
    ilshape_t	*rv;
    ilcurve_t	*route;

    i = 0;

    for (seg = end; seg; seg = seg->sp.prev_seg)
        i += 3;

    route = il_newcurve(agheap(ergraph(D)),IL_POLYLINE, i + 1);

    i = 0;

    p = (Pt_t*)route->p;

    for (seg = end; (prev = seg->sp.prev_seg); seg = prev) {
        horz = ERhorizontal(seg);
        fst = seg->sp.loc;
        lst = prev->sp.loc;
        b = seg->sp.tile;

        switch (seg->sp.trans) {

                case t_straight:
                p[i++] = fst;
                chk(p,i);
                break;

                case t_jog:
                q = ERavgpt(fst,lst);
                p[i++] = fst;
                chk(p,i);
                p[i++] = ERcombine(fst,q,horz);
                chk(p,i);
                p[i++] = ERcombine(lst,q,horz);
                chk(p,i);
                break;

                case t_bend:
                p[i++] = fst;
                chk(p,i);
                p[i++] = ERcombine(fst,lst,horz);
                chk(p,i);
                break;

                case t_uturn:
                dd = u_turn_delta(seg,prev,b);
                p[i++] = fst;
                chk(p,i);
                p[i++] = ERaddpoint(fst,dd);
                chk(p,i);
                p[i++] = ERaddpoint(lst,dd);
                chk(p,i);
                break;

                default:
                abort();
        }
    }

    p[i++] = seg->sp.loc;
    chk(p,i);
    route->n = i;
    rv = il_newshape(agheap(ergraph(D)),route,NIL(ilshape_t*));
    return rv;
}

static ilshape_t *construct_route(ERview_t *D, Seg_t *end) {
    ilshape_t	*rv;

    fix_windows(end);
    rv = segroute(D,end);
    return rv;
}

static void init_segments(ERview_t *D) {
    int		i,j;
    Dir_t	side;
    Tile_t	*b;
    Seg_t	*seg;

    for (i = 0; (b = D->config->list[i]); i++) {
        for (side = 0; side < 2; side++) {
            for (j = 0; (seg = b->segs[side]->list[j]); j++) {
                seg->sp.cost = MAXFLOAT;
                seg->sp.finalized = FALSE;
                seg->sp.is_terminal = FALSE;
                seg->sp.prev_seg = NIL(Seg_t*);
                seg->sp.tile = NIL(Tile_t*);
                seg->sp.win[0] = seg->p[0];
                seg->sp.win[1] = seg->p[1];
            }
        }
    }
}

ilshape_t *ERauto_route(ERview_t *D, Tile_t *arg_end, Pt_t ep,
                        Tile_t *arg_start, Pt_t sp) {
    Seg_t		*seg,*start_seg,*end_seg;
    ilshape_t	*route;
    Tile_t		*start,*end;
    Pt_t		sep;

    ERlocate_endpoint(D,arg_start,sp,&start,&start_seg);
    ERlocate_endpoint(D,arg_end,ep,&end,&end_seg);

    if ((start_seg == NIL(Seg_t*)) || (end_seg == NIL(Seg_t*)))
        return NIL(ilshape_t*);

    init_segments(D);

    ERmark_container_segs(D,start,s_plain);

    ERmark_container_segs(D,end,s_plain);

    start_seg->sp.loc = sp;

    start_seg->sp.win[0] = start_seg->sp.win[1] = sp;

    start_seg->sp.trans = t_none;

    start_seg->sp.cost = 0.0;

    end_seg->sp.loc = ep;

    end_seg->sp.win[0] = end_seg->sp.win[1] = ep;

    end_seg->sp.is_terminal = TRUE;

    sep = ERpt(il_nodesep(BASE(D)));

    Minsep = MAX(x(sep),y(sep));

    while (end_seg->sp.finalized == FALSE) {
        seg = get_best_seg(D);
        relabel_neighbors(seg);
    }

    route = construct_route(D,end_seg);
    ERuser_route(D,route);

    ERmark_container_segs(D,start,s_node);
    ERmark_container_segs(D,end,s_node);
    return route;
}

/* add a new node to the configuration */

Tile_t *ERnodetile(ERview_t *D, Pt_t LL, Pt_t UR) {
    int			i;
    Tile_t		*b,*nb;
    Pt_t		corner[4];
    Dir_t		side;

    b = ERtile(D,LL,UR);

    for (i = 0; (nb = D->nodes->list[i]); i++) {
        if (ERtiles_nontrivially_intersect(b,nb) &&
                (ERtile_covers_tile(b,nb) == FALSE) &&
                (ERtile_covers_tile(nb,b) == FALSE) ) {
            ERfree_tile(D,b);
            b = NIL(Tile_t*);
            break;
        }

    }

    if (b) {
        ERcorners(b,corner);

        for (side = 0; side < NSIDES; side++)
            ERsplit_config(D,corner[side],corner[(side+1)%4]);

        for (side = 0; side < NSIDES; side++)
            ERmark_segs(D,corner[side],corner[(side+1)%NSIDES],s_node);

        ERtileset_append(D,D->nodes,b);
    }

    return b;
}

Tile_t *ERtile(ERview_t *D, Pt_t LL, Pt_t UR) {
    Tile_t	*rv;
    int		i;
    static int id = 1;

    rv = agalloc(ergraph(D),sizeof(Tile_t));
    rv->LL = LL;
    rv->UR = UR;

    for (i = 0; i < NSIDES; i++)
        rv->segs[i] = ERmake_seglist(D);

    rv->id = id++;

    return rv;
}

/* add a new route to the configuration */
void ERuser_route(ERview_t *D, ilshape_t *user) {
    int			i;
    ilshape_t	*sptr;
    ilcurve_t	*crv;

    for (sptr = user; sptr; sptr = sptr->next) {
        if (sptr->type != IL_POLYGON)
            continue;

        crv = &(sptr->def.curve);

        for (i = 0; i < crv->n - 1; i++)
            ERsplit_config(D,ERpt(crv->p[i]),ERpt(crv->p[i+1]));

        for (i = 0; i < crv->n - 1; i++)
            ERmark_segs(D,ERpt(crv->p[i]),ERpt(crv->p[i+1]),s_edge);
    }
}

void ERnode_remove(ERview_t *D, Tile_t *tile) {}

void ERroute_remove(ERview_t *D, ilshape_t *route) {}

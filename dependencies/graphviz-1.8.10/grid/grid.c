#include <grid.h>

#ifdef DMALLOC
#include "dmalloc.h"
#endif

/* OrthoGrid engine interface.
 * maps between graph model and layout tiles.
 */

#define ilpt_eq(p,q)  ((p.x == q.x) && (p.y == q.y))

#ifdef NEWPORTS
static ilcoord_t Origin;
#endif

/* utilities */

static ilcoord_t add_pt(ilcoord_t p0, ilcoord_t p1) {
    ilcoord_t	rv;
    rv.x = p0.x + p1.x;
    rv.y = p0.y + p1.y;
    return rv;
}

static ilcoord_t sub_pt(ilcoord_t p0, ilcoord_t p1) {
    ilcoord_t	rv;
    rv.x = p0.x - p1.x;
    rv.y = p0.y - p1.y;
    return rv;
}

static ilcoord_t avg_pt(ilcoord_t p0, ilcoord_t p1) {
    ilcoord_t	rv;
    rv.x = (p0.x + p1.x) / 2.0;
    rv.y = (p0.y + p1.y) / 2.0;
    return rv;
}


Pt_t ERpt(ilcoord_t p) {
    Pt_t	rv;
    rv.c[0] = p.x;
    rv.c[1] = p.y;
    return rv;
}

static ilcoord_t ilc(Pt_t p) {
    ilcoord_t	rv;
    rv.x = x(p);
    rv.y = y(p);
    return rv;
}

static double manhattan_dist(ilcoord_t p, ilcoord_t q) {
    return fabs(p.x - q.x) + fabs(p.y - q.y);
}

static ilcoord_t mkpt(double x, double y) {
    ilcoord_t rv;
    rv.x = x;
    rv.y = y;
    return rv;
}

static ilcoord_t snap_port(ERnode_t *er_node, ilcoord_t p) {
    ilcoord_t 	ll,ur;
    ilcoord_t	rv,tp;
    double		d,d0;

    ll = ilc(er_node->tile->LL);
    ur = ilc(er_node->tile->UR);

    p.x = MAX(ll.x,p.x);
    p.x = MIN(p.x,ur.x);
    p.y = MAX(ll.y,p.y);
    p.y = MIN(p.y,ur.y);

    /* west */
    rv = mkpt(ll.x,p.y);
    d = manhattan_dist(rv,p);

    /* south */
    tp = mkpt(p.x,ll.y);
    d0 = manhattan_dist(tp,p);

    if (d0 < d) {
        d = d0;
        rv = tp;
    }

    /* east */
    tp = mkpt(ur.x,p.y);

    d0 = manhattan_dist(tp,p);

    if (d0 < d) {
        d = d0;
        rv = tp;
    }

    /* north */
    tp = mkpt(p.x,ur.y);

    d0 = manhattan_dist(tp,p);

    if (d0 < d) {
        d = d0;
        rv = tp;
    }

    return rv;
}

ilbool ERopen(ILview_t *client) {
    ERview_t		*rv;
    extern ILengine_t OrthoGrid;
    unsigned int S[] = {
                           sizeof(ERview_t),sizeof(ERnode_t),sizeof(ERedge_t)
                       };

    rv = (ERview_t*) il_open_view(&OrthoGrid, client, Agundirected, S);
    rv->config = ERmake_tileset(rv);
    rv->nodes = ERmake_tileset(rv);
    ERtileset_append(rv,rv->config,ERtile(rv,ERmkpoint(-10000,-10000),ERmkpoint(10000,10000)));
    return TRUE;
}

void ERclose(ILview_t *D) {
    il_close_view(D->pvt);
}

static Tile_t	*find_tile(ERview_t *D, ilcoord_t size) {
    Tile_t	*b;
    Seg_t	*seg;
    int		i, j, side;

    for (i = 0; (b = D->config->list[i]); i++) {
        if ((x(b->UR) < 0) || (y(b->UR) < 0))
            continue;

        for (side = 0; side < NSIDES; side++) {
            for (j = 0; (seg = b->segs[side]->list[j]); j++) {
                if (seg->kind == s_plain) {
                    if ((y(b->UR) - y(b->LL) > size.y)
                            && (x(b->UR) - x(b->LL) > size.x))
                        return b;
                }
            }
        }
    }

    return NIL(Tile_t*);
}

static ilcoord_t find_place(ERview_t *D, Tile_t *tile, ilcoord_t sz) {
    Tile_t	*nb, *pick;
    int		i;
    ilcoord_t	pos,sep;

    sep = BASE(D)->client->separation;
    pick = NIL(Tile_t*);

    for (i = 0; (nb = D->nodes->list[i]); i++) {
        if (drand48() < 1/(i+1))
            pick = nb;
    }

    if (pick) {
        pos = ilc(ERavgpt(pick->LL, pick->UR));

        if (pos.x < x(tile->LL) + sep.x)
            pos.x = x(tile->LL) + sep.x;

        if (pos.x > x(tile->UR) - sep.x)
            pos.x = x(tile->UR) - sep.x;

        if (pos.y < y(tile->LL) + sep.y)
            pos.y = y(tile->LL) + sep.y;

        if (pos.y > y(tile->UR) - sep.y)
            pos.y = y(tile->UR) - sep.y;

    } else {
        pos.x = pos.y = 0;
    }

    return pos;
}

ilbool ERinsnode(ILview_t *view, ILnode_t *spec) {
    ERview_t	*D;
    engview_t	*baseview;
    Agnode_t	*n;
    ilrect_t	rect;
    ildelta 	sz;
    ilcoord_t	pos;
    Pt_t		ll,ur;
    Tile_t		*tile,*empty;

    baseview = view->pvt;
    D = (ERview_t*)baseview;

    /* don't insert twice */

    if (il_find_node(baseview,spec))
        return FALSE;

    rect = il_get_bounding_rect(spec->shape);

    sz.x = (rect.ur.x - rect.ll.x) / 2.0;

    sz.y = (rect.ur.y - rect.ll.y) / 2.0;

    if (spec->pos_valid)
        pos = spec->pos;
    else {
        empty = find_tile(D,sz);
        pos = find_place(D,empty,sz);
    }

    ll = ERpt(sub_pt(pos,sz));
    ur = ERpt(add_pt(pos,sz));

    if ((tile = ERnodetile(D,ll,ur))) {
        n = il_open_node(baseview,spec);
        er_nd(n)->tile = tile;
        il_register_node_callback(baseview,spec,IL_INS);
    }

    return TRUE;
}

ilbool ERinsedge(ILview_t *view, ILedge_t *spec) {
    ERview_t	*D;
    engview_t	*baseview;
    Agnode_t	*vsrc,*vdst;
    ERnode_t	*src,*dst;
    ilcoord_t	src_pt,dst_pt;
    ilshape_t	*route;
    Agedge_t	*e;

    baseview = view->pvt;
    D = (ERview_t*)baseview;

    vsrc = il_find_node(baseview,(ILnode_t*)(spec->tail.term));
    vdst = il_find_node(baseview,(ILnode_t*)(spec->head.term));

    if (il_find_edge(baseview,spec))
        return FALSE;

    if (vsrc && vdst) {
        src = er_nd(vsrc);
        dst = er_nd(vdst);
#ifndef NEWPORTS

        if (spec->pos && (spec->pos->def.curve.n > 0)) {
            src_pt = spec->pos->def.curve.p[0];
            dst_pt = spec->pos->def.curve.p[spec->pos->def.curve.n - 1];

        } else {
            src_pt = avg_pt(ilc(src->tile->LL), ilc(src->tile->UR));
            dst_pt = avg_pt(ilc(dst->tile->LL), ilc(dst->tile->UR));
        }

#else
        src_pt = spec->tail.port;

        if (ilpt_eq(src_pt,Origin))
            src_pt = avg_pt(ilc(dst->tile->LL), ilc(dst->tile->UR));

        dst_pt = spec->head.port;

        if (ilpt_eq(dst_pt,Origin))
            dst_pt = avg_pt(ilc(src->tile->LL), ilc(src->tile->UR));

#endif

        src_pt = snap_port(src,src_pt);

        dst_pt = snap_port(dst,dst_pt);

        route = ERauto_route(D, src->tile, ERpt(src_pt), dst->tile, ERpt(dst_pt));

        if (route) {
            e = il_open_edge(baseview,spec);
            il_register_edge_callback(baseview,spec,IL_INS);
            er_ed(e)->route = route;	/* ### dead? */
            il_edge(e)->pos = route;
            return TRUE;
        }

        return TRUE;

    } else
        return FALSE;
}

static ilbool callback(ILview_t *view, ilbool flag) {
    ilbool	rv;

    if (view->enable_immediate_callbacks || flag) {
        rv = il_issue_callbacks(view->pvt);
        il_clear_callbacks(view->pvt);

    } else
        rv = FALSE;

    return rv;
}


static void deledge(ILview_t *view, Agedge_t *view_e) {
    ERview_t	*D;
    engview_t	*baseview;
    ERedge_t	*edata;
    ILedge_t	*spec;

    baseview = view->pvt;
    D = (ERview_t*)baseview;

    /* remove edge route from tile set */
    edata = er_ed(view_e);
    ERroute_remove(D, edata->route);

    /* mark edge spec as deleted */
    spec = il_edge(view_e);
    spec->pos = NIL(ilshape_t*);

    /* set up callback */
    il_register_edge_callback(baseview, spec, IL_DEL);
}

ilbool ERdeledge(ILview_t *view, ILedge_t *spec) {
    Agedge_t	*e;

    e = il_find_edge(view->pvt, spec);

    if (e) {
        deledge(view,e);
        il_close_edge(view->pvt,spec);
        return TRUE;
    }

    return FALSE;
}

ilbool ERdelnode(ILview_t *view, ILnode_t *spec) {
    ERview_t	*D;
    engview_t	*baseview;
    Agnode_t	*n;
    Agedge_t	*e,*f;
    ILnode_t	*myspec;

    baseview = view->pvt;
    D = (ERview_t*)baseview;
    n =il_find_node(baseview,spec);

    if (n) {
        for (e = agfstedge(n); e; e = f) {
            f = agnxtedge(e,n);
            deledge(view,e);
        }

        myspec = er_nd(n)->base.client;
        myspec->pos_valid = FALSE;
        ERnode_remove(D,er_nd(n)->tile);

        /* set up callback */
        il_register_node_callback(baseview, spec, IL_DEL);
        return TRUE;
    }

    return FALSE;
}

ilbool ERins(ILview_t *view, ILobj_t *spec) {
    static ILfnlist_t f = {ERinsnode,ERinsedge,NIL(ILhyperfn_t),NIL(ILviewfn_t)};
    ilbool rv;

    rv = ildispatch(view,spec,&f);
    callback(view,FALSE);
    return rv;
}


ilbool ERmov(ILview_t *view, ILobj_t *spec) {
    return FALSE;
}

ilbool ERopt(ILview_t *view, ILobj_t *spec) {
    return FALSE;
}

ilbool ERdel(ILview_t *view, ILobj_t *spec) {
    static ILfnlist_t f = {ERdelnode,ERdeledge,NIL(ILhyperfn_t)};
    ilbool rv;

    rv = ildispatch(view,spec,&f);
    ildispatch(view,spec,&f);
    callback(view,FALSE);
    return rv;
}

Agraph_t *ERmodel(ILview_t *view) {
    return view->pvt->model.main;
}

ilbool ERcallback(ILview_t *view) {
    return callback(view,TRUE);
}

ILengine_t OrthoGrid = {
                           ERopen,
                           ERclose,
                           ERins,
                           ERmov,
                           ERdel,
                           ERcallback,
                           ilmdlobj_to_spec,
                           ilspec_to_mdlobj
                       };

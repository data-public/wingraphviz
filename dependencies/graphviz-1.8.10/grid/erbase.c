#include <grid.h>

#ifdef DMALLOC
#include "dmalloc.h"
#endif

ERnode_t *er_nd(Agnode_t *n) {
    return (ERnode_t*)(AGDATA(n));
}

ERedge_t *er_ed(Agedge_t *e) {
    return (ERedge_t*)(AGDATA(e));
}

Agraph_t *ergraph(ERview_t *D) {
    return BASE(D)->model.main;
}

/* generic sets */
Set_t *ERmake_set(ERview_t *D) {
    Set_t	*rv;
    rv = agalloc(ergraph(D),sizeof(Set_t));
    rv->extent = 20;
    rv->list = agalloc(ergraph(D),rv->extent * sizeof(void*));
    return rv;
}

void ERfree_set(ERview_t *D, Set_t *set
               ) {
    if (set
       ) {

        agfree(ergraph(D),set
               ->list);

        agfree(ergraph(D),set
              );
    }
}

void ERset_append(ERview_t *D, Set_t *set
                  , void *obj) {

    assert(set
           ->extent > 0);

    if (set
            ->size >= set
                ->extent - 1) {
            set
                ->list = agrealloc(ergraph(D),set
                                   ->list,
                                   set
                                       ->extent*sizeof(void*),set
                                       ->extent * 2 * sizeof(void*));

            set
                ->extent = set
                               ->extent * 2;
        }

    set
        ->list[set
               ->size++] = obj;
}

void ERset_delete(Set_t *set
                  , void *obj) {
    int		i;
    void	*obj0;

    for (i = 0; (obj0 = set
                            ->list[i]); i++) {
            if (obj0 == obj) {
                if (i < set
                            ->size - 1)
                        set
                            ->list[i] = set
                                            ->list[set
                                                   ->size - 1];

                set
                    ->size--;

                set
                    ->list[set
                           ->size] = NIL(void*);

                break;
            }
        }

    assert(obj == obj0);
}

/* tile sets */
Tileset_t *ERmake_tileset(ERview_t *D) {
    return (Tileset_t*)  ERmake_set(D);
}

void ERfree_tileset(ERview_t *D, Tileset_t *set
                   ) {

    ERfree_set(D,(Set_t*)set
              );
}

void ERtileset_append(ERview_t *D, Tileset_t *set
                      , Tile_t *b) {

    ERset_append(D,(Set_t*)set
                 ,b);
}

void ERtileset_delete(Tileset_t *set
                      , Tile_t *b) {

    ERset_delete((Set_t*)set
                 ,b);
}

/* segment sets */
Seglist_t *ERmake_seglist(ERview_t *D) {
    return (Seglist_t*) ERmake_set(D);
}

void ERfree_seglist(ERview_t *D, Seglist_t *set
                   ) {

    ERfree_set(D,(Set_t*)set
              );
}

void ERseglist_append(ERview_t *D, Seglist_t *set
                      , Seg_t *s) {

    ERset_append(D,(Set_t*)set
                 ,s);
}

void ERseglist_delete(Seglist_t *set
                      , Seg_t *seg) {

    ERset_delete((Set_t*)set
                 ,seg);
}

/* points */
Pt_t ERmkpoint(double x, double y) {
    Pt_t rv;
    rv.c[0] = x;
    rv.c[1] = y;
    return rv;
}

Pt_t ERaddpoint(Pt_t p0, Pt_t p1) {
    return ERmkpoint( x(p0) + x(p1),  y(p0) + y(p1));
}

Pt_t ERavgpt(Pt_t p0, Pt_t p1) {
    return ERmkpoint((x(p0)+x(p1))/2.,(y(p0)+y(p1))/2.);
}

Pt_t ERcombine(Pt_t p, Pt_t q, ilbool px) {
    double	x,y;

    if (px) {
        x = x(p);
        y = y(q);

    } else  {
        x = x(q);
        y = y(p);
    }

    return ERmkpoint(x,y);
}


/* points of compass & orientation */

#ifdef NOTDEF
char *name_of_dir(Dir_t arg) {
    static char *n[] = {"west","north","east","south","nosuchdir"};
    return n[arg];
}

#endif

/* returns true if horizontal, false if vertical */
ilbool ERhorizontal(Seg_t *seg) {
    if (y(seg->p[0]) == y(seg->p[1]))
        return TRUE;

    assert(x(seg->p[0]) == x(seg->p[1]));

    return FALSE;
}

/* tiles */
/* construct a tile from two points.
 *  seglists are initially empty.
 *	the tile is not entered into the global configuration.
 */
Tile_t	*ERmake_tile(ERview_t *D, Pt_t p0, Pt_t p1) {
    int			i,d;
    Tile_t		*rv;
    Seglist_t	*segs;
    static int	next_id = 0;

    assert (x(p0) != x(p1));
    assert (y(p0) != y(p1));

    rv = agalloc(ergraph(D),sizeof(Tile_t));

    for (d = 0; d < 2; d++) {
        rv->LL.c[d] = MIN(p0.c[d],p1.c[d]);
        rv->UR.c[d] = MAX(p0.c[d],p1.c[d]);
    }

    for (i = 0; i < NSIDES; i++) {
        segs = rv->segs[i] = ERmake_seglist(D);
    }

    rv->id = next_id++;
    return rv;
}

void ERfree_tile(ERview_t *D, Tile_t *b) {
    Dir_t	side;

    for (side = 0; side < NSIDES; side++)
        ERfree_seglist(D,b->segs[side]);

    agfree(ergraph(D),b);
}

Pt_t ERtile_LL(Tile_t *b) {
    return b->LL;
}

Pt_t ERtile_LR(Tile_t *b) {
    return ERmkpoint(x(b->UR),y(b->LL));
}

Pt_t ERtile_UR(Tile_t *b) {
    return b->UR;
}

Pt_t ERtile_UL(Tile_t *b) {
    return ERmkpoint(x(b->LL),y(b->UR));
}

void ERcorners(Tile_t *b, Pt_t *p) {
    p[0] = ERtile_LL(b);
    p[1] = ERtile_UL(b);
    p[2] = ERtile_UR(b);
    p[3] = ERtile_LR(b);
}

/* note, must return points in ascending order. */
void ERget_tile_side(Tile_t *b, Dir_t side, Pt_t *p) {
    switch (side) {

            case north:
            p[0] = ERtile_UL(b);
            p[1] = ERtile_UR(b);
            break;

            case east:
            p[0] = ERtile_LR(b);
            p[1] = ERtile_UR(b);
            break;

            case south:
            p[0] = ERtile_LL(b);
            p[1] = ERtile_LR(b);
            break;

            case west:
            p[0] = ERtile_LL(b);
            p[1] = ERtile_UL(b);
            break;

            default:
            abort();
    }
}

Dir_t ERtile_side_of(Tile_t *b, Pt_t p, Pt_t q) {
    Dir_t side = nosuchdir;

    if (x(p) == x(q)) {
        if (NONDECREASING(y(b->LL),y(p),y(b->UR))
                && NONDECREASING(y(b->LL),y(q),y(b->UR))) {
            if (x(p) == x(b->LL))
                side = west;
            else if (x(p) == x(b->UR))
                side = east;
        }

    } else {
        assert (y(p) == y(q));

        if (NONDECREASING(x(b->LL),x(p),x(b->UR))
                && NONDECREASING(x(b->LL),x(q),x(b->UR))) {
            if (y(p) == y(b->LL))
                side = south;
            else if (y(p) == y(b->UR))
                side = north;
        }
    }

    return side;
}

static ilbool on_side(Tile_t *b, Pt_t p, Pt_t q) {
    return (ERtile_side_of(b,p,q) != nosuchdir);
}

static ilbool pt_in_tile(Pt_t p, Tile_t *b) {
    return (NONDECREASING(x(b->LL),x(p),x(b->UR)) &&
            NONDECREASING(y(b->LL),y(p),y(b->UR)));
}

ilbool ERpt_strictly_in_tile(Pt_t p, Tile_t *b) {
    return (INCREASING(x(b->LL),x(p),x(b->UR)) &&
            INCREASING(y(b->LL),y(p),y(b->UR)));
}

ilbool ERtiles_nontrivially_intersect(Tile_t *b0, Tile_t *b1) {
    if ((x(b0->UR) <= x(b1->LL))	/* b0 is left */
            ||  (x(b0->LL) >= x(b1->UR))	/* b0 is right */
            ||  (y(b0->UR) <= y(b1->LL))	/* b0 is below */
            ||  (y(b0->LL) >= y(b1->UR)))	/* b0 is above */
        return FALSE;

    return TRUE;
}

ilbool ERtile_covers_tile(Tile_t *outer, Tile_t *inner) {
    if ((x(outer->LL) > x(inner->LL))
            ||  (x(outer->UR) < x(inner->UR))
            ||  (y(outer->LL) > x(inner->LL))
            ||  (y(outer->UR) < x(inner->UR)))
        return FALSE;

    return TRUE;
}

/* segments */

Seg_t *ERmkseg(ERview_t *D, Pt_t p, Pt_t q, Tile_t *b0, Tile_t *b1, Segkind_t kind) {
    Pt_t	t;
    Seg_t	*rv;

    if ( (x(p) == x(q)) + (y(p) == y(q)) != 1)
        abort();

    if NOT(on_side(b0,p,q))
        abort();

    if NOT(on_side(b1,p,q))
        abort();

    if ( ((y(p) == y(q)) && (x(p) > x(q))) ||
            ((x(p) == x(q)) && (y(p) > y(q)))) {
        t = p;
        p = q;
        q = t;
    }		/* pts always in ascending order */

    rv = agalloc(ergraph(D),sizeof(Seg_t));

    rv->p[0] = p;

    rv->p[1] = q;

    rv->b[0] = b0;

    rv->b[1] = b1;

    rv->kind = kind;

    return rv;
}

void ERfree_seg(ERview_t *D, Seg_t *s) {
    agfree(ergraph(D),s);
}

void ERinstall_new_seg(ERview_t *D, Pt_t p, Pt_t q, Segkind_t kind, Tile_t *b0, Dir_t side, Tile_t *b1) {
    Seg_t	*newseg;
    newseg = ERmkseg(D,p,q,b0,b1,kind);
    ERseglist_append(D,b0->segs[side],newseg);
    ERseglist_append(D,b1->segs[opposite(side)],newseg);
}


static Dir_t ERside_toward(Tile_t *b, Pt_t p) {
    Dir_t	side;

    if (y(p) > y(b->UR))
        side = north;
    else if (y(p) < y(b->LL))
        side = south;
    else if (x(p) > x(b->UR))
        side = east;
    else if (x(p) < x(b->LL))
        side = west;
    else
        side = nosuchdir;

    return side;
}

static int varying(Dir_t side)	/* depends on internal values of Dir_t enum */
{
    return (side + 1) % 2;
}

/* walk one step from b toward p */
Tile_t *ERneighbor(Tile_t *b, Pt_t p) {
    int		i, c;
    Seg_t	*seg;
    Tile_t	*rv;
    Dir_t	side;

    side = ERside_toward(b,p);

    if (side == nosuchdir)
        rv = b;
    else {
        c = varying(side);

        for (i = 0; (seg = b->segs[side]->list[i]); i++)
            if (NONDECREASING(seg->p[0].c[c],p.c[c],seg->p[1].c[c]))
                break;

        if (b != seg->b[0])
            rv = seg->b[0];
        else
            rv = seg->b[1];
    }

    return rv;
}

static Seg_t *find_seg(Tile_t *b, Pt_t p) {
    int		i,d0,d1;
    Pt_t	cp[2];
    Dir_t	side;
    Seglist_t *seglist;
    Seg_t 	*seg;

    for (side = 0; side < NSIDES; side++) {
        ERget_tile_side(b,side,cp);
        d1 = side % 2;
        d0 = !d1;

        if (p.c[d1] == cp[0].c[d1]) {
            if (NONDECREASING(cp[0].c[d0],p.c[d0],cp[1].c[d0])) {
                seglist = b->segs[side];

                for (i = 0; (seg = seglist->list[i]); i++) {
                    if (NONDECREASING(seg->p[0].c[d0],p.c[d0],seg->p[1].c[d0]))
                        return seg;
                }

            } else
                break;
        }
    }

    return NIL(Seg_t*);
}


/* locate the tile of a given point */
Tile_t *ERlocate(ERview_t *D, Pt_t p) {
    Tile_t	*b;
    int		i;

    for (i = 0; (b = D->config->list[i]); i++)
        if (pt_in_tile(p,b))
            break;

    return b;
}

void ERlocate_endpoint(ERview_t *D, Tile_t *user_node, Pt_t pt,
                       Tile_t **pb, Seg_t **pseg) {
    Tile_t	*b;
    Seg_t	*seg;

    b = ERlocate(D,pt);
    seg = find_seg(b,pt);

    if (seg) {
        if (NOT(ERtiles_nontrivially_intersect(user_node,b)))
            b = (b != seg->b[0]?  seg->b[0] : seg->b[1]);

    } else
        b = NIL(Tile_t*);

    *pb = b;

    *pseg = seg;
}

static Pt_t snap_to(Pt_t p, Tile_t *b) {
    int		c;

    for (c = 0; c < 2; c++) {
        if (p.c[c] < b->LL.c[c])
            p.c[c] = b->LL.c[c];
        else if (p.c[c] > b->UR.c[c])
            p.c[c] = b->UR.c[c];
    }

    return p;
}

/* split the configuration by a given line segment */

void ERmark_segs(ERview_t *D, Pt_t p, Pt_t q, Segkind_t kind) {
    int		i,c,d;
    Tile_t	*b,*last_b;
    Pt_t	p0;
    Dir_t	side;
    Seg_t	*seg;

    if (y(p) == y(q)) {
        c = 0;
        d = 1;

    } else {
        c = 1;
        d = 0;
    }

    if (p.c[c] > q.c[c]) {
        Pt_t t;
        t = p;
        p = q;
        q = t;
    }

    b = ERlocate(D,p);

    do {
        p0 = snap_to(q,b);
        assert(p0.c[d] == p.c[d]);
        assert(p0.c[d] == q.c[d]);

        if (NOT(pt_eq(p,p0))) {
            side = ERtile_side_of(b,p,p0);

            for (i = 0; (seg = b->segs[side]->list[i]); i++) {
                if ((seg->p[0].c[c] < q.c[c]) &&
                        (seg->p[1].c[c] > p.c[c])) {
                    seg->kind = kind;
                }
            }
        }

        p = p0;
        last_b = b;
        b = ERneighbor(b,q);

    } while (b != last_b);
}

void ERmark_container_segs(ERview_t *d, Tile_t *b, Segkind_t kind) {
    int		i;
    Tile_t	*nb;
    Pt_t	corner[4];
    Dir_t	side;

    for (i = 0; (nb = d->nodes->list[i]); i++) {
        if (ERtile_covers_tile(nb,b)) {
            ERcorners(b,corner);

            for (side = 0; side < NSIDES; side++) {
                ERmark_segs(d,corner[side],corner[(side+1)%4],kind);
            }
        }
    }
}

Dir_t ERtiles_share_side(Tile_t *b, Tile_t *cb, Pt_t *res) {
    int			d0,d1;
    Dir_t		side, opp;
    Pt_t		bseg[2],cbseg[2];

    for (side = 0; side < NSIDES; side++) {
        d0 = (side % 2);
        d1 = (d0? 0 : 1);
        opp = opposite(side);
        ERget_tile_side(b,side,bseg);
        ERget_tile_side(cb,opp,cbseg);

        if (bseg[0].c[d0] != cbseg[0].c[d0])
            continue;

        if (bseg[1].c[d1] <= cbseg[0].c[d1])
            continue;

        if (bseg[0].c[d1] >= cbseg[1].c[d1])
            continue;

        if (bseg[0].c[d1] < cbseg[0].c[d1])
            res[0] = cbseg[0];
        else
            res[0] = bseg[0];

        if (bseg[1].c[d1] > cbseg[1].c[d1])
            res[1] = cbseg[1];
        else
            res[1] = bseg[1];

        if NOT(on_side(b,res[0],res[1]))
            abort();

        if NOT(on_side(cb,res[0],res[1]))
            abort();

        return side;
    }

    return nosuchdir;
}

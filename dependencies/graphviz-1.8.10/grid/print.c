#include <grid.h>

#ifdef DMALLOC
#include "dmalloc.h"
#endif

static void printseg(FILE *fp, Seg_t *seg) {
    fprintf(fp,"%.2f %.2f map moveto %.2f %.2f map lineto stroke\n",
            x(seg->p[0]),y(seg->p[0]),x(seg->p[1]),y(seg->p[1]));
}

void ERprint(FILE *fp, ERview_t *d, ilbool all) {
    int		i,j;
    Dir_t	side;
    Tile_t	*b;
    Seg_t	*seg;
    static int onetime = TRUE;

    if (onetime) {
        fprintf(fp,"%%!PS\n");
        fprintf(fp,"/map {72 mul exch 72 mul exch} bind def\n");
        onetime = FALSE;
    }

    for (i = 0; (b = d->config->list[i]); i++) {
        for (side = 0; side < NSIDES; side++) {
            for (j = 0; (seg = b->segs[side]->list[j]); j++) {
                switch (seg->kind) {

                        case s_forbidden:
                        break;

                        case s_plain:

                        if (all)
                            printseg(fp,seg);

                        break;

                        case s_edge:
                        fprintf(fp,"gsave 1 0 0 setrgbcolor\n");

                        printseg(fp,seg);

                        fprintf(fp,"grestore\n");

                        break;

                        case s_node:
                        fprintf(fp,"gsave 3 setlinewidth\n");

                        printseg(fp,seg);

                        fprintf(fp,"grestore\n");

                        break;
                }
            }
        }
    }

    fprintf(fp,"showpage\n");
}

void ERpr(ERview_t *d, ilbool all) {
    ERprint(stderr,d,all);
}

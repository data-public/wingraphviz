/*
 * Tcl extension for Dynamic Graphs by John Ellson (ellson@lucent.com)
 * 
 * Builds on libincr + libgraph by Stephen North (north@research.att.com)
 */

#include "dgr.h"

#ifdef DMALLOC
#include "dmalloc.h"
#endif

int
edgecmd(ClientData clientData, Tcl_Interp * interp, int argc, char *argv[]) {
    dgrInterp_t     *dg = (dgrInterp_t *)clientData;
    char            c, *s, **argv2;
    int             i, j, length, argc2, err, save;
    Agraph_t       *g;
    Agedge_t       *e;
    Agsym_t        *sym;
    char		    gbuf[16], ebuf[16];

    if (argc < 2) {
        Tcl_AppendResult(interp, "wrong # args: should be \"",
                         argv[0], "\" option ?arg arg ...?", (char *) NULL);
        return TCL_ERROR;
    }

    if (!(e = handle_to_edge(dg, argv[0]))) {
        Tcl_AppendResult(interp, "Invalid handle: \"", argv[0],
                         "\" not found.", (char *) NULL);
        return TCL_ERROR;
    }

    g = agraphof(e);
    c = argv[1][0];
    length = strlen(argv[1]);

    if ((c == 'd') && (strncmp(argv[1], "delete", length) == 0)) {
        save = dgrCallbacks(dg, g, FALSE);	/* delay callbacks until all done */
        agdeledge(e);

        if (dg->object_commands) {
            Tcl_DeleteCommand(interp, argv[0]);
        }

        dgrCallbacks(dg, g, save);
        return TCL_OK;

    } else if ((c == 'g') && (strncmp(argv[1], "graphof", length) == 0)) {
        Tcl_AppendResult(interp, graph_to_handle(g, gbuf), (char *) NULL);
        return TCL_OK;

    } else if ((c == 'l') && (strncmp(argv[1], "listattributes", length) == 0)) {
        g = agroot(g);

        for (sym = agnxtattr(g, AGEDGE, (Agsym_t *) NULL); sym;
                sym = agnxtattr(g, AGEDGE, sym)) {
            if (argc == 2 || Tcl_StringMatch(sym->name, argv[2])) {
                Tcl_AppendElement(interp, sym->name);
            }
        }

        return TCL_OK;

    } else if ((c == 'l') && (strncmp(argv[1], "listnodes", length) == 0)) {
        if (attributematch(interp, (Agobj_t *)agtail(e), argc-2, &argv[2]) != TCL_OK)
            return TCL_ERROR;

        if (attributematch(interp, (Agobj_t *)aghead(e), argc-2, &argv[2]) != TCL_OK)
            return TCL_ERROR;

        return TCL_OK;

    } else if (((c == 'l') && (strncmp(argv[1], "listheadnodes", length) == 0))
               || ((c == 'h') && (strncmp(argv[1], "headof", length) == 0))) {
        return (attributematch(interp, (Agobj_t *)aghead(e), argc-2, &argv[2]));

    } else if (((c == 'l') && (strncmp(argv[1], "listtailnodes", length) == 0))
               || ((c == 't') && (strncmp(argv[1], "tailof", length) == 0))) {
        return (attributematch(interp, (Agobj_t *)agtail(e), argc-2, &argv[2]));

    } else if ((c == 'q') && (strncmp(argv[1], "queryattributes", length) == 0)) {
        for (i = 2; i < argc; i++) {
            if ((err = Tcl_SplitList(interp, argv[i], &argc2, (CONST84 char ***) &argv2)) != TCL_OK)
                return err;

            for (j = 0; j < argc2; j++) {
                if (!(sym = (Agsym_t *) agattrsym(e, argv2[j]))) {
                    Tcl_AppendResult(interp, " No attribute named \"",
                                     argv2[j], "\"", (char *) 0);
                    return TCL_ERROR;
                }

                Tcl_AppendElement(interp, agxget(e, sym));
            }

            Tcl_Free((char *) argv2);
        }

        return TCL_OK;

    } else if ((c == 's') && (strncmp(argv[1], "set", length) == 0)) {
        if (argc < 3) {
            Tcl_AppendResult(interp, "wrong # args: should be: ",
                             argv[0], "set attributename ?attributevalue?", (char *) NULL);
            return TCL_ERROR;
        }

        if (argc == 3) {
            if (!(sym = (Agsym_t *) agattrsym(e, argv[2]))) {
                Tcl_AppendResult(interp, " No attribute named \"",
                                 argv[2], "\"", (char *) 0);
                return TCL_ERROR;
            }

            Tcl_AppendResult(interp, agxget(e, sym), (char *) NULL);

        } else {
            save = dgrCallbacks(dg, g, FALSE);	/* delay callbacks until all done */

            if (!(sym = (Agsym_t *) agattrsym(e, argv[2])))
                sym = agattr(g, AGEDGE, argv[2], "");

            if (strcmp(agxget(e, sym), argv[3]))
                agxset(e, sym, argv[3]);

            dgrCallbacks(dg, g, save);

            Tcl_ResetResult(interp);

            Tcl_AppendResult(interp, argv[3], (char *) NULL);
        }

        return TCL_OK;

    } else if ((c == 's') && (strncmp(argv[1], "setattributes", length) == 0)) {
        if ((argc < 4) || (argc % 2)) {
            Tcl_AppendResult(interp, "wrong # args: should be \"",
                             argv[0], "\" setattributes attributename attributevalue ?attributename attributevalue? ?...?",
                             (char *) NULL);
            return TCL_ERROR;
        }

        save = dgrCallbacks(dg, g, FALSE);	/* delay callbacks until all attributes changed */

        for (i = 2; i < argc; i = i + 2) {
            if (!(sym = (Agsym_t *) agattrsym(e, argv[i]))) {
                sym = agattr(g, AGEDGE, argv[i], "");
            }

            agxset(e, sym, argv[i + 1]);
        }

        dgrCallbacks(dg, g, save);
        return TCL_OK;

    } else if ((c == 's') && (strncmp(argv[1], "showname", length) == 0)) {
        /*
         * can't do this in one go because agnameof() uses a static 
         * string buffer that gets overwritten by the second call
         */
        Tcl_AppendResult(interp, agnameof(agtail(e)),
                         agisdirected(g)?"->":"--", (char *) NULL);
        s = agnameof(aghead(e));
        Tcl_AppendResult(interp, s, (char *) NULL);
        return TCL_OK;

    } else if (c == '_') {
        /* eval user defined method stored in attribute */

        if (!(sym = (Agsym_t *) agattrsym(e, argv[1]))) {
            Tcl_AppendResult(interp, " No attribute named \"", argv[1],
                             "\"", (char *) 0);
            return TCL_ERROR;
        }

        /*
         * if there are any other args then pass them as a proper list in the
         * substitution for %a
         */
        i = 0;

        s = "";

        if (argc > 2) {
            i++;
            s = Tcl_Merge(argc - 2, (CONST84 char * CONST *) argv + 2);
        }

        dgrExpandPercentsEval(interp, agxget(e, sym),
                              graph_to_handle(g, gbuf), "", edge_to_handle(e, ebuf), "", s);

        if (i)
            ckfree(s);

        return TCL_OK;

    } else {
        Tcl_AppendResult(interp, "bad option \"", argv[1], "\" must be one of:",
                         "\n\tdelete, graphof, headof, listattributes, listnodes,",
                         "\n\tlisttailnodes, listheadnodes, queryattributes,",
                         "\n\tset, setattributes, showname, tailof.",
                         (char *) NULL);
        return TCL_ERROR;
    }
}

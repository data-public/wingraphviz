/*
 * Tcl extension for Dynamic Graphs by John Ellson (ellson@lucent.com)
 * 
 * Builds on libagraph by Stephen North (north@research.att.com)
 */

#include "dgr.h"

#ifdef DMALLOC
#include "dmalloc.h"
#endif

char *
graph_to_handle(Agraph_t * g, char * buf) {
    sprintf(buf, "dgG%lu", AGID(g));
    return buf;
}

char *
edge_to_handle(Agedge_t * e, char * buf) {
    sprintf(buf, "dgE%lu", AGID(e));
    return buf;
}

char *
node_to_handle(Agnode_t * n, char * buf) {
    sprintf(buf, "dgN%lu", AGID(n));
    return buf;
}

dgGraph_t *
handle_to_graph(dgrInterp_t *dg, char *s) {
    unsigned long		i;
    dgGraph_t  *g;

    if ((sscanf(s, "dgG%lu", &i)) != 1) {
        return NULL;
    }

    if (!(g = (dgGraph_t *)tclhandleXlateIndex(dg->graphTable, i))) {
        return NULL;
    }

    return g;
}

Agedge_t *
handle_to_edge(dgrInterp_t *dg, char *s) {
    unsigned long	   i;
    Agedge_t  **ep;

    if ((sscanf(s, "dgE%lu", &i)) != 1)
        return NULL;

    if (!(ep = (Agedge_t **)tclhandleXlateIndex(dg->edgeTable, i)))
        return NULL;

    return *ep;
}

Agnode_t *
handle_to_node(dgrInterp_t *dg, char *s) {
    unsigned long	   i;
    Agnode_t  **np;

    if ((sscanf(s, "dgN%lu", &i)) != 1)
        return NULL;

    if (!(np = (Agnode_t **)tclhandleXlateIndex(dg->nodeTable, i)))
        return NULL;

    return *np;
}

int
attributematch(Tcl_Interp *interp, Agobj_t *obj, int argc, char **argv) {
    int i = 0, j = 0;
    Agsym_t *sym;
    char buf[16], *val;

    if (argc % 2 ) {
        Tcl_AppendResult(interp, "invalid attribute pattern, should be \"",
                         "?attributename attributevaluepattern? ...\"",
                         (char *) NULL);
        return TCL_ERROR;
        ;
    }

    while (i<argc) {
        if (!(sym = (Agsym_t *) agattrsym(obj, argv[i]))) {
            Tcl_AppendResult(interp, " No attribute named \"",
                             argv[i], "\"", (char *) 0);
            return TCL_ERROR;
        }

        i++;
        val = agxget(obj, sym);

        if (!val)
            val = sym->defval;

        if (Tcl_StringMatch(val, argv[i])) {
            j = 1;
            break;
        }

        i++;
    }

    if (j || !argc) {
        switch (AGTYPE(obj)) {

                case AGRAPH:
                graph_to_handle((Agraph_t *)obj, buf);
                break;

                case AGNODE:
                node_to_handle((Agnode_t *)obj, buf);
                break;

                case AGINEDGE:

                case AGOUTEDGE:
                edge_to_handle((Agedge_t *)obj, buf);
                break;

                default: {
                    Tcl_AppendResult(interp,
                                     "invalid object tag (internal error)", (char *)NULL);
                    return TCL_ERROR;
                }
        }

        Tcl_AppendElement(interp, buf);
    }

    return TCL_OK;
}

/********************************************************************/
/* evaluate bindings after % substitutions						  */

char *
dgrExpandPercentsEval(
    Tcl_Interp * interp,	 /* interpreter context */
    register char *before,   /* Command with percent expressions */
    char *g,	 /* graphHandle string to substitute for "%g" */
    char *n,	 /* nodeHandle string to substitute for "%n"
        					or tailnode in insert_edge
        					or "node" in modify_graph */
    char *e,	 /* edgeHandle string to substitute for "%e"
        					or "edge" in modify_graph */
    char *A,	 /* attributeName/Value list for inserts or modifies
        					or headnode in insert_edge */
    char *a		 /* attributeName for modify
        					or arglist string to substitute for "%a" */
) {
    register char  *string;
    Tcl_DString	 scripts;

    Tcl_DStringInit(&scripts);

    while (1) {
        /*
        	* Find everything up to the next % character and append it to the
        	* result string.
        	*/

        for (string = before; (*string != 0) && (*string != '%'); string++) {
            /* Empty loop body. */
        }

        if (string != before) {
            Tcl_DStringAppend(&scripts, before, string - before);
            before = string;
        }

        if (*before == 0) {
            break;
        }

        /*
        	* There's a percent sequence here.  Process it.
        	*/

        switch (before[1]) {

                case 'g':
                Tcl_DStringAppend(&scripts, g, strlen(g));	/* graphHandle */
                break;

                case 'n':

                case 't':
                Tcl_DStringAppend(&scripts, n, strlen(n));	/* nodeHandle */
                break;

                case 'e':
                Tcl_DStringAppend(&scripts, e, strlen(e));	/* edgeHandle */
                break;

                case 'A':

                case 'h':
                Tcl_DStringAppend(&scripts, A, strlen(A));	/* attributeName */
                break;

                case 'a':
                Tcl_DStringAppend(&scripts, a, strlen(a)); /* attributeValue */
                break;

                default:
                Tcl_DStringAppend(&scripts, before+1, 1);
                break;
        }

        before += 2;
    }

    if (Tcl_GlobalEval(interp, Tcl_DStringValue(&scripts)) != TCL_OK)
        fprintf(stderr, "%s while in binding: %s\n\n",
                interp->result, Tcl_DStringValue(&scripts));

    Tcl_DStringFree(&scripts);

    return interp->result;
}

static void
insert_graph_cb(Agobj_t *obj, void *arg) {
    dgrInterp_t *dg = (dgrInterp_t *)arg;
    char		gbuf[16];
    dgGraph_t	  *gp;

    gp = (dgGraph_t *)tclhandleXlateIndex(dg->graphTable,
                                          AGID(agroot((Agraph_t *)obj)));

    if (gp->insert_graph_cmd) {
        dgrExpandPercentsEval(dg->interp, gp->insert_graph_cmd,
                              graph_to_handle((Agraph_t *)obj, gbuf),
                              "", "", "", "");
    }
}

static void
insert_node_cb(Agobj_t *obj, void *arg) {
    dgrInterp_t *dg = (dgrInterp_t *)arg;
    char		gbuf[16], nbuf[16];
    Agraph_t	   *g;
    dgGraph_t	  *gp;

    g = agraphof((Agnode_t *)obj);
    gp = (dgGraph_t *)tclhandleXlateIndex(dg->graphTable, AGID(agroot(g)));

    if (gp->insert_node_cmd) {
        dgrExpandPercentsEval(dg->interp, gp->insert_node_cmd,
                              graph_to_handle(g, gbuf),
                              node_to_handle((Agnode_t *)obj, nbuf),
                              "", "", "");
    }
}

static void
insert_edge_cb(Agobj_t *obj, void *arg) {
    dgrInterp_t *dg = (dgrInterp_t *)arg;
    char		gbuf[16], ebuf[16], tbuf[16], hbuf[16];
    Agraph_t	   *g;
    dgGraph_t	  *gp;

    g = agraphof((Agedge_t *)obj);
    gp = (dgGraph_t *)tclhandleXlateIndex(dg->graphTable, AGID(agroot(g)));

    if (gp->insert_edge_cmd) {
        dgrExpandPercentsEval(dg->interp, gp->insert_edge_cmd,
                              graph_to_handle(g, gbuf),
                              node_to_handle(agtail((Agedge_t *)obj), tbuf),
                              edge_to_handle((Agedge_t *)obj, ebuf),
                              node_to_handle(aghead((Agedge_t *)obj), hbuf),
                              "");
    }
}

static void
delete_graph_cb(Agobj_t *obj, void *arg) {
    dgrInterp_t *dg = (dgrInterp_t *)arg;
    char		gbuf[16];
    dgGraph_t	  *gp;

    gp = (dgGraph_t *)tclhandleXlateIndex(dg->graphTable,
                                          AGID(agroot((Agraph_t *)obj)));

    if (gp->delete_graph_cmd) {
        dgrExpandPercentsEval(dg->interp, gp->delete_graph_cmd,
                              graph_to_handle((Agraph_t *)obj, gbuf),
                              "", "", "", "");
    }
}

static void
delete_node_cb(Agobj_t *obj, void *arg) {
    dgrInterp_t *dg = (dgrInterp_t *)arg;
    char		gbuf[16], nbuf[16];
    Agraph_t	   *g;
    dgGraph_t	  *gp;

    g = agraphof((Agnode_t *)obj);
    gp = (dgGraph_t *)tclhandleXlateIndex(dg->graphTable, AGID(agroot(g)));

    if (gp->delete_node_cmd) {
        dgrExpandPercentsEval(dg->interp, gp->delete_node_cmd,
                              graph_to_handle(g, gbuf),
                              node_to_handle((Agnode_t *)obj, nbuf),
                              "", "", "");
    }
}

static void
delete_edge_cb(Agobj_t *obj, void *arg) {
    dgrInterp_t *dg = (dgrInterp_t *)arg;
    char		gbuf[16], ebuf[16];
    Agraph_t	   *g;
    dgGraph_t	  *gp;

    g = agraphof((Agedge_t *)obj);
    gp = (dgGraph_t *)tclhandleXlateIndex(dg->graphTable, AGID(agroot(g)));

    if (gp->delete_edge_cmd) {
        dgrExpandPercentsEval(dg->interp, gp->delete_edge_cmd,
                              graph_to_handle(g, gbuf),
                              "", edge_to_handle((Agedge_t *)obj, ebuf),
                              "", "");
    }
}

static void
modify_graph_cb(Agobj_t *obj, void *arg, Agsym_t *sym) {
    dgrInterp_t *dg = (dgrInterp_t *)arg;
    char		gbuf[16], *val, *node="", *edge="";
    dgGraph_t	  *gp;

    gp = (dgGraph_t *)tclhandleXlateIndex(dg->graphTable,
                                          AGID(agroot((Agraph_t *)obj)));

    if (gp->modify_graph_cmd) {
        assert(sym);
        val = agxget((Agraph_t *)obj, sym);

        if (!val)
            val = sym->defval;

        if (sym->kind == AGNODE)
            node = "node";

        if (sym->kind == AGINEDGE || sym->kind == AGOUTEDGE)
            edge = "edge";

        dgrExpandPercentsEval(dg->interp, gp->modify_graph_cmd,
                              graph_to_handle((Agraph_t *)obj, gbuf),
                              node, edge, sym->name, val);
    }
}

static void
modify_node_cb(Agobj_t *obj, void *arg, Agsym_t *sym) {
    dgrInterp_t *dg = (dgrInterp_t *)arg;
    char		gbuf[16], nbuf[16], *val;
    Agraph_t	   *g;
    dgGraph_t	  *gp;

    g = agraphof((Agnode_t *)obj);
    gp = (dgGraph_t *)tclhandleXlateIndex(dg->graphTable, AGID(agroot(g)));

    if (gp->modify_node_cmd) {
        val = agxget((Agnode_t *)obj, sym);

        if (!val)
            val = sym->defval;

        dgrExpandPercentsEval(dg->interp, gp->modify_node_cmd,
                              graph_to_handle(g, gbuf),
                              node_to_handle((Agnode_t *)obj, nbuf), "", sym->name, val);
    }
}

static void
modify_edge_cb(Agobj_t *obj, void *arg, Agsym_t *sym) {
    dgrInterp_t *dg = (dgrInterp_t *)arg;
    char		gbuf[16], ebuf[16], *val;
    Agraph_t	   *g;
    dgGraph_t	  *gp;

    g = agraphof((Agedge_t *)obj);
    gp = (dgGraph_t *)tclhandleXlateIndex(dg->graphTable, AGID(agroot(g)));

    if (gp->modify_edge_cmd) {
        val = agxget((Agedge_t *)obj, sym);

        if (!val)
            val = sym->defval;

        dgrExpandPercentsEval(dg->interp, gp->modify_edge_cmd,
                              graph_to_handle(g, gbuf),
                              "", edge_to_handle((Agedge_t *)obj, ebuf), sym->name, val);
    }
}

void
deleteEdges(dgrInterp_t *dg, Agnode_t * n) {
    char		ebuf[16];
    Agedge_t	   *e, *prev_e;

    e = agfstedge(n);

    while (e) {
        prev_e = e;
        edge_to_handle(prev_e, ebuf);
        e = agnxtedge(prev_e, n);
        agdeledge(prev_e);

        if (dg->object_commands) {
            Tcl_DeleteCommand(dg->interp, ebuf);
        }
    }
}

void
deleteNodes(dgrInterp_t *dg, Agraph_t * g) {
    Agnode_t	   *n, *prev_n;
    char		nbuf[16];

    n = agfstnode(g);

    while (n) {
        prev_n = n;
        deleteEdges(dg, prev_n);
        node_to_handle(prev_n, nbuf);
        n = agnxtnode(prev_n);
        agdelnode(prev_n);

        if (dg->object_commands) {
            Tcl_DeleteCommand(dg->interp, nbuf);
        }
    }
}

/* recursively delete subgraphs */
void
deleteSubgraphs(dgrInterp_t *dg, Agraph_t * g) {
    Agraph_t	   *sg, *prev_sg;
    char		gbuf[16];

    sg = agfstsubg(g);

    while (sg) {
        prev_sg = sg;
        deleteSubgraphs(dg, sg);
        graph_to_handle(sg, gbuf);
        sg = agnxtsubg(prev_sg);
        agclose(prev_sg);

        if (dg->object_commands) {
            Tcl_DeleteCommand(dg->interp, gbuf);
        }
    }
}

/* recursively register all subgraphs as tcl commands */
static void
registerSubgraphs(dgrInterp_t *dg, Agraph_t *g) {
    Agraph_t	   *sg;
    char			gbuf[16];

    for (sg = agfstsubg(g); sg; sg = agnxtsubg(sg)) {
        Tcl_CreateCommand(dg->interp, graph_to_handle(sg, gbuf),
                          graphcmd, (ClientData) NULL, (Tcl_CmdDeleteProc *) NULL);
        registerSubgraphs(dg, sg);
    }
}

void dg_gpstruct_init (dgrInterp_t *dg, Agraph_t *g)  {
    char gbuf[16];
    dgGraph_t       *gp;

    gp = (dgGraph_t *)tclhandleXlateIndex(dg->graphTable, AGID(g));
    gp->g = g;
    gp->callbacks_enabled = TRUE;
    gp->batch_cmd = (char *) NULL;
    gp->insert_graph_cmd = (char *) NULL;
    gp->modify_graph_cmd = (char *) NULL;
    gp->delete_graph_cmd = (char *) NULL;
    gp->insert_node_cmd = (char *) NULL;
    gp->modify_node_cmd = (char *) NULL;
    gp->delete_node_cmd = (char *) NULL;
    gp->insert_edge_cmd = (char *) NULL;
    gp->modify_edge_cmd = (char *) NULL;
    gp->delete_edge_cmd = (char *) NULL;
    agpushdisc(g, &gcbdisc, (void *)dg);
    graph_to_handle(g, gbuf);

    if (dg->object_commands) {
        Tcl_CreateCommand(dg->interp, gbuf, graphcmd, (ClientData) dg,
                          (Tcl_CmdDeleteProc *) NULL);
    }

    Tcl_AppendResult(dg->interp, gbuf, (char *) NULL);
}

void dg_gpfromdot_init (dgrInterp_t *dg, Agraph_t *g) {
    char buf[16];
    Agnode_t *n, **np;
    Agedge_t *e, **ep;

    if (dg->object_commands) {
        for (n = agfstnode(g); n; n = agnxtnode(n)) {
            np = (Agnode_t **)tclhandleXlateIndex(dg->nodeTable, AGID(n));
            *np = n;
            Tcl_CreateCommand(dg->interp, node_to_handle(n, buf),
                              nodecmd, (ClientData)dg, (Tcl_CmdDeleteProc *) NULL);

            for (e = agfstout(n); e; e = agnxtout(e)) {
                ep = (Agedge_t **)tclhandleXlateIndex(dg->edgeTable, AGID(e));
                *ep = e;
                Tcl_CreateCommand(dg->interp, edge_to_handle(e, buf),
                                  edgecmd, (ClientData)dg, (Tcl_CmdDeleteProc *) NULL);
            }
        }

        registerSubgraphs(dg, g);

    } else {
        for (n = agfstnode(g); n; n = agnxtnode(n)) {
            np = (Agnode_t **)tclhandleXlateIndex(dg->nodeTable, AGID(n));
            *np = n;

            for (e = agfstout(n); e; e = agnxtout(e)) {
                ep = (Agedge_t **)tclhandleXlateIndex(dg->edgeTable, AGID(e));
                *ep = e;
            }
        }
    }
}

/********************************************************************/
/* event batching                                                   */

int dgrCallbacks (dgrInterp_t *dg, Agraph_t *g, int flag) {
    char		gbuf[16];
    dgGraph_t	*gp;

    gp = (dgGraph_t *)tclhandleXlateIndex(dg->graphTable,AGID(agroot(g)));

    if (gp->callbacks_enabled) {
        if (! flag) {
            gp->callbacks_enabled = FALSE;
            agcallbacks(g, FALSE);
        }

        return TRUE;
    }

    if (flag) { /* && NOT gp->callbacks_enabled */

        if (gp->batch_cmd) {
            dgrExpandPercentsEval(dg->interp, gp->batch_cmd,
                                  graph_to_handle(g, gbuf),"", "", "", "1");
        }

        agcallbacks(g, TRUE);

        if (gp->batch_cmd) {
            dgrExpandPercentsEval(dg->interp, gp->batch_cmd,
                                  graph_to_handle(g, gbuf),"", "", "", "0");
        }

        gp->callbacks_enabled = TRUE;
    }

    return FALSE;
}

/********************************************************************/
/* id discipline													*/

static void *
dgidopen(Agraph_t *g) {
    /* This is taken care of in Tcldg_Init */
    return (void *) NULL;
}

static long
dgidmap(void *state, int objtype, char *str, unsigned long *id, int createflag) {
    /* tcldg doesn't support string names in handle table */

    if (str)
        return FALSE;

    if (createflag) {
        switch (objtype) {

                case AGRAPH:
                return (long)tclhandleAlloc(dgrInterp.graphTable, (char *)NULL, id);

                case AGNODE:
                return (long)tclhandleAlloc(dgrInterp.nodeTable, (char *)NULL, id);

                case AGINEDGE:

                case AGOUTEDGE:
                return (long)tclhandleAlloc(dgrInterp.edgeTable, (char *)NULL, id);
        }

    } else {
        switch (objtype) {

                case AGRAPH:
                return (long)tclhandleXlateIndex(dgrInterp.graphTable, *id);

                case AGNODE:
                return (long)tclhandleXlateIndex(dgrInterp.nodeTable, *id);

                case AGINEDGE:

                case AGOUTEDGE:
                return (long)tclhandleXlateIndex(dgrInterp.edgeTable, *id);
        }
    }

    return FALSE;
}

static long
dgidalloc(void *state, int objtype, unsigned long id) {
    /* id should already be allocated by map.  check only. */

    switch (objtype) {

            case AGRAPH:
            return (long)tclhandleXlateIndex(dgrInterp.graphTable, id);

            case AGNODE:
            return (long)tclhandleXlateIndex(dgrInterp.nodeTable, id);

            case AGINEDGE:

            case AGOUTEDGE:
            return (long)tclhandleXlateIndex(dgrInterp.edgeTable, id);
    }

    return FALSE;
}

static void
dgidfree(void *state, int objtype, unsigned long id) {
    void *rv = (void *)NULL;

    switch (objtype) {

            case AGRAPH:
            rv = tclhandleFreeIndex(dgrInterp.graphTable, id);
            break;

            case AGNODE:
            rv = tclhandleFreeIndex(dgrInterp.nodeTable, id);
            break;

            case AGINEDGE:

            case AGOUTEDGE:
            rv = tclhandleFreeIndex(dgrInterp.edgeTable, id);
            break;

            default:
            rv = 0;
    }

    if (!rv)
        agerror(AGERROR_BADOBJ,"dgidfree");
}

static char *
dgidprint(void *state, int objtype, unsigned long id) {
    /* we don't support string names in handle table */
    return (char *) NULL;
}

static void
dgidclose(void *state) {
    /* Nothing to do in Tcldg */
}

Agiddisc_t iddisc = { dgidopen, dgidmap, dgidalloc,
                      dgidfree, dgidprint, dgidclose };

/********************************************************************/
/* io disciplines - files											*/

#ifdef TCL_CHANNELS

static int
fileiofread(void *chan, char *buf, int bufsize) {
    return Tcl_Read((Tcl_Channel)chan, buf, bufsize);
}

static int
fileioputstr(void *chan, char *str) {
    return Tcl_Write((Tcl_Channel)chan, str, -1);
}

static int
fileioflush(void *chan) {
    return Tcl_Flush((Tcl_Channel)chan);
}

Agiodisc_t file_iodisc = {fileiofread, fileioputstr, fileioflush};

#else
/* no TCL_CHANNELS so use libgraph default io routines */

Agiodisc_t file_iodisc = {NULL, NULL, NULL};

#endif

/********************************************************************/
/* io disciplines - strings											*/

static int
stringiofread(void *chan, char *buf, int bufsize) {
    char *s = *(char **)chan;
    int len = strlen(s);

    strncpy(buf, s, bufsize);

    if (len>bufsize) {
        *(char **)chan += bufsize;
        return bufsize;

    } else {
        return len;
    }
}

static int
stringioputstr(void *chan, char *str) {
    int len = strlen(str);

    Tcl_DStringAppend((Tcl_DString *)chan, str, len);
    return len;
}

static int
stringioflush(void *chan) {
    return TCL_OK;
}

Agiodisc_t string_iodisc = {stringiofread, stringioputstr, stringioflush};

/********************************************************************/

char		   *
buildBindings(char *s1, char *s2)
/*
 * previous binding in s1 binding to be added in s2 result in s3
 *
 * if s2 begins with + then append (separated by \n) else s2 replaces if
 * resultant string is null then bindings are deleted
 */
{
    char		   *s3;
    int			 l;

    if (s2[0] == '+') {
        if (s1) {
            l = strlen(s2) - 1;

            if (l) {
                s3 = Tcl_Alloc(strlen(s1) + l + 2);
                assert (s3);
                strcpy(s3, s1);
                strcat(s3, "\n");
                strcat(s3, s2 + 1);
                Tcl_Free(s1);

            } else {
                s3 = s1;
            }

        } else {
            l = strlen(s2) - 1;

            if (l) {
                s3 = Tcl_Alloc(l + 2);
                assert (s3);
                strcpy(s3, s2 + 1);

            } else {
                s3 = (char *) NULL;
            }
        }

    } else {
        if (s1)
            Tcl_Free(s1);

        l = strlen(s2);

        if (l) {
            s3 = Tcl_Alloc(l + 2);
            assert (s3);
            strcpy(s3, s2);

        } else {
            s3 = (char *) NULL;
        }
    }

    return s3;
}

Agcbdisc_t gcbdisc = {
                         {insert_graph_cb, modify_graph_cb, delete_graph_cb},
                         {insert_node_cb, modify_node_cb, delete_node_cb},
                         {insert_edge_cb, modify_edge_cb, delete_edge_cb}
                     };

Agdisc_t gdisc = {(Agmemdisc_t *)NULL, &iddisc, &file_iodisc};

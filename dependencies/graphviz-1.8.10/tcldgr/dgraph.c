/*
 * Tcl extension for Dynamic Graphs by John Ellson (ellson@lucent.com)
 * 
 * Builds on libincr + libgraph by Stephen North (north@research.att.com)
 */

#include "dgr.h"

#ifdef DMALLOC
#include "dmalloc.h"
#endif

int
graphcmd(ClientData clientData, Tcl_Interp * interp, int argc, char *argv[]) {

    dgrInterp_t	   *dg = (dgrInterp_t *)clientData;
    dgGraph_t	   *gp, *sgp;
    Agraph_t	   *g, *sg;
    Agnode_t	   *n, **np, *tail, *head;
    Agedge_t	   *e, **ep;
    Agsym_t		   *sym;
    Tcl_DString        dstr;
    char			c, buf[256], *s, **argv2;
    char			gbuf[16], nbuf[16], ebuf[16];
    int			 	i, j, length, argc2, err, save;

#ifdef TCL_CHANNELS

    int				mode;
    Tcl_Channel		f;
#else
    FILE		   *f;
#endif

    if (argc < 2) {
        Tcl_AppendResult(interp, "wrong # args: should be \"",
                         argv[0], " option ?arg arg ...?\"", (char *) NULL);
        return TCL_ERROR;
    }

    if (!(gp = handle_to_graph(dg, argv[0]))) {
        Tcl_AppendResult(interp, "Invalid handle: \"", argv[0], "\"", (char *) NULL);
        return TCL_ERROR;
    }

    g = gp->g;
    c = argv[1][0];
    length = strlen(argv[1]);

    if ((c == 'a') && (strncmp(argv[1], "addedge", length) == 0)) {
        if (argc < 4) {
            Tcl_AppendResult(interp, "wrong # args: should be \"", argv[0],
                             " addedge tail head ?edgename? ?attributename attributevalue? ?...?\"",
                             (char *) NULL);
            return TCL_ERROR;
        }

        if (! (tail = handle_to_node(dg, argv[2]))) {
            if (!(tail = agnode(g, argv[2], FALSE))) {
                Tcl_AppendResult(interp, "Tail node \"", argv[2],
                                 "\" not found.", (char *) NULL);
                return TCL_ERROR;
            }
        }

        if (g != agraphof(tail)) {
            if (!(tail = agsubnode(g, tail, FALSE))) {
                Tcl_AppendResult(interp, "Tail node \"", argv[2],
                                 "\" is not in the graph.", (char *) NULL);
                return TCL_ERROR;
            }
        }

        if (! (head = handle_to_node(dg, argv[3]))) {
            if (!(head = agnode(g, argv[3], FALSE))) {
                Tcl_AppendResult(interp, "Head node \"", argv[3],
                                 "\" not found.", (char *) NULL);
                return TCL_ERROR;
            }
        }

        if (g != agraphof(head)) {
            if (!(head = agsubnode(g, head, FALSE))) {
                Tcl_AppendResult(interp, "Head node \"", argv[3],
                                 "\" is not in the graph.", (char *) NULL);
                return TCL_ERROR;
            }
        }

        save = dgrCallbacks(dg, g, FALSE);	/* delay callbacks until all done */
        i = 4;

        if (argc % 2) {
            /* if odd number of args then argv[4] is name */
            e = agedge(tail, head, argv[4], TRUE);
            i++;

        } else {
            e = agedge(tail, head, (char *) NULL, TRUE);
        }

        if (!e) {
            Tcl_AppendResult(interp, "Invalid edge for graph type.",
                             (char *) NULL);
            dgrCallbacks(dg, g, save);
            return TCL_ERROR;
        }

        ep = (Agedge_t **)tclhandleXlateIndex(dgrInterp.edgeTable, AGID(e));
        *ep = e;
        edge_to_handle(e, ebuf);

        if (dg->object_commands) {
            Tcl_CreateCommand(interp, ebuf,
                              edgecmd, clientData, (Tcl_CmdDeleteProc *) NULL);
        }

        for (; i < argc; i = i + 2) {
            if (!(sym = (Agsym_t *) agattrsym(e, argv[i]))) {
                sym = agattr(g, AGEDGE, argv[i], "");
            }

            agxset(e, sym, argv[i + 1]);
        }

        dgrCallbacks(dg, g, save);
        Tcl_ResetResult(interp);
        Tcl_AppendResult(interp, ebuf, (char *) NULL);
        return TCL_OK;

    } else if ((c == 'a') && (strncmp(argv[1], "addnode", length) == 0)) {
        save = dgrCallbacks(dg, g, FALSE);	/* delay callbacks until all done */
        i = 2;

        if (argc % 2) {
            /* if odd number of args then argv[2] is name */

            if (agisstrict(g) && (agnode(g, argv[2], FALSE))) {
                Tcl_AppendResult(interp, "Duplicate node in strict graph.",
                                 (char *) NULL);
                dgrCallbacks(dg, g, save);
                return TCL_ERROR;
            }

            n = agnode(g, argv[2], TRUE);
            i++;

        } else {
            n = agnode(g, (char *) NULL, TRUE);
        }

        if (!n) {
            Tcl_AppendResult(interp, "Invalid node for graph type.",
                             (char *) NULL);
            dgrCallbacks(dg, g, save);
            return TCL_ERROR;
        }

        np = (Agnode_t **)tclhandleXlateIndex(dgrInterp.nodeTable, AGID(n));
        *np = n;
        node_to_handle(n, nbuf);

        if (dg->object_commands) {
            Tcl_CreateCommand(interp, nbuf,
                              nodecmd, clientData, (Tcl_CmdDeleteProc *) NULL);
        }

        for (; i < argc; i = i + 2) {
            if (!(sym = (Agsym_t *) agattrsym(n, argv[i]))) {
                sym = agattr(g, AGNODE, argv[i], "");
            }

            agxset(n, sym, argv[i + 1]);
        }

        dgrCallbacks(dg, g, save);
        Tcl_ResetResult(interp);
        Tcl_AppendResult(interp, nbuf, (char *) NULL);
        return TCL_OK;

    } else if ((c == 'a') && (strncmp(argv[1], "addsubgraph", length) == 0)) {
        if (argc < 2) {
            Tcl_AppendResult(interp, "wrong # args: should be \"", argv[0],
                             "\" addsubgraph ?name? ?attributename attributevalue? ?...?",
                             (char *) NULL);
            return TCL_ERROR;
        }

        save = dgrCallbacks(dg, g, FALSE);	/* delay callbacks until all done */
        i = 2;

        if (argc % 2) {
            /* if odd number of args then argv[2] is name */
            sg = agsubg(g, argv[2], TRUE);
            i++;

        } else {
            sg = agsubg(g, (char *) NULL, TRUE);
        }

        sgp = (dgGraph_t *)tclhandleXlateIndex(dgrInterp.graphTable, AGID(sg));
        sgp->g = sg;

        /* not used in subgraphs
         *    	sgp->batch_cmd = (char *) NULL; 
         *    	sgp->insert_graph_cmd = (char *) NULL; 
         *    	sgp->modify_graph_cmd = (char *) NULL;
         *    	sgp->delete_graph_cmd = (char *) NULL;
         *    	sgp->insert_node_cmd = (char *) NULL;
         *   	sgp->modify_node_cmd = (char *) NULL;
         *  	sgp->delete_node_cmd = (char *) NULL;
         *    	sgp->insert_edge_cmd = (char *) NULL;
         *   	sgp->modify_edge_cmd = (char *) NULL;
         *   	sgp->delete_edge_cmd = (char *) NULL;
         */
        graph_to_handle(sg, gbuf);

        if (dg->object_commands) {
            Tcl_CreateCommand(interp, gbuf,
                              graphcmd, clientData, (Tcl_CmdDeleteProc *) NULL);
        }

        for (; i < argc; i = i + 2) {
            if (!(sym = (Agsym_t *) agattrsym(g, argv[i]))) {
                sym = agattr(g, AGRAPH, argv[i], "");
            }

            agxset(g, sym, argv[i + 1]);
        }

        dgrCallbacks(dg, g, save);
        Tcl_ResetResult(interp);
        Tcl_AppendResult(interp, gbuf, (char *) NULL);
        return TCL_OK;

    } else if ((c == 'b') && (strncmp(argv[1], "batch", length) == 0)) {
        if (argc < 3) {
            Tcl_AppendResult(interp, "wrong # args: should be \"",
                             argv[0], " batch <boolean>\"",
                             (char *) NULL);
            return TCL_ERROR;
        }

        if ((Tcl_GetBoolean(interp, argv[2], &i)) != TCL_OK) {
            return TCL_ERROR;
        }

        dgrCallbacks (dg, g, !i);
        return TCL_OK;

    } else if ((c == 'b') && (strncmp(argv[1], "bind", length) == 0)) {
        if ((argc < 2) || (argc > 4)) {
            Tcl_AppendResult(interp, "wrong # args: should be \"",
                             argv[0], " bind event ?command?\"",
                             "\nwhere \"event\" is one of:",
                             "\n  batch,",
                             "\n  insert_graph, insert_node, insert_edge,",
                             "\n  modify_graph, modify_node, modify_edge,",
                             "\n  delete_graph, delete_node, delete_edge.",
                             (char *) NULL);
            return TCL_ERROR;
        }

        if (argc == 2) {
            Tcl_AppendElement(interp, "batch");
            Tcl_AppendElement(interp, "insert_graph");
            Tcl_AppendElement(interp, "modify_graph");
            Tcl_AppendElement(interp, "delete_graph");
            Tcl_AppendElement(interp, "insert_node");
            Tcl_AppendElement(interp, "modify_node");
            Tcl_AppendElement(interp, "delete_node");
            Tcl_AppendElement(interp, "insert_edge");
            Tcl_AppendElement(interp, "modify_edge");
            Tcl_AppendElement(interp, "delete_edge");
            return TCL_OK;
        }

        length = strlen(argv[2]);

        if (strncmp(argv[2], "batch", length) == 0) {
            s = gp->batch_cmd;

            if (argc == 4)
                gp->batch_cmd = s = buildBindings(s, argv[3]);

        } else if (strncmp(argv[2], "insert_graph", length) == 0) {
            s = gp->insert_graph_cmd;

            if (argc == 4)
                gp->insert_graph_cmd = s = buildBindings(s, argv[3]);

        } else if (strncmp(argv[2], "modify_graph", length) == 0) {
            s = gp->modify_graph_cmd;

            if (argc == 4)
                gp->modify_graph_cmd = s = buildBindings(s, argv[3]);

        } else if (strncmp(argv[2], "delete_graph", length) == 0) {
            s = gp->delete_graph_cmd;

            if (argc == 4)
                gp->delete_graph_cmd = s = buildBindings(s, argv[3]);

        } else if (strncmp(argv[2], "insert_node", length) == 0) {
            s = gp->insert_node_cmd;

            if (argc == 4)
                gp->insert_node_cmd = s = buildBindings(s, argv[3]);

        } else if (strncmp(argv[2], "modify_node", length) == 0) {
            s = gp->modify_node_cmd;

            if (argc == 4)
                gp->modify_node_cmd = s = buildBindings(s, argv[3]);

        } else if (strncmp(argv[2], "delete_node", length) == 0) {
            s = gp->delete_node_cmd;

            if (argc == 4)
                gp->delete_node_cmd = s = buildBindings(s, argv[3]);

        } else if (strncmp(argv[2], "insert_edge", length) == 0) {
            s = gp->insert_edge_cmd;

            if (argc == 4)
                gp->insert_edge_cmd = s = buildBindings(s, argv[3]);

        } else if (strncmp(argv[2], "modify_edge", length) == 0) {
            s = gp->modify_edge_cmd;

            if (argc == 4)
                gp->modify_edge_cmd = s = buildBindings(s, argv[3]);

        } else if (strncmp(argv[2], "delete_edge", length) == 0) {
            s = gp->delete_edge_cmd;

            if (argc == 4)
                gp->delete_edge_cmd = s = buildBindings(s, argv[3]);

        } else {
            Tcl_AppendResult(interp, "unknown event \"", argv[2],
                             "\": must be one of:",
                             "\n\tbatch,",
                             "\n\tinsert_graph, modify_graph, delete_graph,",
                             "\n\tinsert_node, modify_node, delete_node,",
                             "\n\tinsert_edge, modify_edge, delete_edge.",
                             (char *) NULL);
            return TCL_ERROR;
        }

        if (argc == 3)
            Tcl_AppendResult(interp, s, (char *) NULL);

        return TCL_OK;

    } else if ((c == 'c') && (strncmp(argv[1], "concatfile", length) == 0)) {
        if (argc < 3) {
            Tcl_AppendResult(interp, "wrong # args: should be \"",
                             argv[0], " concatfile fileHandle\"", (char *) NULL);
            return TCL_ERROR;
        }

#ifdef TCL_CHANNELS
        f = Tcl_GetChannel(interp, argv[2], &mode);

        if (f == (Tcl_Channel) NULL) {
            Tcl_AppendResult(interp, "unable to find existing channel: ",
                             argv[2], (char *) NULL);
            return TCL_ERROR;
        }

#else
        if (Tcl_GetOpenFile(interp, argv[2], 0, 0, &f) != TCL_OK) {
            Tcl_AppendResult(interp, "failed to find open file: ",
                             argv[1], (char *) NULL);
            return TCL_ERROR;
        }

#endif
        g = agroot(g);

        save = dgrCallbacks(dg, g, FALSE);	/* delay callbacks until all done */

        gdisc.io = &file_iodisc;

        if (!(g = agconcat(g, f, &gdisc))) {
            dgrCallbacks(dg, g, save);
            Tcl_AppendResult(interp, "failure reading file: ",
                             argv[1], (char *) NULL);
            return TCL_ERROR;
        }

        dg_gpfromdot_init(dg, g);
        dgrCallbacks(dg, g, save);
        Tcl_ResetResult(interp);
        Tcl_AppendResult(interp, graph_to_handle(g, gbuf), (char *) NULL);
        return TCL_OK;

    } else if ((c == 'c') && (strncmp(argv[1], "concatstring", length) == 0)) {
        if (argc < 3) {
            Tcl_AppendResult(interp, "wrong # args: should be \"",
                             argv[0], " concatstring dot_string\"", (char *) NULL);
            return TCL_ERROR;
        }

        g = agroot(g);
        save = dgrCallbacks(dg, g, FALSE);	/* delay callbacks until all done */

        gdisc.io = &string_iodisc;

        /* s gets modified to point to next 8k block from argv[2] */
        s = argv[2];

        if (!(g = agconcat(g, &s, &gdisc))) {
            dgrCallbacks(dg, g, save);
            Tcl_AppendResult(interp, "failure reading string: ",
                             argv[1], (char *) NULL);
            return TCL_ERROR;
        }

        dg_gpfromdot_init(dg, g);
        dgrCallbacks(dg, g, save);
        Tcl_ResetResult(interp);
        Tcl_AppendResult(interp, graph_to_handle(g, gbuf), (char *) NULL);
        return TCL_OK;

    } else if ((c == 'c') && (strncmp(argv[1], "countnodes", length) == 0)) {
        sprintf(buf, "%u", agnnodes(g));
        Tcl_AppendResult(interp, buf, (char *) NULL);
        return TCL_OK;

    } else if ((c == 'c') && (strncmp(argv[1], "countedges", length) == 0)) {
        sprintf(buf, "%u", agnedges(g));
        Tcl_AppendResult(interp, buf, (char *) NULL);
        return TCL_OK;

    } else if ((c == 'd') && (strncmp(argv[1], "delete", length) == 0)) {
        save = dgrCallbacks(dg, g, FALSE);	/* delay callbacks until all done */
        deleteSubgraphs(dg, g);
        deleteNodes(dg, g);
        /* allow node and edge callbacks to occur */
        dgrCallbacks(dg, g, save);
        agclose(g);

        if (dg->object_commands) {
            Tcl_DeleteCommand(interp, argv[0]);
        }

        return TCL_OK;

    } else if ((c == 'f') && (strncmp(argv[1], "findedge", length) == 0)) {
        if (argc < 4) {
            Tcl_AppendResult(interp, "wrong # args: should be \"",
                             argv[0], " findedge tailnodename headnodename\"",
                             (char *) NULL);
            return TCL_ERROR;
        }

        if (!(tail = agnode(g, argv[2], FALSE))) {
            if (!(tail = handle_to_node(dg, argv[2]))) {
                Tcl_AppendResult(interp, "Tail node \"", argv[2],
                                 "\" not found.", (char *) NULL);
                return TCL_ERROR;
            }
        }

        if (!(head = agnode(g, argv[3], FALSE))) {
            if (!(head = handle_to_node(dg, argv[3]))) {
                Tcl_AppendResult(interp, "Head node \"", argv[3],
                                 "\" not found.", (char *) NULL);
                return TCL_ERROR;
            }
        }

        if (!(e = agedge(tail, head, (char *) NULL, FALSE))) {
            Tcl_AppendResult(interp, "Edge \"", argv[2],
                             " -- ", argv[3], "\" not found.",
                             (char *) NULL);
            return TCL_ERROR;
        }

        Tcl_AppendResult(interp, edge_to_handle(e, ebuf), (char *) NULL);
        return TCL_OK;

    } else if ((c == 'f') && (strncmp(argv[1], "findnode", length) == 0)) {
        if (argc < 3) {
            Tcl_AppendResult(interp, "wrong # args: should be \"",
                             argv[0], " findnode nodename\"",
                             (char *) NULL);
            return TCL_ERROR;
        }

        if (!(n = agnode(g, argv[2], FALSE))) {
            Tcl_AppendResult(interp, "Node not found.",
                             (char *) NULL);
            return TCL_ERROR;
        }

        Tcl_AppendResult(interp, node_to_handle(n, nbuf), (char *) NULL);
        return TCL_OK;

    } else if ((c == 'g') && (strncmp(argv[1], "graphof", length) == 0)) {
        Tcl_AppendResult(interp, graph_to_handle(agroot(g), gbuf), (char *) NULL);
        return TCL_OK;

    } else if ((c == 'l') && (strncmp(argv[1], "listattributes", length) == 0)) {
        for (sym = agnxtattr(agroot(g), AGRAPH, (Agsym_t *) NULL); sym;
                sym = agnxtattr(agroot(g), AGRAPH, sym)) {
            if (argc == 2 || Tcl_StringMatch(sym->name, argv[2])) {
                Tcl_AppendElement(interp, sym->name);
            }
        }

        return TCL_OK;

    } else if ((c == 'l') && (strncmp(argv[1], "listedgeattributes", length) == 0)) {
        for (sym = agnxtattr(g, AGEDGE, (Agsym_t *) NULL); sym;
                sym = agnxtattr(g, AGEDGE, sym)) {
            if (argc == 2 || Tcl_StringMatch(sym->name, argv[2])) {
                Tcl_AppendElement(interp, sym->name);
            }
        }

        return TCL_OK;

    } else if ((c == 'l') && (strncmp(argv[1], "listnodeattributes", length) == 0)) {
        for (sym = agnxtattr(g, AGNODE, (Agsym_t *) NULL); sym;
                sym = agnxtattr(g, AGNODE, sym)) {
            if (argc == 2 || Tcl_StringMatch(sym->name, argv[2])) {
                Tcl_AppendElement(interp, sym->name);
            }
        }

        return TCL_OK;

    } else if ((c == 'l') && (strncmp(argv[1], "listedges", length) == 0)) {
        if ((n = agfstnode(g)) && (e = agfstedge(n))) {
            for (; n; n = agnxtnode(n)) {
                for (e = agfstout(n); e; e = agnxtout(e)) {
                    if (attributematch(interp, (Agobj_t *)e, argc-2, &argv[2]) != TCL_OK)
                        return TCL_ERROR;
                }
            }
        }

        return TCL_OK;

    } else if ((c == 'l') && (strncmp(argv[1], "listnodes", length) == 0)) {
        n = agfstnode(g);

        for (; n; n = agnxtnode(n)) {
            if (attributematch(interp, (Agobj_t *)n, argc-2, &argv[2]) != TCL_OK)
                return TCL_ERROR;
        }

        return TCL_OK;

    } else if ((c == 'l') && (strncmp(argv[1], "listsubgraphs", length) == 0)) {
        sg = agfstsubg(g);

        for (; sg; sg = agnxtsubg(sg)) {
            if (attributematch(interp, (Agobj_t *)sg, argc-2, &argv[2]) != TCL_OK)
                return TCL_ERROR;
        }

        return TCL_OK;

    } else if ((c == 'n') && (strncmp(argv[1], "nextedge", length) == 0)) {
        if (argc % 2) {
            i = 3;

            if (!(e = handle_to_edge(dg, argv[2]))) {
                Tcl_AppendResult(interp, "edge \"", argv[2],
                                 "\" not found.", (char *) NULL);
                return TCL_ERROR;
            }

            n = agtail(e);

        } else {
            i = 2;
            n = agfstnode(g);

            if (n)
                e = agfstedge(n);
            else
                e = (Agedge_t *)NULL;
        }

        while(n) {
            while(e) {
                if (attributematch(interp, (Agobj_t *)e, argc-i, &argv[i]) != TCL_OK)
                    return TCL_ERROR;

                if (interp->result[0])
                    return TCL_OK;

                e = agnxtout(e);
            }

            n = agnxtnode(n);

            if (n)
                e = agfstout(n);
        }

        return TCL_OK;

    } else if ((c == 'n') && (strncmp(argv[1], "nextnode", length) == 0)) {
        if (argc % 2) {
            i = 3;

            if (!(n = handle_to_node(dg, argv[2]))) {
                Tcl_AppendResult(interp, "node \"", argv[2],
                                 "\" not found.", (char *) NULL);
                return TCL_ERROR;
            }

            n = agnxtnode(n);

        } else {
            i = 2;
            n = agfstnode(g);
        }

        for (; n; n = agnxtnode(n)) {
            if (attributematch(interp, (Agobj_t *)n, argc-i, &argv[i]) != TCL_OK)
                return TCL_ERROR;

            if (interp->result[0])
                return TCL_OK;
        }

        return TCL_OK;

    } else if ((c == 'p') && (strncmp(argv[1], "parentgraph", length) == 0)) {
        if (g != agroot(g)) {
            Tcl_AppendResult(interp, graph_to_handle(agparent(g), gbuf),
                             (char *) NULL);
        }

        return TCL_OK;

    } else if ((c == 'q') && (strncmp(argv[1], "queryattributes", length) == 0)) {
        for (i = 2; i < argc; i++) {
            if ((err = Tcl_SplitList(interp, argv[i], &argc2, (CONST84 char ***) &argv2)) != TCL_OK)
                return err;

            g = agroot(g);  /* subgraph attributes don't work */

            for (j = 0; j < argc2; j++) {
                if (!(sym = (Agsym_t *) agattrsym(g, argv2[j]))) {
                    Tcl_AppendResult(interp, " No attribute named \"",
                                     argv2[j], "\"", (char *) 0);
                    Tcl_Free((char *) argv2);
                    return TCL_ERROR;
                }

                Tcl_AppendElement(interp, agxget(g, sym));
            }

            Tcl_Free((char *) argv2);
        }

        return TCL_OK;

    } else if ((c == 'q') && (strncmp(argv[1], "queryedgeattributes", length) == 0)) {
        for (i = 2; i < argc; i++) {
            if ((err = Tcl_SplitList(interp, argv[i], &argc2, (CONST84 char ***) &argv2)) != TCL_OK)
                return err;

            for (j = 0; j < argc2; j++) {
                if (!(sym = agattr(g, AGEDGE, argv2[j], (char *) NULL))) {
                    Tcl_AppendResult(interp, " No attribute named \"",
                                     argv2[j], "\"", (char *) 0);
                    Tcl_Free((char *) argv2);
                    return TCL_ERROR;
                }

                Tcl_AppendElement(interp, sym->defval);
            }

            Tcl_Free((char *) argv2);
        }

        return TCL_OK;

    } else if ((c == 'q') && (strncmp(argv[1], "querynodeattributes", length) == 0)) {
        for (i = 2; i < argc; i++) {
            if ((err = Tcl_SplitList(interp, argv[i], &argc2, (CONST84 char ***) &argv2)) != TCL_OK)
                return err;

            for (j = 0; j < argc2; j++) {
                if (!(sym = agattr(g, AGNODE, argv2[j], (char *) NULL))) {
                    Tcl_AppendResult(interp, " No attribute named \"",
                                     argv2[j], "\"", (char *) 0);
                    Tcl_Free((char *) argv2);
                    return TCL_ERROR;
                }

                Tcl_AppendElement(interp, sym->defval);
            }

            Tcl_Free((char *) argv2);
        }

        return TCL_OK;

    } else if ((c == 's') && (strncmp(argv[1], "set", length) == 0)) {
        if (argc < 3) {
            Tcl_AppendResult(interp, "wrong # args: should be: ",
                             argv[0], "set attributename ?attributevalue?", (char *) NULL);
            return TCL_ERROR;
        }

        g = agroot(g);

        if (argc == 3) {
            if (!(sym = (Agsym_t *) agattrsym(g, argv[2]))) {
                Tcl_AppendResult(interp, " No attribute named \"",
                                 argv[2], "\"", (char *) 0);
                return TCL_ERROR;
            }

            Tcl_AppendResult(interp, agxget(g, sym), (char *) NULL);

        } else {
            save = dgrCallbacks(dg, g, FALSE);	/* delay callbacks until all done */

            if (!(sym = (Agsym_t *) agattrsym(g, argv[2])))
                sym = agattr(g, AGRAPH, argv[2], "");

            if (strcmp(agxget(g, sym), argv[3]))
                agxset(g, sym, argv[3]);

            dgrCallbacks(dg, g, save);

            Tcl_ResetResult(interp);

            Tcl_AppendResult(interp, argv[3], (char *) NULL);
        }

        return TCL_OK;

    } else if ((c == 's') && (strncmp(argv[1], "setattributes", length) == 0)) {
        if ((argc < 4) || (argc % 2)) {
            Tcl_AppendResult(interp, "wrong # args: should be \"", argv[0],
                             " setattributes attributename attributevalue ?attributename attributevalue? ?...?\"",
                             (char *) NULL);
            return TCL_ERROR;
        }

        save = dgrCallbacks(dg, g, FALSE);	/* delay callbacks until all done */

        for (i = 2; i < argc; i = i + 2) {
            if (!(sym = (Agsym_t *) agattrsym(g, argv[i]))) {
                sym = agattr(g, AGRAPH, argv[i], "");
            }

            agxset(g, sym, argv[i + 1]);
        }

        dgrCallbacks(dg, g, save);
        return TCL_OK;

    } else if ((c == 's') && (strncmp(argv[1], "setedgeattributes", length) == 0)) {
        if ((argc < 4) || (argc % 2)) {
            Tcl_AppendResult(interp, "wrong # args: should be \"", argv[0],
                             "\" setedgeattributes attributename attributevalue ?attributename attributevalue? ?...?",
                             (char *) NULL);
            return TCL_ERROR;
        }

        save = dgrCallbacks(dg, g, FALSE);	/* delay callbacks until all done */

        for (i = 2; i < argc; i = i + 2) {
            agattr(g, AGEDGE, argv[i], argv[i + 1]);
        }

        dgrCallbacks(dg, g, save);
        return TCL_OK;

    } else if ((c == 's') && (strncmp(argv[1], "setnodeattributes", length) == 0)) {
        if ((argc < 4) || (argc % 2)) {
            Tcl_AppendResult(interp, "wrong # args: should be \"", argv[0],
                             "\" setnodeattributes attributename attributevalue ?attributename attributevalue? ?...?",
                             (char *) NULL);
            return TCL_ERROR;
        }

        save = dgrCallbacks(dg, g, FALSE);	/* delay callbacks until all done */

        for (i = 2; i < argc; i = i + 2) {
            agattr(g, AGNODE, argv[i], argv[i + 1]);
        }

        dgrCallbacks(dg, g, save);
        return TCL_OK;

    } else if ((c == 's') && (strncmp(argv[1], "showname", length) == 0)) {
        Tcl_AppendResult(interp, agnameof(g), (char *) NULL);
        return TCL_OK;

    } else if ((c == 's') && (strncmp(argv[1], "showtype", length) == 0)) {
        if (g->desc.strict)
            Tcl_AppendResult(interp, "strict", (char *) NULL);

        if (g->desc.directed)
            Tcl_AppendResult(interp, "di", (char *) NULL);

        Tcl_AppendResult(interp, "graph", (char *) NULL);

        return TCL_OK;

    } else if ((c == 'w') && (strncmp(argv[1], "write", length) == 0)) {
        if (argc < 3) {
            /* this is ugly! */
            g->clos->disc.io = &string_iodisc;
            Tcl_DStringInit(&dstr);

            if (agwrite(g, &dstr)) {
                Tcl_AppendResult(interp, "error while writing to string.",
                                 (char *) NULL);
                return TCL_ERROR;
            }

            Tcl_DStringResult(interp, &dstr);

        } else {
            /* this is ugly! */
            g->clos->disc.io = &file_iodisc;
#ifdef TCL_CHANNELS

            f = Tcl_GetChannel(interp, argv[2], &mode);

            if (f == (Tcl_Channel) NULL) {
                Tcl_AppendResult(interp, "unable to open channel: ",
                                 argv[2], (char *) NULL);
                return TCL_ERROR;
            }

            if ((mode & TCL_WRITABLE) == 0) {
                Tcl_AppendResult(interp, "channel \"", argv[2],
                                 "\" wasn't opened for writing", (char *) NULL);
                return TCL_ERROR;
            }

#else
            if (Tcl_GetOpenFile(interp, argv[2], 1, 1, &f) != TCL_OK) {
                Tcl_AppendResult(interp, "failed to find open file: ",
                                 argv[1], (char *) NULL);
                return TCL_ERROR;
            }

#endif
            if (agwrite(g, f)) {
                Tcl_AppendResult(interp, "error while writing to file.",
                                 (char *) NULL);
                return TCL_ERROR;
            }
        }

        return TCL_OK;

    } else if (c == '_') {
        /* eval user defined method with is stored in an attribute */

        if (!(sym = (Agsym_t *) agattrsym(g, argv[1]))) {
            Tcl_AppendResult(interp, " No attribute named \"", argv[1],
                             "\"", (char *) 0);
            return TCL_ERROR;
        }

        /*
         * if there are any other args then pass them as a proper list in the
         * substitution for %a
         */
        i = 0;

        s = "";

        if (argc > 2) {
            i++;
            s = Tcl_Merge(argc - 2, (CONST84 char * CONST *) argv + 2);
        }

        dgrExpandPercentsEval(interp, agxget(g, sym),
                              graph_to_handle(g, gbuf), "", "", "", s);

        if (i)
            ckfree(s);

        return TCL_OK;

    } else {
        Tcl_AppendResult(interp,
                         "bad option \"", argv[1], "\" must be one of:",
                         "\n\taddedge, addnode, addsubgraph, batch, bind, concatfile,",
                         "\n\tconcatstring, countedges, countnodes, findedge, graphof",
                         "\n\tlistattributes, listedgeattributes, listnodeattributes,",
                         "\n\tlistedges, listnodes, listsubgraphs,",
                         "\n\tnextnode, nextedge, parentgraph, queryattributes,",
                         "\n\tqueryedgeattributes, querynodeattributes, set,",
                         "\n\tsetattributes, setedgeattributes, setnodeattributes,",
                         "\n\tshowname, showtype, write.", (char *) NULL);
        return TCL_ERROR;
    }
}

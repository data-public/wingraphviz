#include <assert.h>
#include <tcl.h>
#include <agraph.h>
#include "tclhandle.h"

#ifdef HAVE_CONFIG_H
#include "gvconfig.h"
#endif

#if TCL_MAJOR_VERSION == 7
#if TCL_MINOR_VERSION >= 5
#define TCL_CHANNELS
#endif
#else
#if TCL_MAJOR_VERSION > 7
#define TCL_CHANNELS
#endif
#endif

#ifndef CONST84
#define CONST84
#endif

typedef struct dgrInterp_s dgrInterp_t;

typedef struct dgGraph_s dgGraph_t;

struct dgrInterp_s {
    Tcl_Interp		*interp;
    int				object_commands;
    tblHeader_pt	graphTable, nodeTable, edgeTable;
};

struct dgGraph_s {
    Agraph_t	   *g;
    int				callbacks_enabled;
    char           *batch_cmd;
    char           *insert_graph_cmd;
    char           *modify_graph_cmd;
    char           *delete_graph_cmd;
    char           *insert_node_cmd;
    char           *modify_node_cmd;
    char           *delete_node_cmd;
    char           *insert_edge_cmd;
    char           *modify_edge_cmd;
    char           *delete_edge_cmd;
};

/* functions from dgraph.c */
int graphcmd(ClientData clientData, Tcl_Interp *interp, int argc, char *argv[]);

/* functions from dgnode.c */
int nodecmd(ClientData clientData, Tcl_Interp *interp, int argc, char *argv[]);

/* functions from dgedge.c */
int edgecmd(ClientData clientData, Tcl_Interp *interp, int argc, char *argv[]);

/* functions from dgrutil.c */
char * graph_to_handle(Agraph_t * g, char * buf);
char * edge_to_handle(Agedge_t * e, char * buf);
char * node_to_handle(Agnode_t * n, char * buf);
dgGraph_t * handle_to_graph(dgrInterp_t *dg, char *s);
Agedge_t * handle_to_edge(dgrInterp_t *dg, char *s);
Agnode_t * handle_to_node(dgrInterp_t *dg, char *s);
int dgrCallbacks (dgrInterp_t *dg, Agraph_t *g, int flag);
void dg_gpstruct_init (dgrInterp_t *dg, Agraph_t *g);
void dg_gpfromdot_init (dgrInterp_t *dg, Agraph_t *g);

int attributematch(Tcl_Interp *interp, Agobj_t *obj, int argc, char **argv);

char *dgrExpandPercentsEval(
    Tcl_Interp * interp,     /* interpreter context */
    register char *before,   /* Command with percent expressions */
    char *g,     /* graphHandle string to substitute for "%g" */
    char *n,     /* nodeHandle string to substitute for "%n" */
    char *e,     /* edgeHandle string to substitute for "%e" */
    char *A,     /* attributeName or shape string to substitute for "%A" */
    char *a      /* attributeValue or arglist string to substitute for "%a" */
);

void deleteEdges(dgrInterp_t *dg, Agnode_t * n);
void deleteNodes(dgrInterp_t *dg, Agraph_t * g);
void deleteSubgraphs(dgrInterp_t *dg, Agraph_t * g);

char *buildBindings(char *s1, char *s2);

extern dgrInterp_t dgrInterp;
extern Agcbdisc_t gcbdisc;
extern Agiodisc_t file_iodisc;
extern Agiodisc_t string_iodisc;
extern Agdisc_t gdisc;

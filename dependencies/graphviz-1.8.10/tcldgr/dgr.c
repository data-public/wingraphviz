/*
 * Tcl extension for Dynamic Graphs by John Ellson (ellson@lucent.com)
 * 
 * Builds on libincr + libgraph by Stephen North (north@research.att.com)
 */

#include "dgr.h"

#ifdef DMALLOC
#include "dmalloc.h"
#endif

/* FIXME - not thread safe */
dgrInterp_t dgrInterp;

static int
dgnew_cmd(ClientData clientData, Tcl_Interp *interp, int argc, char *argv[]) {
    dgrInterp_t		*dg = (dgrInterp_t *)clientData;
    Agraph_t		*g;
    Agsym_t			*sym;
    Agdesc_t		kind;
    char			c;
    int			i, length;

    if ((argc < 2)) {
        Tcl_AppendResult(interp, "wrong # args: should be \"", argv[0],
                         " graphtype ?graphname? ?attributename attributevalue? ?...?\"",
                         (char *) NULL);
        return TCL_ERROR;
    }

    c = argv[1][0];
    length = strlen(argv[1]);
    /* string matching order is important when leading chars match.
    	The shorter string must be tested first. */

    if ((c == 'd') && (strncmp(argv[1], "digraph", length) == 0)) {
        kind = Agdirected;

    } else if ((c == 'd') && (strncmp(argv[1], "digraphstrict", length) == 0)) {
        kind = Agstrictdirected;

    } else if ((c == 'g') && (strncmp(argv[1], "graph", length) == 0)) {
        kind = Agundirected;

    } else if ((c == 'g') && (strncmp(argv[1], "graphstrict", length) == 0)) {
        kind = Agstrictundirected;

    } else if ((c == 's') && (strncmp(argv[1], "strictgraph", length) == 0)) {
        kind = Agstrictundirected;

    } else if ((c == 's') && (strncmp(argv[1], "strictdigraph", length) == 0)) {
        kind = Agstrictdirected;

    } else {
        Tcl_AppendResult(interp, "bad graphtype \"", argv[1],
                         "\": must be one of:",
                         "\n\tdigraph, strictdigraph, graph, strictgraph.",
                         (char *) NULL);
        return TCL_ERROR;
    }

    i = 2;

    if (argc % 2) {
        /* if odd number of args then argv[2] is name */
        g = agopen(argv[2], kind, &gdisc);
        i++;

    } else {
        g = agopen((char *) NULL, kind, &gdisc);
    }

    for (; i < argc; i = i + 2) {
        if (!(sym = (Agsym_t *) agattrsym(g, argv[i]))) {
            sym = agattr(g, AGRAPH, argv[i], "");
        }

        agxset(g, sym, argv[i + 1]);
    }

    dg_gpstruct_init (dg, g);
    return TCL_OK;
}

static int
dgread_cmd(ClientData clientData, Tcl_Interp * interp, int argc, char *argv[]) {
    dgrInterp_t	  *dg = (dgrInterp_t *)clientData;
    Agraph_t	   *g;

    /* TCL_CHANNELS are available in tcl7.5 and later */
#ifdef TCL_CHANNELS

    int					mode;
    Tcl_Channel	 f;
#else
    FILE		   *f;
#endif

    /* first use determines mode */

    if (dg->object_commands == -1) {
        dg->object_commands = 1;
        Tcl_DeleteCommand(interp, "dg");
    }

    if (argc < 2) {
        Tcl_AppendResult(interp, "wrong # args: should be \"",
                         argv[0], " fileHandle\"", (char *) NULL);
        return TCL_ERROR;
    }

#ifdef TCL_CHANNELS
    f = Tcl_GetChannel(interp, argv[1], &mode);

    if (f == (Tcl_Channel) NULL) {
        Tcl_AppendResult(interp, "unable to open channel: ",
                         argv[1], (char *) NULL);
        return TCL_ERROR;
    }

#else
    if (Tcl_GetOpenFile(interp, argv[1], 0, 0, &f) != TCL_OK) {
        Tcl_AppendResult(interp, "failed to find open file: ",
                         argv[1], (char *) NULL);
        return TCL_ERROR;
    }

#endif

    gdisc.io = &file_iodisc;

    if (!(g = agconcat((Agraph_t *) NULL, f, &gdisc))) {
        Tcl_AppendResult(interp, "failure reading file: ",
                         argv[1], (char *) NULL);
        return TCL_ERROR;
    }

    dg_gpfromdot_init(dg, g);
    dg_gpstruct_init(dg, g);
    return TCL_OK;
}

static int
dgstring_cmd(ClientData clientData, Tcl_Interp * interp, int argc, char *argv[]) {
    dgrInterp_t	   *dg = (dgrInterp_t *)clientData;
    Agraph_t	   *g;
    char		   *s;

    /* first use determines mode */

    if (dg->object_commands == -1) {
        dg->object_commands = 1;
        Tcl_DeleteCommand(interp, "dg");
    }

    if (argc < 2) {
        Tcl_AppendResult(interp, "wrong # args: should be \"",
                         argv[0], " dot_string\"", (char *) NULL);
        return TCL_ERROR;
    }

    gdisc.io = &string_iodisc;

    /* s gets modified to point to 8K blocks in argv[1] */
    s = argv[1];

    if (!(g = agconcat((Agraph_t *) NULL, &s, &gdisc))) {
        Tcl_AppendResult(interp, "failure reading string: ",
                         argv[1], (char *) NULL);
        return TCL_ERROR;
    }

    dg_gpfromdot_init(dg, g);
    dg_gpstruct_init(dg, g);
    return TCL_OK;
}

/*
 * dgreset is intended for regression test use only.  It resets the handle
 * tables so that we get predictable handles that are independent of any
 * previous tests.
 * 
 * dgreset will return an error if handles in the old tables are still in use
 */
static int
dgreset_cmd(ClientData clientData, Tcl_Interp * interp, int argc, char *argv[]) {
    dgrInterp_t *dg = (dgrInterp_t *)clientData;

    if (tclhandleReset(dg->graphTable, 10) == TCL_ERROR) {
        Tcl_AppendResult(interp, "graph handles still in use", (char *) NULL);
        return TCL_ERROR;
    }

    if (tclhandleReset(dg->nodeTable, 100) == TCL_ERROR) {
        Tcl_AppendResult(interp, "node handles still in use", (char *) NULL);
        return TCL_ERROR;
    }

    if (tclhandleReset(dg->edgeTable, 100) == TCL_ERROR) {
        Tcl_AppendResult(interp, "edge handles still in use", (char *) NULL);
        return TCL_ERROR;
    }

    return TCL_OK;
}

static int
dg_cmd(ClientData clientData, Tcl_Interp * interp, int argc, char *argv[]) {
    char		c;
    dgrInterp_t *dg = (dgrInterp_t *)clientData;

    /* first use determines mode */

    if (dg->object_commands == -1) {
        dg->object_commands = 0;
        Tcl_DeleteCommand(interp, "dgnew");
        Tcl_DeleteCommand(interp, "dgread");
        Tcl_DeleteCommand(interp, "dgstring");
    }

    if ((argc < 2) || strlen(argv[1]) < 3) {
        Tcl_AppendResult(interp, "wrong # args: should be \"", argv[0],
                         "\"method ...\"",  "where method is one of:",
                         "\n\tread, graph, digraph, strictgraph, strictdigraph,",
                         "\n\t<graphhandle>, <nodehandle>, <edgehandle>.", (char *) NULL);
        return TCL_ERROR;
    }

    c = argv[1][2];

    if (c == 'N') {
        return nodecmd(clientData, interp, argc-1, &argv[1]);

    } else if (c == 'E') {
        return edgecmd(clientData, interp, argc-1, &argv[1]);

    } else if (c == 'G') {
        return graphcmd(clientData, interp, argc-1, &argv[1]);

    } else if (strncmp(argv[1], "new", 3) == 0) {
        return dgnew_cmd(clientData, interp, argc-1, &argv[1]);

    } else if (strncmp(argv[1], "read", 4) == 0) {
        return dgread_cmd(clientData, interp, argc-1, &argv[1]);

    } else if (strncmp(argv[1], "string", 6) == 0) {
        return dgstring_cmd(clientData, interp, argc-1, &argv[1]);
    }

    Tcl_AppendResult(interp, "invalid method: ", argv[1], (char *) NULL);
    return TCL_ERROR;
}

int
Tcldgr_Init(Tcl_Interp * interp) {
    dgrInterp_t *dg = &dgrInterp;

#ifdef USE_TCL_STUBS

    if (Tcl_InitStubs(interp, TCL_VERSION, 0) == NULL) {
        return TCL_ERROR;
    }

#else
    if (Tcl_PkgRequire(interp, "Tcl", TCL_VERSION, 0) == NULL) {
        return TCL_ERROR;
    }

#endif
    if (Tcl_PkgProvide(interp, "Tcldgr", VERSION) != TCL_OK) {
        return TCL_ERROR;
    }

    /* FIXME - this is what we want
     *	dg = (dgrInterp_t *)Tcl_Alloc(sizeof(dgrInterp_t));
     */

    dg->object_commands = -1;

    dg->graphTable = tclhandleInit("dgG",sizeof(dgGraph_t), 10);

    dg->nodeTable = tclhandleInit("dgN",sizeof(Agnode_t *), 100);

    dg->edgeTable = tclhandleInit("dgE",sizeof(Agedge_t *), 100);

    dg->interp = interp;

    Tcl_CreateCommand(interp, "dg", dg_cmd, (ClientData)dg,
                      (Tcl_CmdDeleteProc *) NULL);

    Tcl_CreateCommand(interp, "dgnew", dgnew_cmd, (ClientData)dg,
                      (Tcl_CmdDeleteProc *) NULL);

    Tcl_CreateCommand(interp, "dgread", dgread_cmd, (ClientData)dg,
                      (Tcl_CmdDeleteProc *) NULL);

    Tcl_CreateCommand(interp, "dgstring", dgstring_cmd, (ClientData)dg,
                      (Tcl_CmdDeleteProc *) NULL);

    Tcl_CreateCommand(interp, "dgreset", dgreset_cmd, (ClientData)dg,
                      (Tcl_CmdDeleteProc *) NULL);

    return TCL_OK;
}

int
Tcldgr_SafeInit(Tcl_Interp * interp) {
    return Tcldgr_Init(interp);
}

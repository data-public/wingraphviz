/*
 * Tcl extension for Dynamic Graphs by John Ellson (ellson@lucent.com)
 * 
 * Builds on libincr + libgraph by Stephen North (north@research.att.com)
 */

#include "dgr.h"

#ifdef DMALLOC
#include "dmalloc.h"
#endif

int
nodecmd(ClientData clientData, Tcl_Interp * interp, int argc, char *argv[]) {

    dgrInterp_t     *dg = (dgrInterp_t *)clientData;
    char            c, buf[16];
    char           *s, **argv2;
    int             i, j, length, argc2, err, save;
    Agraph_t       *g, *g1, *groot, *ghead;
    Agnode_t       *n, *head;
    Agedge_t       *e, **ep;
    Agsym_t        *sym;
    char		    gbuf[16], nbuf[16], ebuf[16];

    if (argc < 2) {
        Tcl_AppendResult(interp, "wrong # args: should be \"",
                         argv[0], " option ?arg arg ...?\"", (char *) NULL);
        return TCL_ERROR;
    }

    if (!(n = handle_to_node(dg, argv[0]))) {
        Tcl_AppendResult(interp, "Invalid handle: \"", argv[0], "\"", (char *) NULL);
        return TCL_ERROR;
    }

    g = agraphof(n);
    c = argv[1][0];
    length = strlen(argv[1]);

    if ((c == 'a') && (strncmp(argv[1], "addedge", length) == 0)) {
        if (argc < 3) {
            Tcl_AppendResult(interp, "wrong # args: should be \"",
                             argv[0], " addedge head ?edgename? ?attributename attributevalue? ?...?\"",
                             (char *) NULL);
            return TCL_ERROR;
        }

        groot = agroot(g);

        if (! (head = handle_to_node(dg, argv[2]))) {
            if (!(head = agnode(groot, argv[2], FALSE))) {
                Tcl_AppendResult(interp, "Head node \"", argv[2],
                                 "\" not found.", (char *) NULL);
                return TCL_ERROR;
            }
        }

        ghead = agraphof(head);

        if (groot != agroot(ghead)) {
            Tcl_AppendResult(interp, "Nodes ", argv[0], " and ", argv[2],
                             " are not in the same graph.", (char *) NULL);
            return TCL_ERROR;
        }

        /* find the lowest subgraph that contains both nodes */
        /* is guaranteed to terminate at groot */
        for ( ; g != groot; g = agparent(g)) {
            for (g1 = ghead; g1 != groot ; g1 = agparent(g1)) {
                if (g == g1)
                    break;
            }

            if (g == g1)
                break;
        }

        n = agsubnode(g, n, FALSE);
        head = agsubnode(g, head, FALSE);
        save = dgrCallbacks(dg, g, FALSE);	/* delay callbacks until all done */
        i = 3;

        if (!(argc % 2)) {
            /* if even number of args then argv[3] is name */
            e = agedge(n, head, argv[3], TRUE);
            i++;

        } else {
            e = agedge(n, head, (char *) NULL, TRUE);
        }

        if (!e) {
            Tcl_AppendResult(interp, "Invalid edge for graph type.",
                             (char *) NULL);
            dgrCallbacks(dg, g, save);
            return TCL_ERROR;
        }

        ep = (Agedge_t **)tclhandleXlateIndex(dgrInterp.edgeTable, AGID(e));
        *ep = e;
        edge_to_handle(e, ebuf);

        if (dg->object_commands) {
            Tcl_CreateCommand(interp, ebuf,
                              edgecmd, clientData, (Tcl_CmdDeleteProc *) NULL);
        }

        for (; i < argc; i = i + 2) {
            if (!(sym = (Agsym_t *) agattrsym(e, argv[i]))) {
                sym = agattr(g, AGEDGE, argv[i], "");
            }

            agxset(e, sym, argv[i + 1]);
        }

        dgrCallbacks(dg, g, save);
        Tcl_ResetResult(interp);
        Tcl_AppendResult(interp, ebuf, (char *) NULL);
        return TCL_OK;

    } else if ((c == 'c') && (strncmp(argv[1], "countedges", length) == 0)) {
        sprintf(buf, "%u", agdegree(n, TRUE, TRUE));
        Tcl_AppendResult(interp, buf, (char *) NULL);
        return TCL_OK;

    } else if ((c == 'c') && (strncmp(argv[1], "countinedges", length) == 0)) {
        sprintf(buf, "%u", agdegree(n, TRUE, FALSE));
        Tcl_AppendResult(interp, buf, (char *) NULL);
        return TCL_OK;

    } else if ((c == 'c') && (strncmp(argv[1], "countoutedges", length) == 0)) {
        sprintf(buf, "%u", agdegree(n, FALSE, TRUE));
        Tcl_AppendResult(interp, buf, (char *) NULL);
        return TCL_OK;

    } else if ((c == 'd') && (strncmp(argv[1], "delete", length) == 0)) {
        save = dgrCallbacks(dg, g, FALSE);	/* delay callbacks until all done */
        deleteEdges(dg, n);
        agdelnode(n);

        if (dg->object_commands) {
            Tcl_DeleteCommand(interp, argv[0]);
        }

        dgrCallbacks(dg, g, save);
        return TCL_OK;

    } else if ((c == 'f') && (strncmp(argv[1], "findedge", length) == 0)) {
        if (argc < 3) {
            Tcl_AppendResult(interp, "wrong # args: should be \"",
                             argv[0], " findedge headnodename\"",
                             (char *) NULL);
            return TCL_ERROR;
        }

        if (!(head = agnode(g, argv[2], FALSE))) {
            if (!(head = handle_to_node(dg, argv[2]))) {
                Tcl_AppendResult(interp, "Head node \"", argv[2],
                                 "\" not found.", (char *) NULL);
                return TCL_ERROR;
            }
        }

        if (!(e = agedge(n, head, (char *) NULL, FALSE))) {
            Tcl_AppendResult(interp, "Edge \"", argv[0],
                             " -- ", argv[2], "\" not found.",
                             (char *) NULL);
            return TCL_ERROR;
        }

        Tcl_AppendElement(interp, edge_to_handle(e, ebuf));
        return TCL_OK;

    } else if ((c == 'g') && (strncmp(argv[1], "graphof", length) == 0)) {
        Tcl_AppendResult(interp, graph_to_handle(g, gbuf),
                         (char *) NULL);
        return TCL_OK;

    } else if ((c == 'l') && (strncmp(argv[1], "listattributes", length) == 0)) {
        g = agroot(g);

        for (sym = agnxtattr(g, AGNODE, (Agsym_t *) NULL); sym;
                sym = agnxtattr(g, AGNODE, sym)) {
            if (argc == 2 || Tcl_StringMatch(sym->name, argv[2])) {
                Tcl_AppendElement(interp, sym->name);
            }
        }

        return TCL_OK;

    } else if ((c == 'l') && (strncmp(argv[1], "listedges", length) == 0)) {
        if ((e = agfstedge(n))) {
            for (; e; e = agnxtedge(e, n)) {
                if (attributematch(interp, (Agobj_t *)e, argc-2, &argv[2]) != TCL_OK)
                    return TCL_ERROR;
            }
        }

        return TCL_OK;

    } else if ((c == 'l') && (strncmp(argv[1], "listinedges", length) == 0)) {
        if ((e = agfstin(n))) {
            for (; e; e = agnxtin(e)) {
                if (attributematch(interp, (Agobj_t *)e, argc-2, &argv[2]) != TCL_OK)
                    return TCL_ERROR;
            }
        }

        return TCL_OK;

    } else if ((c == 'l') && (strncmp(argv[1], "listoutedges", length) == 0)) {
        if ((e = agfstout(n))) {
            for (; e; e = agnxtout(e)) {
                if (attributematch(interp, (Agobj_t *)e, argc-2, &argv[2]) != TCL_OK)
                    return TCL_ERROR;
            }
        }

        return TCL_OK;

    } else if ((c == 'n') && (strncmp(argv[1], "nextedge", length) == 0)) {
        /*
         * <node> nextedge ?<edge>? ?<attributename> <attributevaluepattern>? ...
         *
         * if <edge> is omitted then returns first edge
         */

        if (argc % 2) {
            i = 3;

            if (!(e = handle_to_edge(dg, argv[2]))) {
                Tcl_AppendResult(interp, "edge not found: \"",
                                 argv[i], "\"", (char *) 0);
                return TCL_ERROR;
            }

            e = agnxtedge(e, n);

        } else {
            i = 2;
            e = agfstedge(n);
        }

        for (; e; e = agnxtedge(e, n)) {
            if (attributematch(interp, (Agobj_t *)e, argc-i, &argv[i]) != TCL_OK)
                return TCL_ERROR;

            if (interp->result[0])
                return TCL_OK;
        }

        return TCL_OK;

    } else if ((c == 'n') && (strncmp(argv[1], "nextinedge", length) == 0)) {
        /*
         * <node> nextinedge ?<edge>? ?<attributename> <attributevaluepattern>? ...
         *
         * if <edge> is omitted then returns first edge
         */

        if (argc % 2) {
            i = 3;

            if (!(e = handle_to_edge(dg, argv[2]))) {
                Tcl_AppendResult(interp, "edge not found: \"",
                                 argv[2], "\"", (char *) 0);
                return TCL_ERROR;
            }

            e = agnxtin(e);

        } else {
            i = 2;
            e = agfstin(n);
        }

        for (; e; e = agnxtin(e)) {
            if (attributematch(interp, (Agobj_t *)e, argc-i, &argv[i]) != TCL_OK)
                return TCL_ERROR;

            if (interp->result[0])
                return TCL_OK;
        }

        return TCL_OK;

    } else if ((c == 'n') && (strncmp(argv[1], "nextoutedge", length) == 0)) {
        /*
         * <node> nextoutedge ?<edge>? ?<attributename> <attributevaluepattern>? ...
         *
         * if <edge> is omitted then returns first edge
         */

        if (argc % 2) {
            i = 3;

            if (!(e = handle_to_edge(dg, argv[2]))) {
                Tcl_AppendResult(interp, "edge not found: \"",
                                 argv[2], "\"", (char *) 0);
                return TCL_ERROR;
            }

            e = agnxtout(e);

        } else {
            i = 2;
            e = agfstout(n);
        }

        for (; e; e = agnxtout(e)) {
            if (attributematch(interp, (Agobj_t *)e, argc-i, &argv[i]) != TCL_OK)
                return TCL_ERROR;

            if (interp->result[0])
                return TCL_OK;
        }

        return TCL_OK;

    } else if ((c == 'q') && (strncmp(argv[1], "queryattributes", length) == 0)) {
        for (i = 2; i < argc; i++) {
            if ((err = Tcl_SplitList(interp, argv[i], &argc2, (CONST84 char ***) &argv2)) != TCL_OK)
                return err;

            for (j = 0; j < argc2; j++) {
                if (!(sym = (Agsym_t *) agattrsym(n, argv2[j]))) {
                    Tcl_AppendResult(interp, " No attribute named \"",
                                     argv2[j], "\"", (char *) 0);
                    return TCL_ERROR;
                }

                Tcl_AppendElement(interp, agxget(n, sym));
            }

            Tcl_Free((char *) argv2);
        }

        return TCL_OK;

    } else if ((c == 's') && (strncmp(argv[1], "set", length) == 0)) {
        if (argc < 3) {
            Tcl_AppendResult(interp, "wrong # args: should be: ",
                             argv[0], "set attributename ?attributevalue?", (char *) NULL);
            return TCL_ERROR;
        }

        if (argc == 3) {
            if (!(sym = (Agsym_t *) agattrsym(n, argv[2]))) {
                Tcl_AppendResult(interp, " No attribute named \"",
                                 argv[2], "\"", (char *) 0);
                return TCL_ERROR;
            }

            Tcl_AppendResult(interp, agxget(n, sym), (char *) NULL);

        } else {
            save = dgrCallbacks(dg, g, FALSE);	/* delay callbacks until all done */

            if (!(sym = (Agsym_t *) agattrsym(n, argv[2])))
                sym = agattr(g, AGNODE, argv[2], "");

            if (strcmp(agxget(n, sym), argv[3]))
                agxset(n, sym, argv[3]);

            dgrCallbacks(dg, g, save);

            Tcl_ResetResult(interp);

            Tcl_AppendResult(interp, argv[3], (char *) NULL);
        }

        return TCL_OK;

    } else if ((c == 's') && (strncmp(argv[1], "setattributes", length) == 0)) {
        if ((argc < 4) || (argc % 2)) {
            Tcl_AppendResult(interp, "wrong # args: should be \"", argv[0],
                             "\" setattributes attributename attributevalue ?attributename attributevalue? ?...?",
                             (char *) NULL);
        }

        save = dgrCallbacks(dg, g, FALSE);	/* delay callbacks until all done */

        for (i = 2; i < argc; i = i + 2) {
            if (!(sym = (Agsym_t *) agattrsym(n, argv[i]))) {
                sym = agattr(g, AGNODE, argv[i], "");
            }

            agxset(n, sym, argv[i + 1]);
        }

        dgrCallbacks(dg, g, save);
        return TCL_OK;

    } else if ((c == 's') && (strncmp(argv[1], "showname", length) == 0)) {
        Tcl_AppendResult(interp, agnameof(n), (char *) NULL);
        return TCL_OK;

    } else if (c == '_') {
        /* eval user defined method stored in attribute */

        if (!(sym = (Agsym_t *) agattrsym(n, argv[1]))) {
            Tcl_AppendResult(interp, " No attribute named \"", argv[1],
                             "\"", (char *) 0);
            return TCL_ERROR;
        }

        /*
         * if there are any other args then pass them as a proper list in the
         * substitution for %a
         */
        i = 0;

        s = "";

        if (argc > 2) {
            i++;
            s = Tcl_Merge(argc - 2, (CONST84 char * CONST *) argv + 2);
        }

        dgrExpandPercentsEval(interp, agxget(n, sym),
                              graph_to_handle(g, gbuf), node_to_handle(n, nbuf), "", "", s);

        if (i)
            ckfree(s);

        return TCL_OK;

    } else {
        Tcl_AppendResult(interp, "bad option \"", argv[1], "\" must be one of:",
                         "\n\taddedge, countedges, countinedges, countoutedges, delete,",
                         "\n\tfindedge, graphof, listattributes, listedges, listinedges,",
                         "\n\tlistoutedges, nextedge, nextinedge, nextoutedge, queryattributes,",
                         "\n\tset, setattributes, showname.", (char *) NULL);
        return TCL_ERROR;
    }
}

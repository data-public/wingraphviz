/* gvconfig.h.in.  Generated automatically from configure.in by autoheader.  */
#ifndef MSWIN32
#define MSWIN32
#endif

#define _USE_INTEL_COMPILER

#undef VERBOSE

#define DMALLOC

#undef O_BINARY

/* Define if using alloca.c.  */
#undef C_ALLOCA

/* Define to one of _getb67, GETB67, getb67 for Cray-2 and Cray-YMP systems.
   This function is required for alloca.c support on those systems.  */
#undef CRAY_STACKSEG_END

/* Define if you have alloca, as a function or macro.  */
#undef HAVE_ALLOCA

/* Define if you have <alloca.h> and it should be used (not on Ultrix).  */
#undef HAVE_ALLOCA_H

/* If using the C implementation of alloca, define if you know the
   direction of stack growth for your system; otherwise it will be
   automatically deduced at run-time.
 STACK_DIRECTION > 0 => grows toward higher addresses
 STACK_DIRECTION < 0 => grows toward lower addresses
 STACK_DIRECTION = 0 => direction of growth unknown
 */
#undef STACK_DIRECTION

/* Define if you can safely include both <sys/time.h> and <time.h>.  */
#undef TIME_WITH_SYS_TIME

/* Define if the X Window System is missing or not being used.  */
#undef X_DISPLAY_MISSING

/* Define if lex declares yytext as a char * by default, not a char[].  */
#undef YYTEXT_POINTER

/* Define if you have the _sysconf function.  */
#undef HAVE__SYSCONF

/* Define if you have the cbrt function.  */
#undef HAVE_CBRT

/* Define if you have the drand48 function.  */
#undef HAVE_DRAND48

/* Define if you have the feenableexcept function.  */
#undef HAVE_FEENABLEEXCEPT

/* Define if you have the getrusage function.  */
#undef HAVE_GETRUSAGE

/* Define if you have the iconv function.  */
#undef HAVE_ICONV

/* Define if you have the lrand48 function.  */
#undef HAVE_LRAND48

/* Define if you have the lsqrt function.  */
#undef HAVE_LSQRT

/* Define if you have the setenv function.  */
#undef HAVE_SETENV

/* Define if you have the setmode function.  */
#undef HAVE_SETMODE

/* Define if you have the srand48 function.  */
#undef HAVE_SRAND48

/* Define if you have the strcasecmp function.  */
#undef HAVE_STRCASECMP

/* Define if you have the strerror function.  */
#undef HAVE_STRERROR

/* Define if you have the strncasecmp function.  */
#undef HAVE_STRNCASECMP

/* Define if you have the strtoll function.  */
#undef HAVE_STRTOLL

/* Define if you have the strtoul function.  */
#undef HAVE_STRTOUL

/* Define if you have the strtoull function.  */
#undef HAVE_STRTOULL

/* Define if you have the vsnprintf function.  */
#undef HAVE_VSNPRINTF

/* Define if you have the <dirent.h> header file.  */
#undef HAVE_DIRENT_H

/* Define if you have the <dlfcn.h> header file.  */
#undef HAVE_DLFCN_H

/* Define if you have the <errno.h> header file.  */
#undef HAVE_ERRNO_H

/* Define if you have the <fenv.h> header file.  */
#undef HAVE_FENV_H

/* Define if you have the <fpu_control.h> header file.  */
#undef HAVE_FPU_CONTROL_H

/* Define if you have the <getopt.h> header file.  */
#undef HAVE_GETOPT_H

/* Define if you have the <malloc.h> header file.  */
#undef HAVE_MALLOC_H

/* Define if you have the <ndir.h> header file.  */
#undef HAVE_NDIR_H

/* Define if you have the <search.h> header file.  */
#undef HAVE_SEARCH_H

/* Define if you have the <strings.h> header file.  */
#undef HAVE_STRINGS_H

/* Define if you have the <sys/dir.h> header file.  */
#undef HAVE_SYS_DIR_H

/* Define if you have the <sys/fpu.h> header file.  */
#undef HAVE_SYS_FPU_H

/* Define if you have the <sys/ndir.h> header file.  */
#undef HAVE_SYS_NDIR_H

/* Define if you have the <sys/time.h> header file.  */
#undef HAVE_SYS_TIME_H

/* Define if you have the <termios.h> header file.  */
#undef HAVE_TERMIOS_H

/* Define if you have the <unistd.h> header file.  */
#undef HAVE_UNISTD_H

/* Define if you have the <values.h> header file.  */
#undef HAVE_VALUES_H

/* Define if you have the X11 library (-lX11).  */
#undef HAVE_LIBX11

/* Define if you have the m library (-lm).  */
#undef HAVE_LIBM

/* Name of package */
#undef PACKAGE

/* Version number of package */
#undef VERSION

/* Path to TrueType fonts. */
#define		DEFAULT_FONTPATH	"C:/WINDOWS/FONTS;C:/WINNT/Fonts;C:/winnt/fonts"

/* Path separator character. */
#define PATHSEPARATOR ";"

/* Define if you have the FREETYPE library */
#undef HAVE_LIBFREETYPE

/* Define if you have the TTF library */
#define HAVE_LIBTTF

//#define JISX0208

/* Define if you have the Z library */
#define HAVE_LIBZ

/* Define if you have the PNG library */
#define HAVE_LIBPNG

/* Define if you have the JPEG library */
#define HAVE_LIBJPEG

/* Define if you have the XAW library */
#undef HAVE_LIBXAW

/* Define to 1 if you have struct dioattr */
#undef HAVE_STRUCT_DIOATTR

/* Define if libm provides a *working* sincos function */
#undef HAVE_SINCOS

/* Define if FILE structure provides _cnt */
#undef HAVE_FILE_CNT

/* Define if FILE structure provides _r */
#undef HAVE_FILE_R

/* Define if FILE structure provides _next */
#undef HAVE_FILE_NEXT

/* Define if FILE structure provides _IO_read_end */
#undef HAVE_FILE_IO_READ_END

/* Define if errno externs are declared */
#undef HAVE_ERRNO_DECL

/* Define if getopt externs are declared */
#undef HAVE_GETOPT_DECL

#include "stdio.h"

#ifndef ILSHAPE_H
#define ILSHAPE_H
#include <assert.h>
#include <incr.h>		/* import base types */

#if HAVE_VMALLOC || HAVE_AST
#include	<vmalloc.h>
#else
#include	<stdlib.h>

typedef struct _vmalloc_s Vmalloc_t;
#define vmalloc(arena,request) malloc(request)
#define vmfree(arena,ptr) free(ptr)
#define vmregion(ptr) 0
#endif

typedef enum ilcurvetype_e { IL_SPLINE, IL_POLYLINE, IL_NOCURVE } ilcurvetype_t;

typedef enum ilshapetype_e {
    IL_POLYGON, IL_CIRCLE, IL_ELLIPSE, IL_SPLINEGON, IL_NOSHAPE
} ilshapetype_t;

struct ilcurve_s {
    ilcurvetype_t	type;
    int				n;		/* must be > 0 */
    ilcoord_t		*p;
} ;

struct ilshape_s {
    ilshapetype_t	type;
    union {
        /* this should be ilcurve_t*, but too much code depends ..*/
        ilcurve_t		curve;	/* if polygon, splinegon */

        struct {
            double radius_a, radius_b;
        }

        ellipse;
    } def;

    struct ilshape_s *next;
} ;

ilcoord_t	ilcoord(double x, double y);
ilcurve_t 	*il_newcurve(Vmalloc_t *arena, ilcurvetype_t kind, int npts);
void		il_freecurve(Vmalloc_t *arena, ilcurve_t *curve);

ilshape_t	*il_newshape(Vmalloc_t *arena, ilcurve_t *contents, ilshape_t *link);
ilshape_t	*il_copyshape(Vmalloc_t *arena, ilshape_t *shape);
void		il_freeshape(Vmalloc_t *arena, ilshape_t *shape);

ilcurve_t	*il_get_bounding_poly(ilshape_t *shape);
ilrect_t	il_get_bounding_rect(ilshape_t *shape);
int			il_inshape(ilshape_t *shape, ilcoord_t pt);	/* 0,1 predicate */
#endif

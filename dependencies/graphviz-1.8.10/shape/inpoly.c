#include "shape.h"

#ifdef DMALLOC
#include "dmalloc.h"
#endif

#ifndef min
#define min(A,B) ((A) < (B) ? (A) : (B))
#define max(A,B) ((A) > (B) ? (A) : (B))
#endif

#define NOT(x)	(!(x))
#ifndef FALSE
#define FALSE	0
#define TRUE	(NOT(FALSE))
#endif

ilrect_t	il_get_bounding_rect(ilshape_t *shape) {
    ilrect_t rc;

    switch (shape->type) {

            case IL_SPLINEGON:

            case IL_POLYGON:	{
                int i;
                ilcoord_t pt = shape->def.curve.p[0];
                rc.ll.x = pt.x;
                rc.ll.y = pt.y;
                rc.ur.x = pt.x;
                rc.ur.y = pt.y;

                for (i=1; i<shape->def.curve.n; i++) {
                    pt = shape->def.curve.p[i];
                    rc.ll.x	=	min (rc.ll.x, pt.x);
                    rc.ur.x	=	max (rc.ur.x, pt.x);
                    rc.ll.y	=	min (rc.ll.y, pt.y);
                    rc.ur.y	=	max (rc.ur.y, pt.y);
                }
            }

            return rc;

            case IL_CIRCLE:

            case IL_ELLIPSE:
            rc.ll.x = - shape->def.ellipse.radius_a;
            rc.ur.x = shape->def.ellipse.radius_a;
            rc.ll.y = - shape->def.ellipse.radius_b;
            rc.ur.y = shape->def.ellipse.radius_b;
            return rc;

            default:
            rc.ll.x = rc.ll.y = rc.ur.x = rc.ur.y = 0.0;
            return rc;
    }
}

static int same_side(ilcoord_t p0, ilcoord_t p1, ilcoord_t L0, ilcoord_t L1) {
    int		s0,s1;
    double	a,b,c;

    /* a x + b y = c */
    a = -(L1.y - L0.y);
    b = (L1.x - L0.x);
    c = a * L0.x + b * L0.y;

    s0 = (a*p0.x + b*p0.y - c >= 0);
    s1 = (a*p1.x + b*p1.y - c >= 0);
    return (s0 == s1);
}

static int point_in_poly(ilcoord_t P, ilshape_t *shape) {
    static int	last;		/* hint for converging on a given side of a poly */
    static ilcoord_t		O;	/* origin */

    int			i, i1, j, sides, s;
    ilcoord_t		Q, R, *vertex;

    assert (shape->type == IL_POLYGON);

    sides = shape->def.curve.n;
    vertex = shape->def.curve.p;
    i = last % sides; /*in case last is left over from larger polygon*/
    i1 = (i + 1) % sides;
    Q = vertex[i];
    R = vertex[i1];

    if (NOT(same_side(P,O,Q,R)))
        return FALSE;

    if ((s = same_side(P,Q,R,O)) && (same_side(P,R,O,Q)))
        return TRUE;

    for (j = 1; j < sides; j++) {
        if (s) {
            i = i1;
            i1 = (i + 1) % sides;

        } else {
            i1 = i;
            i = (i + sides - 1) % sides;
        }

        if (NOT(same_side(P,O,vertex[i],vertex[i1]))) {
            last = i;
            return FALSE;
        }
    }

    last = i;  /* in case next call is near the same side */
    return TRUE;
}

static double CalcDistSquared (ilcoord_t pt) {
    return ((pt.x  * pt.x) + (pt.y * pt.y));
}

int	il_inshape(ilshape_t *shape, ilcoord_t pt)	/* 0,1 predicate */
{

    switch (shape->type) {

            case IL_CIRCLE:
            return CalcDistSquared(pt) <= (shape->def.ellipse.radius_a)*(shape->def.ellipse.radius_a);

            case IL_ELLIPSE:

            if (shape->def.ellipse.radius_a > shape->def.ellipse.radius_b) {
                pt.y *= shape->def.ellipse.radius_a/shape->def.ellipse.radius_b;
                return CalcDistSquared(pt) <= (shape->def.ellipse.radius_a)*(shape->def.ellipse.radius_a);

            } else if (shape->def.ellipse.radius_a < shape->def.ellipse.radius_b) {
                pt.x *= shape->def.ellipse.radius_b/shape->def.ellipse.radius_a;
                return CalcDistSquared(pt) <= (shape->def.ellipse.radius_b)*(shape->def.ellipse.radius_b);

            } else
                return CalcDistSquared(pt) <= (shape->def.ellipse.radius_a)*(shape->def.ellipse.radius_a);

            case IL_POLYGON:
            return point_in_poly(pt,shape);

            default:
            return 0;
    }
}

ilcoord_t ilcoord(double x, double y) {
    ilcoord_t rv;

    rv.x = x;
    rv.y = y;
    return rv;
}

#include "shape.h"

#ifdef DMALLOC
#include "dmalloc.h"
#endif

static void *zmalloc(Vmalloc_t *arena, size_t request) {
    void	*rv;

    rv = vmalloc(arena, request);

    if (rv)
        memset(rv, request, '\0');

    return rv;
}

ilcurve_t *il_newcurve(Vmalloc_t *arena, ilcurvetype_t kind, int maxpts) {
    ilcurve_t *rv;

    rv = zmalloc(arena,sizeof(ilcurve_t));
    rv->type = kind;
    rv->n = 0;
    rv->p = zmalloc(arena,sizeof(ilcoord_t)*maxpts);
    return rv;
}

/* note that the value of contents is copied and contents is freed */
ilshape_t *il_newshape(Vmalloc_t *arena, ilcurve_t *contents, ilshape_t *link) {
    ilshape_t *rv;

    rv = zmalloc(arena, sizeof(ilshape_t));

    if (contents) {
        if (contents->type == IL_POLYLINE)
            rv->type = IL_POLYGON;
        else
            rv->type = IL_SPLINEGON;

        rv->def.curve = *contents;

        /* i designed the interface wrong.  either def.curve should
        be a pointer, or the contents arg should be a value not a
        pointer, but this is the easiest way to deal with the error
        right now. */
        vmfree(vmregion(contents),contents);

    } else
        rv->type = IL_NOSHAPE;

    rv->next = link;

    return rv;
}

void il_freeshape(Vmalloc_t *arena, ilshape_t *shape) {
    ilshape_t	*link;


    while (shape) {
        link = shape->next;

        switch(shape->type) {

                case IL_POLYGON:

                case IL_SPLINEGON:

                if (shape->def.curve.p)
                    vmfree(arena,shape->def.curve.p);

                break;

                default:
                break;
        }

        vmfree(arena,shape);
        shape = link;
    }
}

void il_freecurve(Vmalloc_t *arena, ilcurve_t *curve) {
    vmfree(arena,curve->p);
    vmfree(arena,curve);
}

ilshape_t *il_copyshape(Vmalloc_t *arena, ilshape_t *src) {
    ilshape_t	*rv;
    size_t		sz;

    rv = zmalloc(arena,sizeof(ilshape_t));
    rv->type = src->type;

    switch (rv->type) {

            case IL_POLYGON:

            case IL_SPLINEGON:
            rv->def.curve.type = src->def.curve.type;
            rv->def.curve.n = src->def.curve.n;
            sz = rv->def.curve.n * sizeof(ilcoord_t);
            rv->def.curve.p = zmalloc(arena,sz);
            memcpy(rv->def.curve.p,src->def.curve.p,sz);
            break;

            case IL_ELLIPSE:

            case IL_CIRCLE:
            rv->def.ellipse = src->def.ellipse;
            break;

            default:
            abort();
    }

    return rv;
}

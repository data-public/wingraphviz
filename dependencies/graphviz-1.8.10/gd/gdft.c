
/********************************************/
/* gd interface to freetype library         */
/*                                          */
/* John Ellson   ellson@lucent.com          */
/********************************************/

#include "gd.h"

char *
gdImageStringFT (gdImage * im, int *brect, int fg, char *fontlist,
                 double ptsize, double angle, int x, int y, char *string) {

    return gdImageStringTTF(im,brect,fg,fontlist,ptsize,angle,x,y,string);

}

char *
gdImageStringFT2(gdImage * im, int *brect, int fg, char *fontlist,char *fontface,
                 double ptsize, double angle, int x, int y, char *string) {

    return gdImageStringTTF2(im,brect,fg,fontlist,fontface,ptsize,angle,x,y,string);

}


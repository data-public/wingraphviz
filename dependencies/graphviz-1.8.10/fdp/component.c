/* component.c
 */

#include "structs.h"
#include "component.h"

#ifdef DMALLOC
#include "dmalloc.h"
#endif

static Agnode_t*
getRoot (Agnode_t* n) {
    Agnode_t*  root = n;
    Agnode_t*  pp;

    while ((pp = rootOf(root)) != 0)
        root = pp;

    for (; n != root; n = pp) {
        pp = rootOf(n);
        rootOf(n) = root;
    }

    return root;
}

int
sameComponent (Agnode_t* p, Agnode_t* q) {
    return (getRoot(p) == getRoot(q));
}

void
merge (Agnode_t* p, Agnode_t* q) {
    Agnode_t*  rootp;
    Agnode_t*  rootq;

    rootp = getRoot (p);
    rootq = getRoot (q);

    if (rootp != rootq) {
        if (sizeOf(rootp) > sizeOf(rootq)) {
            rootOf(rootq) = rootp;
            sizeOf(rootp) += sizeOf(rootq);

        } else {
            rootOf(rootp) = rootq;
            sizeOf(rootq) += sizeOf(rootp);
        }
    }
}

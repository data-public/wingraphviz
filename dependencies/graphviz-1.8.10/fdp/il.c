/* il.c
 */

/* TODO:
 *
 * Set K per component.
 * If component unmodified, don't do layout.
 * K should vary by no. edges/no. vertices
 *   (i.e., trees need smaller K's than complete graphs)
 * Position components to avoid overlaps
 * 
 */

#include <stdio.h>
#include <math.h>
#include "engine.h"
#include "options.h"
#include "structs.h"
#include "adjust.h"
#include "component.h"
#include "exprval.h"

#ifdef DMALLOC
#include "dmalloc.h"
#endif

#ifdef WIN32
#define drand48() (((double)rand())/(double)RAND_MAX)
#endif

typedef struct {
    engview_t	base;
    bb_t        bb;
}

fdpview;

typedef fdp_data fdpnode;

typedef struct {
    engedge_t	base;
}

fdpedge;

#define max(a,b)           ((a)<(b)?(b):(a))
#define baseOf(obj)        (&((obj)->base))

#define getEdgeData(e)     ((fdpedge*)AGDATA(e))
#define getEdgeSpec(e)     (((fdpedge*)AGDATA(e))->base.client)

#define viewToILView(v)    (v->base.client)
#define engviewOfIL(v)     (v->pvt)
#define engviewOf(v)       (baseOf(v))
#define bbOf(v)            (engviewOf(v)->bb)
#define ILViewToView(v)    ((fdpview*)(v->pvt))
#define localBBOf(v)       ((ILViewToView(v))->bb)
#define graphOf(v)         (v->base.model.main)
#define nodeGraphOf(vw,m)  (vw->base.model.v[m])
#define edgeGraphOf(vw,m)  (vw->base.model.e[m])

#define deletions(v) (agnnodes(nodeGraphOf(v,IL_DEL)) || agnnodes(edgeGraphOf(v,IL_DEL)))
#define modifications(v) (agnnodes(nodeGraphOf(v,IL_MOD)))

void
trace (char* msg) {
    fprintf (stderr, "%s\n", msg);
}

/*
static void
printComponents (Agraph_t* g)
{
    Agnode_t*    mn;
 
    for (mn = agfstnode(g); mn; mn = agnxtnode(mn)) {
      printNode (mn);
    }
    fprintf (stderr, "==\n");
}
*/

#define LIMIT HUGE_VAL
extern  ILengine_t FDPGraph;	      /* forward reference */

static char*
skipWhite (char* s) {
    char   c;

    while ((c = *s) && isspace((int)c))
        s++;

    return s;
}

static char*
skipNonwhite (char* s) {
    char   c;

    while ((c = *s) && !isspace((int)c))
        s++;

    return s;
}

static int
numToks (char *args) {
    int cnt = 0;

    args = skipWhite (args);

    while (*args != '\0') {
        cnt++;
        args = skipNonwhite (args);
        args = skipWhite (args);
    }

    return cnt;
}

static char**
mkOpts (char* args, int* argc) {
    static char*  SEP = " \t\n";
    int cnt = numToks (args) + 1;
    char**  toks;
    char**  p;
    char*   arg;

    p = toks = (char**)malloc(sizeof(char*)*(cnt+1));
    *p++ = "fdp";
    arg = strtok (args, SEP);

    while (arg) {
        *p++ = strdup (arg);
        arg = strtok (0, SEP);
    }

    *p = 0;

    *argc = cnt;
    return toks;
}

static void
freeOpts (char** opts) {
    char*  p;
    char** q;

    q = opts+1;

    while ((p = *q++) != 0)
        free(p);

    free (opts);
}


static ilbool FDPOpen(ILview_t *client_view) {
    char*       opts;
    int         argc;

    unsigned int S[] = {
                           sizeof(fdpview),
                           sizeof(fdpnode),
                           sizeof(fdpedge)
                       };

    il_open_view(&FDPGraph, client_view, Agundirected, S);
    (localBBOf (client_view)).valid = 0;

    if ((opts = getenv("FDP_OPTS")) != 0) {
        Agraph_t*   graph;
        char**      argv = mkOpts (opts, &argc);

        initialize (argc,argv);

        if (Verbose) {
            showOpts (stderr);
            graph = graphOf (ILViewToView(client_view));
            agattr (graph, AGNODE, "pos", "");
            agattr (graph, AGNODE, "bb", "");
        }

        freeOpts (argv);
    }

    return TRUE;
}

static void FDPClose(ILview_t *client_view) {
    il_close_view(client_view->pvt);
}

static int
moved (fdpnode* dp) {
    return ((dp->cur.x != dp->pos.x) || (dp->cur.y != dp->pos.y));
}

static void
updateLayout (fdpview* view) {
    Agraph_t*    graph;
    Agnode_t*    tl;
    Agnode_t*    hd;
    Agedge_t*    me;
    ILnode_t*    spec;
    ILedge_t*    espec;
    ILview_t*    ilview;
    fdpnode*     tp;
    fdpnode*     hp;
    ilcurve_t    temp;
    ilcurve_t*   curve;
    ilcoord_t    A[2];
    engview_t*   eview;
    ilrect_t     size;

    size.ll.x = size.ur.y = LIMIT;
    size.ll.y = size.ur.x = -LIMIT;

    graph = graphOf (view);
    ilview = viewToILView (view);
    eview = engviewOf (view);

    for (tl = agfstnode(graph); tl; tl = agnxtnode(tl)) {
        tp = getData(tl);

        if (moved(tp)) {
            tp->cur.x = tp->pos.x;
            tp->cur.y = tp->pos.y;
            tp->moved = 1;
            spec = il_node (tl);
            spec->pos.x = tp->cur.x;
            spec->pos.y = tp->cur.y;
            spec->update |= IL_UPD_MOVE;
            spec->pos_valid = TRUE;
            il_register_node_callback(engviewOf(view),spec,IL_MOD);
        }

        if (tp->cur.x < size.ll.x)
            size.ll.x = tp->cur.x;

        if (tp->cur.x > size.ur.x)
            size.ur.x = tp->cur.x;

        if (tp->cur.y < size.ur.y)
            size.ur.y = tp->cur.y;

        if (tp->cur.y > size.ll.y)
            size.ll.y = tp->cur.y;
    }

    (bbOf(view)).size = size;
    view->bb.valid = 1;
    view->bb.size = size;

    for (tl = agfstnode(graph); tl; tl = agnxtnode(tl)) {
        tp = getData(tl);

        for (me = agfstout(tl); me; me = agnxtout(me)) {
            hd = aghead(me);
            hp = getData (hd);

            if ((tp->moved) || (hp->moved)) {
                espec = getEdgeSpec (me);

                if (espec->pos)
                    il_freeshape(vmregion(espec->pos),espec->pos);

                A[0].x = tp->cur.x;

                A[0].y = tp->cur.y;

                A[1].x = hp->cur.x;

                A[1].y = hp->cur.y;

                temp.p = A;

                temp.n = 2;

                temp.type = IL_POLYLINE;

                curve = il_clip_endpoints(eview, &temp, il_node(tl), il_node(hd));

                espec->pos = il_newshape(agheap(ilmodel(ilview)), curve, 0);

                il_register_edge_callback(eview, espec, IL_MOD);
            }
        }
    }
}

static void
insertNode (Agnode_t *mn, ilcoord_t origin) {
    fdpnode*    dp;
    ILnode_t*   spec;

    dp = getData (mn);
    spec = il_node (mn);

    if (spec->pos_valid) {
        dp->cur.x = spec->pos.x;
        dp->cur.y = spec->pos.y;

    } else {
        dp->cur.x = origin.x + Width/4 + drand48()*(Width/2);
        dp->cur.y = origin.y + Height/4 + drand48()*(Height/2);
    }

    if (spec->update & IL_UPD_NAIL)
        dp->fixed = 1;

}

static void
modifyNode (Agnode_t *mn) {
    ILnode_t*   spec;
    fdpnode*    dp;

    spec = il_node (mn);
    dp = getData (mn);

    if (spec->update & IL_UPD_MOVE) {
        dp->cur.x = spec->pos.x;
        dp->cur.y = spec->pos.y;
    }

    if (spec->update & IL_UPD_NAIL)
        dp->fixed = 1;
}

static bb_t
calculateBB (fdpview* view) {
    Agnode_t*    mn;
    ILnode_t*    spec;
    ilcoord_t    pos;
    bb_t         bb;

    bb.size.ll.x = bb.size.ur.y = LIMIT;
    bb.size.ll.y = bb.size.ur.x = -LIMIT;

    for (mn = agfstnode(graphOf(view)); mn; mn = agnxtnode(mn)) {
        spec = il_node(mn);

        if (spec->pos_valid) {
            pos = spec->pos;

            if (pos.x < bb.size.ll.x)
                bb.size.ll.x = pos.x;

            if (pos.x > bb.size.ur.x)
                bb.size.ur.x = pos.x;

            if (pos.y < bb.size.ur.y)
                bb.size.ur.y = pos.y;

            if (pos.y > bb.size.ll.y)
                bb.size.ll.y = pos.y;
        }
    }

    if (bb.size.ll.x == LIMIT) {
        bb.size.ll.x = 0;
        bb.size.ur.y = 0;
        bb.size.ur.x = 1000;
        bb.size.ll.y = 1000;
    }

    return bb;
}

static void
calculateComp (fdpview* view) {
    Agnode_t*    mn;
    Agedge_t*    me;
    fdpnode*     dp;

    for (mn = agfstnode(graphOf(view)); mn; mn = agnxtnode(mn)) {
        dp = getData (mn);
        dp->root = 0;
        dp->size = 1;
    }

    for (mn = agfstnode(graphOf(view)); mn; mn = agnxtnode(mn)) {
        for (me = agfstout(mn); me; me = agnxtout(me)) {
            merge (mn, aghead(me));
        }
    }
}

static bb_t
adjustBB (bb_t bb, Agnode_t* mn) {
    ILnode_t*    spec;
    ilcoord_t    pos;

    for (; mn; mn = agnxtnode(mn)) {
        spec = il_node(mn);

        if (spec->pos_valid) {
            pos = spec->pos;

            if (pos.x < bb.size.ll.x)
                bb.size.ll.x = pos.x;

            if (pos.x > bb.size.ur.x)
                bb.size.ur.x = pos.x;

            if (pos.y < bb.size.ur.y)
                bb.size.ur.y = pos.y;

            if (pos.y > bb.size.ll.y)
                bb.size.ll.y = pos.y;
        }
    }

    return bb;
}

static void
adjustComp (fdpview* view, Agraph_t* graph) {
    Agedge_t*   me;
    Agnode_t*   tl;
    Agraph_t*   g = graphOf (view);

    for (tl = agfstnode(graph); tl; tl = agnxtnode(tl)) {
        for (me = agfstout(tl); me; me = agnxtout(me)) {
            merge (agsubnode(g,tl,FALSE), agsubnode(g,aghead(me),FALSE));
        }
    }
}

static void
adjustVertLen (Agraph_t* graph) {
    Agnode_t*   n;
    ILnode_t*   spec;
    ilrect_t    rect;
    double      v;

    for (n = agfstnode(graph); n; n = agnxtnode(n)) {
        spec = il_node (n);
        rect = il_get_bounding_rect(spec->shape);
        v = rect.ur.x - rect.ll.x;

        if (v > maxVertLen)
            maxVertLen = v;

        v = rect.ur.y - rect.ll.y;

        if (v > maxVertLen)
            maxVertLen = v;
    }
}

static void
calculateVertLen (fdpview* view) {
    maxVertLen = 0.0;  /* minimum */
    adjustVertLen (graphOf(view));
}

static void
updateGraph (fdpview* view) {
    Agnode_t*   mn;
    bb_t        bb;
    ilcoord_t   origin;

    /* initialize component data for new nodes */

    for (mn = agfstnode(nodeGraphOf(view,IL_INS)); mn; mn = agnxtnode(mn)) {
        rootOf(mn) = 0;
        sizeOf(mn) = 1;
    }

    /* If there have been deletions or modifications, recompute
     * bounding box and connected components. Otherwise, incrementally
     * update information.
     */
    bb = bbOf(view);

    if (deletions(view) || modifications(view)) {
        if (!bb.valid)
            bb = calculateBB (view);

        calculateComp (view);

        calculateVertLen (view);

    } else {
        if (!bb.valid) {
            bb = view->bb;

            if (bb.valid)
                bb = adjustBB (bb, agfstnode(nodeGraphOf(view,IL_INS)));
            else
                bb = calculateBB (view);
        }

        adjustComp (view,edgeGraphOf(view,IL_INS));
        adjustVertLen (edgeGraphOf(view,IL_INS));
    }

    /* set Width and Height based on bounding box */
    Width = max(bb.size.ur.x - bb.size.ll.x,1);

    Height = max(bb.size.ll.y - bb.size.ur.y,1);

    origin.x = bb.size.ll.x;

    origin.y = bb.size.ur.y;

    /* register new nodes */
    for (mn = agfstnode(nodeGraphOf(view,IL_INS)); mn; mn = agnxtnode(mn)) {
        insertNode (mn,origin);
    }

    /* modify nodes */
    for (mn = agfstnode(nodeGraphOf(view,IL_MOD)); mn; mn = agnxtnode(mn)) {
        modifyNode (mn);
    }
}

/* initialize nodes and edges for layout */
static void
initGraph (Agraph_t* model) {
    Agnode_t*    mn;
    fdpnode*     dp;

    for (mn = agfstnode(model); mn; mn = agnxtnode(mn)) {
        dp = getData(mn);
        dp->moved = 0;
        dp->pos.x = dp->cur.x;
        dp->pos.y = dp->cur.y;
    }
}

static void
showGraph (Agraph_t* g) {
    Agsym_t*    symp;
    Agsym_t*    symbb;
    char        buf[1000];
    Agnode_t*   n;
    ILnode_t*   spec;
    fdp_data*   data;
    ilrect_t    rect;

    symp = agattr (g, AGNODE, "pos", 0);
    symbb = agattr (g, AGNODE, "bb", 0);

    for (n = agfstnode(g); n; n = agnxtnode(n)) {
        spec = il_node (n);
        data = getData (n);
        sprintf (buf, "%f,%f", data->cur.x, data->cur.y);
        agxset (n, symp, buf);
        rect = il_get_bounding_rect(spec->shape);
        sprintf (buf, "%f,%f,%f,%f",
                 rect.ll.x, rect.ll.y, rect.ur.x, rect.ur.y);
        agxset (n, symbb, buf);

    }

    agwrite (g, stderr);

}

static ilbool FDPWork(ILview_t *client_view) {
    fdpview*      view;
    ilbool        rv;
    Agraph_t*     graph;

    view = ILViewToView(client_view);
    graph = graphOf (view);
    updateGraph (view);
    initValues (agnnodes(graph));
    initGraph (graph);

    for (timer = 0; timer < NumIters; timer++) {
        adjust (graph, cool());
    }

    updateLayout (view);

    if (Verbose > 1)
        showGraph (graph);

    rv = il_issue_callbacks(engviewOfIL(client_view));

    il_clear_callbacks(engviewOfIL(client_view));

    return rv;
}

ILengine_t FDPGraph = {
                          FDPOpen,
                          FDPClose,
                          il_batch_ins,
                          il_batch_mod,
                          il_batch_del,
                          FDPWork,
                          ilmdlobj_to_spec,
                          ilspec_to_mdlobj
                      };

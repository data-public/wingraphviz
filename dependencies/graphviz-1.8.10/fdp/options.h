#ifndef OPTIONS_H
#define OPTIONS_H

#include <stdio.h>

extern char**     Files;
extern FILE*      Output_file;
extern int        PS;
extern int        UseComp;
extern int        Verbose;
extern int        UseGrid;
extern int        DoArrow;
extern double     ArrowScale;

extern void   initialize(int argc, char** argv);
extern void   showOpts (FILE*);

#endif

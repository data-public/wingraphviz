/* options.c
 */

#include <stdio.h>
#include <stdlib.h>
#include <exprval.h>
#include <math.h>
#include "options.h"
/* #include "expr.h" */
#include "macros.h"

#ifdef DMALLOC
#include "dmalloc.h"
#endif

char**     Files;
FILE*      Output_file;
int        PS = 0;
int        UseComp = 0;
int        Verbose = 0;
int        UseGrid = 0;
int        DoArrow = 0;
double     ArrowScale = 1.0;

static int        SinterFlag = 0;

static char* trueStr = "true";
static char* falseStr = "false";

static char*
boolOf (int v) {
    if (v)
        return trueStr;
    else
        return falseStr;
}

static FILE*
file_select (char *str) {
    FILE    *rv;

    rv = fopen(str,"w");

    if (rv == NULL) {
        perror(str);
        exit(1);
    }

    return rv;
}

static char* useString = "nat [opts] files\n\
                         -p         -- postscript output\n\
                         -v[n]      -- verbose mode\n\
                         -?         -- print usage\n\
                         -C         -- use connected components\n\
                         -S         -- sinter mode\n\
                         -G         -- use grid\n\
                         -A[s]      -- add arrows on digraphs, scaled by s\n\
                         -h height       (%d)\n\
                         -o outfile      (<stdout>)\n\
                         -w width        (%d)\n\
                         -x stretch\n\
                         -k len\n\
                         -n iterations   (%d)\n\
                         where d is distance between points and t is current iteration\n";

static void
usage () {
    printf (useString, DfltHeight, DfltWidth, DfltIters);
    exit (0);
}

void initialize(int argc, char** argv) {
    char        *rest,c;
    int         i, nfiles;
    double      tmp;

    nfiles = 0;

    for (i = 1; i < argc; i++)
        if (argv[i][0] != '-')
            nfiles++;

    Files = (char**)malloc((nfiles + 1)*sizeof(char *));

    nfiles = 0;

    for (i = 1; i < argc; i++) {
        if (argv[i][0] == '-') {
            rest = &(argv[i][2]);

            switch (c = argv[i][1]) {

                    case 'o':
                    Output_file = file_select(rest[0]?rest:argv[++i]);
                    break;

                    case 'A':
                    DoArrow = 1;

                    if (rest[0])
                        ArrowScale = atof(rest);

                    break;

                    case 'G':
                    UseGrid = 1;

                    break;

                    case 'S':
                    SinterFlag = 1;

                    break;

                    case 'C':
                    UseComp = 1;

                    break;

                    case 'p':
                    PS = 1;

                    break;

                    case 'v':
                    Verbose = rest[0]?atoi(rest):1;

                    break;

                    case 'k':
                    UserK = atof(rest[0]?rest:argv[++i]);

                    break;

                    case 'x':
                    tmp = atof(rest[0]?rest:argv[++i]);

                    if (tmp > 0.0)
                        Stretch = tmp;

                    break;

                    case 'w':
                    Width = atof(rest[0]?rest:argv[++i]);

                    break;

                    case 'h':
                    Height = atof(rest[0]?rest:argv[++i]);

                    break;

                    case 'n':
                    NumIters = atoi(rest[0]?rest:argv[++i]);

                    break;

                    case '?':
                    usage ();

                    break;

                    default:
                    fprintf(stderr,"nat: option -%c unrecognized\n",c);
            }

        } else
            Files[nfiles++] = argv[i];
    }

    if (Output_file == NULL)
        Output_file = stdout;
}

void   showOpts (FILE* outf) {
    fprintf (outf, "Connected components (C) : %s\n", boolOf (UseComp));
    fprintf (outf, "Sinter mode (S) : %s\n", boolOf (SinterFlag));
    fprintf (outf, "Grid mode (G) : %s\n", boolOf (UseGrid));
    fprintf (outf, "Verbose level (v) : %d\n", Verbose);
    fprintf (outf, "Width (w) : %f, Height (h) : %f, Iterations (n) :%d\n",
             Width, Height, NumIters);
}


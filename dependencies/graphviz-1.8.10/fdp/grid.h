#ifndef GRID_H
#define GRID_H

#include <agraph.h>
#include <cdt.h>

typedef struct _grid Grid;

typedef struct _node_list {
    Agnode_t*          node;

    struct _node_list* next;
}

node_list;

typedef struct {
    int          i, j;
}

gridpt;

typedef struct {
    gridpt       p;
    node_list*   nodes;
    Dtlink_t     link;
}

cell;

extern Grid*      resetGrid (int, int, Grid*);
extern void       addGrid (Grid*, int, int, Agnode_t*);
extern void       walkGrid (Grid*, int(*)(Dt_t*,void*,void*));
extern cell*      findGrid (Grid*, int, int);
extern int        length (cell*);

#endif

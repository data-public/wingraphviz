#ifndef EXPR_H
#define EXPR_H

extern double     UserK;
extern double     Stretch;
extern double     K;
extern double     K2;
extern double     Radius2;
extern double     CellW;
extern double     CellH;
extern int        timer;
extern double     Width;
extern double     Height;
extern int        NumIters;
extern double     T0;

#ifdef INCR
extern double     maxVertLen;
#endif

extern int DfltHeight;
extern int DfltWidth;
extern int DfltIters;

extern void printValues ();
extern void initValues (int);

extern double cool ();

#endif

/* grid.c
 */
#include <stdio.h>
#include "grid.h"
#include "options.h"
#include "macros.h"

#ifdef DMALLOC
#include "dmalloc.h"
#endif

typedef struct _block {
    cell*          mem;
    cell*          cur;
    cell*          endp;

    struct _block* next;
}

block_t;

static block_t*
newBlock (int size) {
    block_t*     newb;

    newb = (block_t*)malloc(sizeof(block_t));
    newb->next = 0;
    newb->mem = (cell*)malloc(size*(sizeof(cell)));
    newb->endp = newb->mem + size;
    newb->cur = newb->mem;

    return newb;
}

struct _grid {
    Dt_t*        data;
    block_t*     cellMem;
    block_t*     cellCur;
    int          listSize;
    node_list*   listMem;
    node_list*   listCur;
};

static cell*
getCell (Grid* g) {
    cell*     cp;
    block_t*  bp = g->cellCur;

    if (bp->cur == bp->endp) {
        if (bp->next == 0) {
            bp->next = newBlock (2*(bp->endp - bp->mem));
        }

        bp = g->cellCur = bp->next;
        bp->cur = bp->mem;
    }

    cp = bp->cur++;
    return cp;
}

#ifndef offsetof
#define offsetof(typ,fld)  ((int)(&(((typ*)0)->fld)))
#endif

#define max(a,b)           ((a)<(b)?(b):(a))

static int
ijcmpf (Dt_t* d, void* key1, void* key2, Dtdisc_t* disc) {
    cell*    cell1 = (cell*)key1;
    cell*    cell2 = (cell*)key2;
    int      diff;

    NOTUSED(d);
    NOTUSED(disc);
    /* return ((cell1->p.i != cell2->p.i) || (cell1->p.j != cell2->p.j)); */
    diff = cell1->p.i - cell2->p.i;

    if (diff != 0)
        return diff;
    else
        return (cell1->p.j - cell2->p.j);
}

static Grid*   _grid;  /* hack because can't attach info. to Dt_t */
static void*
newCell (Dt_t* d, void* obj, Dtdisc_t* disc) {
    cell*    cellp = (cell*)obj;
    cell*    newp;

    NOTUSED(disc);
    newp = getCell (_grid);
    newp->p.i = cellp->p.i;
    newp->p.j = cellp->p.j;
    newp->nodes = 0;

    return newp;
}

static node_list*
newNode (Grid* g, Agnode_t* n, node_list* nxt) {
    node_list*   newp;

    newp = g->listCur++;
    newp->node = n;
    newp->next = nxt;

    return newp;
}

static Dtdisc_t gridDisc = {
                               offsetof (cell, p),
                               sizeof (gridpt),
                               offsetof(cell,link),
                               newCell,
                               NIL(Dtfree_f),
                               ijcmpf,
                               NIL(Dthash_f),
                               NIL(Dtmemory_f),
                               NIL(Dtevent_f)
                           } ;

Grid*
resetGrid (int nnodes, int cellHint, Grid* g) {
    int nsize;

    if (g == 0) {
        g = (Grid*)malloc (sizeof (Grid));
        _grid = g;  /* see comment above */
        g->data = dtopen (&gridDisc, Dtoset);
        g->listMem = 0;
        g->listSize = 0;
        g->cellMem = newBlock (cellHint);
    }

    if (nnodes > g->listSize) {
        nsize = max (nnodes, 2*(g->listSize));
        g->listMem = (node_list*)realloc (g->listMem, nsize*(sizeof (node_list)));
        g->listSize = nsize;
    }

    g->listCur = g->listMem;
    g->cellCur = g->cellMem;
    g->cellCur->cur = g->cellCur->mem;

    return g;
}

void
addGrid (Grid* g, int i, int j, Agnode_t* n) {
    cell*       cellp;
    cell        key;

    key.p.i = i;
    key.p.j = j;
    cellp = dtinsert (g->data, &key);
    cellp->nodes = newNode (g, n, cellp->nodes);

    if (Verbose >= 3) {
        fprintf (stderr, "grid(%d,%d): %s\n", i, j, agnameof(n));
    }
}

void
walkGrid (Grid* g, int(*walkf)(Dt_t*,void*,void*)) {
    dtwalk (g->data, walkf, g);
}

cell*
findGrid (Grid* g, int i, int j) {
    cell    key;

    key.p.i = i;
    key.p.j = j;
    return ((cell*)dtsearch (g->data, &key));
}

int
length (cell* p) {
    int         len = 0;
    node_list*  nodes = p->nodes;

    while (nodes) {
        len++;
        nodes = nodes->next;
    }

    return len;
}

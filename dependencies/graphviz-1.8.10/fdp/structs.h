#ifndef STRUCTS_H
#define STRUCTS_H

#include <agraph.h>
#ifdef INCR
#include <engine.h>
#endif

#define getData(n) ((fdp_data*)AGDATA(n))
#define rootOf(n)  ((getData(n))->root)
#define sizeOf(n)  ((getData(n))->size)

typedef unsigned char bool;

typedef struct {
    int         x,y;
}

point;

typedef struct {
    double      x,y;
}

pointf;

typedef struct {
    Agedge_t**      list;
    int             size;
}

elist;

typedef struct {
#ifdef INCR
    engnode_t           base;
    pointf              cur;           /* current position */
    bool                moved;         /* true if node has moved */
#else

    Agrec_t             h;
#endif

    Agnode_t*           root;          /* root of connected component */
    int                 size;          /* size of rooted tree */

    bool                fixed;         /* true if node should not move */

pointf              pos;           /* new position */

pointf              disp;          /* incremental displacement */

float               wd;            /* width of bounding box */

float               ht;            /* height of bounding box */
}

fdp_data;

extern void printNode (Agnode_t*)
    ;

#endif


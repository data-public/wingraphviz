#ifndef COMPONENT_H
#define COMPONENT_H

#include <agraph.h>

extern int sameComponent (Agnode_t*, Agnode_t*);
extern void merge (Agnode_t*, Agnode_t*);

#endif

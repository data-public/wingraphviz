/* expr.c
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "options.h"
#include "macros.h"

#ifdef DMALLOC
#include "dmalloc.h"
#endif

int DfltHeight  = 1000;
int DfltWidth   = 1000;
int DfltIters   = 40;

double     Width;    /* Width of bounding box */
double     Height;   /* Height of bounding box */
int        NumIters; /* Number of iterations in loop */

int        timer;    /* Iteration index */
double     Radius2;  /* Radius of interaction squared */
/* Anything outside of the radius has no effect on node */
double     CellW;    /* Width of interaction cell */
double     CellH;    /* Height of interaction cell */

double     UserK = 0.0;    /* User-supplied length */
double     Stretch = 1.2;  /* If maxVertLen used for K, multiply by Stretch
                              to avoid overlap */
double     K;        /* Edge length */
double     K2;       /* K*K */
double     T0;       /* Initial temperature */
#ifdef INCR
double     maxVertLen = 0.0;   /* maximum width or length of vertices */
#endif

void printValues () {
    fprintf (stderr, "NumIters %d ", NumIters);
    fprintf (stderr, "Width %g Height %g\n", Width, Height);
    fprintf (stderr, "CellW %g CellH %g\n", CellW, CellH);
    fprintf (stderr, "Radius2 %g Spring %g T0 %g\n", Radius2, K, T0);
}

double cool () {
    return (T0*(NumIters-timer))/NumIters;
}


void initValues (int v) {
    if (Width == 0)
        Width = DfltWidth;

    if (Height == 0)
        Height = DfltHeight;

    if (NumIters == 0)
        NumIters = DfltIters;

    if (UserK > 0.0)
        K = UserK;

#ifdef INCR

    else
        K = Stretch * maxVertLen;

#else

    else
        K = sqrt((Width*Height)/v);

#endif

    K2 = K*K;

    T0 = Width/10;

    Radius2 = 9*K*K;

    CellW= 2*K;

    CellH= 2*K;

    if (Verbose)
        fprintf (stderr, "initValues: W %g H %g K %g T0 %g\n", Width, Height, K, T0);
}


#ifndef ADJUST_H
#define ADJUST_H

#include <agraph.h>

extern void adjust (Agraph_t*, double);

#endif

/* adjust.c
 */
#include "adjust.h"
#include "grid.h"
#include "structs.h"
#include "exprval.h"
#include "options.h"
#include "component.h"
#include "macros.h"
#include <math.h>
#include <assert.h>

#ifdef DMALLOC
#include "dmalloc.h"
#endif

static void
doRep (fdp_data* p, fdp_data* q, double xdelta, double ydelta, double dist2) {
    double    force;

    while (dist2 == 0.0) {
        xdelta = 5 - rand()%10;
        ydelta = 5 - rand()%10;
        dist2 = xdelta*xdelta + ydelta*ydelta;
    }

    force = K2/dist2;
    q->disp.x += xdelta * force;
    q->disp.y += ydelta * force;
    p->disp.x -= xdelta * force;
    p->disp.y -= ydelta * force;
}

static void
applyRep (fdp_data* p, fdp_data* q) {
    double    xdelta, ydelta;

    xdelta = q->pos.x - p->pos.x;
    ydelta = q->pos.y - p->pos.y;
    doRep (p, q, xdelta, ydelta, xdelta*xdelta + ydelta*ydelta);
}

static void
applyAttr (fdp_data* p, fdp_data* q) {
    double    xdelta, ydelta;
    double    force;
    double    dist;
    double    dist2;

    xdelta = q->pos.x - p->pos.x;
    ydelta = q->pos.y - p->pos.y;
    dist2 = xdelta*xdelta + ydelta*ydelta;
    dist = sqrt (dist2);
    force = dist/K;
    q->disp.x -= xdelta * force;
    q->disp.y -= ydelta * force;
    p->disp.x += xdelta * force;
    p->disp.y += ydelta * force;
}

static void
doNeighbor (Grid* grid, int i, int j, node_list* nodes) {
    cell*         cellp = findGrid (grid, i, j);
    node_list*    p;
    node_list*    q;
    fdp_data*     datap;
    fdp_data*     dataq;
    double        xdelta, ydelta;
    double        dist2;

    if (cellp) {
        if (Verbose >= 3)
            fprintf (stderr, "  doNeighbor (%d,%d) : %d\n", i, j, length (cellp));

        for (p = nodes; p != 0; p = p->next) {
            datap = getData (p->node);

            for (q = cellp->nodes; q != 0; q = q->next) {
#ifndef CONN_COMP

                if (UseComp && !sameComponent (p->node,q->node))
                    continue;

#else
                if (!sameComponent (p->node,q->node))
                    continue;

#endif

                dataq = getData (q->node);

                xdelta = dataq->pos.x - datap->pos.x;

                ydelta = dataq->pos.y - datap->pos.y;

                dist2 = xdelta*xdelta + ydelta*ydelta;

                if (dist2 < Radius2)
                    doRep (datap, dataq, xdelta, ydelta, dist2);
            }
        }
    }
}

static int
gridRepulse (Dt_t* dt, void* v, void* g) {
    cell*         cellp = (cell*)v;
    Grid*         grid = (Grid*)g;
    node_list*    nodes = cellp->nodes;
    int           i = cellp->p.i;
    int           j = cellp->p.j;
    node_list*    p;
    node_list*    q;
    fdp_data*     data;

    NOTUSED (dt);

    if (Verbose >= 3)
        fprintf (stderr, "gridRepulse (%d,%d) : %d\n", i, j, length (cellp));

    for (p = nodes; p != 0; p = p->next) {
        data = getData (p->node);

        for (q = nodes; q != 0; q = q->next)
#ifndef CONN_COMP

            if ((p != q) && (!UseComp || sameComponent (p->node,q->node)))
#else
            if ((p != q) && sameComponent (p->node,q->node))
#endif

                applyRep (data, getData (q->node));
    }

    doNeighbor(grid, i - 1, j - 1, nodes);
    doNeighbor(grid, i - 1, j    , nodes);
    doNeighbor(grid, i - 1, j + 1, nodes);
    doNeighbor(grid, i    , j - 1, nodes);
    doNeighbor(grid, i    , j + 1, nodes);
    doNeighbor(grid, i + 1, j - 1, nodes);
    doNeighbor(grid, i + 1, j    , nodes);
    doNeighbor(grid, i + 1, j + 1, nodes);

    return 0;
}

void
adjust (Agraph_t* g, double temp) {
    fdp_data*    data;
    Agnode_t*    n;
    Agnode_t*    n1;
    Agedge_t*    e;
    double       temp2;
    double       len;
    double       len2;
    pointf       disp;
    static Grid* grid = 0;

    if (temp <= 0.0)
        return;

    if (UseGrid) {
        int nnodes = agnnodes(g);
        grid = resetGrid (nnodes, nnodes, grid);
    }

    for (n = agfstnode(g); n; n = agnxtnode(n)) {
        data = getData (n);
        data->disp.x = data->disp.y = 0;

        if (UseGrid)
            addGrid (grid, (int)floor(data->pos.x/CellW), (int)floor(data->pos.y/CellH), n);
    }

    if (UseGrid) {
        for (n = agfstnode(g); n; n = agnxtnode(n)) {
            data = getData (n);

            for (e = agfstout(n); e; e = agnxtout(e))
                applyAttr (data, getData (aghead(e)));
        }

        walkGrid (grid, gridRepulse);

    } else {
        for (n = agfstnode(g); n; n = agnxtnode(n)) {
            data = getData (n);

            for (n1 = agnxtnode(n); n1; n1 = agnxtnode(n1)) {
#ifndef CONN_COMP

                if (!UseComp || sameComponent (n,n1))
#else
                if (sameComponent (n,n1))
#endif

                    applyRep (data, getData(n1));
            }

            for (e = agfstout(n); e; e = agnxtout(e)) {
                applyAttr (data, getData(aghead(e)));
            }
        }
    }

    temp2 = temp * temp;

    for (n = agfstnode(g); n; n = agnxtnode(n)) {
        data = getData (n);

        if (data->fixed)
            continue;

        disp = data->disp;

        len2 = disp.x*disp.x + disp.y*disp.y;

        if (len2 < temp2) {
            data->pos.x += disp.x;
            data->pos.y += disp.y;

        } else {
            /* to avoid sqrt, consider abs(x) + abs(y) */
            len = sqrt (len2);
            data->pos.x += (disp.x * temp)/len;
            data->pos.y += (disp.y * temp)/len;
        }

        /* Keep withing box */
        /*
              if (data->pos.x < 0) data->pos.x = 0;
              else if (data->pos.x > Width) data->pos.x = Width;
              if (data->pos.y < 0) data->pos.y = 0;
              else if (data->pos.y > Height) data->pos.y = Height;
        */
    }

}

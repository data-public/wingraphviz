# Microsoft Developer Studio Generated NMAKE File, Based on neato.dsp
!IF "$(CFG)" == ""
CFG=neato - Win32 Debug
!MESSAGE No configuration specified. Defaulting to neato - Win32 Debug.
!ENDIF 

!IF "$(CFG)" != "neato - Win32 Release" && "$(CFG)" != "neato - Win32 Debug"
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "neato.mak" CFG="neato - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "neato - Win32 Release" (based on "Win32 (x86) Console Application")
!MESSAGE "neato - Win32 Debug" (based on "Win32 (x86) Console Application")
!MESSAGE 
!ERROR An invalid configuration is specified.
!ENDIF 

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE 
NULL=nul
!ENDIF 

CPP=cl.exe
RSC=rc.exe

!IF  "$(CFG)" == "neato - Win32 Release"

OUTDIR=.\Release
INTDIR=.\Release
# Begin Custom Macros
OutDir=.\Release
# End Custom Macros

ALL : "$(OUTDIR)\neato.exe"


CLEAN :
	-@erase "$(INTDIR)\Error_win32_app_Imp.obj"
	-@erase "$(INTDIR)\neato.obj"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(OUTDIR)\neato.exe"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP_PROJ=/nologo /MT /W3 /GX /O1 /I ".." /I "..\pathplan" /I "..\cdt" /I "..\graph" /I "common" /I "neatogen" /D "NDEBUG" /D "WIN32" /D "_CONSOLE" /D "_MBCS" /D "HAVE_CONFIG_H" /D "MSWIN32" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /c 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\neato.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib freetype.lib zlib.lib libpng.lib libgd.lib libgraph.lib libcdt.lib libpathplan.lib libneato.lib libdotneato.lib libpack.lib /nologo /subsystem:console /incremental:no /pdb:"$(OUTDIR)\neato.pdb" /machine:I386 /out:"$(OUTDIR)\neato.exe" /libpath:"D:\Project\graphviz-1.8.10\lib\Release" /libpath:"D:\Project\lib\Release" 
LINK32_OBJS= \
	"$(INTDIR)\neato.obj" \
	"$(INTDIR)\Error_win32_app_Imp.obj"

"$(OUTDIR)\neato.exe" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ELSEIF  "$(CFG)" == "neato - Win32 Debug"

OUTDIR=.\Debug
INTDIR=.\Debug
# Begin Custom Macros
OutDir=.\Debug
# End Custom Macros

ALL : "$(OUTDIR)\neato.exe"


CLEAN :
	-@erase "$(INTDIR)\Error_win32_app_Imp.obj"
	-@erase "$(INTDIR)\neato.obj"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(INTDIR)\vc60.pdb"
	-@erase "$(OUTDIR)\neato.exe"
	-@erase "$(OUTDIR)\neato.ilk"
	-@erase "$(OUTDIR)\neato.pdb"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP_PROJ=/nologo /MTd /W3 /Gm /GX /ZI /Od /I ".." /I "..\pathplan" /I "..\cdt" /I "..\graph" /I "common" /I "neatogen" /D "_DEBUG" /D "HAVE_CONFIG_H" /D "WIN32" /D "_CONSOLE" /D "_MBCS" /D "MSWIN32" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /GZ /c 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\neato.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib freetype.lib zlib.lib libpng.lib libgd.lib libgraph.lib libcdt.lib libpathplan.lib libneato.lib libdotneato.lib libpack.lib /nologo /subsystem:console /incremental:yes /pdb:"$(OUTDIR)\neato.pdb" /debug /machine:I386 /out:"$(OUTDIR)\neato.exe" /pdbtype:sept /libpath:"D:\Project\graphviz-1.8.10\lib\Debug" /libpath:"D:\Project\lib\Debug" 
LINK32_OBJS= \
	"$(INTDIR)\neato.obj" \
	"$(INTDIR)\Error_win32_app_Imp.obj"

"$(OUTDIR)\neato.exe" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ENDIF 

.c{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<


!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("neato.dep")
!INCLUDE "neato.dep"
!ELSE 
!MESSAGE Warning: cannot find "neato.dep"
!ENDIF 
!ENDIF 


!IF "$(CFG)" == "neato - Win32 Release" || "$(CFG)" == "neato - Win32 Debug"
SOURCE=..\tools\error\Error_win32_app_Imp.c

"$(INTDIR)\Error_win32_app_Imp.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\neato.c

"$(INTDIR)\neato.obj" : $(SOURCE) "$(INTDIR)"



!ENDIF 


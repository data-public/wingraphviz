/*
    This software may only be used by you under license from AT&T Corp.
    ("AT&T").  A copy of AT&T's Source Code Agreement is available at
    AT&T's Internet website having the URL:
    <http://www.research.att.com/sw/tools/graphviz/license/source.html>
    If you received this software without first entering into a license
    with AT&T, you have an infringing copy of this software and cannot use
    it without violating AT&T's intellectual property rights.
*/


/*
 * Written by Stephen North and Eleftherios Koutsofios.
 */

#include	"neato.h"
#ifdef HAVE_CONFIG_H
#include "gvconfig.h"
#endif
#include	<time.h>
#ifndef MSWIN32
#include	<unistd.h>
#endif

#ifdef DMALLOC
#include "dmalloc.h"
#endif

char *Info[] = {
                   "neato",            /* Program */
                   VERSION,            /* Version */
                   DATE                /* Build Date */
               };

static graph_t *G;

#ifndef MSWIN32
static void intr(int s) {
    if (G)
        dotneato_write(G);

    dotneato_terminate();

    exit(1);
}

#endif


int main(int argc, char** argv) {
    dotneato_initialize(argc,argv);
#ifndef MSWIN32

    signal (SIGUSR1, toggle);
    signal (SIGINT, intr);
#endif

    {
#define NUMNODES 5

        Agnode_t *node[NUMNODES];
        char name[10];
        int j, k;


        int count = 0;

        while (1) {

            /* Create a new graph */
            G = agopen("new_graph", AGRAPH);

            /* Add nodes */

            for (j=0; j<NUMNODES; j++) {
                sprintf(name, "%d", j);
                node[j] = agnode(G, name);
            }

            /* Connect nodes */
            for (j=0; j<NUMNODES; j++) {
                for (k=j+1; k<NUMNODES; k++) {
                    agedge(G,node[j],node[k]);
                }
            }

            /* Perform layout */
            neato_layout(G);

            /* Delete graph */
            neato_cleanup(G);

            agclose(G);

#ifdef DMALLOC
            /* first log is baseline, second log shows leaks. */
            dmalloc_log_unfreed();

            if (count)
                return 1;

#endif

            count++;
        }
    }
}

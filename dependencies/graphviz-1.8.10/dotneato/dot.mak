# Microsoft Developer Studio Generated NMAKE File, Based on dot.dsp
!IF "$(CFG)" == ""
CFG=dot - Win32 Debug
!MESSAGE No configuration specified. Defaulting to dot - Win32 Debug.
!ENDIF 

!IF "$(CFG)" != "dot - Win32 Release" && "$(CFG)" != "dot - Win32 Debug"
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "dot.mak" CFG="dot - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "dot - Win32 Release" (based on "Win32 (x86) Console Application")
!MESSAGE "dot - Win32 Debug" (based on "Win32 (x86) Console Application")
!MESSAGE 
!ERROR An invalid configuration is specified.
!ENDIF 

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE 
NULL=nul
!ENDIF 

!IF  "$(CFG)" == "dot - Win32 Release"

OUTDIR=.\Release
INTDIR=.\Release
# Begin Custom Macros
OutDir=.\Release
# End Custom Macros

ALL : "$(OUTDIR)\dot.exe"


CLEAN :
	-@erase "$(INTDIR)\dot.obj"
	-@erase "$(INTDIR)\Error_win32_app_Imp.obj"
	-@erase "$(INTDIR)\shortest.obj"
	-@erase "$(INTDIR)\utils.obj"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(OUTDIR)\dot.exe"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP=xicl6.exe
CPP_PROJ=/nologo /G6 /MT /W3 /GX /I ".." /I "..\pathplan" /I "..\cdt" /I "..\graph" /I "common" /I "dotgen" /D "NDEBUG" /D "HAVE_CONFIG_H" /D "WIN32" /D "_CONSOLE" /D "_MBCS" /D "MSWIN32" /D "_USE_INTEL_COMPILER" /Fp"$(INTDIR)\dot.pch" /YX /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /c 

.c{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

RSC=rc.exe
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\dot.bsc" 
BSC32_SBRS= \
	
LINK32=xilink6.exe
LINK32_FLAGS=kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib freetype.lib zlib.lib libpng.lib libgd.lib libgraph.lib libcdt.lib libpathplan.lib libdot.lib libdotneato.lib libjpeg.lib /nologo /subsystem:console /incremental:no /pdb:"$(OUTDIR)\dot.pdb" /machine:I386 /out:"$(OUTDIR)\dot.exe" /libpath:"D:\Project\graphviz-1.8.10\lib\Release" /libpath:"D:\Project\lib\Release" 
LINK32_OBJS= \
	"$(INTDIR)\dot.obj" \
	"$(INTDIR)\Error_win32_app_Imp.obj" \
	"$(INTDIR)\shortest.obj" \
	"$(INTDIR)\utils.obj"

"$(OUTDIR)\dot.exe" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ELSEIF  "$(CFG)" == "dot - Win32 Debug"

OUTDIR=.\Debug
INTDIR=.\Debug
# Begin Custom Macros
OutDir=.\Debug
# End Custom Macros

ALL : "$(OUTDIR)\dot.exe" "$(OUTDIR)\dot.bsc"


CLEAN :
	-@erase "$(INTDIR)\dot.obj"
	-@erase "$(INTDIR)\dot.sbr"
	-@erase "$(INTDIR)\Error_win32_app_Imp.obj"
	-@erase "$(INTDIR)\Error_win32_app_Imp.sbr"
	-@erase "$(INTDIR)\shortest.obj"
	-@erase "$(INTDIR)\shortest.sbr"
	-@erase "$(INTDIR)\utils.obj"
	-@erase "$(INTDIR)\utils.sbr"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(INTDIR)\vc60.pdb"
	-@erase "$(OUTDIR)\dot.bsc"
	-@erase "$(OUTDIR)\dot.exe"
	-@erase "$(OUTDIR)\dot.ilk"
	-@erase "$(OUTDIR)\dot.map"
	-@erase "$(OUTDIR)\dot.pdb"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP=xicl6.exe
CPP_PROJ=/nologo /MTd /W3 /Gm /GX /ZI /Od /I ".." /I "..\pathplan" /I "..\cdt" /I "..\graph" /I "common" /I "dotgen" /D "_DEBUG" /D "HAVE_CONFIG_H" /D "WIN32" /D "_CONSOLE" /D "_MBCS" /D "MSWIN32" /D "HAVE_STDLIB_H" /FR"$(INTDIR)\\" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /GZ /c 

.c{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

RSC=rc.exe
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\dot.bsc" 
BSC32_SBRS= \
	"$(INTDIR)\dot.sbr" \
	"$(INTDIR)\Error_win32_app_Imp.sbr" \
	"$(INTDIR)\shortest.sbr" \
	"$(INTDIR)\utils.sbr"

"$(OUTDIR)\dot.bsc" : "$(OUTDIR)" $(BSC32_SBRS)
    $(BSC32) @<<
  $(BSC32_FLAGS) $(BSC32_SBRS)
<<

LINK32=xilink6.exe
LINK32_FLAGS=kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib freetype.lib zlib.lib libpng.lib libgd.lib libgraph.lib libcdt.lib libpathplan.lib libdot.lib libdotneato.lib libjpeg.lib /nologo /subsystem:console /incremental:yes /pdb:"$(OUTDIR)\dot.pdb" /map:"$(INTDIR)\dot.map" /debug /machine:I386 /out:"$(OUTDIR)\dot.exe" /pdbtype:sept /libpath:"D:\Project\graphviz-1.8.10\lib\Debug" /libpath:"D:\Project\lib\Debug" 
LINK32_OBJS= \
	"$(INTDIR)\dot.obj" \
	"$(INTDIR)\Error_win32_app_Imp.obj" \
	"$(INTDIR)\shortest.obj" \
	"$(INTDIR)\utils.obj"

"$(OUTDIR)\dot.exe" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ENDIF 


!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("dot.dep")
!INCLUDE "dot.dep"
!ELSE 
!MESSAGE Warning: cannot find "dot.dep"
!ENDIF 
!ENDIF 


!IF "$(CFG)" == "dot - Win32 Release" || "$(CFG)" == "dot - Win32 Debug"
SOURCE=.\dot.c

!IF  "$(CFG)" == "dot - Win32 Release"


"$(INTDIR)\dot.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "dot - Win32 Debug"


"$(INTDIR)\dot.obj"	"$(INTDIR)\dot.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=..\tools\error\Error_win32_app_Imp.c

!IF  "$(CFG)" == "dot - Win32 Release"


"$(INTDIR)\Error_win32_app_Imp.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "dot - Win32 Debug"


"$(INTDIR)\Error_win32_app_Imp.obj"	"$(INTDIR)\Error_win32_app_Imp.sbr" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ENDIF 

SOURCE=..\pathplan\shortest.c

!IF  "$(CFG)" == "dot - Win32 Release"


"$(INTDIR)\shortest.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "dot - Win32 Debug"


"$(INTDIR)\shortest.obj"	"$(INTDIR)\shortest.sbr" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ENDIF 

SOURCE=.\common\utils.c

!IF  "$(CFG)" == "dot - Win32 Release"


"$(INTDIR)\utils.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "dot - Win32 Debug"


"$(INTDIR)\utils.obj"	"$(INTDIR)\utils.sbr" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ENDIF 


!ENDIF 


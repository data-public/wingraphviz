#include	"dot.h"

#ifdef HAVE_CONFIG_H
#include "gvconfig.h"
#endif

#include	<time.h>
#include	<stdio.h>
#include	<string.h>


#ifdef HAVE_UNISTD_H
#include	<unistd.h>
#endif

#ifdef MSWIN32
#include	"stamp.h"
#endif



#ifdef DMALLOC
#include "dmalloc.h"
#endif


char *Info[] = {
                   "dot",              /* Program */
                   "VERSION",            /* Version */
                   "DATE"                /* Build Date */
               };

unsigned int DOT_CODEPAGE;

static graph_t *G;

int main(int argc, char** argv) {

    clock_t start,middle, finish;
    double  duration1,duration2;



    //	Default_URL = "http://127.0.0.1";

    //DOT_CODEPAGE = 51932;
    DOT_CODEPAGE = 932;  //Japan
    //DOT_CODEPAGE = 936;
    //    DOT_CODEPAGE = 850;  //Big5
    //DOT_CODEPAGE = 852;

    start = clock();

    dotneato_initialize(argc,argv);

	G = next_input_graph();

	if (!G)
		return 0;

    dot_layout(G);

    middle = clock();
    duration1 = (double)(middle - start) / CLOCKS_PER_SEC;
    printf( "%2.4f seconds\n", duration1 );

    dotneato_write(G);


    finish = clock();
    duration2 = (double)(finish - start) / CLOCKS_PER_SEC;
    printf( "%2.4f seconds\n", duration2 );

    dot_cleanup(G);

    dotneato_terminate();

    return 1;
}

#include "CP_949_Koreal.h"
#include <string.h>

char *
get_fontname_CP_949(char *font) {
    char *fontlist;

    fontlist = font;

    if (strcmp(font,"Batang")==0) {
        fontlist = "Batang.ttc";

    } else if (strcmp(font,"BatangChe")==0) {
        fontlist = "Batang.ttc";

    } else if (strcmp(font,"Gungsuh")==0) {
        fontlist = "Batang.ttc";

    } else if (strcmp(font,"GungsuhChe")==0) {
        fontlist = "Batang.ttc";

    } else if (strcmp(font,"Gulim")==0) {
        fontlist = "gulim.ttc";

    } else if (strcmp(font,"GulimChe")==0) {
        fontlist = "gulim.ttc";

    } else if (strcmp(font,"Dutom")==0) {
        fontlist = "gulim.ttc";

    } else if (strcmp(font,"DutomChe")==0) {
        fontlist = "gulim.ttc";

    } else if (strcmp(font,"New Gulim")==0) {
        fontlist = "NGULIM.ttf";

    } else if (strcmp(font,"SDKwangsoo L")==0) {
        fontlist = "sandolL.ttf";

    } else if (strcmp(font,"Arial Unicode MS")==0) {
        fontlist = "ARIALUNI.ttf";
    }

    return fontlist;
}

int get_fontindex_CP_949(char *font) {

    if (strcmp(font,"Batang")==0) {
        return  0;

    } else if (strcmp(font,"BatangChe")==0) {
        return  1;

    } else if (strcmp(font,"Gungsuh")==0) {
        return  2;

    } else if (strcmp(font,"GungsuhChe")==0) {
        return  3;

    } else if (strcmp(font,"Gulim")==0) {
        return  0;

    } else if (strcmp(font,"GulimChe")==0) {
        return  1;

    } else if (strcmp(font,"Dutom")==0) {
        return  2;

    } else if (strcmp(font,"DutomChe")==0) {
        return  3;

    } else {
        return  0;
    }

}

char *
get_fontfam_CP_949(char *font) {
    char *fontlist;

    fontlist = font;


    return fontlist;
}

#include "CP_950_Big5.h"
#include <string.h>

char *
get_fontname_CP_950(char *font) {
    char *fontlist;

    fontlist = font;

    if (strcmp(font,"標楷體")==0) {
        fontlist = "KAIU";

    } else if (strcmp(font,"新細明體")==0) {
        fontlist = "MINGLIU.ttc";

    } else if (strcmp(font,"MING-LIU")==0) {
        fontlist = "MINGLIU.ttc";

    } else if (strcmp(font,"MING LIU")==0) {
        fontlist = "MINGLIU.ttc";

    } else if (strcmp(font,"細明體")==0) {
        fontlist = "MINGLIU.ttc";

    } else if (strcmp(font,"Arial Unicode MS")==0) {
        fontlist = "ARIALUNI.ttf";
    }

    return fontlist;
}

int get_fontindex_CP_950(char *font) {

    if (strcmp(font,"新細明體")==0) {
        return  1;

    } else if (strcmp(font,"MING-LIU")==0) {
        return  1;

    } else {
        return  0;
    }
}

char *
get_fontfam_CP_950(char *font) {
    char *fontlist;

    fontlist = font;

    if (strcmp(font,"標楷體")==0) {
        fontlist = "MING-LIU";

    } else if (strcmp(font,"新細明體")==0) {
        fontlist = "MING-LIU";

    } else if (strcmp(font,"細明體")==0) {
        fontlist = "MING-LIU";
    }

    return fontlist;
}

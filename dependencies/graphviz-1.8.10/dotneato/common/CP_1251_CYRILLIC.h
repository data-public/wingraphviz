#define CP_1251_DEFAULTFONT "Arial"

char * get_fontname_CP_1251(char *font);

char * get_fontfam_CP_1251(char *font);

int get_fontindex_CP_1251(char *font);

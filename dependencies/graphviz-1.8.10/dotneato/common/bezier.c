#include "render.h"
#include "stdlib.h"
/*
point Beziers (point *p, int n, double mu){
   int k,kn,nn,nkn;
   double blend,muk,munk;
   point b = {0.0,0.0};
 
   muk = 1;
   munk = pow(1-mu,(double)n);
 
   for (k=0;k<=n;k++) {
      nn = n;
      kn = k;
      nkn = n - k;
      blend = muk * munk;
      muk *= mu;
      munk /= (1-mu);
      while (nn >= 1) {
         blend *= nn;
         nn--;
         if (kn > 1) {
            blend /= (double)kn;
            kn--;
         }
         if (nkn > 1) {
            blend /= (double)nkn;
            nkn--;
         }
      }
      b.x += ROUND(p[k].x * blend);
      b.y += ROUND(p[k].y * blend);      
   }
 
   return b;
}
*/
pointf Bezierf (pointf *p, int n, double mu) {
    int k,kn,nn,nkn;
    double blend,muk,munk;
    pointf b = {0.0,0.0};

    muk = 1;
    munk = pow(1-mu,(double)n);

    for (k=0;k<=n;k++) {
        nn = n;
        kn = k;
        nkn = n - k;
        blend = muk * munk;
        muk *= mu;
        munk /= (1-mu);

        while (nn >= 1) {
            blend *= nn;
            nn--;

            if (kn > 1) {
                blend /= (double)kn;
                kn--;
            }

            if (nkn > 1) {
                blend /= (double)nkn;
                nkn--;
            }
        }

        b.x += p[k].x * blend;
        b.y += p[k].y * blend;
    }

    return b;
}

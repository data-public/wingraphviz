
//#include	"error.h"
//#include	<stdarg.h>
#include	"render.h"
/*
#include	"CP_932_ShiftJIS.h"
#include	"CP_936_GB.h"
#include	"CP_949_Koreal.h"
#include	"CP_950_Big5.h"
#include	"unicodeconst.h"
*/
#include	"utils.h"

extern unsigned int DOT_CODEPAGE;

#include "windows.h"

#ifdef DMALLOC
#include "dmalloc.h"
#endif

/* wmf font modifiers */
#define REGULAR 0
#define BOLD	1
#define ITALIC	2

/* wmf patterns */
#define P_SOLID	0
#define P_NONE  15
#define P_DOTTED 4	/* i wasn't sure about this */
#define P_DASHED 11 /* or this */

/* wmf bold line constant */
#define WIDTH_NORMAL 1
#define WIDTH_BOLD 3

//#define WMF_RESOLUTION 96.0
//#define SCALE (WMF_RESOLUTION/72.0)
#define SCALE 1

/* wmf dash array */
//static char * sdarray = "5,2";
/* wmf dot array */
//static char * sdotarray = "1,5";

static	int		N_pages;
static 	point	Pages;
static	double	Scale;
static	pointf	Offset;
static	int		Rot;
static	box		PB;
static int		onetime = TRUE;

static node_t		*Curnode;
static int		GraphURL, ClusterURL, NodeURL, EdgeURL;
static HDC hdc0,hdc;
static HMETAFILE hMF;
static HBRUSH currentBrush = 0;
static HPEN currentPen = 0;

typedef struct context_t {
    COLORREF pencolor, fillcolor;
    char	*fontfam, fontopt, font_was_set, pen, fill, style_was_set;
    long	penwidth;
    double   fontsz;
}

context_t;


#define MAXNEST 4
static context_t cstk[MAXNEST] ={0};
static int SP;

static char             *op[] = {"graph", "node", "edge", "graph"};
#define W_DEGREE 5

extern pointf Bezierf (pointf *p, int n, double mu);
/*
point Beziers (point *p, int n, double mu){
   int k,kn,nn,nkn;
   double blend,muk,munk;
   point b = {0.0,0.0};
 
   muk = 1;
   munk = pow(1-mu,(double)n);
 
   for (k=0;k<=n;k++) {
      nn = n;
      kn = k;
      nkn = n - k;
      blend = muk * munk;
      muk *= mu;
      munk /= (1-mu);
      while (nn >= 1) {
         blend *= nn;
         nn--;
         if (kn > 1) {
            blend /= (double)kn;
            kn--;
         }
         if (nkn > 1) {
            blend /= (double)nkn;
            nkn--;
         }
      }
      b.x += ROUND(p[k].x * blend);
      b.y += ROUND(p[k].y * blend);      
   }
 
   return b;
}
*/
/*
point Beziers (point *V, int degree, double t, point* Left, point* Right) {
    int i, j;		
    point Vtemp[W_DEGREE + 1][W_DEGREE + 1];
    
    for (j =0; j <= degree; j++) {
        Vtemp[0][j] = V[j];
    }
    
    for (i = 1; i <= degree; i++) {
        for (j =0 ; j <= degree - i; j++) {
            Vtemp[i][j].x =
                (1.0 - t) * Vtemp[i-1][j].x + t * Vtemp[i-1][j+1].x;
            Vtemp[i][j].y =
                (1.0 - t) * Vtemp[i-1][j].y + t * Vtemp[i-1][j+1].y;
        }
    }
 
    if (Left != NULL)
        for (j = 0; j <= degree; j++)
            Left[j] = Vtemp[j][0];
 
    if (Right != NULL)
        for (j = 0; j <= degree; j++)
            Right[j] = Vtemp[degree-j][j];
 
    return (Vtemp[degree][0]);
}
*/

static void
wmf_reset(void) {
    onetime = TRUE;
}

COLORREF
wmf_resolve_color(char *name) {
    color_t	color;

    if (!(strcmp(name,"transparent"))) {
        return RGB(0x00,0x00,0x00);

    } else {
        colorxlate(name,&color,RGBA_BYTE);
        return RGB(color.u.rgba[0],color.u.rgba[1],color.u.rgba[2]);
    }
}

static void
init_wmf(HDC hDC) {

    SIZE size;
    BOOL flag;
    COLORREF white = RGB(0xFF,0xFF,0xFF);

    if(currentBrush) {
        DeleteObject(currentBrush);
        currentBrush = 0;
    }


    flag = SetMapMode(hDC,MM_TEXT);
    flag = GetWindowExtEx(hDC,&size);
    flag = SetWindowExtEx(hDC,2,2,&size);
    flag = GetViewportExtEx(hDC,&size);
    flag = SetViewportExtEx(hDC,2,2,&size);

    currentBrush = CreateSolidBrush(white);

    SelectObject(hDC, currentBrush);

    SetBkColor(hDC,white);

    SetViewportExtEx(hdc, 0, 0,NULL);

    SP = 0;

    if(cstk[0].fontfam)
        free(cstk[0].fontfam);

    cstk[0].fontfam = getDefaultFont();

    cstk[0].fontsz = DEFAULT_FONTSIZE;						/* font size */

    cstk[0].fontopt = REGULAR;								/* modifier: REGULAR, BOLD or ITALIC */

    cstk[0].fill = PS_NULL;

    cstk[0].fillcolor = wmf_resolve_color(DEFAULT_FILL);	/* fillcolor */

    cstk[0].pencolor  = wmf_resolve_color(DEFAULT_COLOR);	/* pencolor */

    cstk[0].pen = PS_SOLID;									/* pen pattern style, default is solid */

    cstk[0].penwidth = WIDTH_NORMAL;

}

static point
wmfpt(point p) {
    point	rv;

    if (Rot == 0) {
        rv.x = PB.LL.x + ROUND(p.x * Scale) + Offset.x;
        rv.y = PB.UR.y - 1 - ROUND(p.y * Scale) - Offset.y;

    } else {
        rv.x = PB.UR.x - 1 - ROUND(p.y * Scale) - Offset.x;
        rv.y = PB.UR.y - 1 - ROUND(p.x * Scale) - Offset.y;
    }

    return rv;
}

static
ToPOINT(POINT *pointOut,point p) {


    if (Rot == 0) {
        pointOut->x = PB.LL.x + (int)(p.x * Scale) + Offset.x;
        pointOut->y = PB.UR.y - 1 - (int)(p.y * Scale) - Offset.y;

    } else {
        pointOut->x = PB.UR.x - 1 - (int)(p.y * Scale) - Offset.x;
        pointOut->y = PB.UR.y - 1 - (int)(p.x * Scale) - Offset.y;
    }

}

static void
wmf_comment(void* obj, attrsym_t* sym) {
    BOOL flag;
    flag = GdiComment(hdc,13,"WinGraphviz\n");
}

static void
wmf_begin_job(FILE *ofp, graph_t *g, char **lib, char *user, char *info[], point pages) {
    Pages = pages;
    N_pages = pages.x * pages.y;

}

static  void
wmf_end_job(void) {
}

static void
wmf_begin_graph(graph_t* g, box bb, point pb) {

    hdc = CreateMetaFile(NULL);

    PB.LL.x = PB.LL.y = 0;
    PB.UR.x =ROUND((bb.UR.x - bb.LL.x + 2*g->u.drawing->margin.x) * SCALE);
    PB.UR.y =ROUND((bb.UR.y - bb.LL.y + 2*g->u.drawing->margin.y) * SCALE);
    Offset.x = g->u.drawing->margin.x * SCALE;
    Offset.y = g->u.drawing->margin.y * SCALE;

    RealizePalette(hdc);

    init_wmf(hdc);

}

static void
wmf_end_graph(void) {
    UINT size,size2;
    char * buf;
    char * buf2;
    BOOL flag;
    METAFILEPICT metapict;
    HENHMETAFILE hEMF;

    hMF = CloseMetaFile(hdc);

    size = GetMetaFileBitsEx(hMF,0,NULL);

    if(size > 0) {
        buf = malloc(size);

        metapict.mm = MM_HIENGLISH;
        metapict.xExt = 300;
        metapict.yExt = 300;
        metapict.hMF = hMF;

        size = GetMetaFileBitsEx(hMF,size,buf);

        hEMF = SetWinMetaFileBits(size,buf,NULL,&metapict);

        size2 = GetEnhMetaFileBits(hEMF,0,NULL);

        if(size2 > 0) {
            buf2 = malloc(size2);

            size2 = GetEnhMetaFileBits(hEMF,size2,buf2);

            fwrite(buf2,sizeof(char),(unsigned)size2,Output_file);

            free(buf2);

            flag = DeleteEnhMetaFile(hEMF);
        }

        free(buf);
    }

    flag = DeleteMetaFile(hMF);

    if(currentBrush) {
        DeleteObject(currentBrush);
        currentBrush = 0;
    }

    if(currentPen) {
        DeleteObject(currentPen);
        currentPen = 0;
    }
}

static void
wmf_begin_page(graph_t *g, point page, double scale, int rot, point offset) {
    int		page_number;

    point		sz;

    Scale = scale * SCALE;
    Rot = rot;
    page_number =  page.x + page.y * Pages.x + 1;
    sz = sub_points(PB.UR, PB.LL);

}

static  void
wmf_end_page(void) {
}

static  void
wmf_begin_cluster(graph_t* g) {
}

static  void
wmf_end_cluster (void) {
}

static void wmf_begin_nodes(void) {
}

static void wmf_end_nodes(void) {
}

static void wmf_begin_edges(void) {
}

static void wmf_end_edges(void) {
}

static void
wmf_begin_node(node_t* n) {
    Curnode = n;
}

static  void
wmf_end_node (void) {
}

static  void
wmf_begin_edge (edge_t* e) {
}

static  void
wmf_end_edge (void) {
}

static  void
wmf_begin_context(void) {
    assert(SP + 1 < MAXNEST);
    cstk[SP+1] = cstk[SP];
    SP++;
}

static  void
wmf_end_context(void) {
    int			psp = SP - 1;
    assert(SP > 0);
    SP = psp;
}

static void
wmf_set_font(char* name, double size) {
    char	*p;

    context_t	*cp;

    cp = &(cstk[SP]);
    cp->font_was_set = TRUE;
    cp->fontsz = size;
    p = _strdup(name);


    cp->fontfam = p;

}

static  void
wmf_set_pencolor(char* name) {
    cstk[SP].pencolor = wmf_resolve_color(name);
}

static void
wmf_get_shapestyle(HDC hDC,context_t* cp,int filled) {
    LOGPEN logpen;
    POINT penwidth;
    /*
    	if(currentBrush){
    		DeleteObject(currentBrush);
    		currentBrush = 0;
    	}
     
    	if(currentPen){
    		DeleteObject(currentPen);
    		currentPen = 0;
    	}
    */

    if(filled)
        currentBrush = CreateSolidBrush(cp->fillcolor );
    else
        currentBrush = CreateSolidBrush(RGB(0xFF,0xFF,0xFF));

    SelectObject(hDC, currentBrush);

    logpen.lopnColor = cp->pencolor;

    penwidth.x = cp->penwidth;

    logpen.lopnWidth = penwidth ;

    logpen.lopnStyle = cp->pen;

    currentPen = CreatePenIndirect(&logpen);

    SelectObject(hDC, currentPen);

}

static void
wmf_get_fillstyle(HDC hDC,context_t* cp) {
    /*
    	if(currentBrush){
    		DeleteObject(currentBrush);
    		currentBrush = 0;
    	}
    */
    currentBrush = CreateSolidBrush(cp->fillcolor );
    SelectObject(hDC, currentBrush);

}

static BOOL
isFontItalic(const char * fontname) {

    if (strncasecmp(fontname,"italic",5) == 0)
        return TRUE;
    else
        return FALSE;
}

static BOOL
getFontWeight(const char * fontname) {
    if (strncasecmp(fontname,"bold",4) == 0)
        return FW_BOLD;
    else
        return FW_NORMAL;
}

static char *
getFontFace(const char * fontname) {
}

static void
wmf_get_textstyle(HDC hDC,context_t* cp) {
    HFONT hfnt,hfntPrev;
    const char * fontfam;
    int angle;

    PLOGFONT plf = (PLOGFONT) LocalAlloc(LPTR, sizeof(LOGFONT));
    /* In order for the stock fonts to be independent of
        * mapping mode, the height (& width) must be 0
        */

    plf->lfHeight = cp->fontsz * Scale ;
    plf->lfWidth = 0 ;

    fontfam = cp->fontfam;

    if(strlen(fontfam)) {
        lstrcpy(plf->lfFaceName, fontfam);
        plf->lfWeight = getFontWeight(fontfam);
        plf->lfItalic = isFontItalic(fontfam);

    } else {
        plf->lfCharSet = DOT_CODEPAGE;
    }

    plf->lfEscapement = 0;
    plf->lfQuality = PROOF_QUALITY;
    angle = (Rot? 90.0 : 0.0) * 10;
    plf->lfOrientation = angle ;
    plf->lfEscapement = angle ;

    hfnt = CreateFontIndirect(plf);

    hfntPrev = SelectObject(hdc, hfnt);

    SetTextColor(hDC, cp->pencolor);
    //SetTextAlign(hDC,TA_BOTTOM | TA_LEFT );
    SetTextAlign(hDC,TA_CENTER );
    SetBkMode(hDC,OPAQUE);
    SetBkColor(hDC,RGB(0xFF,0xFF,0xFF));

    DeleteObject(hfnt);

    LocalFree((LOCALHANDLE) plf);

}

static  void
wmf_set_fillcolor(char* name) {
    cstk[SP].fillcolor = wmf_resolve_color(name);
}

static  void
wmf_set_style(char** s) {
    char		*line, *p;
    context_t	*cp;

    cp = &(cstk[SP]);

    while ((p = line = *s++)) {
        if (streq(line,"solid"))
            cp->pen = PS_SOLID;
        else if (streq(line,"dashed"))
            cp->pen = PS_DASH;
        else if (streq(line,"dotted"))
            cp->pen = PS_DOT;
        else if (streq(line,"invis"))
            cp->pen = PS_NULL;
        else if (streq(line,"bold"))
            cp->penwidth = WIDTH_BOLD;
        else if (streq(line, "setlinewidth")) {
            while (*p)
                p++;

            p++;

            cp->penwidth = atol(p);

        } else if (streq(line,"filled"))
            cp->fill = P_SOLID;
        else if (streq(line,"unfilled"))
            cp->fill = PS_NULL;


        cp->style_was_set = TRUE;
    }
}

static void
wmf_textline(point p, textline_t *line) {
    point	mp;
    context_t *cp;
    UINT size;
    RECT rect;
    int flag = 0;

    size = strlen(line->str);

    cp = &(cstk[SP]);

    if( cp->pen == PS_NULL ) {
        return;
    }

    switch(line->just) {

            case 'l':
            mp.x = p.x + line->width /2;
            break;

            case 'r':
            mp.x = p.x - line->width /2;
            break;

            default:

            case 'n':
            mp.x = p.x ;
            break;
    }

    mp.y = p.y + cp->fontsz ;

    mp = wmfpt(mp);

    wmf_get_textstyle(hdc,cp);

    flag = TextOut(hdc, mp.x,mp.y,line->str, size);
    //flag = MoveToEx(hdc,0,0 ,NULL);
    //flag = DrawText(hdc,line->str,size,&rect,DT_BOTTOM | DT_SINGLELINE );
}

static void
wmf_ellipse(point p, int rx, int ry, int filled) {
    point	mp;
    int nLeftRect;
    int nTopRect;
    int nRightRect;
    int nBottomRect ;

    if( cstk[SP].pen == PS_NULL	 ) {
        return;
    }

    mp.x = p.x;
    mp.y = p.y;
    mp = wmfpt(mp);


    if (Rot) {
        int t;
        t = rx;
        rx = ry;
        ry = t;
    }

    nLeftRect = ROUND(mp.x - Scale * rx);
    nTopRect = ROUND(mp.y - Scale * ry );

    nRightRect = ROUND(mp.x + Scale * rx);
    nBottomRect = ROUND(mp.y + Scale * ry) ;

    wmf_get_shapestyle(hdc,&(cstk[SP]),filled);

    Ellipse(hdc,nLeftRect,nTopRect,nRightRect,nBottomRect);

}

static void
wmf_bezier(point* A, int n, int arrow_at_start, int arrow_at_end) {
    pointf	pFrom, pTo;
    pointf * V;
    point	pTmp;
    int i,j;
    int div;

    if( cstk[SP].pen == PS_NULL ) {
        return;
    }

    V = malloc(sizeof(pointf) * n);

    wmf_get_shapestyle(hdc,&(cstk[SP]),0);

    for (i = 0; i <= n-1; i++) {
        pTmp = wmfpt(A[i]);
        V[i].x = pTmp.x ;
        V[i].y = pTmp.y ;
    }

    pFrom = V[0];

    div = (abs(V[n-1].x - V[0].x) + abs(V[n-1].y - V[0].y)) >> 2;

    div = (div < 2)  ? 2 : div;

    for (j = 1; j < div; j++) {
        pTo = Bezierf(V, n -1, (double)j/div);
        MoveToEx(hdc,pFrom.x,pFrom.y,NULL);
        LineTo(hdc,pTo.x,pTo.y);
        pFrom = pTo;
    }

    LineTo(hdc,V[n-1].x,V[n-1].y);

    free(V);
}

static void
wmf_polygon(point *A, int n, int filled) {
    int	i;
    POINT  * points;


    if( cstk[SP].pen == PS_NULL ) {
        return;
    }

    points = (POINT *)malloc(n*sizeof(POINT));

    for (i = 0; i < n ; i++) {

        ToPOINT(&points[i],A[i]);

    }

    wmf_get_shapestyle(hdc,&(cstk[SP]),filled);

    Polygon(hdc,points,n);

    free(points);

}

static void
wmf_polyline(point* A, int n) {
    int	i;
    POINT	points[2];


    if( cstk[SP].pen == PS_NULL ) {
        return;
    }

    ToPOINT(&points[0],A[0]);

    wmf_get_shapestyle(hdc,&(cstk[SP]),1);

    for (i = 1; i < n - 1; i++) {

        ToPOINT(&points[1],A[i]);

        Polyline(hdc,(CONST POINT *)&points,2);

        points[0].x = points[1].x ;
        points[0].y = points[1].x ;

    }


}

static void
wmf_user_shape(char *name, point *A, int n, int filled) {
}

codegen_t	EMF_CodeGen = {
                            wmf_reset,
                            wmf_begin_job, wmf_end_job,
                            wmf_begin_graph, wmf_end_graph,
                            wmf_begin_page, wmf_end_page,
                            wmf_begin_cluster, wmf_end_cluster,
                            wmf_begin_nodes, wmf_end_nodes,
                            wmf_begin_edges, wmf_end_edges,
                            wmf_begin_node, wmf_end_node,
                            wmf_begin_edge, wmf_end_edge,
                            wmf_begin_context, wmf_end_context,
                            wmf_set_font, wmf_textline,
                            wmf_set_pencolor, wmf_set_fillcolor, wmf_set_style,
                            wmf_ellipse, wmf_polygon,
                            wmf_bezier, wmf_polyline,
                            0 /* wmf_arrowhead */, wmf_user_shape,
                            0 /* wmf_comment */, 0 /* wmf_textsize */
                        };

#define CP_950_DEFAULTFONT "MING LIU"

char * get_fontname_CP_950(char *font);

char * get_fontfam_CP_950(char *font);

int get_fontindex_CP_950(char *font);

#define CP_932_DEFAULTFONT "MS Gothic"

char * get_fontname_CP_932(char *font);

char * get_fontfam_CP_932(char *font);

int get_fontindex_CP_932(char *font);

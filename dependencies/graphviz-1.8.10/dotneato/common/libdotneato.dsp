# Microsoft Developer Studio Project File - Name="libdotneato" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Static Library" 0x0104

CFG=libdotneato - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "libdotneato.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "libdotneato.mak" CFG="libdotneato - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "libdotneato - Win32 Release" (based on "Win32 (x86) Static Library")
!MESSAGE "libdotneato - Win32 Debug" (based on "Win32 (x86) Static Library")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""$/graphviz/libdotneato", CFAAAAAA"
# PROP Scc_LocalPath "."
CPP=cl.exe
RSC=rc.exe

!IF  "$(CFG)" == "libdotneato - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_MBCS" /D "_LIB" /YX /FD /c
# ADD CPP /nologo /GB /MT /W3 /GX /I "..\..\tools\os" /I ".." /I "..\.." /I "..\..\graph" /I "..\..\cdt" /I "..\..\pathplan" /I "..\..\tools\src" /I "..\..\gd" /I "..\..\..\zlib" /I "..\..\tools\error" /D "NDEBUG" /D "WIN32" /D "_MBCS" /D "_LIB" /D "HAVE_CONFIG_H" /D "MSWIN32" /FR /YX /FD -o3 -Qxi /c
# ADD BASE RSC /l 0x404 /d "NDEBUG"
# ADD RSC /l 0x404 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LIB32=link.exe -lib
# ADD BASE LIB32 /nologo
# ADD LIB32 /nologo /out:"..\..\lib\Release\libdotneato.lib"

!ELSEIF  "$(CFG)" == "libdotneato - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_MBCS" /D "_LIB" /YX /FD /GZ /c
# ADD CPP /nologo /MTd /W3 /Gm /ZI /Od /I "D:\Project\graphviz-1.8.10\tools\os" /I ".." /I "..\.." /I "..\..\graph" /I "..\..\cdt" /I "..\..\pathplan" /I "..\..\tools\src" /I "..\..\gd" /I "..\..\..\zlib" /I "..\..\tools\error" /I "..\..\tools\os" /D "_DEBUG" /D "WIN32" /D "_MBCS" /D "_LIB" /D "HAVE_CONFIG_H" /D "MSWIN32" /FR /FD /GZ /c
# SUBTRACT CPP /YX
# ADD BASE RSC /l 0x404 /d "_DEBUG"
# ADD RSC /l 0x404 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LIB32=link.exe -lib
# ADD BASE LIB32 /nologo
# ADD LIB32 /nologo /out:"..\..\lib\Debug\libdotneato.lib"

!ENDIF 

# Begin Target

# Name "libdotneato - Win32 Release"
# Name "libdotneato - Win32 Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=.\arrows.c
# End Source File
# Begin Source File

SOURCE=.\base64.c
# End Source File
# Begin Source File

SOURCE=.\bezier.c
# End Source File
# Begin Source File

SOURCE=.\colxlate.c
# End Source File
# Begin Source File

SOURCE=.\CP_1251_CYRILLIC.c
# End Source File
# Begin Source File

SOURCE=.\CP_932_ShiftJIS.c
# End Source File
# Begin Source File

SOURCE=.\CP_936_GB.c
# End Source File
# Begin Source File

SOURCE=.\CP_949_Koreal.c
# End Source File
# Begin Source File

SOURCE=.\CP_950_Big5.c
# End Source File
# Begin Source File

SOURCE=.\emit.c
# End Source File
# Begin Source File

SOURCE=.\fontmetrics.c
# End Source File
# Begin Source File

SOURCE=.\gdgen.c
# End Source File
# Begin Source File

SOURCE=.\globals.c
# End Source File
# Begin Source File

SOURCE=.\input.c
# End Source File
# Begin Source File

SOURCE=.\mapgen.c
# End Source File
# Begin Source File

SOURCE=.\output.c
# End Source File
# Begin Source File

SOURCE=.\postproc.c
# End Source File
# Begin Source File

SOURCE=.\psgen.c
# End Source File
# Begin Source File

SOURCE=.\shapes.c
# End Source File
# Begin Source File

SOURCE=.\strcasecmp.c
# End Source File
# Begin Source File

SOURCE=.\strncasecmp.c
# End Source File
# Begin Source File

SOURCE=.\svggen.c
# End Source File
# Begin Source File

SOURCE=.\utils.c
# End Source File
# Begin Source File

SOURCE=.\wmfgen.c
# End Source File
# Begin Source File

SOURCE=.\xbuf.c
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=.\base64.h
# End Source File
# Begin Source File

SOURCE=.\const.h
# End Source File
# Begin Source File

SOURCE=.\CP_1251_CYRILLIC.h
# End Source File
# Begin Source File

SOURCE=.\CP_932_ShiftJIS.h
# End Source File
# Begin Source File

SOURCE=.\CP_936_GB.h
# End Source File
# Begin Source File

SOURCE=.\CP_949_Koreal.h
# End Source File
# Begin Source File

SOURCE=.\CP_950_Big5.h
# End Source File
# Begin Source File

SOURCE=.\globals.h
# End Source File
# Begin Source File

SOURCE=.\macros.h
# End Source File
# Begin Source File

SOURCE=.\render.h
# End Source File
# Begin Source File

SOURCE=.\renderprocs.h
# End Source File
# Begin Source File

SOURCE=.\types.h
# End Source File
# Begin Source File

SOURCE=.\utils.h
# End Source File
# Begin Source File

SOURCE=.\xbuf.h
# End Source File
# End Group
# End Target
# End Project

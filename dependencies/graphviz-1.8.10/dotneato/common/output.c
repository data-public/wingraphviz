/*
    This software may only be used by you under license from AT&T Corp.
    ("AT&T").  A copy of AT&T's Source Code Agreement is available at
    AT&T's Internet website having the URL:
    <http://www.research.att.com/sw/tools/graphviz/license/source.html>
    If you received this software without first entering into a license
    with AT&T, you have an infringing copy of this software and cannot use
    it without violating AT&T's intellectual property rights.
*/

#include	"error.h"
#include	"render.h"
#include	"xbuf.h"

#ifdef DMALLOC
#include "dmalloc.h"
#endif

int        Output_lang;
codegen_t  *CodeGen;
FILE       *Output_file;

void dotneato_set_margins(graph_t* g) {
    double	xf, yf;
    char	*p;
    int	i;

    /* margins */

    if ((p = agget(g,"margin"))) {
        i = sscanf(p,"%lf,%lf",&xf,&yf);

        if (i > 0)
            g->u.drawing->margin.x = g->u.drawing->margin.y = POINTS(xf);

        if (i > 1)
            g->u.drawing->margin.y = POINTS(yf);

    } else {
        /* set default margins depending on format */

        switch (Output_lang) {

                case GIF:

                case PNG:

                case JPEG:

                case WBMP:

                case EMF:

                case GD:

                case memGD:

                case GD2:

                case ISMAP:

                case IMAP:

                case CMAP:

                case VRML:

                case SVG:

                case SVGZ:
                g->u.drawing->margin.x = g->u.drawing->margin.y = DEFAULT_EMBED_MARGIN;
                break;

                case POSTSCRIPT:

                case PDF:

                case HPGL:

                case PCL:

                case MIF:

                case METAPOST:

                case FIG:

                case VTX:

                case ATTRIBUTED_DOT:

                case PLAIN:

                case PLAIN_EXT:
                g->u.drawing->margin.x = g->u.drawing->margin.y = DEFAULT_MARGIN;
                break;

                case CANONICAL_DOT:
                break;
        }
    }
}

void dotneato_write(graph_t* g) {
    dotneato_set_margins(g);

    switch (Output_lang) {

            case POSTSCRIPT:

            case PDF:

            case HPGL:

            case PCL:

            case MIF:

            case PIC_format:

            case GIF:

            case PNG:

            case JPEG:

            case WBMP:

            case GD:

            case memGD:

            case GD2:

            case VRML:

            case METAPOST:

            case SVG:

            case SVGZ:

            case EMF:
            /* output in breadth first graph walk order */
            //emit_graph(g,0);
            emit_graph(g,EMIT_COLORS);
            break;

            case ISMAP:

            case IMAP:

            case CMAP:
            /* output in breadth first graph walk order, but
             * with nodes edges and nested clusters before
             * clusters */
            emit_graph(g,EMIT_CLUSTERS_LAST);
            break;

            case FIG:
            /* output color definition objects first */
            emit_graph(g,EMIT_COLORS);
            break;

            case VTX:
            /* output sorted, i.e. all nodes then all edges */
            emit_graph(g,EMIT_SORTED);
            break;

            case ATTRIBUTED_DOT:
            attach_attrs(g);
            agwrite(g,Output_file);
            break;

            case CANONICAL_DOT:
            agwrite(g,Output_file);
            break;

            case PLAIN:
            attach_attrs(g);
            write_plain(g,Output_file);
            break;

            case PLAIN_EXT:
            attach_attrs(g);
            write_plain_ext(g,Output_file);
            break;
    }

    fflush(Output_file);
}

static void
set_record_rects (node_t* n, field_t* f, xbuf* xb) {
    int    i;
    char   buf[BUFSIZ];

    if (f->n_flds == 0) {
        sprintf(buf, "%d,%d,%d,%d ",
                f->b.LL.x + n->u.coord.x,
                f->b.LL.y + n->u.coord.y,
                f->b.UR.x + n->u.coord.x,
                f->b.UR.y + n->u.coord.y);
        xbput (xb, buf);
    }

    for (i = 0; i < f->n_flds; i++)
        set_record_rects (n, f->fld[i], xb);
}

static attrsym_t *safe_dcl(graph_t *g, void *obj, char *name, char *def,
                           attrsym_t*(*fun)(Agraph_t*, char*, char*)) {
    attrsym_t	*a = agfindattr(obj,name);

    if (a == NULL)
        a = fun(g,name,def);

    return a;
}

void attach_attrs(graph_t* g) {
    int		i,j,sides;
    char    buf[BUFSIZ];     /* Used only for small strings */
    unsigned char	xbuffer[BUFSIZ]; /* Initial buffer for xb */
    xbuf	xb;
    node_t	*n;
    edge_t	*e;
    point	pt;

    xbinit (&xb, BUFSIZ, xbuffer);
    safe_dcl(g,g->proto->n,"pos","",agnodeattr);
    safe_dcl(g,g->proto->n,"rects","",agnodeattr);
    N_width = safe_dcl(g,g->proto->n,"width","",agnodeattr);
    N_height = safe_dcl(g,g->proto->n,"height","",agnodeattr);
    safe_dcl(g,g->proto->e,"pos","",agedgeattr);

    if (g->u.has_edge_labels)
        safe_dcl(g,g->proto->e,"lp","",agedgeattr);

    if (g->u.label) {
        safe_dcl(g,g,"lp","",agraphattr);
        pt = g->u.label->p;
        sprintf(buf,"%d,%d",pt.x,pt.y);
        agset(g,"lp",buf);
    }

    safe_dcl(g,g,"bb","",agraphattr);

    for (n = agfstnode(g); n; n = agnxtnode(g,n)) {
        sprintf(buf,"%d,%d",n->u.coord.x,n->u.coord.y);
        agset(n,"pos",buf);
        sprintf(buf,"%.2f",PS2INCH(n->u.ht));
        agxset(n,N_height->index,buf);
        sprintf(buf,"%.2f",PS2INCH(n->u.lw + n->u.rw));
        agxset(n,N_width->index,buf);

        if (strcmp (n->u.shape->name, "record") == 0) {
            set_record_rects (n, n->u.shape_info, &xb);
            xbpop (&xb); /* get rid of last space */
            agset(n,"rects",xbuse(&xb));

        } else {
            extern void	poly_init(node_t *);
            polygon_t *poly;
            int i;

            if (N_vertices && (n->u.shape->initfn == poly_init)) {
                poly = (polygon_t*) n->u.shape_info;
                sides = poly->sides;

                if (sides < 3) {
                    char *p = agget(n,"samplepoints");

                    if (p)
                        sides = atoi(p);
                    else
                        sides = 8;

                    if (sides < 3)
                        sides = 8;
                }

                for (i = 0; i < sides; i++) {
                    if (i > 0)
                        xbputc (&xb, ' ');

                    if (poly->sides >= 3)
                        sprintf(buf,"%.3f %.3f",
                                poly->vertices[i].x,poly->vertices[i].y);
                    else
                        sprintf(buf,"%.3f %.3f",
                                n->u.width/2.0 * cos(i/(double)sides * PI * 2.0),
                                n->u.height/2.0 * sin(i/(double)sides * PI * 2.0));

                    xbput(&xb, buf);
                }

                agxset(n,N_vertices->index,xbuse(&xb));
            }
        }

        if (State >= GVSPLINES) {
            for (e = agfstout(g,n); e; e = agnxtout(g,e)) {
                if (e->u.edge_type == IGNORED)
                    continue;

                if (e->u.spl == NULL) {
                    errorPrintf(stderr,"lost spline of %s %s\n",e->tail->name,e->head->name);
                    continue;
                }

                for (i = 0; i < e->u.spl->size; i++) {
                    if (i > 0)
                        xbputc (&xb, ';');

                    if (e->u.spl->list[i].sflag) {
                        sprintf (buf, "s,%d,%d ",e->u.spl->list[i].sp.x,e->u.spl->list[i].sp.y);
                        xbput(&xb, buf);
                    }

                    if (e->u.spl->list[i].eflag) {
                        sprintf (buf, "e,%d,%d ",e->u.spl->list[i].ep.x,e->u.spl->list[i].ep.y);
                        xbput(&xb, buf);
                    }

                    for (j = 0; j < e->u.spl->list[i].size; j++) {
                        if (j > 0)
                            xbputc (&xb, ' ');

                        pt = e->u.spl->list[i].list[j];

                        sprintf(buf,"%d,%d",pt.x,pt.y);

                        xbput(&xb, buf);
                    }
                }

                agset(e,"pos",xbuse(&xb));

                if (e->u.label) {
                    pt = e->u.label->p;
                    sprintf(buf,"%d,%d",pt.x,pt.y);
                    agset(e,"lp",buf);
                }
            }
        }
    }

    rec_attach_bb(g);
    xbfree (&xb);
}

void rec_attach_bb(graph_t* g) {
    int		c;
    char	buf[32];

    sprintf(buf,"%d,%d,%d,%d", g->u.bb.LL.x, g->u.bb.LL.y,
            g->u.bb.UR.x, g->u.bb.UR.y);
    agset(g,"bb",buf);

    for (c = 1; c <= g->u.n_cluster; c++)
        rec_attach_bb(g->u.clust[c]);
}

static char *getoutputbuffer(char *str) {
    static char             *rv;
    static int              len;
    int                     req;

    req = MAX(2 * strlen(str) + 2, BUFSIZ);

    if (req > len) {
        if (rv)
            rv = realloc(rv,req);
        else
            rv = malloc(req);

        len = req;
    }

    return rv;
}


static char *canonical(char *str) {
    return agstrcanon(str,getoutputbuffer(str));
}

static void writenodeandport(FILE *fp, char *node, char *port) {
    fprintf(fp,"%s",canonical(node));       /* slimey i know*/

    if (port && *port)
        fprintf(fp,"%c%s",port[0],canonical(port+1));
}

void write_plain(graph_t* g, FILE* f) {
    int		i,j,splinePoints;
    node_t		*n;
    edge_t		*e;
    bezier		bz;

    setup_graph(g);
    fprintf(f,"graph %.3f",g->u.drawing->scale);
    printptf(f,g->u.bb.UR);
    fprintf(f,"\n");

    for (n = agfstnode(g); n; n = agnxtnode(g,n)) {
        fprintf(f,"node %s ",canonical(n->name));
        printptf(f,n->u.coord);
        fprintf(f," %.3f %.3f %s %s %s %s %s\n",
                n->u.width,n->u.height,canonical(n->u.label->text),
                late_nnstring(n,N_style,"solid"),
                n->u.shape->name,
                late_nnstring(n,N_color,DEFAULT_COLOR),
                late_nnstring(n,N_fillcolor,DEFAULT_FILL));
    }

    for (n = agfstnode(g); n; n = agnxtnode(g,n)) {
        for (e = agfstout(g,n); e; e = agnxtout(g,e)) {
            if (e->u.spl) {
                splinePoints = 0;

                for (i = 0; i < e->u.spl->size; i++) {
                    bz = e->u.spl->list[i];
                    splinePoints += bz.size;
                }

                fprintf(f,"edge ");
                writenodeandport(f,e->tail->name,"");
                fprintf(f," ");
                writenodeandport(f,e->head->name,"");
                fprintf(f," %d",splinePoints);

                for (i = 0; i < e->u.spl->size; i++) {
                    bz = e->u.spl->list[i];

                    for (j = 0; j < bz.size; j++)
                        printptf(f,bz.list[j]);
                }
            }

            if (e->u.label) {
                fprintf(f," %s",canonical(e->u.label->text));
                printptf(f,e->u.label->p);
            }

            fprintf(f," %s %s\n",late_nnstring(e,E_style,"solid"),
                    late_nnstring(e,E_color,DEFAULT_COLOR));
        }
    }

    fprintf(f,"stop\n");
}

/* FIXME - there must be a proper way to get port info - these are
 * supposed to be private to libgraph - from libgraph.h */
#define TAILX 1
#define HEADX 2

void write_plain_ext(graph_t* g, FILE* f) {
    int			i;
    node_t		*n;
    edge_t		*e;
    bezier		bz;
    char		*tport, *hport;

    setup_graph(g);
    fprintf(f,"graph %.3f",g->u.drawing->scale);
    printptf(f,g->u.bb.UR);
    fprintf(f,"\n");

    for (n = agfstnode(g); n; n = agnxtnode(g,n)) {
        fprintf(f,"node %s ",canonical(n->name));
        printptf(f,n->u.coord);
        fprintf(f," %.3f %.3f %s %s %s %s %s\n",
                n->u.width,n->u.height,canonical(n->u.label->text),
                late_nnstring(n,N_style,"solid"),
                n->u.shape->name,
                late_nnstring(n,N_color,DEFAULT_COLOR),
                late_nnstring(n,N_fillcolor,DEFAULT_FILL));
    }

    for (n = agfstnode(g); n; n = agnxtnode(g,n)) {
        for (e = agfstout(g,n); e; e = agnxtout(g,e)) {
            bz = e->u.spl->list[0];

            if (e->attr) {
                tport = e->attr[TAILX];
                hport = e->attr[HEADX];

            } else
                tport = hport = "";

            fprintf(f,"edge ");

            writenodeandport(f,e->tail->name,tport);

            fprintf(f," ");

            writenodeandport(f,e->head->name,hport);

            fprintf(f," %d",bz.size);

            for (i = 0; i < bz.size; i++)
                printptf(f,bz.list[i]);

            if (e->u.label) {
                fprintf(f," %s",canonical(e->u.label->text));
                printptf(f,e->u.label->p);
            }

            fprintf(f," %s %s\n",late_nnstring(e,E_style,"solid"),
                    late_nnstring(e,E_color,DEFAULT_COLOR));
        }
    }

    fprintf(f,"stop\n");
}

void printptf(FILE* f, point pt) {
    fprintf(f," %.3f %.3f",PS2INCH(pt.x),PS2INCH(pt.y));
}

int codegen_bezier_has_arrows(void) {
    return CodeGen &&
           CodeGen->bezier_has_arrows
           /* (CodeGen->arrowhead == 0)) */;
}

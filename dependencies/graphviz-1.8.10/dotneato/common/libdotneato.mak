# Microsoft Developer Studio Generated NMAKE File, Based on libdotneato.dsp
!IF "$(CFG)" == ""
CFG=libdotneato - Win32 Debug
!MESSAGE No configuration specified. Defaulting to libdotneato - Win32 Debug.
!ENDIF 

!IF "$(CFG)" != "libdotneato - Win32 Release" && "$(CFG)" != "libdotneato - Win32 Debug"
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "libdotneato.mak" CFG="libdotneato - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "libdotneato - Win32 Release" (based on "Win32 (x86) Static Library")
!MESSAGE "libdotneato - Win32 Debug" (based on "Win32 (x86) Static Library")
!MESSAGE 
!ERROR An invalid configuration is specified.
!ENDIF 

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE 
NULL=nul
!ENDIF 

CPP=cl.exe
RSC=rc.exe

!IF  "$(CFG)" == "libdotneato - Win32 Release"

OUTDIR=.\Release
INTDIR=.\Release
# Begin Custom Macros
OutDir=.\Release
# End Custom Macros

ALL : "..\..\lib\Release\libdotneato.lib" "$(OUTDIR)\libdotneato.bsc"


CLEAN :
	-@erase "$(INTDIR)\arrows.obj"
	-@erase "$(INTDIR)\arrows.sbr"
	-@erase "$(INTDIR)\base64.obj"
	-@erase "$(INTDIR)\base64.sbr"
	-@erase "$(INTDIR)\bezier.obj"
	-@erase "$(INTDIR)\bezier.sbr"
	-@erase "$(INTDIR)\colxlate.obj"
	-@erase "$(INTDIR)\colxlate.sbr"
	-@erase "$(INTDIR)\CP_1251_CYRILLIC.obj"
	-@erase "$(INTDIR)\CP_1251_CYRILLIC.sbr"
	-@erase "$(INTDIR)\CP_932_ShiftJIS.obj"
	-@erase "$(INTDIR)\CP_932_ShiftJIS.sbr"
	-@erase "$(INTDIR)\CP_936_GB.obj"
	-@erase "$(INTDIR)\CP_936_GB.sbr"
	-@erase "$(INTDIR)\CP_949_Koreal.obj"
	-@erase "$(INTDIR)\CP_949_Koreal.sbr"
	-@erase "$(INTDIR)\CP_950_Big5.obj"
	-@erase "$(INTDIR)\CP_950_Big5.sbr"
	-@erase "$(INTDIR)\emit.obj"
	-@erase "$(INTDIR)\emit.sbr"
	-@erase "$(INTDIR)\fontmetrics.obj"
	-@erase "$(INTDIR)\fontmetrics.sbr"
	-@erase "$(INTDIR)\gdgen.obj"
	-@erase "$(INTDIR)\gdgen.sbr"
	-@erase "$(INTDIR)\globals.obj"
	-@erase "$(INTDIR)\globals.sbr"
	-@erase "$(INTDIR)\input.obj"
	-@erase "$(INTDIR)\input.sbr"
	-@erase "$(INTDIR)\mapgen.obj"
	-@erase "$(INTDIR)\mapgen.sbr"
	-@erase "$(INTDIR)\output.obj"
	-@erase "$(INTDIR)\output.sbr"
	-@erase "$(INTDIR)\postproc.obj"
	-@erase "$(INTDIR)\postproc.sbr"
	-@erase "$(INTDIR)\psgen.obj"
	-@erase "$(INTDIR)\psgen.sbr"
	-@erase "$(INTDIR)\shapes.obj"
	-@erase "$(INTDIR)\shapes.sbr"
	-@erase "$(INTDIR)\strcasecmp.obj"
	-@erase "$(INTDIR)\strcasecmp.sbr"
	-@erase "$(INTDIR)\strncasecmp.obj"
	-@erase "$(INTDIR)\strncasecmp.sbr"
	-@erase "$(INTDIR)\svggen.obj"
	-@erase "$(INTDIR)\svggen.sbr"
	-@erase "$(INTDIR)\utils.obj"
	-@erase "$(INTDIR)\utils.sbr"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(INTDIR)\wmfgen.obj"
	-@erase "$(INTDIR)\wmfgen.sbr"
	-@erase "$(INTDIR)\xbuf.obj"
	-@erase "$(INTDIR)\xbuf.sbr"
	-@erase "$(OUTDIR)\libdotneato.bsc"
	-@erase "..\..\lib\Release\libdotneato.lib"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP_PROJ=/nologo /G5 /MT /W3 /GX /O1 /I "..\..\tools\os" /I ".." /I "..\.." /I "..\..\graph" /I "..\..\cdt" /I "..\..\pathplan" /I "..\..\tools\src" /I "..\..\gd" /I "..\..\..\zlib" /I "..\..\tools\error" /D "NDEBUG" /D "WIN32" /D "_MBCS" /D "_LIB" /D "HAVE_CONFIG_H" /D "MSWIN32" /FR"$(INTDIR)\\" /Fp"$(INTDIR)\libdotneato.pch" /YX /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /c 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\libdotneato.bsc" 
BSC32_SBRS= \
	"$(INTDIR)\arrows.sbr" \
	"$(INTDIR)\base64.sbr" \
	"$(INTDIR)\bezier.sbr" \
	"$(INTDIR)\colxlate.sbr" \
	"$(INTDIR)\CP_932_ShiftJIS.sbr" \
	"$(INTDIR)\CP_936_GB.sbr" \
	"$(INTDIR)\CP_949_Koreal.sbr" \
	"$(INTDIR)\CP_950_Big5.sbr" \
	"$(INTDIR)\emit.sbr" \
	"$(INTDIR)\fontmetrics.sbr" \
	"$(INTDIR)\gdgen.sbr" \
	"$(INTDIR)\globals.sbr" \
	"$(INTDIR)\input.sbr" \
	"$(INTDIR)\mapgen.sbr" \
	"$(INTDIR)\output.sbr" \
	"$(INTDIR)\postproc.sbr" \
	"$(INTDIR)\psgen.sbr" \
	"$(INTDIR)\shapes.sbr" \
	"$(INTDIR)\strcasecmp.sbr" \
	"$(INTDIR)\strncasecmp.sbr" \
	"$(INTDIR)\svggen.sbr" \
	"$(INTDIR)\utils.sbr" \
	"$(INTDIR)\wmfgen.sbr" \
	"$(INTDIR)\xbuf.sbr" \
	"$(INTDIR)\CP_1251_CYRILLIC.sbr"

"$(OUTDIR)\libdotneato.bsc" : "$(OUTDIR)" $(BSC32_SBRS)
    $(BSC32) @<<
  $(BSC32_FLAGS) $(BSC32_SBRS)
<<

LIB32=link.exe -lib
LIB32_FLAGS=/nologo /out:"..\..\lib\Release\libdotneato.lib" 
LIB32_OBJS= \
	"$(INTDIR)\arrows.obj" \
	"$(INTDIR)\base64.obj" \
	"$(INTDIR)\bezier.obj" \
	"$(INTDIR)\colxlate.obj" \
	"$(INTDIR)\CP_932_ShiftJIS.obj" \
	"$(INTDIR)\CP_936_GB.obj" \
	"$(INTDIR)\CP_949_Koreal.obj" \
	"$(INTDIR)\CP_950_Big5.obj" \
	"$(INTDIR)\emit.obj" \
	"$(INTDIR)\fontmetrics.obj" \
	"$(INTDIR)\gdgen.obj" \
	"$(INTDIR)\globals.obj" \
	"$(INTDIR)\input.obj" \
	"$(INTDIR)\mapgen.obj" \
	"$(INTDIR)\output.obj" \
	"$(INTDIR)\postproc.obj" \
	"$(INTDIR)\psgen.obj" \
	"$(INTDIR)\shapes.obj" \
	"$(INTDIR)\strcasecmp.obj" \
	"$(INTDIR)\strncasecmp.obj" \
	"$(INTDIR)\svggen.obj" \
	"$(INTDIR)\utils.obj" \
	"$(INTDIR)\wmfgen.obj" \
	"$(INTDIR)\xbuf.obj" \
	"$(INTDIR)\CP_1251_CYRILLIC.obj"

"..\..\lib\Release\libdotneato.lib" : "$(OUTDIR)" $(DEF_FILE) $(LIB32_OBJS)
    $(LIB32) @<<
  $(LIB32_FLAGS) $(DEF_FLAGS) $(LIB32_OBJS)
<<

!ELSEIF  "$(CFG)" == "libdotneato - Win32 Debug"

OUTDIR=.\Debug
INTDIR=.\Debug
# Begin Custom Macros
OutDir=.\Debug
# End Custom Macros

ALL : "..\..\lib\Debug\libdotneato.lib" "$(OUTDIR)\libdotneato.bsc"


CLEAN :
	-@erase "$(INTDIR)\arrows.obj"
	-@erase "$(INTDIR)\arrows.sbr"
	-@erase "$(INTDIR)\base64.obj"
	-@erase "$(INTDIR)\base64.sbr"
	-@erase "$(INTDIR)\bezier.obj"
	-@erase "$(INTDIR)\bezier.sbr"
	-@erase "$(INTDIR)\colxlate.obj"
	-@erase "$(INTDIR)\colxlate.sbr"
	-@erase "$(INTDIR)\CP_1251_CYRILLIC.obj"
	-@erase "$(INTDIR)\CP_1251_CYRILLIC.sbr"
	-@erase "$(INTDIR)\CP_932_ShiftJIS.obj"
	-@erase "$(INTDIR)\CP_932_ShiftJIS.sbr"
	-@erase "$(INTDIR)\CP_936_GB.obj"
	-@erase "$(INTDIR)\CP_936_GB.sbr"
	-@erase "$(INTDIR)\CP_949_Koreal.obj"
	-@erase "$(INTDIR)\CP_949_Koreal.sbr"
	-@erase "$(INTDIR)\CP_950_Big5.obj"
	-@erase "$(INTDIR)\CP_950_Big5.sbr"
	-@erase "$(INTDIR)\emit.obj"
	-@erase "$(INTDIR)\emit.sbr"
	-@erase "$(INTDIR)\fontmetrics.obj"
	-@erase "$(INTDIR)\fontmetrics.sbr"
	-@erase "$(INTDIR)\gdgen.obj"
	-@erase "$(INTDIR)\gdgen.sbr"
	-@erase "$(INTDIR)\globals.obj"
	-@erase "$(INTDIR)\globals.sbr"
	-@erase "$(INTDIR)\input.obj"
	-@erase "$(INTDIR)\input.sbr"
	-@erase "$(INTDIR)\mapgen.obj"
	-@erase "$(INTDIR)\mapgen.sbr"
	-@erase "$(INTDIR)\output.obj"
	-@erase "$(INTDIR)\output.sbr"
	-@erase "$(INTDIR)\postproc.obj"
	-@erase "$(INTDIR)\postproc.sbr"
	-@erase "$(INTDIR)\psgen.obj"
	-@erase "$(INTDIR)\psgen.sbr"
	-@erase "$(INTDIR)\shapes.obj"
	-@erase "$(INTDIR)\shapes.sbr"
	-@erase "$(INTDIR)\strcasecmp.obj"
	-@erase "$(INTDIR)\strcasecmp.sbr"
	-@erase "$(INTDIR)\strncasecmp.obj"
	-@erase "$(INTDIR)\strncasecmp.sbr"
	-@erase "$(INTDIR)\svggen.obj"
	-@erase "$(INTDIR)\svggen.sbr"
	-@erase "$(INTDIR)\utils.obj"
	-@erase "$(INTDIR)\utils.sbr"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(INTDIR)\vc60.pdb"
	-@erase "$(INTDIR)\wmfgen.obj"
	-@erase "$(INTDIR)\wmfgen.sbr"
	-@erase "$(INTDIR)\xbuf.obj"
	-@erase "$(INTDIR)\xbuf.sbr"
	-@erase "$(OUTDIR)\libdotneato.bsc"
	-@erase "..\..\lib\Debug\libdotneato.lib"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP_PROJ=/nologo /MTd /W3 /Gm /ZI /Od /I ".." /I "..\.." /I "..\..\graph" /I "..\..\cdt" /I "..\..\pathplan" /I "..\..\tools\src" /I "..\..\gd" /I "..\..\..\zlib" /I "..\..\tools\error" /I "D:\Project\graphviz-1.8.10\tools\os" /D "_DEBUG" /D "WIN32" /D "_MBCS" /D "_LIB" /D "HAVE_CONFIG_H" /D "MSWIN32" /FR"$(INTDIR)\\" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /GZ /c 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\libdotneato.bsc" 
BSC32_SBRS= \
	"$(INTDIR)\arrows.sbr" \
	"$(INTDIR)\base64.sbr" \
	"$(INTDIR)\bezier.sbr" \
	"$(INTDIR)\colxlate.sbr" \
	"$(INTDIR)\CP_932_ShiftJIS.sbr" \
	"$(INTDIR)\CP_936_GB.sbr" \
	"$(INTDIR)\CP_949_Koreal.sbr" \
	"$(INTDIR)\CP_950_Big5.sbr" \
	"$(INTDIR)\emit.sbr" \
	"$(INTDIR)\fontmetrics.sbr" \
	"$(INTDIR)\gdgen.sbr" \
	"$(INTDIR)\globals.sbr" \
	"$(INTDIR)\input.sbr" \
	"$(INTDIR)\mapgen.sbr" \
	"$(INTDIR)\output.sbr" \
	"$(INTDIR)\postproc.sbr" \
	"$(INTDIR)\psgen.sbr" \
	"$(INTDIR)\shapes.sbr" \
	"$(INTDIR)\strcasecmp.sbr" \
	"$(INTDIR)\strncasecmp.sbr" \
	"$(INTDIR)\svggen.sbr" \
	"$(INTDIR)\utils.sbr" \
	"$(INTDIR)\wmfgen.sbr" \
	"$(INTDIR)\xbuf.sbr" \
	"$(INTDIR)\CP_1251_CYRILLIC.sbr"

"$(OUTDIR)\libdotneato.bsc" : "$(OUTDIR)" $(BSC32_SBRS)
    $(BSC32) @<<
  $(BSC32_FLAGS) $(BSC32_SBRS)
<<

LIB32=link.exe -lib
LIB32_FLAGS=/nologo /out:"..\..\lib\Debug\libdotneato.lib" 
LIB32_OBJS= \
	"$(INTDIR)\arrows.obj" \
	"$(INTDIR)\base64.obj" \
	"$(INTDIR)\bezier.obj" \
	"$(INTDIR)\colxlate.obj" \
	"$(INTDIR)\CP_932_ShiftJIS.obj" \
	"$(INTDIR)\CP_936_GB.obj" \
	"$(INTDIR)\CP_949_Koreal.obj" \
	"$(INTDIR)\CP_950_Big5.obj" \
	"$(INTDIR)\emit.obj" \
	"$(INTDIR)\fontmetrics.obj" \
	"$(INTDIR)\gdgen.obj" \
	"$(INTDIR)\globals.obj" \
	"$(INTDIR)\input.obj" \
	"$(INTDIR)\mapgen.obj" \
	"$(INTDIR)\output.obj" \
	"$(INTDIR)\postproc.obj" \
	"$(INTDIR)\psgen.obj" \
	"$(INTDIR)\shapes.obj" \
	"$(INTDIR)\strcasecmp.obj" \
	"$(INTDIR)\strncasecmp.obj" \
	"$(INTDIR)\svggen.obj" \
	"$(INTDIR)\utils.obj" \
	"$(INTDIR)\wmfgen.obj" \
	"$(INTDIR)\xbuf.obj" \
	"$(INTDIR)\CP_1251_CYRILLIC.obj"

"..\..\lib\Debug\libdotneato.lib" : "$(OUTDIR)" $(DEF_FILE) $(LIB32_OBJS)
    $(LIB32) @<<
  $(LIB32_FLAGS) $(DEF_FLAGS) $(LIB32_OBJS)
<<

!ENDIF 

.c{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<


!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("libdotneato.dep")
!INCLUDE "libdotneato.dep"
!ELSE 
!MESSAGE Warning: cannot find "libdotneato.dep"
!ENDIF 
!ENDIF 


!IF "$(CFG)" == "libdotneato - Win32 Release" || "$(CFG)" == "libdotneato - Win32 Debug"
SOURCE=.\arrows.c

"$(INTDIR)\arrows.obj"	"$(INTDIR)\arrows.sbr" : $(SOURCE) "$(INTDIR)"


SOURCE=.\base64.c

"$(INTDIR)\base64.obj"	"$(INTDIR)\base64.sbr" : $(SOURCE) "$(INTDIR)"


SOURCE=.\bezier.c

"$(INTDIR)\bezier.obj"	"$(INTDIR)\bezier.sbr" : $(SOURCE) "$(INTDIR)"


SOURCE=.\colxlate.c

"$(INTDIR)\colxlate.obj"	"$(INTDIR)\colxlate.sbr" : $(SOURCE) "$(INTDIR)"


SOURCE=.\CP_1251_CYRILLIC.c

"$(INTDIR)\CP_1251_CYRILLIC.obj"	"$(INTDIR)\CP_1251_CYRILLIC.sbr" : $(SOURCE) "$(INTDIR)"


SOURCE=.\CP_932_ShiftJIS.c

"$(INTDIR)\CP_932_ShiftJIS.obj"	"$(INTDIR)\CP_932_ShiftJIS.sbr" : $(SOURCE) "$(INTDIR)"


SOURCE=.\CP_936_GB.c

"$(INTDIR)\CP_936_GB.obj"	"$(INTDIR)\CP_936_GB.sbr" : $(SOURCE) "$(INTDIR)"


SOURCE=.\CP_949_Koreal.c

"$(INTDIR)\CP_949_Koreal.obj"	"$(INTDIR)\CP_949_Koreal.sbr" : $(SOURCE) "$(INTDIR)"


SOURCE=.\CP_950_Big5.c

"$(INTDIR)\CP_950_Big5.obj"	"$(INTDIR)\CP_950_Big5.sbr" : $(SOURCE) "$(INTDIR)"


SOURCE=.\emit.c

"$(INTDIR)\emit.obj"	"$(INTDIR)\emit.sbr" : $(SOURCE) "$(INTDIR)"


SOURCE=.\fontmetrics.c

"$(INTDIR)\fontmetrics.obj"	"$(INTDIR)\fontmetrics.sbr" : $(SOURCE) "$(INTDIR)"


SOURCE=.\gdgen.c

"$(INTDIR)\gdgen.obj"	"$(INTDIR)\gdgen.sbr" : $(SOURCE) "$(INTDIR)"


SOURCE=.\globals.c

"$(INTDIR)\globals.obj"	"$(INTDIR)\globals.sbr" : $(SOURCE) "$(INTDIR)"


SOURCE=.\input.c

"$(INTDIR)\input.obj"	"$(INTDIR)\input.sbr" : $(SOURCE) "$(INTDIR)"


SOURCE=.\mapgen.c

"$(INTDIR)\mapgen.obj"	"$(INTDIR)\mapgen.sbr" : $(SOURCE) "$(INTDIR)"


SOURCE=.\output.c

"$(INTDIR)\output.obj"	"$(INTDIR)\output.sbr" : $(SOURCE) "$(INTDIR)"


SOURCE=.\postproc.c

"$(INTDIR)\postproc.obj"	"$(INTDIR)\postproc.sbr" : $(SOURCE) "$(INTDIR)"


SOURCE=.\psgen.c

"$(INTDIR)\psgen.obj"	"$(INTDIR)\psgen.sbr" : $(SOURCE) "$(INTDIR)"


SOURCE=.\shapes.c

"$(INTDIR)\shapes.obj"	"$(INTDIR)\shapes.sbr" : $(SOURCE) "$(INTDIR)"


SOURCE=.\strcasecmp.c

"$(INTDIR)\strcasecmp.obj"	"$(INTDIR)\strcasecmp.sbr" : $(SOURCE) "$(INTDIR)"


SOURCE=.\strncasecmp.c

"$(INTDIR)\strncasecmp.obj"	"$(INTDIR)\strncasecmp.sbr" : $(SOURCE) "$(INTDIR)"


SOURCE=.\svggen.c

"$(INTDIR)\svggen.obj"	"$(INTDIR)\svggen.sbr" : $(SOURCE) "$(INTDIR)"


SOURCE=.\utils.c

"$(INTDIR)\utils.obj"	"$(INTDIR)\utils.sbr" : $(SOURCE) "$(INTDIR)"


SOURCE=.\wmfgen.c

"$(INTDIR)\wmfgen.obj"	"$(INTDIR)\wmfgen.sbr" : $(SOURCE) "$(INTDIR)"


SOURCE=.\xbuf.c

"$(INTDIR)\xbuf.obj"	"$(INTDIR)\xbuf.sbr" : $(SOURCE) "$(INTDIR)"



!ENDIF 


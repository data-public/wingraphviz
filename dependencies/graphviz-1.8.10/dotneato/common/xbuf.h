/*
    This software may only be used by you under license from AT&T Corp.
    ("AT&T").  A copy of AT&T's Source Code Agreement is available at
    AT&T's Internet website having the URL:
    <http://www.research.att.com/sw/tools/graphviz/license/source.html>
    If you received this software without first entering into a license
    with AT&T, you have an infringing copy of this software and cannot use
    it without violating AT&T's intellectual property rights.
*/
#ifndef         XBUF_H
#define         XBUF_H

/* Extensible buffer:
 *  Malloc'ed memory is never released until xbfree is called.
 */

typedef struct {
    unsigned char*  buf;   /* start of buffer */
    unsigned char*  ptr;   /* next place to write */
    unsigned char*  eptr;  /* end of buffer */
    int             dyna;  /* true if buffer is malloc'ed */
}

xbuf;

/* xbinit:
 * Initializes new xbuf; caller provides memory.
 * Assume if init is non-null, hint = sizeof(init[])
 */
extern void xbinit (xbuf* xb, unsigned int hint, unsigned char* init);

/* xbput:
 * Append string s into xb
 */
extern int xbput (xbuf* xb, char* s);

/* xbfree:
 * Free any malloced resources.
 */
extern void xbfree (xbuf* xb);

/* xbpop:
 * Removes last character added, if any.
 */
extern int xbpop (xbuf* xb);

/* xbmore:
 * Expand buffer to hold at least ssz more bytes.
 */
extern int xbmore (xbuf* xb, int unsigned ssz);

/* xbputc:
 * Add character to buffer.
 *  int xbputc(xbuf*, char)
 */
#define xbputc(X,C) ((((X)->ptr >= (X)->eptr) ? xbmore(X,1) : 0), \
(int)(*(X)->ptr++ = ((unsigned char)C)))

/* xbuse:
 * Null-terminates buffer; resets and returns pointer to data;
 *  char* xbuse(xbuf* xb)
 */
#define xbuse(X) (xbputc(X,'\0'),(char*)((X)->ptr = (X)->buf))

/* xbnext:
 * Next position for writing.
 *  char* xbnext(xbuf* xb)
 */
#define xbnext(X) ((char*)((X)->ptr))

#endif

#include "CP_932_ShiftJIS.h"
#include <string.h>
#if defined _MSC_VER
#define strcasecmp _stricmp
#endif

char *
get_fontname_CP_932(char *font) {
    char *fontlist;

    fontlist = font;

    if (strcmp(font,"�l�r �S�V�b�N")==0) {
        fontlist = "msgothic.ttc";

    } else if (strcmp(font,"�l�r �o�S�V�b�N")==0) {
        fontlist = "msgothic.ttc";

    } else if (strcmp(font,"�l�r UI �S�V�b�N")==0) {
        fontlist = "msgothic.ttc";

    } else if (strcmp(font,"�l�r �o����")==0) {
        fontlist = "msmincho.ttc";

    } else if (strcmp(font,"�l�r ����")==0) {
        fontlist = "msmincho.ttc";

    } else if (strcasecmp(font,"MS Gothic")==0) {
        fontlist = "msgothic.ttc";

    } else if (strcasecmp(font,"Gothic")==0) {
        fontlist = "msgothic.ttc";

    } else if (strcasecmp(font,"Mincho")==0) {
        fontlist = "msmincho.ttc";

    } else if (strcmp(font,"Arial Unicode MS")==0) {
        fontlist = "ARIALUNI.ttf";
    }

    return fontlist;
}

int
get_fontindex_CP_932(char *font) {

    if (strcmp(font,"�l�r �S�V�b�N")==0) {
        return 0;

    } else if (strcmp(font,"�l�r �o�S�V�b�N")==0) {
        return 1;

    } else if (strcmp(font,"�l�r UI �S�V�b�N")==0) {
        return 2;

    } else if (strcmp(font,"�l�r �o����")==0) {
        return 1;

    } else if (strcmp(font,"�l�r ����")==0) {
        return 0;

    } else if (strcasecmp(font,"MS Gothic")==0) {
        return 0;

    } else if (strcasecmp(font,"Gothic")==0) {
        return 0;

    } else if (strcasecmp(font,"Mincho")==0) {
        return 0;

    } else {

        return 0;
    }
}

char *
get_fontfam_CP_932(char *font) {
    char *fontlist;

    fontlist = font;

    if (strcmp(font,"�l�r �S�V�b�N")==0) {
        fontlist = "msgothic";

    } else if (strcmp(font,"�l�r �o�S�V�b�N")==0) {
        fontlist = "msgothic";

    } else if (strcmp(font,"�l�r �o����")==0) {
        fontlist = "msmincho";

    } else if (strcmp(font,"�l�r ����")==0) {
        fontlist = "msmincho";

    } else if (strcasecmp(font,"MS Gothic")==0) {
        fontlist = "msgothic";

    } else if (strcasecmp(font,"Gothic")==0) {
        fontlist = "msgothic";

    } else if (strcasecmp(font,"Mincho")==0) {
        fontlist = "msmincho";
    }

    return fontlist;
}

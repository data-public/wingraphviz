/*
    This software may only be used by you under license from AT&T Corp.
    ("AT&T").  A copy of AT&T's Source Code Agreement is available at
    AT&T's Internet website having the URL:
    <http://www.research.att.com/sw/tools/graphviz/license/source.html>
    If you received this software without first entering into a license
    with AT&T, you have an infringing copy of this software and cannot use
    it without violating AT&T's intellectual property rights.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "xbuf.h"

#include "errorImp.h"

/* xbinit:
 * Assume if init is non-null, hint = sizeof(init[])
 */
void
xbinit (xbuf* xb, unsigned int hint, unsigned char* init) {
    if (init) {
        xb->buf = init;
        xb->dyna = 0;

    } else {
        if (hint == 0)
            hint = BUFSIZ;

        xb->dyna = 1;

        xb->buf = (unsigned char*)malloc (hint);
    }

    xb->eptr = xb->buf + hint;
    xb->ptr = xb->buf;
    *xb->ptr = '\0';
}

/* xbmore;
 * Expand buffer to hold at least ssz more bytes.
 */
int
xbmore (xbuf* xb, unsigned int ssz) {
    int    cnt;         /* current no. of characters in buffer */
    int    size;        /* current buffer size */
    int    nsize;       /* new buffer size */
    unsigned char*  nbuf;        /* new buffer */

    size = xb->eptr - xb->buf;
    nsize = 2*size;

    if (size+ssz > nsize)
        nsize = size+ssz;

    cnt = xb->ptr - xb->buf;

    if (xb->dyna) {
        nbuf = (unsigned char*)realloc(xb->buf,nsize);

        if (!nbuf) {
            errorPrintf(stderr,"xbmore: out of memory\n");
            exit(1);
        }

    } else {
        nbuf = (unsigned char*)malloc(nsize);

        if (!nbuf) {
            errorPrintf(stderr,"xbmore: out of memory\n");
            exit(1);
        }

        memcpy (nbuf, xb->buf, cnt);
        xb->dyna = 1;
    }

    xb->buf = nbuf;
    xb->ptr = xb->buf + cnt;
    xb->eptr = xb->buf + nsize;
    return 0;
}

/* xbput:
 * Append string s into xb
 */
int
xbput (xbuf* xb, char* s) {
    unsigned int    ssz = strlen (s);

    if (xb->ptr + ssz > xb->eptr)
        xbmore (xb, ssz);

    memcpy (xb->ptr, s, ssz);

    xb->ptr += ssz;

    return ssz;
}

/* xbfree:
 * Free any malloced resources.
 */
void
xbfree (xbuf* xb) {
    if (xb->dyna)
        free (xb->buf);
}

/* xbpop:
 * Removes last character added, if any.
 */
int
xbpop (xbuf* xb) {
    int c;

    if (xb->ptr > xb->buf) {
        c = *xb->ptr--;
        return c;

    } else
        return -1;

}


/*
    This software may only be used by you under license from AT&T Corp.
    ("AT&T").  A copy of AT&T's Source Code Agreement is available at
    AT&T's Internet website having the URL:
    <http://www.research.att.com/sw/tools/graphviz/license/source.html>
    If you received this software without first entering into a license
    with AT&T, you have an infringing copy of this software and cannot use
    it without violating AT&T's intellectual property rights.
*/

#include		"render.h"
#include		"unicodeconst.h"
#include		"gd.h"
#include		"utils.h"
#include		<fcntl.h>
#ifdef MSWIN32
#include <io.h>
#include <stdio.h>
#include <stdlib.h>  
#endif

#ifdef DMALLOC
#include "dmalloc.h"
#endif
#include "errorImp.h"
#include "CP_932_ShiftJIS.h"
#include "CP_936_GB.h"
#include "CP_949_Koreal.h"
#include "CP_950_Big5.h"
#include "CP_1251_CYRILLIC.h"

extern unsigned int DOT_CODEPAGE;

#define SCALE (GD_RESOLUTION/72.0)

/* font modifiers */
#define REGULAR		0
#define BOLD		1
#define ITALIC		2

/* patterns */
#define P_SOLID		0
#define P_DOTTED	4
#define P_DASHED	11
#define P_NONE		15

/* bold line constant */
#define WIDTH_NORMAL	1
#define WIDTH_BOLD		3

typedef struct {
    unsigned char r, g, b;
}

Color;

static int	N_pages;
static point	Pages;
static double   Scale;
static pointf	Offset;
static double	ArgScale;
static int	Rot;
static box	PB;
static int	onetime = TRUE;

static gdImagePtr im;
static Dict_t	*ImageDict;

typedef struct context_t {
    unsigned char	pencolor_ix, fillcolor_ix;
    char	*fontfam, fontopt, font_was_set, pen, fill, penwidth;
    double   fontsz;
}

context_t;

#define MAXNEST	4
static context_t cstk[MAXNEST];
static int	SP;
static node_t	*Curnode;
extern pointf Bezierf (pointf *p, int n, double mu);

static void gd_reset(void) {
    onetime = TRUE;
}

static void init_gd(void) {
    int white, transparent, black;

    SP = 0;
    /* must create default background color first... */
    white = gdImageColorResolve(im, 255,255,255);
    /* we have to force the background to be filled for some reason */
    gdImageFilledRectangle(im, 0, 0, im->sx-1, im->sy-1, white);

    /* transparent uses an rgb value very close to white
       so that formats like GIF that don't support
       transparency show a white background */
    /*transparent = gdImageColorResolve(im, 255, 255, 254);*/
    transparent = gdImageColorResolveAlpha(im, 255, 255, 255, 127);

    gdImageColorTransparent(im, transparent);

    /* ...then any other colors that we need */
    black = gdImageColorResolve(im, 0, 0, 0);

    cstk[0].pencolor_ix = black;		/* set pen black*/
    cstk[0].fillcolor_ix = black;		/* set fill black*/
    cstk[0].fontfam = getDefaultFont();	/* font family name */
    cstk[0].fontopt = REGULAR;			/* modifier: REGULAR, BOLD or ITALIC */
    cstk[0].pen = P_SOLID;				/* pen pattern style, default is solid */
    cstk[0].fill = P_NONE;
    cstk[0].penwidth = WIDTH_NORMAL;

}

pointf gdpt(pointf p) {
    pointf		  rv;

    if (Rot == 0) {
        rv.x = PB.LL.x + p.x * Scale + Offset.x;
        rv.y = PB.UR.y - 1 - p.y * Scale - Offset.y;

    } else {
        rv.x = PB.UR.x - 1 - p.y * Scale - Offset.x;
        rv.y = PB.UR.y - 1 - p.x * Scale - Offset.y;
    }

    return rv;
}

void gd_font(context_t* cp) {
    /*
    	char		   *fw, *fa;
     
    	fw = fa = "Regular";
    	switch (cp->fontopt) {
    	case BOLD:
    		fw = "Bold";
    		break;
    	case ITALIC:
    		fa = "Italic";
    		break;
    	}
    */
}

static void gd_begin_job(FILE *ofp, graph_t *g, char **lib, char *user,
                         char *info[], point pages) {
    Pages = pages;
    N_pages = pages.x * pages.y;

}

static void gd_end_job(void) {
}

void gd_begin_graph(graph_t* g, box bb, point pb) {
    PB.LL.x = PB.LL.y = 0;
    PB.UR.x = (bb.UR.x - bb.LL.x + 2*g->u.drawing->margin.x) * SCALE;
    PB.UR.y = (bb.UR.y - bb.LL.y + 2*g->u.drawing->margin.y) * SCALE;
    Offset.x = g->u.drawing->margin.x * SCALE;
    Offset.y = g->u.drawing->margin.y * SCALE;
}

static void gd_begin_graph_to_file(graph_t* g, box bb, point pb) {
    gd_begin_graph(g, bb, pb);


    if (mapbool(agget(g,"truecolor")))
        im = gdImageCreateTrueColor((PB.UR.x - PB.LL.x - 1), (PB.UR.y - PB.LL.y - 1));
    else
        im = gdImageCreate((PB.UR.x - PB.LL.x - 1), (PB.UR.y - PB.LL.y - 1));

    if (onetime) {
        init_gd();
        onetime = FALSE;
    }

}

static void gd_begin_graph_to_memory(graph_t* g, box bb, point pb) {
    gd_begin_graph(g, bb, pb);


    im = *(gdImagePtr *)Output_file;

    if (onetime) {
        init_gd();
        onetime = FALSE;
    }

}

//#define DEBUG_DATA
#if defined DEBUG_DATA
#define __INT_LEN 14
static const char* _dump_array_int(int* v, int len) {
	static char* s_str = 0;
	static size_t s_len = 0;
	size_t _len = (len * __INT_LEN) + 8;
	if (_len > s_len) {
		if (s_str)
			free(s_str);
		s_str = malloc(_len);
		s_len = _len;
	}
	char s[__INT_LEN + 1];
	char* p = strcpy(s_str, "[");
	for (int i = 0; i < len; ++i) {
		int r = _snprintf(s, __INT_LEN, "%s %d", (i ? "," : ""), v[i]);
		s[r] = 0;
		p = strcat(p, s) + r;
	}
	strcat(p, " ]");
	return s_str;
}

static void _dump_px(FILE* fp, unsigned char **px, int x, int y)
{
	if (!px) {
		fprintf(fp, "\t{null}\n");
		return;
	}
	fprintf(fp, "\t{\n");
	for (int i = 0; i < x; ++i) {
		fprintf(fp, "\t\t[");
		for (int j = 0; j < x; ++j) {
			fprintf(fp, "%s%x", j ? ", " : " ", px[i][j]);
		}
		fprintf(fp, "]\n");
	}
	fprintf(fp, "\t}\n");
}

static void _dump_img(gdImagePtr im) {
	FILE* fp = fopen("image_dump.txt", "wb");
	if (!fp)
		return;
	fprintf(fp, "struct gdImageStruct {\n\t/* Palette-based image pixels */\n");
	fprintf(fp, "\tunsigned char ** pixels : %p;\n", im->pixels);
	_dump_px(fp, im->pixels, im->sx, im->sy);
	fprintf(fp, "\tint sx : %d;\n\tint sy : %d;\n", im->sx, im->sy);
	fprintf(fp, "\t/* These are valid in palette images only. See also\n"
		"\t'alpha', which appears later in the structure to\n"
		"\tpreserve binary backwards compatibility * / \n"
		"\tint colorsTotal: %d; \n", im->colorsTotal);
	fprintf(fp, "\tint red[gdMaxColors] : %s; \n"
		"\tint green[gdMaxColors] : %s; \n"
		"\tint blue[gdMaxColors] : %s; \n"
		"\tint open[gdMaxColors] : %s; \n", _dump_array_int(im->red, gdMaxColors), 
		_dump_array_int(im->green, gdMaxColors), _dump_array_int(im->blue, gdMaxColors), 
		_dump_array_int(im->open, gdMaxColors));
	fprintf(fp, "\t/* For backwards compatibility, this is set to the\n"
		"\tfirst palette entry with 100% transparency,\n"
		"\tand is also set and reset by the\n"
		"\tgdImageColorTransparent function. Newer\n"
		"\tapplications can allocate palette entries\n"
		"\twith any desired level of transparency; however,\n"
		"\tbear in mind that many viewers, notably\n"
		"\tmany web browsers, fail to implement\n"
		"\tfull alpha channel for PNG and provide\n"
		"\tsupport for full opacity or transparency only. */\n");
	fprintf(fp, "\tint transparent: %d;\n"
		"\tint *polyInts: %p; \n"
		"\tint polyAllocated: %d; \n", im->transparent, im->polyInts, im->polyAllocated);

	fprintf(fp, "\tstruct gdImageStruct *brush: %p;\n"
		"\tstruct gdImageStruct *tile: %p;\n", im->brush, im->tile);
	fprintf(fp, "\tint brushColorMap[gdMaxColors]: %s;\n"
		"\tint tileColorMap[gdMaxColors]: %s; \n", 
		_dump_array_int(im->brushColorMap, gdMaxColors), 
		_dump_array_int(im->tileColorMap, gdMaxColors));
	fprintf(fp, "\tint styleLength: %d;\n"
		"\tint stylePos: %d;\n"
		"\tint *style: %p;\n"
		"\tint interlace: %d;\n"
		"\t/* New in 2.0: thickness of line. Initialized to 1. */\n"
		"\tint thick: %d;\n"
		"\t/* New in 2.0: alpha channel for palettes. Note that only\n"
		"\tMacintosh Internet Explorer and (possibly) Netscape 6\n"
		"\treally support multiple levels of transparency in\n"
		"\tpalettes, to my knowledge, as of 2/15/01. Most\n"
		"\tcommon browsers will display 100% opaque and\n"
		"\t100% transparent correctly, and do something\n"
		"\tunpredictable and/or undesirable for levels\n"
		"\tin between. TBB */\n", im->styleLength, im->stylePos, im->style, im->interlace, im->thick);
	fprintf(fp, "\tint alpha[gdMaxColors]: %s;\n", _dump_array_int(im->alpha, gdMaxColors));
	fprintf(fp, "\t/* Truecolor flag and pixels. New 2.0 fields appear here at the\n"
		"\tend to minimize breakage of existing object code. * / \n"
		"\tint trueColor: %d; \n"
		"\tint ** tpixels: %p; \n"
		"\t/* Should alpha channel be copied, or applied, each time a\n"
		"\tpixel is drawn? This applies to truecolor images only.\n"
		"\tNo attempt is made to alpha-blend in palette images,\n"
		"\teven if semitransparent palette entries exist.\n"
		"\tTo do that, build your image as a truecolor image,\n"
		"\tthen quantize down to 8 bits. */\n"
		"\tint alphaBlendingFlag: %d; \n"
		"\t/* Should the alpha channel of the image be saved? This affects\n"
		"\tPNG at the moment; other future formats may also\n"
		"\thave that capability. JPEG doesn't. */\n"
		"\tint saveAlphaFlag: %d; \n", im->trueColor, im->tpixels, im->alphaBlendingFlag, im->saveAlphaFlag);

	fclose(fp);

	fp = fopen("image_dump.jpeg", "wb");
	if (fp) {
		gdImageJpeg(im, fp, -1);
		fclose(fp);
	}
}
#endif

static void gd_end_graph_to_file(void) {
    /*
     * Windows will do \n -> \r\n  translations on stdout unless told otherwise.
     */
#ifdef HAVE_SETMODE
#ifdef O_BINARY
    setmode(fileno(Output_file), O_BINARY);
#endif
#endif

#if defined DEBUG_DATA
	_dump_img(im);
#endif

    /*
     * Write IM to OUTFILE as a JFIF-formatted JPEG image, using quality
     * JPEG_QUALITY.  If JPEG_QUALITY is in the range 0-100, increasing values
     * represent higher quality but also larger image size.  If JPEG_QUALITY is
     * negative, the IJG JPEG library's default quality is used (which
     * should be near optimal for many applications).  See the IJG JPEG
     * library documentation for more details.  */

#define JPEG_QUALITY -1

    if (Output_lang == GIF) {

        gdImageGif(im, Output_file)

        ;
#ifdef HAVE_LIBPNG
#ifdef HAVE_LIBZ

    } else if (Output_lang == PNG) {
        gdImagePng(im, Output_file);
#endif
#endif
#ifdef HAVE_LIBJPEG

    } else if (Output_lang == JPEG) {
        gdImageJpeg(im, Output_file, JPEG_QUALITY);
#endif

    } else if (Output_lang == GD) {
        gdImageGd(im, Output_file);
#ifdef HAVE_LIBZ

    } else if (Output_lang == GD2) {
#define GD2_CHUNKSIZE 128
#define GD2_RAW 1
#define GD2_COMPRESSED 2
        //        gdImageGd2(im, Output_file, GD2_CHUNKSIZE, GD2_COMPRESSED);
#endif

    } else if (Output_lang == WBMP) {
        /* Use black for the foreground color for the B&W wbmp image. */
        int foreground = gdImageColorResolve(im, 0, 0, 0);
        gdImageWBMP(im, foreground, Output_file);
#ifdef HAVE_LIBXPM

    } else if (Output_lang == XBM) {
        gdImageXbm(im, Output_file);
#endif

    }

    if (ImageDict) {
        dtclose(ImageDict);
        ImageDict = 0;
    }

    gdImageDestroy(im);
}

static void gd_end_graph_to_memory(void) {
    /* leave image in memory to be handled by Gdtclft output routines */
}

void
gd_begin_page(graph_t *g, point page, double scale, int rot, point offset) {
    int		page_number;
    point		sz;

    ArgScale = scale;
    Scale = scale * SCALE;
    Rot = rot;

    page_number = page.x + page.y * Pages.x + 1;
    sz = sub_points(PB.UR, PB.LL);

}

void gd_end_page(void) {
}

static void gd_begin_cluster(graph_t* g) { }

static void gd_end_cluster(void) { }

static void gd_begin_nodes(void) { }

static void gd_end_nodes(void) { }

static void gd_begin_edges(void) { }

static void gd_end_edges(void) { }

static void gd_begin_node(node_t* n) {
    Curnode = n;
}

static void gd_end_node(void) { }

static  void gd_begin_edge(edge_t* e) { }

static  void gd_end_edge(void) { }

static  void
gd_begin_context(void) {
    assert(SP + 1 < MAXNEST);
    cstk[SP + 1] = cstk[SP];
    SP++;
}

static  void
gd_end_context(void) {
    int			 psp = SP - 1;
    assert(SP > 0);

    if (cstk[SP].font_was_set)
        gd_font(&(cstk[psp]));

    SP = psp;
}

static  void
gd_set_font(char* name, double size) {
    char		   *p, *q;
    context_t	  *cp;

    cp = &(cstk[SP]);
    cp->font_was_set = TRUE;
    cp->fontsz = ArgScale * size;
    p = _strdup(name);

    if ((q = strchr(p, '-'))) {
        *q++ = 0;

        if (strcasecmp(q, "italic") == 0)
            cp->fontopt = ITALIC;
        else if (strcasecmp(q, "bold") == 0)
            cp->fontopt = BOLD;
    }

    cp->fontfam = p;
    gd_font(&cstk[SP]);
}

static unsigned char gd_resolve_color(char* name) {
    color_t	color;

    if (!(strcmp(name,"transparent"))) {
        /* special case for "transparent" color */
        return gdImageGetTransparent(im);

    } else {
        colorxlate(name,&color,RGBA_BYTE);
        return gdImageColorResolve(im,color.u.rgba[0],color.u.rgba[1],color.u.rgba[2]);
    }
}

static void gd_set_pencolor(char* name) {
    cstk[SP].pencolor_ix = gd_resolve_color(name);
}

static void gd_set_fillcolor(char* name) {
    cstk[SP].fillcolor_ix = gd_resolve_color(name);
}

static  void
gd_set_style(char** s) {
    char		*line,*p;
    context_t	*cp;

    cp = &(cstk[SP]);

    while ((p = line = *s++)) {
        if (streq(line, "solid"))
            cp->pen = P_SOLID;
        else if (streq(line, "dashed"))
            cp->pen = P_DASHED;
        else if (streq(line, "dotted"))
            cp->pen = P_DOTTED;
        else if (streq(line, "invis"))
            cp->pen = P_NONE;
        else if (streq(line, "bold"))
            cp->penwidth = WIDTH_BOLD;
        else if (streq(line, "setlinewidth")) {
            while (*p)
                p++;

            p++;

            cp->penwidth = atol(p);

        } else if (streq(line, "filled"))
            cp->fill = P_SOLID;
        else if (streq(line, "unfilled"))
            cp->fill = P_NONE;
        else
            errorPrintf(stderr,
                        "gd_set_style: unsupported style %s - ignoring\n",
                        line);

    }
}

/* sometimes fonts are stored under a different name */
char *
gd_alternate_fontlist(char *font) {
    char *fontlist;

    fontlist = font;

    if(DOT_CODEPAGE == CP_TRADITIONAL_CHINESE) {
        char *tmpname;
        tmpname =0;

        tmpname = get_fontname_CP_950(font);

        if(tmpname)
            return tmpname;
    }

    if(DOT_CODEPAGE == CP_SIMPLIFIED_CHINESE) {
        char *tmpname;
        tmpname =0;

        tmpname = get_fontname_CP_936(font);

        if(tmpname)
            return tmpname;
    }

    if(DOT_CODEPAGE == CP_JAPANESE_SHIFT_JIS) {
        char *tmpname;
        tmpname =0;

        tmpname = get_fontname_CP_932(font);

        if(tmpname)
            return tmpname;
    }

    if(DOT_CODEPAGE == CP_KOREAN) {
        char *tmpname;
        tmpname =0;

        tmpname = get_fontname_CP_949(font);

        if(tmpname)
            return tmpname;
    }

    if (strcasecmp(font,"Times-Roman")==0) {
        fontlist = "Times";

    } else if (strcasecmp(font,"Times-New-Roman")==0) {
        fontlist = "Times";

    } else if (strcasecmp(font,"Times_New_Roman")==0) {
        fontlist = "times";

    } else if (strcasecmp(font,"TimesNewRoman")==0) {
        fontlist = "times";

    } else if (strcasecmp(font,"Times")==0) {
        fontlist = "times";

    } else if (strcasecmp(font,"Times New Roman")==0) {
        fontlist = "times";

    } else if (strcasecmp(font,"Times New Roman Bold")==0) {
        fontlist = "timesbd";

    } else if (strcasecmp(font,"Times New Roman Bold Italic")==0) {
        fontlist = "timesbi";

    } else if (strcasecmp(font,"Times New Roman Italic")==0) {
        fontlist = "timesi";

    } else if (strcasecmp(font,"Helvetica")==0) {
        fontlist = "Helvetica arial";

    } else if (strcasecmp(font,"Arial")==0) {
        fontlist = "ARIAL";

    } else if (strcasecmp(font,"Arial Black")==0) {
        fontlist = "ARIBLK";

    } else if (strcasecmp(font,"Arial Black Italic")==0) {
        fontlist = "ARBLI___";

    } else if (strcasecmp(font,"Arial Bold")==0) {
        fontlist = "ARIALBD";

    } else if (strcasecmp(font,"Arial Bold Italic")==0) {
        fontlist = "ARIALBI";

    } else if (strcasecmp(font,"Arial Italic")==0) {
        fontlist = "ARIALI";

    } else if (strcasecmp(font,"Arial Narrow")==0) {
        fontlist = "ARIALN";

    } else if (strcasecmp(font,"Arial Narrow Bold")==0) {
        fontlist = "ARIALNB";

    } else if (strcasecmp(font,"Arial Narrow Bold Italic")==0) {
        fontlist = "ARIALNBI";

    } else if (strcasecmp(font,"Arial Unicode MS")==0) {
        fontlist = "ARIALUNI";

    } else if (strcasecmp(font,"Batang")==0) {
        fontlist = "batang.ttc";

    } else if (strcasecmp(font,"Book Antiqua")==0) {
        fontlist = "BKANT";

    } else if (strcasecmp(font,"Book Antiqua Bold")==0) {
        fontlist = "ANTQUAB";

    } else if (strcasecmp(font,"Book Antiqua Bold Italic")==0) {
        fontlist = "ANTQUABI";

    } else if (strcasecmp(font,"Book Antiqua Italic")==0) {
        fontlist = "ANTQUAI";

    } else if (strcasecmp(font,"Bookman Old Style")==0) {
        fontlist = "BOOKOS";

    } else if (strcasecmp(font,"Bookman Old Style Bold")==0) {
        fontlist = "BOOKOSB";

    } else if (strcasecmp(font,"Bookman Old Style Bold Italic")==0) {
        fontlist = "BOOKOSBI";

    } else if (strcasecmp(font,"Bookman Old Style Italic")==0) {
        fontlist = "BOOKOSI";

    } else if (strcasecmp(font,"Century")==0) {
        fontlist = "CENTURY";

    } else if (strcasecmp(font,"Century Gothic")==0) {
        fontlist = "GOTHIC";

    } else if (strcasecmp(font,"Century Gothic Bold")==0) {
        fontlist = "GOTHICB";

    } else if (strcasecmp(font,"Century Gothic Bold Italic")==0) {
        fontlist = "GOTHICBI";

    } else if (strcasecmp(font,"Century Gothic Italic")==0) {
        fontlist = "GOTHICI";

    } else if (strcasecmp(font,"Comic Sans MS")==0) {
        fontlist = "comic";

    } else if (strcasecmp(font,"Comic Sans MS Bold")==0) {
        fontlist = "comicbd";

    } else if (strcasecmp(font,"Courier")==0) {
        fontlist = "cour"; //Courier

    } else if (strcasecmp(font,"Courier-New")==0) {
        fontlist = "cour"; //Courier_New

    } else if (strcasecmp(font,"Courier_New")==0) {
        fontlist = "cour"; //Courier-New

    } else if (strcasecmp(font,"Courier New")==0) {
        fontlist = "cour";

    } else if (strcasecmp(font,"Courier New Bold")==0) {
        fontlist = "courbd";

    } else if (strcasecmp(font,"Courier New Bold Italic")==0) {
        fontlist = "courbi";

    } else if (strcasecmp(font,"Courier New Italic")==0) {
        fontlist = "couri";

    } else if (strcasecmp(font,"Franklin Gothic Medium")==0) {
        fontlist = "framd";

    } else if (strcasecmp(font,"Franklin Gothic Medium Italic")==0) {
        fontlist = "framdit";

    } else if (strcasecmp(font,"Garamond")==0) {
        fontlist = "GARA";

    } else if (strcasecmp(font,"Garamond Bold")==0) {
        fontlist = "GARABD";

    } else if (strcasecmp(font,"Garamond Italic")==0) {
        fontlist = "GARAIT";

    } else if (strcasecmp(font,"Georgia")==0) {
        fontlist = "georgia";

    } else if (strcasecmp(font,"Georgia Bold")==0) {
        fontlist = "georgiab";

    } else if (strcasecmp(font,"Georgia Bold Italic")==0) {
        fontlist = "georgiaz";

    } else if (strcasecmp(font,"Georgia Italic")==0) {
        fontlist = "georgiai";

    } else if (strcasecmp(font,"Impact")==0) {
        fontlist = "impact";

    } else if (strcasecmp(font,"Lucida Blackletter")==0) {
        fontlist = "LBLACK";

    } else if (strcasecmp(font,"Lucida Bright")==0) {
        fontlist = "LBRITE";

    } else if (strcasecmp(font,"Lucida Bright Demibold")==0) {
        fontlist = "LBRITED";

    } else if (strcasecmp(font,"Lucida Bright Demibold Italic")==0) {
        fontlist = "LBRITEDI";

    } else if (strcasecmp(font,"Lucida Console")==0) {
        fontlist = "lucon";

    } else if (strcasecmp(font,"Lucida Sans Unicode")==0) {
        fontlist = "l_10646";

    } else if (strcasecmp(font,"Microsoft Sans Serif")==0) {
        fontlist = "micross";

    } else if (strcasecmp(font,"Palatino Linotype")==0) {
        fontlist = "pala";

    } else if (strcasecmp(font,"Palatino Linotype Bold")==0) {
        fontlist = "palab";

    } else if (strcasecmp(font,"Palatino Linotype Bold Italic")==0) {
        fontlist = "palabi";

    } else if (strcasecmp(font,"Palatino Linotype Italic")==0) {
        fontlist = "palai";

    } else if (strcasecmp(font,"Paprus")==0) {
        fontlist = "PAPYRUS";

    } else if (strcasecmp(font,"Poor Richard")==0) {
        fontlist = "POORICH";

    } else if (strcasecmp(font,"Sylfaen")==0) {
        fontlist = "sylfaen";

    } else if (strcasecmp(font,"Tahoma")==0) {
        fontlist = "tahoma";

    } else if (strcasecmp(font,"Tahoma Bold")==0) {
        fontlist = "tahomabd";

    } else if (strcasecmp(font,"Trebuchet MS")==0) {
        fontlist = "trebuc";

    } else if (strcasecmp(font,"Trebuchet MS Bold")==0) {
        fontlist = "trebucbd";

    } else if (strcasecmp(font,"Trebuchet MS Bold Italic")==0) {
        fontlist = "trebucbi";

    } else if (strcasecmp(font,"Trebuchet MS Italic")==0) {
        fontlist = "trebucit";

    } else if (strcasecmp(font,"Verdana")==0) {
        fontlist = "verdana";

    } else if (strcasecmp(font,"Verdana Bold")==0) {
        fontlist = "verdanab";

    } else if (strcasecmp(font,"Verdana Bold Italic")==0) {
        fontlist = "verdanaz";

    } else if (strcasecmp(font,"Verdana Italic")==0) {
        fontlist = "verdanai";

    } else if (strcasecmp(font,"Viner Hand Itc")==0) {
        fontlist = "VINERITC";
    }

    return fontlist;
}

void gd_missingfont(char *err, char *fontreq) {
    static char     *lastmissing = 0;
    static int      n_errors = 0;
    char		*p;

    if (n_errors >= 20)
        return;

    if ((lastmissing == 0) || (strcmp(lastmissing,fontreq))) {
        if (!(p=getenv("GDFONTPATH")))
            p = DEFAULT_FONTPATH;

        errorPrintf(stderr, "%s : %s in %s\n",err,fontreq,p);

        if (lastmissing)
            free(lastmissing);

        lastmissing = _strdup(fontreq);

        n_errors++;

        if (n_errors >= 20)
            errorPrintf(stderr,"(font errors suppressed)\n");
    }
}


static  void
gd_textline(point p, textline_t *line) {
    char		*fontlist, *err;
    pointf		mp;
    int		brect[8];
    char		*str = line->str;
    double		fontsz = cstk[SP].fontsz;

    if (cstk[SP].pen == P_NONE)
        return;

    fontlist = gd_alternate_fontlist(cstk[SP].fontfam);

    switch(line->just) {

            case 'l':
            mp.x = p.x;
            break;

            case 'r':
            mp.x = p.x - line->width;
            break;

            default:

            case 'n':
            mp.x = p.x - line->width / 2;
            break;
    }

    mp.y = p.y;

    mp = gdpt(mp);

    //Get from freetype
    err = gdImageStringFT2(im, brect, cstk[SP].pencolor_ix, fontlist,cstk[SP].fontfam,
                           fontsz, (Rot? 90.0 : 0.0) * PI / 180.0,
                           ROUND(mp.x), ROUND(mp.y), str);

    if(err) {
        err = gdImageStringFT2(im, brect, cstk[SP].pencolor_ix, "times.ttf","Times",
                               fontsz, (Rot? 90.0 : 0.0) * PI / 180.0,
                               ROUND(mp.x), ROUND(mp.y), str);

    }

    /*
       //Get from build in matrix
       if (err) {
           // revert to builtin fonts
           gd_missingfont (err, cstk[SP].fontfam);

           if (fontsz <= 8.5) {
               gdImageString(im, gdFontTiny,
                             ROUND(mp.x), ROUND(mp.y-9.),
                             (unsigned char *)str, cstk[SP].pencolor_ix);

           } else if (fontsz <= 9.5) {
               gdImageString(im, gdFontSmall,
                             ROUND(mp.x), ROUND(mp.y-12.),
                             (unsigned char *)str, cstk[SP].pencolor_ix);

           } else if (fontsz <= 10.5) {
               gdImageString(im, gdFontMediumBold,
                             ROUND(mp.x), ROUND(mp.y-13.),
                             (unsigned char *)str, cstk[SP].pencolor_ix);

           } else if (fontsz <= 11.5) {
               gdImageString(im, gdFontLarge,
                             ROUND(mp.x), ROUND(mp.y-14.),
                             (unsigned char *)str, cstk[SP].pencolor_ix);

           } else {
               gdImageString(im, gdFontGiant,
                             ROUND(mp.x), ROUND(mp.y-15.),
                             (unsigned char *)str, cstk[SP].pencolor_ix);
           }
       }
    */

}

point gd_textsize(char *str, char *fontname, double fontsz) {
    char		*fontlist,*err;
    point		rv;
    int		brect[8];

    fontlist = gd_alternate_fontlist(fontname);

    rv.x = rv.y = 0.0;

    if (fontlist && *str) {
        /* call gdImageStringFT with null *im to get brect */
        err = gdImageStringFT2(NULL, brect, -1, fontlist,fontname,
                               fontsz, 0, 0, 0, str);

        if (!err) {
            rv.x = (brect[4] - brect[0]);
            /*			rv.y = (brect[5] - brect[1]); */
            rv.y = (brect[5] - 0       ); /* ignore descenders */
            rv.x /= SCALE;
            rv.y /= SCALE;
        }
    }

    return rv;
}

static  void
gd_bezier(point* A, int n, int arrow_at_start, int arrow_at_end) {
    //pointf		p0, p1, V[4];
    pointf pFrom,pTo,pTmp;
    pointf * V;
    int		i, j, step;
    int		style[20];
    int		pen, width;
    int		div;
    gdImagePtr	brush = NULL;

    if (cstk[SP].pen != P_NONE) {
        if (cstk[SP].pen == P_DASHED) {
            for (i = 0; i < 10; i++)
                style[i] = cstk[SP].pencolor_ix;

            for (; i < 20; i++)
                style[i] = gdTransparent;

            gdImageSetStyle(im, style, 20);

            pen = gdStyled;

        } else if (cstk[SP].pen == P_DOTTED) {
            for (i = 0; i < 2; i++)
                style[i] = cstk[SP].pencolor_ix;

            for (; i < 12; i++)
                style[i] = gdTransparent;

            gdImageSetStyle(im, style, 12);

            pen = gdStyled;

        } else {
            pen = cstk[SP].pencolor_ix;
        }

        if (cstk[SP].penwidth != WIDTH_NORMAL) {
            width=cstk[SP].penwidth;
            brush = gdImageCreate(width,width);
            gdImagePaletteCopy(brush, im);
            gdImageFilledRectangle(brush,
                                   0,0,width-1, width-1, cstk[SP].pencolor_ix);
            gdImageSetBrush(im, brush);

            if (pen == gdStyled)
                pen = gdStyledBrushed;
            else
                pen = gdBrushed;
        }

        V = malloc(sizeof(pointf) * n);

        for (i = 0; i <= n-1; i++) {
            pTmp.x = A[i].x ;
            pTmp.y = A[i].y ;
            V[i] = gdpt(pTmp);
        }

        pFrom = V[0];

        div = (abs(V[n-1].x - V[0].x) + abs(V[n-1].y - V[0].y)) >> 2 ;

        div = (div < 2)  ? 2 : div;

        for (j = 1; j < div; j++) {
            pTo = Bezierf(V, n -1, (double)j/div);
            gdImageLine(im, ROUND(pFrom.x), ROUND(pFrom.y),
                        ROUND(pTo.x), ROUND(pTo.y), pen);
            pFrom = pTo;
        }

        gdImageLine(im, ROUND(pFrom.x), ROUND(pFrom.y),
                    ROUND(V[n-1].x), ROUND(V[n-1].y), pen);

        free(V);

        if (brush)
            gdImageDestroy(brush);
    }
}

static  void
gd_polygon(point *A, int n, int filled) {
    pointf		p;
    int		i;
    gdPoint		*points;
    int		style[20];
    int		pen, width;
    gdImagePtr	brush = NULL;

    if (cstk[SP].pen != P_NONE) {
        if (cstk[SP].pen == P_DASHED) {
            for (i = 0; i < 10; i++)
                style[i] = cstk[SP].pencolor_ix;

            for (; i < 20; i++)
                style[i] = gdTransparent;

            gdImageSetStyle(im, style, 20);

            pen = gdStyled;

        } else if (cstk[SP].pen == P_DOTTED) {
            for (i = 0; i < 2; i++)
                style[i] = cstk[SP].pencolor_ix;

            for (; i < 12; i++)
                style[i] = gdTransparent;

            gdImageSetStyle(im, style, 12);

            pen = gdStyled;

        } else {
            pen = cstk[SP].pencolor_ix;
        }

        if (cstk[SP].penwidth != WIDTH_NORMAL) {
            width=cstk[SP].penwidth * Scale;
            brush = gdImageCreate(width,width);
            gdImagePaletteCopy(brush, im);
            gdImageFilledRectangle(brush,
                                   0,0,width-1, width-1, cstk[SP].pencolor_ix);
            gdImageSetBrush(im, brush);

            if (pen == gdStyled)
                pen = gdStyledBrushed;
            else
                pen = gdBrushed;
        }

        points = (gdPoint *)malloc(n*sizeof(gdPoint));

        for (i = 0; i < n; i++) {
            p.x = A[i].x;
            p.y = A[i].y;
            p = gdpt(p);
            points[i].x = ROUND(p.x);
            points[i].y = ROUND(p.y);
        }

        if (filled)
            gdImageFilledPolygon(im, points, n, cstk[SP].fillcolor_ix);

        gdImagePolygon(im, points, n, pen);

        free(points);

        if (brush)
            gdImageDestroy(brush);
    }
}

static  void
gd_ellipse(point p, int rx, int ry, int filled) {
    pointf		mp;
    int		i;
    int		style[40];  /* need 2* size for arcs, I don't know why */
    int		pen, width;
    gdImagePtr	brush = NULL;

    if (cstk[SP].pen != P_NONE) {
        if (cstk[SP].pen == P_DASHED) {
            for (i = 0; i < 20; i++)
                style[i] = cstk[SP].pencolor_ix;

            for (; i < 40; i++)
                style[i] = gdTransparent;

            gdImageSetStyle(im, style, 40);

            pen = gdStyled;

        } else if (cstk[SP].pen == P_DOTTED) {
            for (i = 0; i < 2; i++)
                style[i] = cstk[SP].pencolor_ix;

            for (; i < 24; i++)
                style[i] = gdTransparent;

            gdImageSetStyle(im, style, 24);

            pen = gdStyled;

        } else {
            pen = cstk[SP].pencolor_ix;
        }

        if (cstk[SP].penwidth != WIDTH_NORMAL) {
            width = cstk[SP].penwidth;
            brush = gdImageCreate(width,width);
            gdImagePaletteCopy(brush, im);
            gdImageFilledRectangle(brush,
                                   0,0,width-1, width-1, cstk[SP].pencolor_ix);
            gdImageSetBrush(im, brush);

            if (pen == gdStyled)
                pen = gdStyledBrushed;
            else
                pen = gdBrushed;
        }

        if (Rot) {
            int t;
            t = rx;
            rx = ry;
            ry = t;
        }

        mp.x = p.x;
        mp.y = p.y;
        mp = gdpt(mp);

        if (filled) {
            gdImageFilledEllipse(im, ROUND(mp.x), ROUND(mp.y),
                                 ROUND(Scale*(rx + rx)), ROUND(Scale*(ry + ry)),
                                 cstk[SP].fillcolor_ix);
        }

        gdImageArc(im, ROUND(mp.x), ROUND(mp.y),
                   ROUND(Scale*(rx + rx)), ROUND(Scale*(ry + ry)), 0, 360, pen);

        if (brush)
            gdImageDestroy(brush);
    }
}

static  void
gd_polyline(point* A, int n) {
    pointf		p, p1;
    int		i;
    int		style[20];
    int		pen, width;
    gdImagePtr	brush = NULL;

    if (cstk[SP].pen != P_NONE) {
        if (cstk[SP].pen == P_DASHED) {
            for (i = 0; i < 10; i++)
                style[i] = cstk[SP].pencolor_ix;

            for (; i < 20; i++)
                style[i] = gdTransparent;

            gdImageSetStyle(im, style, 20);

            pen = gdStyled;

        } else if (cstk[SP].pen == P_DOTTED) {
            for (i = 0; i < 2; i++)
                style[i] = cstk[SP].pencolor_ix;

            for (; i < 12; i++)
                style[i] = gdTransparent;

            gdImageSetStyle(im, style, 12);

            pen = gdStyled;

        } else {
            pen = cstk[SP].pencolor_ix;
        }

        if (cstk[SP].penwidth != WIDTH_NORMAL) {
            width = cstk[SP].penwidth;
            brush = gdImageCreate(width,width);
            gdImagePaletteCopy(brush, im);
            gdImageFilledRectangle(brush,
                                   0,0,width-1,width-1,cstk[SP].pencolor_ix);
            gdImageSetBrush(im, brush);

            if (pen == gdStyled)
                pen = gdStyledBrushed;
            else
                pen = gdBrushed;
        }

        p.x = A[0].x;
        p.y = A[0].y;
        p = gdpt(p);

        for (i = 1; i < n; i++) {
            p1.x = A[i].x;
            p1.y = A[i].y;
            p1 = gdpt(p1);
            gdImageLine(im, ROUND(p.x), ROUND(p.y),
                        ROUND(p1.x), ROUND(p1.y), pen);
            p.x = p1.x;
            p.y = p1.y;
        }

        if (brush)
            gdImageDestroy(brush);
    }
}

static gdImagePtr loadimage(char *name) {
    gdImagePtr	rv = 0;
    char	*imagefile,*suffix;
    FILE	*in = NULL;
    char		*cmd = 0;
#ifdef HAVE_CURL

    char		*curlopts;
#endif

    imagefile = name;

    if (!imagefile)
        return 0;

    if (!strncmp(imagefile,"file:",5)) {
        imagefile = &imagefile[5];

        if (!strncmp(imagefile,"//",2))
            imagefile = &imagefile[2];

        in = fopen (imagefile, "r");

        cmd = malloc(16);

        strcpy(cmd,"fopen()");
    }

#ifdef HAVE_CURL
    else {
        cmd = malloc(strlen(imagefile) + strlen(CURLCMD) + 16);
        /* the following is a feeble attempt to protect webdot */

        if (getenv("WEBDOTSECURITY") || !(curlopts = agget(Curnode,"curlopts")))
            curlopts = CURLOPTS;

        sprintf(cmd,"%s %s \"%s\"",CURLCMD,curlopts,imagefile);

        in = popen(cmd,"r");
    }

#endif
    if (!in)
        errorPrintf(stderr,"%s: couldn't open image file %s via %s\n",CmdName,name,cmd);
    else {
        suffix = strrchr(imagefile,'.');

        if (!suffix)
            suffix = imagefile;
        else
            suffix++;

        if (!strcasecmp(suffix,"gif"))
            rv = gdImageCreateFromGif(in);

#ifdef HAVE_LIBPNG

        else if (!strcasecmp(suffix,"png"))
            rv = gdImageCreateFromPng(in);

#endif

#ifdef HAVE_LIBJPEG

        else if (!strcasecmp(suffix,"jpeg")||!strcasecmp(suffix,"jpg"))
            rv = gdImageCreateFromJpeg(in);

#endif

        else if (!strcasecmp(suffix,"wbmp"))
            rv = gdImageCreateFromWBMP(in);

        else if (!strcasecmp(suffix,"xbm"))
            rv = gdImageCreateFromXbm(in);

        else
            errorPrintf(stderr,"%s: image file %s suffix not recognized\n",CmdName,name);

        fclose(in);

        if (!rv)
            errorPrintf(stderr,"%s: image file %s contents were not recognized\n",CmdName,name);
    }

    if (cmd)
        free(cmd);

    return rv;
}

typedef struct imagerec_s {
    Dtlink_t        link;
    char            *name;
    gdImagePtr		im;
}

imagerec_t;


static void imagerec_free(Dict_t *dict, Void_t *p, Dtdisc_t *disc) {
    gdImagePtr im = ((imagerec_t*)p)->im;

    if (im)
        gdImageDestroy(im);
}

static Dtdisc_t ImageDictDisc = {
                                    offsetof(imagerec_t,name),   /* key */
                                    -1,                     /* size */
                                    0,                      /* link offset */
                                    NIL(Dtmake_f),
                                    imagerec_free,
                                    NIL(Dtcompar_f),
                                    NIL(Dthash_f),
                                    NIL(Dtmemory_f),
                                    NIL(Dtevent_f)
                                };

static gdImagePtr getimage(char *name) {
    imagerec_t	probe, *val;

    if (!name)
        return 0;  /* cdt does not like NULL keys */

    if (!ImageDict)
        ImageDict = dtopen(&ImageDictDisc,Dttree);

    probe.name = name;

    val = dtsearch(ImageDict,&probe);

    if (!val) {
        val = malloc(sizeof(imagerec_t));
        val->name = name;
        val->im = loadimage(name);
        dtinsert(ImageDict,val);
    }

    return val->im;
}

static  void
gd_user_shape(char *name, point *A, int n, int filled) {
    gdImagePtr im2 = 0;
    pointf	destul, destlr;
    pointf	ul, lr;		/* upper left, lower right */
    int		i;

    im2 = getimage(agget(Curnode,"shapefile"));

    if (im2) {
        /* compute dest origin and size */
        ul.x = lr.x = A[0].x;
        ul.y = lr.y = A[0].y;

        for (i = 1; i < n; i++) {
            if (ul.x > A[i].x)
                ul.x = A[i].x;

            if (ul.y < A[i].y)
                ul.y = A[i].y;

            if (lr.y > A[i].y)
                lr.y = A[i].y;

            if (lr.x < A[i].x)
                lr.x = A[i].x;
        }

        destul = gdpt(ul);
        destlr = gdpt(lr);
        gdImageCopyResized(im,im2,ROUND(destul.x),ROUND(destul.y),0,0,ROUND(destlr.x - destul.x),ROUND(destlr.y - destul.y),im2->sx,im2->sy);
    }

    gd_polygon(A, n, 0);
}

point gd_user_shape_size(node_t *n, char *imagefile) {
    point		rv;
    gdImagePtr	im;

    Curnode = n;
    im = getimage(imagefile);

    if (im) {
        rv.x = im->sx / SCALE;
        rv.y = im->sy / SCALE;

    } else
        rv.x = rv.y = 0;

    return rv;
}


codegen_t	GD_CodeGen = {
                           gd_reset,
                           gd_begin_job, gd_end_job,
                           gd_begin_graph_to_file, gd_end_graph_to_file,
                           gd_begin_page, gd_end_page,
                           gd_begin_cluster, gd_end_cluster,
                           gd_begin_nodes, gd_end_nodes,
                           gd_begin_edges, gd_end_edges,
                           gd_begin_node, gd_end_node,
                           gd_begin_edge, gd_end_edge,
                           gd_begin_context, gd_end_context,
                           gd_set_font, gd_textline,
                           gd_set_pencolor, gd_set_fillcolor, gd_set_style,
                           gd_ellipse, gd_polygon,
                           gd_bezier, gd_polyline,
                           0 /* gd_arrowhead */, gd_user_shape,
                           0 /* gd_comment */, gd_textsize,
                           gd_user_shape_size
                       };

codegen_t	memGD_CodeGen = {		/* see tcldot */
                              gd_reset,
                              gd_begin_job, gd_end_job,
                              gd_begin_graph_to_memory, gd_end_graph_to_memory,
                              gd_begin_page, gd_end_page,
                              gd_begin_cluster, gd_end_cluster,
                              gd_begin_nodes, gd_end_nodes,
                              gd_begin_edges, gd_end_edges,
                              gd_begin_node, gd_end_node,
                              gd_begin_edge, gd_end_edge,
                              gd_begin_context, gd_end_context,
                              gd_set_font, gd_textline,
                              gd_set_pencolor, gd_set_fillcolor, gd_set_style,
                              gd_ellipse, gd_polygon,
                              gd_bezier, gd_polyline,
                              0 /* gd_arrowhead */, gd_user_shape,
                              0 /* gd_comment */, gd_textsize,
                              gd_user_shape_size
                          };

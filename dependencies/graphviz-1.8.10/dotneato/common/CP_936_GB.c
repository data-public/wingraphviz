#include "CP_936_GB.h"
#include <string.h>

char *
get_fontname_CP_936(char *font) {
    char *fontlist;

    fontlist = font;

    if (strcmp(font,"����")==0) {
        fontlist = "SimHei.ttf";

    } else if (strcmp(font,"SimHei")==0) {
        fontlist = "SimHei.ttf";


    } else if (strcmp(font,"����")==0) {
        fontlist = "SimSun.ttc";

    } else if (strcmp(font,"SimSun")==0) {
        fontlist = "SimSun.ttc";

    } else if (strcmp(font,"NSimSun")==0) {
        fontlist = "SimSun.ttc";

    } else if (strcmp(font,"Arial Unicode MS")==0) {
        fontlist = "ARIALUNI.ttf";
    }

    return fontlist;
}

int get_fontindex_CP_936(char *font) {

    if (strcmp(font,"NSimSun")==0) {
        return  1;

    } else {
        return  0;
    }
}

char *
get_fontfam_CP_936(char *font) {
    char *fontlist;

    fontlist = font;

    if (strcmp(font,"����")==0) {
        fontlist = "SimHei";

    } else if (strcmp(font,"����")==0) {
        fontlist = "SimSun";
    }

    return fontlist;
}

#include "CP_1251_CYRILLIC.h"
#include <string.h>

char *
get_fontname_CP_1251(char *font) {
    char *fontlist;

    fontlist = font;

    if (strcmp(font,"Arial Unicode MS")==0) {
        fontlist = "ARIALUNI.ttf";
    }

    return fontlist;
}

int get_fontindex_CP_1251(char *font) {

    return  0;

}

char *
get_fontfam_CP_1251(char *font) {
    char *fontlist;

    fontlist = font;

    return fontlist;
}

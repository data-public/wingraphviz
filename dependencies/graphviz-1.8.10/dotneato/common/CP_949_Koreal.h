#define CP_949_DEFAULTFONT "Batang"

char * get_fontname_CP_949(char *font);

char * get_fontfam_CP_949(char *font);

int get_fontindex_CP_949(char *font);

/*
    This software may only be used by you under license from AT&T Corp.
    ("AT&T").  A copy of AT&T's Source Code Agreement is available at
    AT&T's Internet website having the URL:
    <http://www.research.att.com/sw/tools/graphviz/license/source.html>
    If you received this software without first entering into a license
    with AT&T, you have an infringing copy of this software and cannot use
    it without violating AT&T's intellectual property rights.
*/


/*
 * Written by Emden R. Gansner
 * Derived from Graham Wills' algorithm described in GD'97.
 */

#include    "circle.h"
#include    "adjust.h"
#include    "pack.h"
#include    "neatoprocs.h"

#ifdef DMALLOC
#include "dmalloc.h"
#endif

static void
twopi_nodesize(node_t* n, boolean flip) {
    int         w;

    w = n->u.xsize = POINTS(n->u.width);
    n->u.lw  = n->u.rw = w / 2;
    n->u.ht = n->u.ysize = POINTS(n->u.height);
}

static void
twopi_init_node (node_t* n) {
    common_init_node(n);

    twopi_nodesize(n,n->graph->u.left_to_right);
    n->u.pos = ALLOC(n->graph->u.ndim,0,double);
    n->u.alg = (void*)zmalloc (sizeof (cdata));
}

static void
twopi_initPort(node_t* n, edge_t* e, char* name) {
    port_t  port;

    if (name == NULL)
        return;

    port = n->u.shape->portfn(n,name);

#ifdef NOTDEF

    if (n->graph->u.left_to_right)
        port.p = invflip_pt(port.p);

#endif

    port.order = 0;

    if (e->tail == n)
        e->u.tail_port = port;
    else
        e->u.head_port = port;
}

static void
twopi_init_edge (edge_t* e) {
    common_init_edge(e);

    e->u.factor = late_double(e,E_weight,1.0,0.0);

    twopi_initPort(e->tail,e,agget(e,"tailport"));
    twopi_initPort(e->head,e,agget(e,"headport"));
}

static void
twopi_init_graph(graph_t *g) {
    node_t* n;
    edge_t* e;
    char*   str;
    int     i = 0;

    g->u.neato_nlist = N_NEW(agnnodes(g) + 1,node_t*);

    for (n = agfstnode(g); n; n = agnxtnode(g,n)) {
        g->u.neato_nlist[i++] = n;
        twopi_init_node(n);
    }

    for (n = agfstnode(g); n; n = agnxtnode(g,n)) {
        for (e = agfstout(g,n); e; e = agnxtout(g,e)) {
            twopi_init_edge(e);
            str = agget(e,"label");

            if (str && str[0])
                g->u.has_edge_labels = TRUE;
        }
    }
}

/* twopi_layout:
 */
void
twopi_layout(Agraph_t* g) {
    attrsym_t*  sym;
    Agnode_t*   ctr = 0;
    char*       s;

    /* setting rankdir=LR is currently undefined in neato,
     * but having it set causes has effects in the common routines.
     * So, we turn it off.
     */
    sym = agfindattr(g,"rankdir");

    if (sym)
        agxset (g, sym->index, "");

    graph_init(g);

    g->u.ndim = late_int(g,agfindattr(g,"dim"),2,2);

    Ndim = g->u.ndim = MIN(g->u.ndim,MAXDIM);

    g->u.drawing->engine = TWOPI;

    twopi_init_graph(g);

    s = agget (g, "center");

    if (s && (*s != '\0')) {
        ctr = agfindnode (g, s);

        if (!ctr) {
            fprintf (stderr, "Warning: specified center node \"%s\" was not found.",
                     s);
            fprintf (stderr, "Using default calculation for center\n");
        }
    }

    if (agnnodes(g)) {
        Agraph_t** ccs;
        Agraph_t*  sg;
        Agnode_t*  c = NULL;
        int        ncc;
        int        i;

        ccs = ccomps (g, &ncc, 0);

        if (ncc == 1) {
            circleLayout (g,ctr);
            adjustNodes (g);
            spline_edges(g);

        } else {
            for (i = 0; i < ncc; i++) {
                sg = ccs[i];

                if (ctr && agcontains (sg, ctr))
                    c = ctr;
                else
                    c = 0;

                nodeInduce (sg);

                circleLayout (sg,c);

                adjustNodes (sg);
            }

            spline_edges(g);
            packSubgraphs (ncc, ccs, g, CL_OFFSET, 1);
        }

        for (i = 0; i < ncc; i++) {
            agdelete (g, ccs[i]);
        }
    }

    dotneato_postprocess(g, twopi_nodesize);

}

static void twopi_cleanup_node(node_t* n) {
    free (n->u.alg);

    if (n->u.shape)
        n->u.shape->freefn(n);

    free_label(n->u.label);

    memset(&(n->u),0,sizeof(Agnodeinfo_t));
}

static void twopi_free_splines(edge_t* e) {
    int		i;

    if (e->u.spl) {
        for (i = 0; i < e->u.spl->size; i++)
            free(e->u.spl->list[i].list);

        free(e->u.spl->list);

        free(e->u.spl);
    }

    e->u.spl = NULL;
}

static void twopi_cleanup_edge(edge_t* e) {
    twopi_free_splines(e);
    free_label(e->u.label);
    memset(&(e->u),0,sizeof(Agedgeinfo_t));
}

static void twopi_cleanup_graph(graph_t* g) {
    free(g->u.neato_nlist);
    free_ugraph(g);
    free_label(g->u.label);
    memset(&(g->u),0,sizeof(Agraphinfo_t));
}

void twopi_cleanup(graph_t* g) {
    node_t  *n;
    edge_t  *e;

    for (n = agfstnode(g); n; n = agnxtnode(g, n)) {
        for (e = agfstedge(g, n); e; e = agnxtedge(g, e, n)) {
            twopi_cleanup_edge(e);
        }

        twopi_cleanup_node(n);
    }

    twopi_cleanup_graph(g);
}

#ifndef CIRCLE_H
#define CIRCLE_H
#include "render.h"

typedef struct {
    int      nStepsToLeaf;
    int      subtreeSize;
    int      nChildren;
    int      nStepsToCenter;
    node_t*  parent;
    double   span;
    double   theta;
}

cdata;

#define DATA(n) ((cdata*)((n)->u.alg))
#define SLEAF(n) (DATA(n)->nStepsToLeaf)
#define STSIZE(n) (DATA(n)->subtreeSize)
#define NCHILD(n) (DATA(n)->nChildren)
#define SCENTER(n) (DATA(n)->nStepsToCenter)
#define PARENT(n) (DATA(n)->parent)
#define SPAN(n) (DATA(n)->span)
#define THETA(n) (DATA(n)->theta)

extern void circleLayout(Agraph_t* sg, Agnode_t* center);
extern void twopi_layout(Agraph_t* g);
extern void twopi_cleanup(Agraph_t* g);
#endif

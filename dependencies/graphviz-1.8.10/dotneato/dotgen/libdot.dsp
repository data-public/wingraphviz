# Microsoft Developer Studio Project File - Name="libdot" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Static Library" 0x0104

CFG=libdot - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "libdot.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "libdot.mak" CFG="libdot - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "libdot - Win32 Release" (based on "Win32 (x86) Static Library")
!MESSAGE "libdot - Win32 Debug" (based on "Win32 (x86) Static Library")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""$/graphviz/libdot", GEAAAAAA"
# PROP Scc_LocalPath "."
CPP=cl.exe
RSC=rc.exe

!IF  "$(CFG)" == "libdot - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_MBCS" /D "_LIB" /YX /FD /c
# ADD CPP /nologo /GB /MT /W3 /GX /I "..\common" /I "..\..\\" /I "..\..\pathplan" /I "..\..\graph" /I "..\..\cdt" /I "..\tools\error" /I "..\tools\os" /D "NDEBUG" /D "WIN32" /D "_MBCS" /D "_LIB" /D "HAVE_CONFIG_H" /D "MSWIN32" /FR /YX /FD -o3 -Qxi /c
# ADD BASE RSC /l 0x404 /d "NDEBUG"
# ADD RSC /l 0x404 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LIB32=link.exe -lib
# ADD BASE LIB32 /nologo
# ADD LIB32 /nologo /out:"..\..\lib\Release\libdot.lib"

!ELSEIF  "$(CFG)" == "libdot - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_MBCS" /D "_LIB" /YX /FD /GZ /c
# ADD CPP /nologo /MTd /W3 /Gm /GX /ZI /Od /I "..\common" /I "..\..\\" /I "..\..\pathplan" /I "..\..\graph" /I "..\..\cdt" /I "..\..\tools\error" /I "..\..\tools\os" /D "_DEBUG" /D "WIN32" /D "_MBCS" /D "_LIB" /D "HAVE_CONFIG_H" /D "MSWIN32" /YX /FD /GZ /c
# ADD BASE RSC /l 0x404 /d "_DEBUG"
# ADD RSC /l 0x404 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LIB32=link.exe -lib
# ADD BASE LIB32 /nologo
# ADD LIB32 /nologo /out:"..\..\lib\Debug\libdot.lib"

!ENDIF 

# Begin Target

# Name "libdot - Win32 Release"
# Name "libdot - Win32 Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=.\acyclic.c
# End Source File
# Begin Source File

SOURCE=.\class1.c
# End Source File
# Begin Source File

SOURCE=.\class2.c
# End Source File
# Begin Source File

SOURCE=.\cluster.c
# End Source File
# Begin Source File

SOURCE=.\compound.c
# End Source File
# Begin Source File

SOURCE=.\conc.c
# End Source File
# Begin Source File

SOURCE=.\decomp.c
# End Source File
# Begin Source File

SOURCE=.\fastgr.c
# End Source File
# Begin Source File

SOURCE=.\flat.c
# End Source File
# Begin Source File

SOURCE=.\init.c
# End Source File
# Begin Source File

SOURCE=.\mincross.c
# End Source File
# Begin Source File

SOURCE=.\ns.c
# End Source File
# Begin Source File

SOURCE=.\position.c
# End Source File
# Begin Source File

SOURCE=.\rank.c
# End Source File
# Begin Source File

SOURCE=.\routespl.c
# End Source File
# Begin Source File

SOURCE=.\sameport.c
# End Source File
# Begin Source File

SOURCE=.\splines.c
# End Source File
# Begin Source File

SOURCE=.\timing.c
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=.\dot.h
# End Source File
# Begin Source File

SOURCE=.\dotprocs.h
# End Source File
# End Group
# End Target
# End Project

# Microsoft Developer Studio Generated NMAKE File, Based on libdot.dsp
!IF "$(CFG)" == ""
CFG=libdot - Win32 Debug
!MESSAGE No configuration specified. Defaulting to libdot - Win32 Debug.
!ENDIF 

!IF "$(CFG)" != "libdot - Win32 Release" && "$(CFG)" != "libdot - Win32 Debug"
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "libdot.mak" CFG="libdot - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "libdot - Win32 Release" (based on "Win32 (x86) Static Library")
!MESSAGE "libdot - Win32 Debug" (based on "Win32 (x86) Static Library")
!MESSAGE 
!ERROR An invalid configuration is specified.
!ENDIF 

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE 
NULL=nul
!ENDIF 

CPP=cl.exe
RSC=rc.exe

!IF  "$(CFG)" == "libdot - Win32 Release"

OUTDIR=.\Release
INTDIR=.\Release
# Begin Custom Macros
OutDir=.\Release
# End Custom Macros

ALL : "..\..\lib\Release\libdot.lib" "$(OUTDIR)\libdot.bsc"


CLEAN :
	-@erase "$(INTDIR)\acyclic.obj"
	-@erase "$(INTDIR)\acyclic.sbr"
	-@erase "$(INTDIR)\class1.obj"
	-@erase "$(INTDIR)\class1.sbr"
	-@erase "$(INTDIR)\class2.obj"
	-@erase "$(INTDIR)\class2.sbr"
	-@erase "$(INTDIR)\cluster.obj"
	-@erase "$(INTDIR)\cluster.sbr"
	-@erase "$(INTDIR)\compound.obj"
	-@erase "$(INTDIR)\compound.sbr"
	-@erase "$(INTDIR)\conc.obj"
	-@erase "$(INTDIR)\conc.sbr"
	-@erase "$(INTDIR)\decomp.obj"
	-@erase "$(INTDIR)\decomp.sbr"
	-@erase "$(INTDIR)\fastgr.obj"
	-@erase "$(INTDIR)\fastgr.sbr"
	-@erase "$(INTDIR)\flat.obj"
	-@erase "$(INTDIR)\flat.sbr"
	-@erase "$(INTDIR)\init.obj"
	-@erase "$(INTDIR)\init.sbr"
	-@erase "$(INTDIR)\mincross.obj"
	-@erase "$(INTDIR)\mincross.sbr"
	-@erase "$(INTDIR)\ns.obj"
	-@erase "$(INTDIR)\ns.sbr"
	-@erase "$(INTDIR)\position.obj"
	-@erase "$(INTDIR)\position.sbr"
	-@erase "$(INTDIR)\rank.obj"
	-@erase "$(INTDIR)\rank.sbr"
	-@erase "$(INTDIR)\routespl.obj"
	-@erase "$(INTDIR)\routespl.sbr"
	-@erase "$(INTDIR)\sameport.obj"
	-@erase "$(INTDIR)\sameport.sbr"
	-@erase "$(INTDIR)\splines.obj"
	-@erase "$(INTDIR)\splines.sbr"
	-@erase "$(INTDIR)\timing.obj"
	-@erase "$(INTDIR)\timing.sbr"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(OUTDIR)\libdot.bsc"
	-@erase "..\..\lib\Release\libdot.lib"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP_PROJ=/nologo /MT /W3 /GX /O1 /I "..\common" /I "..\..\\" /I "..\..\pathplan" /I "..\..\graph" /I "..\..\cdt" /I "..\tools\error" /D "NDEBUG" /D "WIN32" /D "_MBCS" /D "_LIB" /D "HAVE_CONFIG_H" /D "MSWIN32" /FR"$(INTDIR)\\" /Fp"$(INTDIR)\libdot.pch" /YX /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /c 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\libdot.bsc" 
BSC32_SBRS= \
	"$(INTDIR)\acyclic.sbr" \
	"$(INTDIR)\class1.sbr" \
	"$(INTDIR)\class2.sbr" \
	"$(INTDIR)\cluster.sbr" \
	"$(INTDIR)\compound.sbr" \
	"$(INTDIR)\conc.sbr" \
	"$(INTDIR)\decomp.sbr" \
	"$(INTDIR)\fastgr.sbr" \
	"$(INTDIR)\flat.sbr" \
	"$(INTDIR)\init.sbr" \
	"$(INTDIR)\mincross.sbr" \
	"$(INTDIR)\ns.sbr" \
	"$(INTDIR)\position.sbr" \
	"$(INTDIR)\rank.sbr" \
	"$(INTDIR)\routespl.sbr" \
	"$(INTDIR)\sameport.sbr" \
	"$(INTDIR)\splines.sbr" \
	"$(INTDIR)\timing.sbr"

"$(OUTDIR)\libdot.bsc" : "$(OUTDIR)" $(BSC32_SBRS)
    $(BSC32) @<<
  $(BSC32_FLAGS) $(BSC32_SBRS)
<<

LIB32=link.exe -lib
LIB32_FLAGS=/nologo /out:"..\..\lib\Release\libdot.lib" 
LIB32_OBJS= \
	"$(INTDIR)\acyclic.obj" \
	"$(INTDIR)\class1.obj" \
	"$(INTDIR)\class2.obj" \
	"$(INTDIR)\cluster.obj" \
	"$(INTDIR)\compound.obj" \
	"$(INTDIR)\conc.obj" \
	"$(INTDIR)\decomp.obj" \
	"$(INTDIR)\fastgr.obj" \
	"$(INTDIR)\flat.obj" \
	"$(INTDIR)\init.obj" \
	"$(INTDIR)\mincross.obj" \
	"$(INTDIR)\ns.obj" \
	"$(INTDIR)\position.obj" \
	"$(INTDIR)\rank.obj" \
	"$(INTDIR)\routespl.obj" \
	"$(INTDIR)\sameport.obj" \
	"$(INTDIR)\splines.obj" \
	"$(INTDIR)\timing.obj"

"..\..\lib\Release\libdot.lib" : "$(OUTDIR)" $(DEF_FILE) $(LIB32_OBJS)
    $(LIB32) @<<
  $(LIB32_FLAGS) $(DEF_FLAGS) $(LIB32_OBJS)
<<

!ELSEIF  "$(CFG)" == "libdot - Win32 Debug"

OUTDIR=.\Debug
INTDIR=.\Debug

ALL : "..\..\lib\Debug\libdot.lib"


CLEAN :
	-@erase "$(INTDIR)\acyclic.obj"
	-@erase "$(INTDIR)\class1.obj"
	-@erase "$(INTDIR)\class2.obj"
	-@erase "$(INTDIR)\cluster.obj"
	-@erase "$(INTDIR)\compound.obj"
	-@erase "$(INTDIR)\conc.obj"
	-@erase "$(INTDIR)\decomp.obj"
	-@erase "$(INTDIR)\fastgr.obj"
	-@erase "$(INTDIR)\flat.obj"
	-@erase "$(INTDIR)\init.obj"
	-@erase "$(INTDIR)\mincross.obj"
	-@erase "$(INTDIR)\ns.obj"
	-@erase "$(INTDIR)\position.obj"
	-@erase "$(INTDIR)\rank.obj"
	-@erase "$(INTDIR)\routespl.obj"
	-@erase "$(INTDIR)\sameport.obj"
	-@erase "$(INTDIR)\splines.obj"
	-@erase "$(INTDIR)\timing.obj"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(INTDIR)\vc60.pdb"
	-@erase "..\..\lib\Debug\libdot.lib"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP_PROJ=/nologo /MTd /W3 /Gm /GX /ZI /Od /I "..\common" /I "..\..\\" /I "..\..\pathplan" /I "..\..\graph" /I "..\..\cdt" /I "..\tools\error" /D "_DEBUG" /D "WIN32" /D "_MBCS" /D "_LIB" /D "HAVE_CONFIG_H" /D "MSWIN32" /Fp"$(INTDIR)\libdot.pch" /YX /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /GZ /c 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\libdot.bsc" 
BSC32_SBRS= \
	
LIB32=link.exe -lib
LIB32_FLAGS=/nologo /out:"..\..\lib\Debug\libdot.lib" 
LIB32_OBJS= \
	"$(INTDIR)\acyclic.obj" \
	"$(INTDIR)\class1.obj" \
	"$(INTDIR)\class2.obj" \
	"$(INTDIR)\cluster.obj" \
	"$(INTDIR)\compound.obj" \
	"$(INTDIR)\conc.obj" \
	"$(INTDIR)\decomp.obj" \
	"$(INTDIR)\fastgr.obj" \
	"$(INTDIR)\flat.obj" \
	"$(INTDIR)\init.obj" \
	"$(INTDIR)\mincross.obj" \
	"$(INTDIR)\ns.obj" \
	"$(INTDIR)\position.obj" \
	"$(INTDIR)\rank.obj" \
	"$(INTDIR)\routespl.obj" \
	"$(INTDIR)\sameport.obj" \
	"$(INTDIR)\splines.obj" \
	"$(INTDIR)\timing.obj"

"..\..\lib\Debug\libdot.lib" : "$(OUTDIR)" $(DEF_FILE) $(LIB32_OBJS)
    $(LIB32) @<<
  $(LIB32_FLAGS) $(DEF_FLAGS) $(LIB32_OBJS)
<<

!ENDIF 

.c{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<


!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("libdot.dep")
!INCLUDE "libdot.dep"
!ELSE 
!MESSAGE Warning: cannot find "libdot.dep"
!ENDIF 
!ENDIF 


!IF "$(CFG)" == "libdot - Win32 Release" || "$(CFG)" == "libdot - Win32 Debug"
SOURCE=.\acyclic.c

!IF  "$(CFG)" == "libdot - Win32 Release"


"$(INTDIR)\acyclic.obj"	"$(INTDIR)\acyclic.sbr" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "libdot - Win32 Debug"


"$(INTDIR)\acyclic.obj" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\class1.c

!IF  "$(CFG)" == "libdot - Win32 Release"


"$(INTDIR)\class1.obj"	"$(INTDIR)\class1.sbr" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "libdot - Win32 Debug"


"$(INTDIR)\class1.obj" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\class2.c

!IF  "$(CFG)" == "libdot - Win32 Release"


"$(INTDIR)\class2.obj"	"$(INTDIR)\class2.sbr" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "libdot - Win32 Debug"


"$(INTDIR)\class2.obj" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\cluster.c

!IF  "$(CFG)" == "libdot - Win32 Release"


"$(INTDIR)\cluster.obj"	"$(INTDIR)\cluster.sbr" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "libdot - Win32 Debug"


"$(INTDIR)\cluster.obj" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\compound.c

!IF  "$(CFG)" == "libdot - Win32 Release"


"$(INTDIR)\compound.obj"	"$(INTDIR)\compound.sbr" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "libdot - Win32 Debug"


"$(INTDIR)\compound.obj" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\conc.c

!IF  "$(CFG)" == "libdot - Win32 Release"


"$(INTDIR)\conc.obj"	"$(INTDIR)\conc.sbr" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "libdot - Win32 Debug"


"$(INTDIR)\conc.obj" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\decomp.c

!IF  "$(CFG)" == "libdot - Win32 Release"


"$(INTDIR)\decomp.obj"	"$(INTDIR)\decomp.sbr" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "libdot - Win32 Debug"


"$(INTDIR)\decomp.obj" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\fastgr.c

!IF  "$(CFG)" == "libdot - Win32 Release"


"$(INTDIR)\fastgr.obj"	"$(INTDIR)\fastgr.sbr" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "libdot - Win32 Debug"


"$(INTDIR)\fastgr.obj" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\flat.c

!IF  "$(CFG)" == "libdot - Win32 Release"


"$(INTDIR)\flat.obj"	"$(INTDIR)\flat.sbr" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "libdot - Win32 Debug"


"$(INTDIR)\flat.obj" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\init.c

!IF  "$(CFG)" == "libdot - Win32 Release"


"$(INTDIR)\init.obj"	"$(INTDIR)\init.sbr" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "libdot - Win32 Debug"


"$(INTDIR)\init.obj" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\mincross.c

!IF  "$(CFG)" == "libdot - Win32 Release"


"$(INTDIR)\mincross.obj"	"$(INTDIR)\mincross.sbr" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "libdot - Win32 Debug"


"$(INTDIR)\mincross.obj" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\ns.c

!IF  "$(CFG)" == "libdot - Win32 Release"


"$(INTDIR)\ns.obj"	"$(INTDIR)\ns.sbr" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "libdot - Win32 Debug"


"$(INTDIR)\ns.obj" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\position.c

!IF  "$(CFG)" == "libdot - Win32 Release"


"$(INTDIR)\position.obj"	"$(INTDIR)\position.sbr" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "libdot - Win32 Debug"


"$(INTDIR)\position.obj" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\rank.c

!IF  "$(CFG)" == "libdot - Win32 Release"


"$(INTDIR)\rank.obj"	"$(INTDIR)\rank.sbr" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "libdot - Win32 Debug"


"$(INTDIR)\rank.obj" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\routespl.c

!IF  "$(CFG)" == "libdot - Win32 Release"


"$(INTDIR)\routespl.obj"	"$(INTDIR)\routespl.sbr" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "libdot - Win32 Debug"


"$(INTDIR)\routespl.obj" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\sameport.c

!IF  "$(CFG)" == "libdot - Win32 Release"


"$(INTDIR)\sameport.obj"	"$(INTDIR)\sameport.sbr" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "libdot - Win32 Debug"


"$(INTDIR)\sameport.obj" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\splines.c

!IF  "$(CFG)" == "libdot - Win32 Release"


"$(INTDIR)\splines.obj"	"$(INTDIR)\splines.sbr" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "libdot - Win32 Debug"


"$(INTDIR)\splines.obj" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\timing.c

!IF  "$(CFG)" == "libdot - Win32 Release"


"$(INTDIR)\timing.obj"	"$(INTDIR)\timing.sbr" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "libdot - Win32 Debug"


"$(INTDIR)\timing.obj" : $(SOURCE) "$(INTDIR)"


!ENDIF 


!ENDIF 


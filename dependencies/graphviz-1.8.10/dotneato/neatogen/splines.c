/*
    This software may only be used by you under license from AT&T Corp.
    ("AT&T").  A copy of AT&T's Source Code Agreement is available at
    AT&T's Internet website having the URL:
    <http://www.research.att.com/sw/tools/graphviz/license/source.html>
    If you received this software without first entering into a license
    with AT&T, you have an infringing copy of this software and cannot use
    it without violating AT&T's intellectual property rights.
*/

#include "error.h"
#include "neato.h"
#include "pathplan.h"
#include "vispath.h"
#ifndef HAVE_DRAND48
extern double drand48(void);
#endif

#ifdef DMALLOC
#include "dmalloc.h"
#endif

#define P2PF(p, pf) (pf.x = p.x, pf.y = p.y)
#define PF2P(pf, p) (p.x = ROUND (pf.x), p.y = ROUND (pf.y))

extern void printvis (vconfig_t *cp);
static boolean swap_ends_p (edge_t *);
static void place_portlabel (edge_t *e, boolean head_p);
static int neato_set_aspect(graph_t *g, pointf* pf);
static splines *getsplinepoints (edge_t* e);
extern int	in_poly(Ppoly_t argpoly, Ppoint_t q);

void neato_compute_bb(graph_t *g) {
    node_t		*n;
    edge_t		*e;
    box			b,bb;
    point		pt,s2;
    int		i,j;

    bb.LL = pointof(MAXINT,MAXINT);
    bb.UR = pointof(-MAXINT,-MAXINT);

    for (n = agfstnode(g); n; n = agnxtnode(g,n)) {
        pt = coord(n);
        s2.x = n->u.xsize/2+1;
        s2.y = n->u.ysize/2+1;
        b.LL = sub_points(pt,s2);
        b.UR = add_points(pt,s2);

        bb.LL.x = MIN(bb.LL.x,b.LL.x);
        bb.LL.y = MIN(bb.LL.y,b.LL.y);
        bb.UR.x = MAX(bb.UR.x,b.UR.x);
        bb.UR.y = MAX(bb.UR.y,b.UR.y);

        for (e = agfstout(g,n); e; e = agnxtout(g,e)) {
            if (e->u.spl == 0)
                continue;

            for (i = 0; i < e->u.spl->size; i++) {
                for (j = 0; j < e->u.spl->list[i].size; j++) {
                    pt = e->u.spl->list[i].list[j];

                    if (bb.LL.x > pt.x)
                        bb.LL.x = pt.x;

                    if (bb.LL.y > pt.y)
                        bb.LL.y = pt.y;

                    if (bb.UR.x < pt.x)
                        bb.UR.x = pt.x;

                    if (bb.UR.y < pt.y)
                        bb.UR.y = pt.y;
                }
            }
        }
    }

    for (i = 1; i <= g->u.n_cluster; i++) {
        bb.LL.x = MIN(bb.LL.x,g->u.clust[i]->u.bb.LL.x);
        bb.LL.y = MIN(bb.LL.y,g->u.clust[i]->u.bb.LL.y);
        bb.UR.x = MAX(bb.UR.x,g->u.clust[i]->u.bb.UR.x);
        bb.UR.y = MAX(bb.UR.y,g->u.clust[i]->u.bb.UR.y);
    }

    g->u.bb = bb;
}


static bezier *new_spline (edge_t *e, int sz) {
    bezier *rv;

    if (e->u.spl == NULL)
        e->u.spl = NEW (splines);

    e->u.spl->list = ALLOC (e->u.spl->size + 1, e->u.spl->list, bezier);

    rv = &(e->u.spl->list[e->u.spl->size++]);

    rv->list = N_NEW (sz, point);

    rv->size = sz;

    rv->sflag = rv->eflag = FALSE;

    return rv;
}

static Ppoint_t mkPoint(point p) {
    Ppoint_t rv;
    rv.x = p.x;
    rv.y = p.y;
    return rv;
}


static void
make_barriers(Ppoly_t **poly, int npoly, int pp, int qp, Pedge_t **barriers, int *n_barriers) {
    int     i, j, k, n, b;
    Pedge_t *bar;

    n = 0;

    for (i = 0; i < npoly; i++) {
        if (i == pp)
            continue;

        if (i == qp)
            continue;

        n = n + poly[i]->pn;
    }

    bar = malloc(n * sizeof(Pedge_t));
    b = 0;

    for (i = 0; i < npoly; i++) {
        if (i == pp)
            continue;

        if (i == qp)
            continue;

        for (j = 0; j < poly[i]->pn; j++) {
            k = j + 1;

            if (k >= poly[i]->pn)
                k = 0;

            bar[b].a = poly[i]->ps[j];

            bar[b].b = poly[i]->ps[k];

            b++;
        }
    }

    assert(b == n);
    *barriers = bar;
    *n_barriers = n;
}

extern int Plegal_arrangement( Ppoly_t	**polys, int	n_polys);

static int
numFields (char* pos) {
    int    cnt = 0;
    char   c;

    while (isspace(*pos))
        pos++;

    while (*pos) {
        cnt++;

        while ((c = *pos) && !isspace(c))
            pos++;  /* skip token */

        while (isspace(*pos))
            pos++;
    }

    return cnt;
}

static int
user_spline (attrsym_t* symptr, edge_t* e, pointf* offset,
             int doScale, pointf* scalef) {
    char*    pos;
    int      i, n, npts, nc;
    point*   ps = 0;
    point*   pp;
    double   x,y;
    int      sflag = 0, eflag = 0;
    point    sp, ep;
    bezier*  newspl;

    if (symptr == NULL)
        return 0;

    pos = agxget(e,symptr->index);

    if (*pos == '\0')
        return 0;

    /* check for s head */
    i = sscanf(pos,"s,%lf,%lf%n",&x,&y,&nc);

    if (i == 2) {
        sflag = 1;
        pos = pos+nc;

        if (doScale) {
            sp.x = (int)((scalef->x)*(x - offset->x));
            sp.y = (int)((scalef->y)*(y - offset->y));

        } else {
            sp.x = (int)(x - offset->x);
            sp.y = (int)(y - offset->y);
        }
    }

    /* check for e head */
    i = sscanf(pos,"e,%lf,%lf%n",&x,&y,&nc);

    if (i == 2) {
        eflag = 1;
        pos = pos+nc;

        if (doScale) {
            ep.x = (int)((scalef->x)*(x - offset->x));
            ep.y = (int)((scalef->y)*(y - offset->y));

        } else {
            ep.x = (int)(x - offset->x);
            ep.y = (int)(y - offset->y);
        }
    }

    npts = numFields (pos);  /* count potential points */
    n = npts;

    if ((n < 4) || (n%3 != 1))
        return 0;

    ps = ALLOC(n,0,point);

    pp = ps;

    while (n) {
        i = sscanf(pos,"%lf,%lf%n",&x,&y,&nc);

        if (i < 2) {
            free (ps);
            return 0;
        }

        pos = pos+nc;

        if (doScale) {
            pp->x = (int)((scalef->x)*(x - offset->x));
            pp->y = (int)((scalef->y)*(y - offset->y));

        } else {
            pp->x = (int)(x - offset->x);
            pp->y = (int)(y - offset->y);
        }

        pp++;
        n--;
    }

    /* parsed successfully; create spline */
    newspl = new_spline (e, npts);

    if (sflag) {
        newspl->sflag = 1;
        newspl->sp = sp;
    }

    if (eflag) {
        newspl->eflag = 1;
        newspl->ep = ep;
    }

    for (i=0; i < npts; i++) {
        newspl->list[i] = ps[i];
    }

    free (ps);
    return 1;
}

/* spline_edges0:
 * Main body for constructing edges.
 * Assumes u.bb for has been computed for g and all clusters
 * (not just top-level clusters), and 
 * that g->u.bb.LL is at the origin.
 *
 * This last criterion is, I believe, mainly to simplify the code
 * in neato_set_aspect. It would be good to remove this constraint,
 * as this would allow nodes pinned on input to have the same coordinates
 * when output in dot or plain format.
 *
 * The offset parameter tells how the original graph was translated to
 * get it to the origin. It is used here to translate user-supplied
 * spline control points.
 *
 * As a side-effect, this function sets the u.coord attribute of nodes
 * from the u.pos attribute. This is needed for output. In addition, 
 * it guarantees that all bounding
 * boxes are current; in particular, the bounding box of g reflects the
 * addition of edges. NOTE: intra-cluster edges are not constrained to
 * remain in the cluster's bounding box and, conversely, a cluster's box
 * is not altered to reflect intra-cluster edges.
 * 
 */
void spline_edges0(graph_t *g, pointf offset) {
    node_t		*n;
    edge_t		*e;
    point		dumb[4],d,ld;
    pointf		polyp;
    Ppoly_t		**obs;
    polygon_t	*poly;
    int 		i=0, j, npoly, sides;
    extern void	poly_init(node_t *);
    Ppoint_t	p,q;
    vconfig_t	*vconfig;
    Pedge_t     *barriers;
    double		adj=0.0, SEP;
    char		*str;
    pointf      scalef;
    int         doScale;
    attrsym_t	*symptr=NULL;

    if ((str = agget(g,"sep"))) {
        SEP = 1.0 + atof(str);

    } else
        SEP = 1.01;

    doScale = neato_set_aspect(g,&scalef);

    /* build configuration */
    if (mapbool(agget(g,"splines"))) {
        obs = N_NEW (agnnodes(g), Ppoly_t*);

        for (n = agfstnode(g); n; n = agnxtnode(g,n)) {
            n->u.coord.x = POINTS(n->u.pos[0]);
            n->u.coord.y = POINTS(n->u.pos[1]);

            if (n->u.shape->initfn == poly_init) {
                obs[i] = NEW(Ppoly_t);
                poly = (polygon_t*) n->u.shape_info;

                if (poly->sides >= 3) {
                    sides = poly->sides;

                } else {	/* ellipse */
                    sides = 8;
                    adj = drand48() * .01;
                }

                obs[i]->pn = sides;
                obs[i]->ps = N_NEW(sides, Ppoint_t);
                /* assuming polys are in CCW order, and pathplan needs CW */

                for (j = 0; j < sides; j++) {
                    if (poly->sides >= 3) {
                        polyp.x = POINTS(poly->vertices[j].x) * SEP;
                        polyp.y = POINTS(poly->vertices[j].y) * SEP;

                    } else {
                        double	c, s;
                        c = cos(2.0 * PI * j / sides + adj);
                        s = sin(2.0 * PI * j / sides + adj);
                        polyp.x = SEP * c * (n->u.lw + n->u.rw) / 2.0;
                        polyp.y = SEP * s * n->u.ht / 2.0;
                    }

                    obs[i]->ps[sides - j - 1].x = polyp.x + n->u.coord.x;
                    obs[i]->ps[sides - j - 1].y = polyp.y + n->u.coord.y;
                }

                i++;
            }
        }

    } else {
        obs = 0;

        for (n = agfstnode(g); n; n = agnxtnode(g,n)) {
            n->u.coord.x = POINTS(n->u.pos[0]);
            n->u.coord.y = POINTS(n->u.pos[1]);
        }
    }

    npoly = i;

    if (obs && NOT(Plegal_arrangement(obs,npoly))) {
        if (Verbose)
            errorPrintf(stderr,"nodes touch - falling back to straight line edges\n");

        vconfig = 0;

    } else {
        if (obs)
            vconfig = Pobsopen(obs,npoly);
        else
            vconfig = 0;
    }

    /* route edges  */
    if (Verbose)
        errorPrintf(stderr,"Creating edges using %s\n",
                    (vconfig ? "splines" : "line segments"));

    if (vconfig) {
        /* path-finding pass */

        for (n = agfstnode(g); n; n = agnxtnode(g,n)) {
            for (e = agfstout(g,n); e; e = agnxtout(g,e)) {
                Ppolyline_t line;
                int			pp, qp;

                p = mkPoint(coord(e->tail));
                q = mkPoint(coord(e->head));

                /* determine the polygons (if any) that contain the endpoints */
                pp = qp = POLYID_NONE;

                for (i = 0; i < npoly; i++) {
                    if ((pp == POLYID_NONE) && in_poly(*obs[i], p))
                        pp = i;

                    if ((qp == POLYID_NONE) && in_poly(*obs[i], q))
                        qp = i;
                }

                /*Pobspath(vconfig, p, POLYID_UNKNOWN, q, POLYID_UNKNOWN, &line);*/
                Pobspath(vconfig, p, pp, q, qp, &line);

                e->u.path = line;
            }
        }
    }

    if (Nop > 1) {
        symptr = agfindattr(g->proto->e,"pos");

        if (PSinputscale) {
            offset.x *= PSinputscale;
            offset.y *= PSinputscale;
        }
    }

    /* spline-drawing pass */
    for (n = agfstnode(g); n; n = agnxtnode(g,n)) {
        for (e = agfstout(g,n); e; e = agnxtout(g,e)) {

            p = mkPoint(coord(e->tail));
            q = mkPoint(coord(e->head));

            if ((Nop > 1) &&
                user_spline(symptr,e,&offset,doScale,&scalef)) {}
            else if (vconfig && (e->tail != e->head)) {
                Ppolyline_t line, spline;
                Pvector_t	slopes[2];
                int			n_barriers;
                int			pp, qp;
                point		*ispline;

                /* determine the polygons (if any) that contain the endpoints */
                pp = qp = POLYID_NONE;

                for (i = 0; i < npoly; i++) {
                    if ((pp == POLYID_NONE) && in_poly(*obs[i], p))
                        pp = i;

                    if ((qp == POLYID_NONE) && in_poly(*obs[i], q))
                        qp = i;
                }

                line = e->u.path;

                make_barriers(obs, npoly, pp, qp, &barriers, &n_barriers);
                slopes[0].x = slopes[0].y = 0.0;
                slopes[1].x = slopes[1].y = 0.0;
                Proutespline (barriers, n_barriers, line, slopes, &spline);

                /* north why did you ever use int coords */
                ispline = malloc(sizeof(point)*spline.pn);

                for (i = 0; i < spline.pn; i++) {
                    ispline[i].x = ROUND(spline.ps[i].x);
                    ispline[i].y = ROUND(spline.ps[i].y);
                }

                if (Verbose > 1)
                    errorPrintf(stderr,"spline %s %s\n",e->tail->name,e->head->name);

                clip_and_install(e,ispline,spline.pn);

                free(ispline);

                free(barriers);

            } else {
                dumb[0] = coord(e->tail);
                dumb[3] = coord(e->head);

                if (e->tail != e->head) {
                    d = sub_points(dumb[3],dumb[0]);
                    d.x = d.x / 3;
                    d.y = d.y / 3;
                    dumb[1] = add_points(dumb[0],d);
                    dumb[2] = sub_points(dumb[3],d);

                } else {	/* self arc */
                    d.x = e->head->u.rw + POINTS(.66 * SEP);
                    d.y = 0;
                    dumb[1] = dumb[2] = add_points(dumb[0],d);
                    dumb[1].y += d.x;
                    dumb[2].y -= d.x;
                }

                clip_and_install(e,dumb,4);
            }

            if (e->u.label) {
                d.x = (q.x + p.x)/ 2;
                d.y = (p.y + q.y)/ 2;

                if (abs(p.x - q.x) > abs(p.y - q.y)) {
                    ld.x = 0;
                    ld.y = POINTS(e->u.label->dimen.y)/2 + 2;

                } else {
                    ld.x = POINTS(e->u.label->dimen.y)/2+e->u.label->fontsize;
                    ld.y = 0;
                }

                d = add_points(d,ld);
                e->u.label->p = d;
            }
        }
    }

    /* g->u.bb.UR = sub_points(g->u.bb.UR,g->u.bb.LL); */
    /* g->u.bb.LL = pointof(0,0); */
    neato_compute_bb(g);

    /* vladimir: place port labels */
    if (E_headlabel || E_taillabel)
        for (n = agfstnode(g); n; n = agnxtnode(g,n)) {
            if (E_headlabel)
                for (e = agfstin(g,n); e; e = agnxtin(g,e))
                    if (e->u.head_label)
                        place_portlabel (e, TRUE);

            if (E_taillabel)
                for (e = agfstout(g,n); e; e = agnxtout(g,e))
                    if (e->u.tail_label)
                        place_portlabel (e, FALSE);
        }

    /* end vladimir */
    State = GVSPLINES;
}

void spline_edges(graph_t *g) {
    node_t		*n;
    pointf		offset;

    neato_compute_bb(g);
    offset = cvt2ptf(g->u.bb.LL);

    for (n = agfstnode(g); n; n = agnxtnode(g,n)) {
        n->u.pos[0] -= offset.x;
        n->u.pos[1] -= offset.y;
    }

    g->u.bb.UR.x -= g->u.bb.LL.x;
    g->u.bb.UR.y -= g->u.bb.LL.y;
    g->u.bb.LL.x = 0;
    g->u.bb.LL.y = 0;
    spline_edges0(g,offset);
}


static int neato_spline_merge(node_t *n) {
    return FALSE;
}

static void neato_arrow_clip (Agedge_t *fe,Agedge_t *le,point *ps,int *startp,int *endp,bezier *spl) {
    edge_t *e;
    int i, j, sflag, eflag;

    for (e = fe; e->u.to_orig; e = e->u.to_orig)

        ;
    j = swap_ends_p(e);

    arrow_flags (e, &sflag, &eflag);

    if (neato_spline_merge (le->head))
        eflag = ARR_NONE;

    if (neato_spline_merge (fe->tail))
        sflag = ARR_NONE;

    if (j) {
        i=sflag;
        sflag=eflag;
        eflag=i;
    } /* swap the two ends */

    if (sflag)
        *startp = arrowStartClip (e, ps, *startp, *endp, spl, sflag);

    if (eflag)
        *endp = arrowEndClip (e, ps, *startp, *endp, spl, eflag);
}

static boolean swap_ends_p (edge_t *e) {
    return FALSE;
}


void clip_and_install (edge_t *e, point *ps, int  pn) {
    pointf 		p2;
    bezier 		*newspl;
    node_t		*tn, *hn;
    int 		start, end, i;
    point		pt;

    tn = e->tail;
    hn = e->head;
    newspl = new_spline (e, pn);
    /* spline may be interior to node */

    for (start = 0; start < pn - 4; start+=3) {
        pt = coord(tn);
        p2.x = ps[start+3].x - pt.x;
        p2.y = ps[start+3].y - pt.y;

        if (tn->u.shape == NULL)
            break;

        if (tn->u.shape->insidefn == NULL)
            break;

        if (tn->u.shape->insidefn (tn, p2, e) == FALSE)
            break;
    }

    neato_shape_clip (tn, &ps[start], e);

    for (end = pn - 4; end > 0; end -= 3) {
        pt = coord(hn);
        p2.x = ps[end].x - pt.x;
        p2.y = ps[end].y - pt.y;

        if (hn->u.shape == NULL)
            break;

        if (hn->u.shape->insidefn == NULL)
            break;

        if (hn->u.shape->insidefn (hn, p2, e) == FALSE)
            break;
    }

    neato_shape_clip (hn, &ps[end], e);

    for (; start < pn - 4; start+=3)
        if (ps[start].x != ps[start + 3].x || ps[start].y != ps[start + 3].y)
            break;

    for (; end > 0; end -= 3)
        if (ps[end].x != ps[end + 3].x || ps[end].y != ps[end + 3].y)
            break;

    neato_arrow_clip (e, e, ps, &start, &end, newspl);

    for (i = start; i < end + 4; i++)
        newspl->list[i - start] = ps[i];

    newspl->size = end - start + 4;
}

void neato_shape_clip (node_t *n, point curve[4], edge_t *e) {
    int			i;
    boolean		found, inside, left_inside;
    pointf		pt, opt, c[4], seg[4], best[4], *left, *right;
    point		p;
    double		low, high, t;

    if (n->u.shape == NULL)
        return;

    if (n->u.shape->insidefn == NULL)
        return;

    for (i = 0; i < 4; i++) {
        p = coord(n);
        c[i].x = curve[i].x - p.x;
        c[i].y = curve[i].y - p.y;
    }

    left_inside = n->u.shape->insidefn (n, c[0], e);

    if (left_inside)
        left = NULL, right = seg;
    else
        left = seg, right = NULL;

    found = FALSE;

    low = 0.0;

    high = 1.0;

    if (left_inside)
        pt = c[0];
    else
        pt = c[3];

    do {
        opt = pt;
        t = (high + low) / 2.0;
        pt = Bezier (c, 3, t, left, right);
        inside = n->u.shape->insidefn (n, pt, e);

        if (inside == FALSE) {
            for (i = 0; i < 4; i++)
                best[i] = seg[i];

            found = TRUE;
        }

        if (inside == left_inside)
            low = t;
        else
            high = t;

    } while (ABS (opt.x - pt.x) > .5 || ABS (opt.y - pt.y) > .5);

    if (found == FALSE)
        for (i = 0; i < 4; i++)
            best[i] = seg[i];

    for (i = 0; i < 4; i++) {
        p = coord(n);
        curve[i].x = ROUND(best[i].x + p.x);
        curve[i].y = ROUND(best[i].y + p.y);
    }
}

/* scaleBB:
 * Scale bounding box of clusters of g by given factors.
 */
static void
scaleBB(graph_t *g, double xf, double yf) {
    int		i;

    g->u.bb.UR.x *= xf;
    g->u.bb.UR.y *= yf;
    g->u.bb.LL.x *= xf;
    g->u.bb.LL.y *= yf;

    for (i = 1; i <= g->u.n_cluster; i++)
        scaleBB(g->u.clust[i],xf,yf);
}

/* neato_set_aspect;
 * Assume all bounding boxes are correct and
 * that g->u.bb.LL is at origin.
 */
static int
neato_set_aspect(graph_t *g, pointf* pf) {
    int		i;
    double	xf,yf,actual,desired;
    char	*str;
    node_t	*n;

    /* neato_compute_bb(g); */

    if (/* (g->u.maxrank > 0) && */(str = agget(g,"ratio"))) {
        /* normalize */
        assert (g->u.bb.LL.x == 0);
        assert (g->u.bb.LL.y == 0);
        /* g->u.bb.UR.x -= g->u.bb.LL.x; */
        /* g->u.bb.UR.y -= g->u.bb.LL.y; */

        if (g->u.left_to_right) {
            int t = g->u.bb.UR.x;
            g->u.bb.UR.x = g->u.bb.UR.y;
            g->u.bb.UR.y = t;
        }

        if (strcmp(str,"fill") == 0) {
            /* fill is weird because both X and Y can stretch */

            if (g->u.drawing->size.x <= 0)
                return 0;

            xf = (double)g->u.drawing->size.x / (double)g->u.bb.UR.x;

            yf = (double)g->u.drawing->size.y / (double)g->u.bb.UR.y;

            if ((xf < 1.0) || (yf < 1.0)) {
                if (xf < yf) {
                    yf = yf / xf;
                    xf = 1.0;

                } else {
                    xf = xf / yf;
                    yf = 1.0;
                }
            }

        } else {
            desired = atof(str);

            if (desired == 0.0)
                return 0;

            actual = ((double)g->u.bb.UR.y)/((double)g->u.bb.UR.x);

            if (actual < desired) {
                yf = desired/actual;
                xf = 1.0;

            } else {
                xf = actual/desired;
                yf = 1.0;
            }
        }

        if (g->u.left_to_right) {
            double t = xf;
            xf = yf;
            yf = t;
        }

        /* Not relying on neato_nlist here allows us not to have to
               * allocate it in the root graph and the connected components. 
         */
        for (n = agfstnode(g); n; n = agnxtnode(g,n)) {
            /* for (i = 0; (n = g->u.neato_nlist[i]); i++) { */
            n->u.pos[0] = n->u.pos[0] * xf;
            n->u.pos[1] = n->u.pos[1] * yf;
        }

        scaleBB (g, xf, yf);
        pf->x = xf;
        pf->y = yf;
        return 1;

    } else
        return 0;
}

/* vladimir */
static void place_portlabel (edge_t *e, boolean head_p)
/* place the {head,tail}label (depending on HEAD_P) of edge E */
{
    textlabel_t *l;
    splines *spl;
    bezier *bez;
    double dist, angle;
    point p;
    pointf c[4], pf;
    int i;

    l = head_p ? e->u.head_label : e->u.tail_label;

    if (swap_ends_p(e))
        head_p = !head_p;

    spl = getsplinepoints(e);

    if (!head_p) {
        bez = &spl->list[0];

        if (bez->sflag != ARR_NONE
                && bez->sflag != ARR_OPEN
                && bez->sflag != ARR_HALFOPEN) {
            p = bez->sp;
            P2PF(bez->list[0],pf);

        } else {
            p = bez->list[0];

            for (i=0; i<4; i++)
                P2PF(bez->list[i], c[i]);

            pf = Bezier (c, 3, 0.1, NULL, NULL);
        }

    } else {
        bez = &spl->list[spl->size-1];

        if (bez->eflag != ARR_NONE
                && bez->eflag != ARR_OPEN
                && bez->eflag != ARR_HALFOPEN) {
            p = bez->ep;
            P2PF(bez->list[bez->size-1],pf);

        } else {
            p = bez->list[bez->size-1];

            for (i=0; i<4; i++)
                P2PF(bez->list[bez->size-4+i], c[i]);

            pf = Bezier (c, 3, 0.9, NULL, NULL);
        }
    }

    angle = atan2 (pf.y-p.y, pf.x-p.x) +
            RADIANS(late_double(e,E_labelangle,PORT_LABEL_ANGLE,-180.0));
    dist = PORT_LABEL_DISTANCE * late_double(e,E_labeldistance,1.0,0.0);
    l->p.x = p.x + ROUND(dist * cos(angle));
    l->p.y = p.y + ROUND(dist * sin(angle));
}

static splines *getsplinepoints (edge_t* e) {
    splines *sp;

    sp = e->u.spl;

    if (sp == NULL)
        abort ();

    return sp;
}

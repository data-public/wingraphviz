# Microsoft Developer Studio Generated NMAKE File, Based on libneato.dsp
!IF "$(CFG)" == ""
CFG=libneato - Win32 Debug
!MESSAGE No configuration specified. Defaulting to libneato - Win32 Debug.
!ENDIF 

!IF "$(CFG)" != "libneato - Win32 Release" && "$(CFG)" != "libneato - Win32 Debug"
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "libneato.mak" CFG="libneato - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "libneato - Win32 Release" (based on "Win32 (x86) Static Library")
!MESSAGE "libneato - Win32 Debug" (based on "Win32 (x86) Static Library")
!MESSAGE 
!ERROR An invalid configuration is specified.
!ENDIF 

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE 
NULL=nul
!ENDIF 

CPP=cl.exe
RSC=rc.exe

!IF  "$(CFG)" == "libneato - Win32 Release"

OUTDIR=.\Release
INTDIR=.\Release

ALL : "..\..\lib\Release\libneato.lib"


CLEAN :
	-@erase "$(INTDIR)\adjust.obj"
	-@erase "$(INTDIR)\circuit.obj"
	-@erase "$(INTDIR)\edges.obj"
	-@erase "$(INTDIR)\find_ints.obj"
	-@erase "$(INTDIR)\geometry.obj"
	-@erase "$(INTDIR)\heap.obj"
	-@erase "$(INTDIR)\hedges.obj"
	-@erase "$(INTDIR)\info.obj"
	-@erase "$(INTDIR)\init.obj"
	-@erase "$(INTDIR)\intersect.obj"
	-@erase "$(INTDIR)\legal.obj"
	-@erase "$(INTDIR)\lu.obj"
	-@erase "$(INTDIR)\matinv.obj"
	-@erase "$(INTDIR)\memory.obj"
	-@erase "$(INTDIR)\poly.obj"
	-@erase "$(INTDIR)\printvis.obj"
	-@erase "$(INTDIR)\site.obj"
	-@erase "$(INTDIR)\solve.obj"
	-@erase "$(INTDIR)\splines.obj"
	-@erase "$(INTDIR)\stuff.obj"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(INTDIR)\voronoi.obj"
	-@erase "..\..\lib\Release\libneato.lib"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP_PROJ=/nologo /MT /W3 /GX /O1 /I ".." /I "..\.." /I "..\..\pathplan" /I "..\..\cdt" /I "..\..\graph" /I "..\common" /I "..\pack" /I "..\..\tools\error" /D "NDEBUG" /D "WIN32" /D "_MBCS" /D "_LIB" /D "HAVE_CONFIG_H" /D "MSWIN32" /Fp"$(INTDIR)\libneato.pch" /YX /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /c 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\libneato.bsc" 
BSC32_SBRS= \
	
LIB32=link.exe -lib
LIB32_FLAGS=/nologo /out:"..\..\lib\Release\libneato.lib" 
LIB32_OBJS= \
	"$(INTDIR)\adjust.obj" \
	"$(INTDIR)\circuit.obj" \
	"$(INTDIR)\edges.obj" \
	"$(INTDIR)\find_ints.obj" \
	"$(INTDIR)\geometry.obj" \
	"$(INTDIR)\heap.obj" \
	"$(INTDIR)\hedges.obj" \
	"$(INTDIR)\info.obj" \
	"$(INTDIR)\init.obj" \
	"$(INTDIR)\intersect.obj" \
	"$(INTDIR)\legal.obj" \
	"$(INTDIR)\lu.obj" \
	"$(INTDIR)\matinv.obj" \
	"$(INTDIR)\memory.obj" \
	"$(INTDIR)\poly.obj" \
	"$(INTDIR)\printvis.obj" \
	"$(INTDIR)\site.obj" \
	"$(INTDIR)\solve.obj" \
	"$(INTDIR)\splines.obj" \
	"$(INTDIR)\stuff.obj" \
	"$(INTDIR)\voronoi.obj"

"..\..\lib\Release\libneato.lib" : "$(OUTDIR)" $(DEF_FILE) $(LIB32_OBJS)
    $(LIB32) @<<
  $(LIB32_FLAGS) $(DEF_FLAGS) $(LIB32_OBJS)
<<

!ELSEIF  "$(CFG)" == "libneato - Win32 Debug"

OUTDIR=.\Debug
INTDIR=.\Debug

ALL : "..\..\lib\Debug\libneato.lib"


CLEAN :
	-@erase "$(INTDIR)\adjust.obj"
	-@erase "$(INTDIR)\circuit.obj"
	-@erase "$(INTDIR)\edges.obj"
	-@erase "$(INTDIR)\find_ints.obj"
	-@erase "$(INTDIR)\geometry.obj"
	-@erase "$(INTDIR)\heap.obj"
	-@erase "$(INTDIR)\hedges.obj"
	-@erase "$(INTDIR)\info.obj"
	-@erase "$(INTDIR)\init.obj"
	-@erase "$(INTDIR)\intersect.obj"
	-@erase "$(INTDIR)\legal.obj"
	-@erase "$(INTDIR)\lu.obj"
	-@erase "$(INTDIR)\matinv.obj"
	-@erase "$(INTDIR)\memory.obj"
	-@erase "$(INTDIR)\poly.obj"
	-@erase "$(INTDIR)\printvis.obj"
	-@erase "$(INTDIR)\site.obj"
	-@erase "$(INTDIR)\solve.obj"
	-@erase "$(INTDIR)\splines.obj"
	-@erase "$(INTDIR)\stuff.obj"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(INTDIR)\vc60.pdb"
	-@erase "$(INTDIR)\voronoi.obj"
	-@erase "..\..\lib\Debug\libneato.lib"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP_PROJ=/nologo /MTd /W3 /Gm /GX /ZI /Od /I ".." /I "..\.." /I "..\..\pathplan" /I "..\..\cdt" /I "..\..\graph" /I "..\common" /I "..\pack" /I "..\..\tools\error" /D "_DEBUG" /D "WIN32" /D "_MBCS" /D "_LIB" /D "HAVE_CONFIG_H" /D "MSWIN32" /Fp"$(INTDIR)\libneato.pch" /YX /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /GZ /c 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\libneato.bsc" 
BSC32_SBRS= \
	
LIB32=link.exe -lib
LIB32_FLAGS=/nologo /out:"..\..\lib\Debug\libneato.lib" 
LIB32_OBJS= \
	"$(INTDIR)\adjust.obj" \
	"$(INTDIR)\circuit.obj" \
	"$(INTDIR)\edges.obj" \
	"$(INTDIR)\find_ints.obj" \
	"$(INTDIR)\geometry.obj" \
	"$(INTDIR)\heap.obj" \
	"$(INTDIR)\hedges.obj" \
	"$(INTDIR)\info.obj" \
	"$(INTDIR)\init.obj" \
	"$(INTDIR)\intersect.obj" \
	"$(INTDIR)\legal.obj" \
	"$(INTDIR)\lu.obj" \
	"$(INTDIR)\matinv.obj" \
	"$(INTDIR)\memory.obj" \
	"$(INTDIR)\poly.obj" \
	"$(INTDIR)\printvis.obj" \
	"$(INTDIR)\site.obj" \
	"$(INTDIR)\solve.obj" \
	"$(INTDIR)\splines.obj" \
	"$(INTDIR)\stuff.obj" \
	"$(INTDIR)\voronoi.obj"

"..\..\lib\Debug\libneato.lib" : "$(OUTDIR)" $(DEF_FILE) $(LIB32_OBJS)
    $(LIB32) @<<
  $(LIB32_FLAGS) $(DEF_FLAGS) $(LIB32_OBJS)
<<

!ENDIF 

.c{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<


!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("libneato.dep")
!INCLUDE "libneato.dep"
!ELSE 
!MESSAGE Warning: cannot find "libneato.dep"
!ENDIF 
!ENDIF 


!IF "$(CFG)" == "libneato - Win32 Release" || "$(CFG)" == "libneato - Win32 Debug"
SOURCE=.\adjust.c

"$(INTDIR)\adjust.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\circuit.c

"$(INTDIR)\circuit.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\edges.c

"$(INTDIR)\edges.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\find_ints.c

"$(INTDIR)\find_ints.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\geometry.c

"$(INTDIR)\geometry.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\heap.c

"$(INTDIR)\heap.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\hedges.c

"$(INTDIR)\hedges.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\info.c

"$(INTDIR)\info.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\init.c

"$(INTDIR)\init.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\intersect.c

"$(INTDIR)\intersect.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\legal.c

"$(INTDIR)\legal.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\lu.c

"$(INTDIR)\lu.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\matinv.c

"$(INTDIR)\matinv.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\memory.c

"$(INTDIR)\memory.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\poly.c

"$(INTDIR)\poly.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\printvis.c

"$(INTDIR)\printvis.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\site.c

"$(INTDIR)\site.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\solve.c

"$(INTDIR)\solve.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\splines.c

"$(INTDIR)\splines.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\stuff.c

"$(INTDIR)\stuff.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\voronoi.c

"$(INTDIR)\voronoi.obj" : $(SOURCE) "$(INTDIR)"



!ENDIF 


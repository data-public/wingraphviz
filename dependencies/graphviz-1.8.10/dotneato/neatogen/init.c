/*
    This software may only be used by you under license from AT&T Corp.
    ("AT&T").  A copy of AT&T's Source Code Agreement is available at
    AT&T's Internet website having the URL:
    <http://www.research.att.com/sw/tools/graphviz/license/source.html>
    If you received this software without first entering into a license
    with AT&T, you have an infringing copy of this software and cannot use
    it without violating AT&T's intellectual property rights.
*/

#include	"error.h"
#include	"neato.h"
#include	"pack.h"

#ifdef DMALLOC
#include "dmalloc.h"
#endif

static int Pack;    /* If >= 0, layout components separately and pack together
                     * The value of Pack gives margins around graphs.
                     */
static char*  cc_pfx = "_neato_cc";

void neato_nodesize(node_t* n, boolean flip) {
    int         w;

    w = n->u.xsize = POINTS(n->u.width);
    n->u.lw  = n->u.rw = w / 2;
    n->u.ht = n->u.ysize = POINTS(n->u.height);
}

void neato_init_node(node_t* n) {
    common_init_node(n);
    n->u.pos = ALLOC(n->graph->u.ndim,0,double);
    neato_nodesize(n,n->graph->u.left_to_right);
}

void neato_init_edge(edge_t* e) {
    common_init_edge(e);

    e->u.factor = late_double(e,E_weight,1.0,1.0);

    init_port(e->tail,e,agget(e,"tailport"));
    init_port(e->head,e,agget(e,"headport"));
}

void neato_init_node_edge(graph_t *g) {
    node_t *n;
    edge_t *e;

    for (n = agfstnode(g); n; n = agnxtnode(g,n))
        neato_init_node(n);

    for (n = agfstnode(g); n; n = agnxtnode(g,n)) {
        for (e = agfstout(g,n); e; e = agnxtout(g,e))
            neato_init_edge(e);
    }
}

int init_port(node_t* n, edge_t* e, char* name) {
    port_t	port;

    if (name == NULL)
        return FALSE;

    port = n->u.shape->portfn(n,name);

#ifdef NOTDEF

    if (n->graph->u.left_to_right)
        port.p = invflip_pt(port.p);

#endif

    port.order = 0;

    if (e->tail == n)
        e->u.tail_port = port;
    else
        e->u.head_port = port;

    return TRUE;
}

void neato_cleanup_node(node_t* n) {
    if (n->u.shape)
        n->u.shape->freefn(n);

    free (n->u.pos);

    free_label(n->u.label);

    memset(&(n->u),0,sizeof(Agnodeinfo_t));
}

void neato_free_splines(edge_t* e) {
    int		i;

    if (e->u.spl) {
        for (i = 0; i < e->u.spl->size; i++)
            free(e->u.spl->list[i].list);

        free(e->u.spl->list);

        free(e->u.spl);
    }

    e->u.spl = NULL;
}

void neato_cleanup_edge(edge_t* e) {
    neato_free_splines(e);
    free_label(e->u.label);
    memset(&(e->u),0,sizeof(Agedgeinfo_t));
}

void neato_cleanup_graph(graph_t* g) {
    if (Nop || (Pack < 0))
        free_scan_graph(g);
    else {
        Agraph_t* mg;
        Agedge_t* me;
        Agnode_t* mn;
        Agraph_t* subg;
        int       slen = strlen (cc_pfx);

        mg = g->meta_node->graph;

        for (me = agfstout(mg,g->meta_node); me; me = agnxtout(mg,me)) {
            mn = me->head;
            subg = agusergraph(mn);

            if (strncmp(subg->name,cc_pfx,slen) == 0)
                free_scan_graph (subg);
        }
    }

    free_ugraph(g);
    free_label(g->u.label);
    memset(&(g->u),0,sizeof(Agraphinfo_t));
}

void neato_cleanup(graph_t* g) {
    node_t  *n;
    edge_t  *e;

    for (n = agfstnode(g); n; n = agnxtnode(g, n)) {
        for (e = agfstedge(g, n); e; e = agnxtedge(g, e, n)) {
            neato_cleanup_edge(e);
        }

        neato_cleanup_node(n);
    }

    neato_cleanup_graph(g);
    emit_reset(g);
    agclose (g);
}

void neato_layout(Agraph_t *g) {
    int         nG;
    attrsym_t*  sym;

    /* setting rankdir=LR is currently undefined in neato,
     * but having it set causes has effects in the common routines.
     * So, we turn it off.
     */
    sym = agfindattr(g,"rankdir");

    if (sym)
        agxset (g, sym->index, "");

    graph_init(g);

    g->u.ndim = late_int(g,agfindattr(g,"dim"),2,2);

    Ndim = g->u.ndim = MIN(g->u.ndim,MAXDIM);

    g->u.drawing->engine = NEATO;

    neato_init_node_edge(g);

    if (Nop) {
        nG = scan_graph(g);
        initial_positions(g, nG);
        adjustNodes(g);
        spline_edges(g);

    } else {
        char *p;

        if ((p = agget(g,"pack"))) {
            if (sscanf(p,"%d",&Pack) != 1) {
                if ((*p == 't') || (*p == 'T'))
                    Pack = CL_OFFSET;
                else
                    Pack = -1;
            }

        } else
            Pack = -1;

        p = agget(g,"model");

        if (Pack >= 0) {
            graph_t*    gc;
            graph_t**   cc;
            int         n_cc;
            int         n_n;
            int         i;
            int         useCircuit;

            useCircuit = (p && (streq(p,"circuit")));
            cc = ccomps (g, &n_cc, cc_pfx);

            for (i = 0; i < n_cc; i++) {
                gc = cc[i];
                nodeInduce (gc);
                n_n = scan_graph(gc);

                if (useCircuit)
                    circuit_model(gc,n_n);
                else
                    shortest_path(gc, n_n);

                initial_positions(gc, n_n);

                diffeq_model(gc, n_n);

                solve_model(gc, n_n);

                final_energy(gc, n_n);

                adjustNodes(gc);
            }

            packGraphs (n_cc, cc, 0, Pack, 0);
            neato_compute_bb (g);
            spline_edges(g);

        } else {
            nG = scan_graph(g);

            if (p && (streq(p,"circuit"))) {
                if (!circuit_model(g,nG)) {
                    errorPrintf (stderr, "Warning: graph %s is disconnected. In this case, the circuit model\n", g->name);
                    errorPrintf (stderr, "is undefined and neato is reverting to the shortest path model.\n");
                    errorPrintf (stderr, "Alternatively, consider running neato using -Gpack=true or decomposing\n");
                    errorPrintf (stderr, "the graph into connected components.\n");

                    shortest_path(g, nG);
                }

            } else
                shortest_path(g, nG);

            initial_positions(g, nG);

            diffeq_model(g, nG);

            solve_model(g, nG);

            final_energy(g, nG);

            adjustNodes(g);

            spline_edges(g);
        }
    }

    dotneato_postprocess(g, neato_nodesize);
}

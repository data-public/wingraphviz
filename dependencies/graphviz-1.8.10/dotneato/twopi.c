/*
    This software may only be used by you under license from AT&T Corp.
    ("AT&T").  A copy of AT&T's Source Code Agreement is available at
    AT&T's Internet website having the URL:
    <http://www.research.att.com/sw/tools/graphviz/license/source.html>
    If you received this software without first entering into a license
    with AT&T, you have an infringing copy of this software and cannot use
    it without violating AT&T's intellectual property rights.
*/


/*
 * Written by Emden R. Gansner
 * Derived from Graham Wills' algorithm described in GD'97.
 */

#include    "circle.h"
#ifdef HAVE_CONFIG_H
#include "gvconfig.h"
#endif
#include        <time.h>
#ifdef HAVE_UNISTD_H
#include	<unistd.h>
#endif
#ifdef MSWIN32
#include	"stamp.h"
#endif

#ifdef DMALLOC
#include "dmalloc.h"
#endif

char *Info[] = {
                   "twopi",            /* Program */
                   "VERSION",            /* Version */
                   "DATE"                /* Build Date */
               };

unsigned int DOT_CODEPAGE;

static graph_t *G;

#ifndef MSWIN32
static void intr(int s) {
    if (G)
        dotneato_write(G);

    dotneato_terminate();

    exit(1);
}

#endif

int main (int argc, char** argv) {
    static graph_t* prev;

    DOT_CODEPAGE = 951;

    dotneato_initialize (argc, argv);
#ifndef MSWIN32

    signal (SIGUSR1, toggle);
    signal (SIGINT, intr);
#endif

    while ((G = next_input_graph())) {
        if (prev)
            twopi_cleanup(prev);

        prev = G;

        twopi_layout(G);

        dotneato_write(G);
    }

    dotneato_terminate();
    return 1;
}

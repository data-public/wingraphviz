# Microsoft Developer Studio Generated NMAKE File, Based on libpack.dsp
!IF "$(CFG)" == ""
CFG=libpack - Win32 Debug
!MESSAGE No configuration specified. Defaulting to libpack - Win32 Debug.
!ENDIF 

!IF "$(CFG)" != "libpack - Win32 Release" && "$(CFG)" != "libpack - Win32 Debug"
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "libpack.mak" CFG="libpack - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "libpack - Win32 Release" (based on "Win32 (x86) Static Library")
!MESSAGE "libpack - Win32 Debug" (based on "Win32 (x86) Static Library")
!MESSAGE 
!ERROR An invalid configuration is specified.
!ENDIF 

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE 
NULL=nul
!ENDIF 

CPP=cl.exe
RSC=rc.exe

!IF  "$(CFG)" == "libpack - Win32 Release"

OUTDIR=.\Release
INTDIR=.\Release

ALL : "..\..\lib\Release\libpack.lib"


CLEAN :
	-@erase "$(INTDIR)\ccomps.obj"
	-@erase "$(INTDIR)\pack.obj"
	-@erase "$(INTDIR)\pointset.obj"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "..\..\lib\Release\libpack.lib"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP_PROJ=/nologo /MT /W3 /GX /O1 /I "..\common" /I "..\..\pathplan" /I "..\..\graph" /I "..\..\cdt" /I "..\neatogen" /I "..\.." /I "..\..\tools\error" /D "NDEBUG" /D "WIN32" /D "_MBCS" /D "_LIB" /D "HAVE_CONFIG_H" /D "MSWIN32" /Fp"$(INTDIR)\libpack.pch" /YX /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /c 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\libpack.bsc" 
BSC32_SBRS= \
	
LIB32=link.exe -lib
LIB32_FLAGS=/nologo /out:"..\..\lib\Release\libpack.lib" 
LIB32_OBJS= \
	"$(INTDIR)\ccomps.obj" \
	"$(INTDIR)\pack.obj" \
	"$(INTDIR)\pointset.obj"

"..\..\lib\Release\libpack.lib" : "$(OUTDIR)" $(DEF_FILE) $(LIB32_OBJS)
    $(LIB32) @<<
  $(LIB32_FLAGS) $(DEF_FLAGS) $(LIB32_OBJS)
<<

!ELSEIF  "$(CFG)" == "libpack - Win32 Debug"

OUTDIR=.\Debug
INTDIR=.\Debug

ALL : "..\..\lib\Debug\libpack.lib"


CLEAN :
	-@erase "$(INTDIR)\ccomps.obj"
	-@erase "$(INTDIR)\pack.obj"
	-@erase "$(INTDIR)\pointset.obj"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(INTDIR)\vc60.pdb"
	-@erase "..\..\lib\Debug\libpack.lib"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP_PROJ=/nologo /MTd /W3 /Gm /GX /ZI /Od /I "..\common" /I "..\..\pathplan" /I "..\..\graph" /I "..\..\cdt" /I "..\neatogen" /I "..\.." /I "..\..\tools\error" /D "_DEBUG" /D "WIN32" /D "_MBCS" /D "_LIB" /D "HAVE_CONFIG_H" /D "MSWIN32" /Fp"$(INTDIR)\libpack.pch" /YX /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /GZ /c 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\libpack.bsc" 
BSC32_SBRS= \
	
LIB32=link.exe -lib
LIB32_FLAGS=/nologo /out:"..\..\lib\Debug\libpack.lib" 
LIB32_OBJS= \
	"$(INTDIR)\ccomps.obj" \
	"$(INTDIR)\pack.obj" \
	"$(INTDIR)\pointset.obj"

"..\..\lib\Debug\libpack.lib" : "$(OUTDIR)" $(DEF_FILE) $(LIB32_OBJS)
    $(LIB32) @<<
  $(LIB32_FLAGS) $(DEF_FLAGS) $(LIB32_OBJS)
<<

!ENDIF 

.c{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<


!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("libpack.dep")
!INCLUDE "libpack.dep"
!ELSE 
!MESSAGE Warning: cannot find "libpack.dep"
!ENDIF 
!ENDIF 


!IF "$(CFG)" == "libpack - Win32 Release" || "$(CFG)" == "libpack - Win32 Debug"
SOURCE=.\ccomps.c

"$(INTDIR)\ccomps.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\pack.c

"$(INTDIR)\pack.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\pointset.c

"$(INTDIR)\pointset.obj" : $(SOURCE) "$(INTDIR)"



!ENDIF 


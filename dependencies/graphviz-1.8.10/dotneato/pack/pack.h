/*
    This software may only be used by you under license from AT&T Corp.
    ("AT&T").  A copy of AT&T's Source Code Agreement is available at
    AT&T's Internet website having the URL:
    <http://www.research.att.com/sw/tools/graphviz/license/source.html>
    If you received this software without first entering into a license
    with AT&T, you have an infringing copy of this software and cannot use
    it without violating AT&T's intellectual property rights.
*/



#ifndef _PACK_H
#define _PACK_H 1

#include <graph.h>

extern point* putGraphs (int, Agraph_t**, Agraph_t*, int, int);
extern int shiftGraphs (int, Agraph_t**, point*, Agraph_t*, int);
extern int packGraphs (int, Agraph_t**, Agraph_t*, int, int);
extern int packSubgraphs (int, Agraph_t**, Agraph_t*, int, int);

extern int isConnected (Agraph_t*);
extern Agraph_t** ccomps (Agraph_t*, int*, char*);
extern int nodeInduce (Agraph_t*);

#endif


/*
    This software may only be used by you under license from AT&T Corp.
    ("AT&T").  A copy of AT&T's Source Code Agreement is available at
    AT&T's Internet website having the URL:
    <http://www.research.att.com/sw/tools/graphviz/license/source.html>
    If you received this software without first entering into a license
    with AT&T, you have an infringing copy of this software and cannot use
    it without violating AT&T's intellectual property rights.
*/


/* Module for packing disconnected graphs together.
 * Based on "Disconnected Graph Layout and the Polyomino Packing Approach", 
 * K. Freivalds et al., GD0'01, LNCS 2265, pp. 378-391.
 */

#include <stdlib.h>
#include "pointset.h"
#include "neatoprocs.h"
#include "pack.h"
#include <math.h>

#ifdef DMALLOC
#include "dmalloc.h"
#endif
#include "errorImp.h"

#define C 100   /* Max. avg. polyomino size */

#define MOVEPT(p) ((p).x += dx, (p).y += dy)
#define GRID(x,s) (((x) + ((s)-1)) / (s))
#define CELL(p,s) ((p).x = (p).x/(s), (p).y = ((p).y/(s)))
#define SGN(a)    (((a)<0)? -1 : 1)

typedef struct {
    Agraph_t*  graph;   /* related graph */
    int        perim;   /* half size of bounding rectangle perimeter */
    point*     cells;   /* cells in covering polyomino */
    int        nc;      /* no. of cells */
    int        index;   /* index in original array */
}

ginfo;

/* computeStep:
 * Compute grid step size. This is a root of the
 * quadratic equation al^2 +bl + c, where a, b and
 * c are defined below.
 */
static int
computeStep (int ng, Agraph_t** gs, int margin) {
    double l1, l2;
    double a, b, c, d, r;
    double W, H;          /* width and height of graph, with margin */
    Agraph_t* g;
    int       i;

    a = C*ng - 1;
    c = 0;
    b = 0;

    for (i=0;i<ng;i++) {
        g = gs[i];
        W = g->u.bb.UR.x - g->u.bb.LL.x + 2*margin;
        H = g->u.bb.UR.y - g->u.bb.LL.y + 2*margin;
        b -= (W + H);
        c -= (W * H);
    }

    d = b*b - 4.0*a*c;

    if (d < 0) {
        errorPrintf (stderr, "disc = %f ( < 0)\n", d);
        exit(1);
    }

    r = sqrt (d);
    l1 = (-b + r)/(2*a);
    l2 = (-b - r)/(2*a);

    if (Verbose > 2) {
        errorPrintf (stderr, "Packing: compute grid size\n");
        errorPrintf (stderr, "a %f b %f c %f d %f r %f\n", a, b, c,d,r);
        errorPrintf (stderr, "root %d (%f) %d (%f)\n", (int)l1, l1, (int)l2, l2);
        errorPrintf (stderr, " r1 %f r2 %f\n", a*l1*l1 + b*l1 + c, a*l2*l2 + b*l2 +c);
    }

    return (int)l1;
}

/* cmpf;
 * Comparison function for polyominoes.
 * Size is determined by perimeter.
 */
static int
cmpf (const void* X, const void* Y) {
    ginfo* x = *(ginfo**)X;
    ginfo* y = *(ginfo**)Y;
    /* flip order to get descending array */
    return (y->perim - x->perim);
}

/* fillLine:
 * Mark cells crossed by line from cell p to cell q.
 * Bresenham's algorithm, from Graphics Gems I, pp. 99-100.
 */
/* static  */
void
fillLine (point p, point q, PointSet* ps) {
    int x1 = p.x;
    int y1 = p.y;
    int x2 = q.x;
    int y2 = q.y;
    int d, x, y, ax, ay, sx, sy, dx, dy;

    dx = x2-x1;
    ax = ABS(dx)<<1;
    sx = SGN(dx);
    dy = y2-y1;
    ay = ABS(dy)<<1;
    sy = SGN(dy);

    /* fprintf (stderr, "fillLine %d %d - %d %d\n", x1,y1,x2,y2); */
    x = x1;
    y = y1;

    if (ax>ay) {                /* x dominant */
        d = ay-(ax>>1);

        for (;;) {
            /* fprintf (stderr, "  addPS %d %d\n", x,y); */
            addPS(ps,x, y);

            if (x==x2)
                return;

            if (d>=0) {
                y += sy;
                d -= ax;
            }

            x += sx;
            d += ay;
        }

    } else {                      /* y dominant */
        d = ax-(ay>>1);

        for (;;) {
            /* fprintf (stderr, "  addPS %d %d\n", x,y); */
            addPS(ps, x, y);

            if (y==y2)
                return;

            if (d>=0) {
                x += sx;
                d -= ay;
            }

            y += sy;
            d += ax;
        }
    }
}

/* fillSpline:
 * It appears that spline_edges always has the start point at the
 * beginning and the end point at the end.
 */
static void
fillSpline (Agedge_t* e, PointSet* ps, int dx, int dy, int ssize) {
    int         j, k;
    bezier      bz;
    point pt, hpt;

    if (e->u.spl == NULL)
        return;

    for (j = 0; j < e->u.spl->size; j++) {
        bz = e->u.spl->list[j];

        if (bz.sflag) {
            pt = bz.sp;
            hpt = bz.list[0];
            k = 1;

        } else {
            pt = bz.list[0];
            hpt = bz.list[1];
            k = 2;
        }

        MOVEPT(pt);
        CELL(pt,ssize);
        MOVEPT(hpt);
        CELL(hpt,ssize);
        fillLine (pt,hpt,ps);

        for (; k < bz.size; k++) {
            pt = hpt;
            hpt = bz.list[k];
            MOVEPT(hpt);
            CELL(hpt,ssize);
            fillLine (pt,hpt,ps);
        }

        if (bz.eflag) {
            pt = hpt;
            hpt = bz.ep;
            MOVEPT(hpt);
            CELL(hpt,ssize);
            fillLine (pt,hpt,ps);
        }
    }

}

/* genPoly:
 * Generate polyomino info from graph.
 * We add all cells covered partially by the bounding box of the 
 * node. If doSplines is true, we assume splines have been computed for
 * edges and use the polyline determined by the control point. Otherwise,
 * we use each cell crossed by a straight edge between the head and tail.
 */
static void
genPoly (Agraph_t* root, Agraph_t* g, ginfo* info,
         int ssize, int margin, int doSplines) {
    PointSet* ps;
    int W, H;
    point LL, UR;
    point pt, hpt;
    point s2;
    Agraph_t* eg;   /* graph containing edges */
    Agnode_t* n;
    Agnode_t* h;
    Agedge_t* e;
    int x,y;
    int dx,dy;

    if (root)
        eg = root;
    else
        eg = g;

    ps = newPS();

    dx = - g->u.bb.LL.x;

    dy = - g->u.bb.LL.y;

    for (n = agfstnode(g); n; n = agnxtnode(g,n)) {
        pt = coord(n);
        MOVEPT(pt);
        s2.x = margin + n->u.xsize/2+1;
        s2.y = margin + n->u.ysize/2+1;
        LL = sub_points(pt,s2);
        UR = add_points(pt,s2);
        CELL (LL,ssize);
        CELL (UR,ssize);

        for (x = LL.x; x <= UR.x; x++)
            for (y = LL.y; y <= UR.y; y++)
                addPS(ps,x,y);

        CELL(pt,ssize);

        for (e = agfstout(eg,n); e; e = agnxtout(eg,e)) {
            if (doSplines)
                fillSpline (e, ps, dx, dy, ssize);
            else {
                h = e->head;
                hpt = coord(h);
                MOVEPT(hpt);
                CELL(hpt,ssize);
                fillLine (pt,hpt,ps);
            }
        }
    }

    info->graph = g;
    info->cells = pointsOf(ps);
    info->nc = sizeOf(ps);
    W = GRID(g->u.bb.UR.x - g->u.bb.LL.x + 2*margin,ssize);
    H = GRID(g->u.bb.UR.y - g->u.bb.LL.y + 2*margin,ssize);
    info->perim = W + H;

    if (Verbose > 2) {
        int i;
        errorPrintf (stderr, "%s no. cells %d W %d H %d\n", g->name, info->nc, W, H);

        for (i = 0; i < info->nc; i++)
            errorPrintf (stderr, "  %d %d cell\n", info->cells[i].x, info->cells[i].y);
    }

    freePS(ps);
}

/* fits:
 * Check if polyomino fits at given point.
 * If so, add cells to pointset, store point in place and return true.
 */
static int
fits (int x, int y, ginfo* info, PointSet* ps, point* place, int step) {
    point* cells = info->cells;
    int    n = info->nc;
    point  cell;
    int    i;

    for (i=0; i < n; i++) {
        cell = *cells;
        cell.x += x;
        cell.y += y;

        if (inPS(ps,cell))
            return 0;

        cells++;
    }

    place->x = step*x;
    place->y = step*y;

    cells = info->cells;

    for (i=0; i < n; i++) {
        cell = *cells;
        cell.x += x;
        cell.y += y;
        insertPS(ps,cell);
        cells++;
    }

    if (Verbose >= 2)
        errorPrintf (stderr, "cc (%d cells) at (%d,%d)\n", n, place->x, place->y);

    return 1;
}

/* placeGraph:
 * Search for points on concentric "circles" out
 * from the origin. Check if polyomino can be placed
 * with bounding box origin at point.
 * First graph (i == 0) is centered on the origin.
 */
static void
placeGraph (int i, ginfo* info, PointSet* ps, point* place, int step, int margin) {
    int  x,y;
    int  W,H;
    int  bnd;

    if (i == 0) {
        Agraph_t*  g = info->graph;
        W = GRID(g->u.bb.UR.x - g->u.bb.LL.x + 2*margin,step);
        H = GRID(g->u.bb.UR.y - g->u.bb.LL.y + 2*margin,step);
        fits(-W/2,-H/2,info,ps,place,step);
        return;
    }

    if (fits(0,0,info,ps,place,step))
        return;

    W = info->graph->u.bb.UR.x - info->graph->u.bb.LL.x;

    H = info->graph->u.bb.UR.y - info->graph->u.bb.LL.y;

    if (W >= H) {
        for (bnd = 1; ; bnd++) {
            x = 0;
            y = -bnd;

            for (; x < bnd; x++)
                if (fits(x,y,info,ps,place,step))
                    return;

            for (; y < bnd; y++)
                if (fits(x,y,info,ps,place,step))
                    return;

            for (; x > -bnd; x--)
                if (fits(x,y,info,ps,place,step))
                    return;

            for (; y > -bnd; y--)
                if (fits(x,y,info,ps,place,step))
                    return;

            for (; x < 0; x++)
                if (fits(x,y,info,ps,place,step))
                    return;
        }

    } else {
        for (bnd = 1; ; bnd++) {
            y = 0;
            x = -bnd;

            for (; y > -bnd; y--)
                if (fits(x,y,info,ps,place,step))
                    return;

            for (; x < bnd; x++)
                if (fits(x,y,info,ps,place,step))
                    return;

            for (; y < bnd; y++)
                if (fits(x,y,info,ps,place,step))
                    return;

            for (; x > -bnd; x--)
                if (fits(x,y,info,ps,place,step))
                    return;

            for (; y > 0; y--)
                if (fits(x,y,info,ps,place,step))
                    return;
        }
    }
}

/* putGraphs:
 *  Given a collection of graphs, reposition them in the plane
 *  to not overlap but pack "nicely".
 *   ng is the number of graphs
 *   gs is a pointer to an array of graph pointers
 *   root gives the graph containing the edges; if null, the function
 *     looks in each graph in gs for its edges
 *   margin gives the amount of extra space left around nodes in points
 *   If doSplines is true, edge splines have been computed; use these
 *     in calculating polyomino.
 *  Returns array of points to which graphs should be translated;
 *  the array needs to be freed;
 * Returns NULL if problem occur.
 * 
 * Depends on graph fields bb, node fields pos, xsize and ysize, and
 * edge field spl.
 */
point*
putGraphs (int ng, Agraph_t** gs, Agraph_t* root, int margin, int doSplines) {
    int      stepSize;
    ginfo*   info;
    ginfo**  sinfo;
    point*   places;
    Dict_t*  ps;
    int      i;

    if (ng == 0)
        return 0;

    /* update bounding box info for each graph */
    for (i = 0; i < ng; i++) {
        neato_compute_bb (gs[i]);

        if (Verbose > 2) {
            Agraph_t* g = gs[i];
            errorPrintf (stderr, "bb[%s] %d %d %d %d\n", g->name, g->u.bb.LL.x,
                         g->u.bb.LL.y, g->u.bb.UR.x, g->u.bb.UR.y);
        }
    }

    /* calculate grid size */
    if (margin < 0)
        margin = 0;

    stepSize = computeStep (ng, gs, margin);

    info = N_NEW(ng, ginfo);

    for (i=0; i < ng; i++) {
        info[i].index = i;
        genPoly (root, gs[i], info+i, stepSize, margin, doSplines);
    }

    /* sort */
    sinfo = N_NEW(ng, ginfo*);

    for (i=0; i < ng; i++) {
        sinfo[i] = info + i;
    }

    qsort(sinfo,ng,sizeof(ginfo*),cmpf);

    ps = newPS ();
    places = N_NEW(ng, point);

    for (i=0; i < ng; i++)
        placeGraph (i, sinfo[i], ps, places+(sinfo[i]->index), stepSize, margin);

    free (sinfo);

    for (i=0; i < ng; i++)
        free (info[i].cells);

    free (info);

    freePS (ps);

    if (Verbose > 1)
        for (i=0; i < ng; i++)
            errorPrintf (stderr, "pos[%d] %d %d\n", i, places[i].x, places[i].y);

    return places;
}

/* shiftEdge:
 * Translate all of the edge components by the given offset.
 */
static void
shiftEdge (Agedge_t* e, int dx, int dy) {
    int         j,k;
    bezier      bz;

    if (e->u.spl == NULL)
        return;

    for (j = 0; j < e->u.spl->size; j++) {
        bz = e->u.spl->list[j];

        for (k = 0; k < bz.size; k++)
            MOVEPT(bz.list[k]);

        if (bz.sflag)
            MOVEPT(e->u.spl->list[j].sp);

        if (bz.eflag)
            MOVEPT(e->u.spl->list[j].ep);
    }

    if (e->u.label)
        MOVEPT(e->u.label->p);

    if (e->u.head_label)
        MOVEPT(e->u.head_label->p);

    if (e->u.tail_label)
        MOVEPT(e->u.tail_label->p);
}

/* shiftGraphs:
 * Uses points computed from putGraphs to shift points and edges.
 * Always shifts pos.
 * If doSplines is true, assumes node position is also in coord, 
 * and edges and edge labels have been placed, so it shifts those.
 * If root is non-null, it is used to find edges.
 *
 * Depends on graph field bb, node field pos and coord, and edge field spl.
 */
int
shiftGraphs (int ng, Agraph_t** gs, point* pp, Agraph_t* root, int doSplines) {
    int       i;
    int       dx, dy;
    double    fx, fy;
    point     p;
    Agraph_t* g;
    Agraph_t* eg;
    Agnode_t* n;
    Agedge_t* e;

    if (ng <= 0)
        return abs(ng);

    for (i = 0; i < ng; i++) {
        g = gs[i];

        if (root)
            eg = root;
        else
            eg = g;

        p = pp[i];

        dx = p.x - g->u.bb.LL.x;

        dy = p.y - g->u.bb.LL.y;

        fx = PS2INCH(dx);

        fy = PS2INCH(dy);

        for (n = agfstnode(g); n; n = agnxtnode(g,n)) {
            n->u.pos[0] += fx;
            n->u.pos[1] += fy;

            if (doSplines) {
                MOVEPT(n->u.coord);

                for (e = agfstout(eg,n); e; e = agnxtout(eg,e))
                    shiftEdge(e,dx,dy);
            }
        }
    }

    return 0;
}

/* packGraphs:
 * Packs graphs.
 *  ng - number of graphs
 *  gs - pointer to array of graphs
 *  root - graph used to find edges
 *  margin - margin added around nodes (in points)
 *  doSplines - if true, use already computed spline control points
 * This decides where to layout the graphs and repositions the graph's
 * position info.
 *
 * Returns 0 on success.
 */
int
packGraphs (int ng, Agraph_t** gs, Agraph_t* root, int margin, int doSplines) {
    int ret;
    point*  pp = putGraphs (ng, gs, root, margin, doSplines);

    if (!pp)
        return 1;

    ret = shiftGraphs(ng, gs, pp, root, doSplines);

    free (pp);

    return ret;
}

/* packSubgraphs:
 *  Packs subgraphs of given root graph, then recalculates root's bounding
 * box.
 * Note that it does not recompute subgraph bounding boxes.
 */
int
packSubgraphs (int ng, Agraph_t** gs, Agraph_t* root, int margin, int doSplines) {
    int ret;

    ret = packGraphs (ng,gs,root,margin,doSplines);
    neato_compute_bb (root);
    return ret;
}

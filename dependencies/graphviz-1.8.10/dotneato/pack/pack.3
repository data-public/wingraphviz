.TH LIBPACK 3 "01 MAY 2002"
.SH NAME
\fBlibpack\fR \- support for connected components
.SH SYNOPSIS
.ta .75i 1.5i 2.25i 3i 3.75i 4.5i 5.25i 6i
.PP
.nf
\f5
#include <graphviz/pack.h>

point*     putGraphs (int, Agraph_t**, Agraph_t*, int, int);
int        shiftGraphs (int, Agraph_t**, point*, Agraph_t*, int, int);
int        packGraphs (int, Agraph_t**, Agraph_t*, int, int);
int        packSubgraphs (int, Agraph_t**, Agraph_t*, int, int);

int        isConnected (Agraph_t*);
Agraph_t** ccomps (Agraph_t*, int*, char*);
int        nodeInduce (Agraph_t*);

\fP
.fi
.SH DESCRIPTION
\fIlibpack\fP supports the use of connected components in the
context of laying out graphs using other \fIgraphviz\fP libraries.
One set of functions can be used to take a single graph and
break it apart into connected components. A complementary set
of functions takes a collection of graphs (not necessarily components
of a single graph) which have been laid out separately, and packs them
together moderately tightly. The packing is done using the polyomino
algorithm of K. Freivalds et al.

As this library is meant to be used with \fIlibcommon\fP, it relies
on the \fIAgraphinfo_t\fP, \fIAgnodeinfo_t\fP and \fIAgedgeinfo_t\fP used 
in that
library. The specific dependencies are given below in the function
descriptions.

.SS "Creating components"
.PP
.SS "  Agraph_t** ccomps (Agraph_t* g, int* cnt, char* pfx)"
The function \fIccomps\fP takes a graph \fIg\fP and returns an array
of pointers to subgraphs of \fIg\fP which are its connected components.
\fIcnt\fP is set to the number of components. If \fIpfx\fP is non-NULL,
it is used as a prefix for the names of the subgraphs; otherwise, the
string ``_cc_'' is used.
Note that the subgraphs only contain the relevant nodes, not any
corresponding edges. Depending on the use, this allows the caller
to retrieve edge information from the root graph.
.PP
The array returned is obtained from \fImalloc\fP and must be freed by
the caller. The function relies on the \fImark\fP field in
\fIAgnodeinfo_t\fP.
.PP
.SS "  int nodeInduce (Agraph_t* g)"
This function takes a subgraph \fIg\fP and finds all edges in its root
graph both of whose endpoints are in \fIg\fP. It returns the number of
such edges and, if this edge is not already
in the subgraph, it is added.
.PP
.SS "  int isConnected (Agraph_t* g)"
This function returns non-zero if the graph \fIg\fP is connected.

.SS "Packing components"
.PP
.SS "  point* putGraphs (int ng, Agraph_t** gs, Agraph_t* root, int margin, int doSplines)"
\fIputGraphs\fP packs together a collection of laid out graphs into a
single layout which avoids any overlap. It takes as input \fIng\fP 
graphs \fIgs\fP. For each graph, it is assumed that all the nodes have
been positioned using \fIpos\fP, and that the \fIxsize\fP and \fIysize\fP
fields have been set.
The packing is done at the node and edge level, using the polyomino-based
algorithm of Freivalds et al. This allows for a fairly tight packing, in
which a convex part of one graph might be inserted into the concave part
of another.
.PP
If \fIdoSplines\fP is zero, the algorithm represents edges as a 
straight line segment connecting node centers.
If \fIdoSplines\fP is non-zero, the function uses the spline information
in the \fIspl\fP field of an edge.
If \fIroot\fP is non-NULL, it is taken as the root
graph of the subgraphs \fIgs\fP and is used to find the edges. Otherwise, 
\fIputGraphs\fP uses the edges found in each graph \fIgs[i]\fP.
.PP
The parameter \fImargin\fP specifies a boundary of \fImargin\fP points 
to be allowed around each node.
.PP
The function returns an array of points which can be used as the origin of 
the bounding box of each graph. If the
graphs are translated to these positions, none of the graph components
will overlap. 
The array returned is obtained from \fImalloc\fP and must be freed by
the caller. If any problem occurs, \fIputGraphs\fP returns NULL.
As a side-effect, \fIputGraphs\fP sets the \fIbb\fP of each graph to
reflect the initial layout. Since the function does not do any translation,
this is also the final layout.
.PP
This function uses the \fIbb\fP field in \fIAgraphinfo_t\fP,
the \fIpos\fP, \fIxsize\fP and \fIysize\fP fields in \fInodehinfo_t\fP and
the \fIspl\fP field in \fIAedgeinfo_t\fP.
.PP
.SS "  int shiftGraphs (int ng, Agraph_t** gs, point* ps, Agraph_t* root, int doSplines)"
The function \fIshiftGraphs\fP takes \fIng\fP graphs \fIgs\fP and a similar
number of points \fIps\fP and translates each graph so
that the lower left corner of the bounding box of graph \fIgs[i]\fP is at
point \fIps[i]\fP. To do this, it assumes the \fIbb\fP field in
\fIAgraphinfo_t\fP accurately reflects the current graph layout.
The graph is repositioned by translating the \fIpos\fP field of each node 
appropriately.
.PP
If \fIdoSplines\fP is non-zero, the function
also translates the \fIcoord\fP field of each node and the splines coordinates
of each edge, if they have been calculated. In addition, edge labels are
repositioned. If \fIroot\fP is non-NULL, it is taken as the root graph of
the graphs in \fIgs\fP and is used to find the edges. Otherwise, the function
uses the edges found in each graph \fIgs[i]\fP.
.PP
It returns 0 on success. Note also that the bounding boxes of all graphs
are left unmodified.
.PP
This function uses the \fIbb\fP field in \fIAgraphinfo_t\fP,
the \fIpos\fP and \fIcoord\fP fields in \fInodehinfo_t\fP and
the \fIspl\fP field in \fIAedgeinfo_t\fP.
.PP
.SS "  int packGraphs (int ng, Agraph_t** gs, Agraph_t* root, int margin, int doSplines)"
This function takes \fIng\fP subgraphs \fIgs\fP of a root graph \fIroot\fP
and calls \fIputGraphs\fP with the given arguments to generate
a packing of the subgraphs. If successful, it then invokes
\fIshiftGraphs\fP to apply the new positions. It returns 0 on success.
.PP
.SS "  int packSubgraphs (int ng, Agraph_t** gs, Agraph_t* root, int margin, int doSplines)"
This function simply calls \fIpackGraphs\fP with the given arguments, and
then recomputes the bounding box of the \fIroot\fP graph.
.SH SEE ALSO
.BR dot (1),
.BR neato (1),
.BR libgraph (3)
.br
K. Freivalds et al., "Disconnected Graph Layout and the Polyomino 
Packing Approach", GD0'01, LNCS 2265, pp. 378-391.

.SH "BUGS"
The packing does not take into account edge or graph labels.

.SH AUTHORS
Emden Gansner (erg@research.att.com).

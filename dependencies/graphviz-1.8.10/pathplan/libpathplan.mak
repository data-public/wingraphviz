# Microsoft Developer Studio Generated NMAKE File, Based on libpathplan.dsp
!IF "$(CFG)" == ""
CFG=libpathplan - Win32 Debug
!MESSAGE No configuration specified. Defaulting to libpathplan - Win32 Debug.
!ENDIF 

!IF "$(CFG)" != "libpathplan - Win32 Release" && "$(CFG)" != "libpathplan - Win32 Debug"
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "libpathplan.mak" CFG="libpathplan - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "libpathplan - Win32 Release" (based on "Win32 (x86) Static Library")
!MESSAGE "libpathplan - Win32 Debug" (based on "Win32 (x86) Static Library")
!MESSAGE 
!ERROR An invalid configuration is specified.
!ENDIF 

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE 
NULL=nul
!ENDIF 

CPP=cl.exe
RSC=rc.exe

!IF  "$(CFG)" == "libpathplan - Win32 Release"

OUTDIR=.\Release
INTDIR=.\Release

ALL : "..\lib\Release\libpathplan.lib"


CLEAN :
	-@erase "$(INTDIR)\cvt.obj"
	-@erase "$(INTDIR)\inpoly.obj"
	-@erase "$(INTDIR)\route.obj"
	-@erase "$(INTDIR)\shortest.obj"
	-@erase "$(INTDIR)\shortestpth.obj"
	-@erase "$(INTDIR)\solvers.obj"
	-@erase "$(INTDIR)\triang.obj"
	-@erase "$(INTDIR)\util.obj"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(INTDIR)\visibility.obj"
	-@erase "..\lib\Release\libpathplan.lib"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP_PROJ=/nologo /MT /W3 /GX /O1 /I "..\\" /I "..\tools\error" /D "NDEBUG" /D "WIN32" /D "_MBCS" /D "_LIB" /D "HAVE_CONFIG_H" /D "MSWIN32" /Fp"$(INTDIR)\libpathplan.pch" /YX /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /c 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\libpathplan.bsc" 
BSC32_SBRS= \
	
LIB32=link.exe -lib
LIB32_FLAGS=/nologo /out:"..\lib\Release\libpathplan.lib" 
LIB32_OBJS= \
	"$(INTDIR)\cvt.obj" \
	"$(INTDIR)\inpoly.obj" \
	"$(INTDIR)\route.obj" \
	"$(INTDIR)\shortest.obj" \
	"$(INTDIR)\shortestpth.obj" \
	"$(INTDIR)\solvers.obj" \
	"$(INTDIR)\triang.obj" \
	"$(INTDIR)\util.obj" \
	"$(INTDIR)\visibility.obj"

"..\lib\Release\libpathplan.lib" : "$(OUTDIR)" $(DEF_FILE) $(LIB32_OBJS)
    $(LIB32) @<<
  $(LIB32_FLAGS) $(DEF_FLAGS) $(LIB32_OBJS)
<<

!ELSEIF  "$(CFG)" == "libpathplan - Win32 Debug"

OUTDIR=.\Debug
INTDIR=.\Debug

ALL : "..\lib\Debug\libpathplan.lib"


CLEAN :
	-@erase "$(INTDIR)\cvt.obj"
	-@erase "$(INTDIR)\inpoly.obj"
	-@erase "$(INTDIR)\route.obj"
	-@erase "$(INTDIR)\shortest.obj"
	-@erase "$(INTDIR)\shortestpth.obj"
	-@erase "$(INTDIR)\solvers.obj"
	-@erase "$(INTDIR)\triang.obj"
	-@erase "$(INTDIR)\util.obj"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(INTDIR)\vc60.pdb"
	-@erase "$(INTDIR)\visibility.obj"
	-@erase "..\lib\Debug\libpathplan.lib"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP_PROJ=/nologo /MTd /W3 /Gm /GX /ZI /Od /I "..\\" /I "..\tools\error" /D "_DEBUG" /D "WIN32" /D "_MBCS" /D "_LIB" /D "HAVE_CONFIG_H" /D "MSWIN32" /Fp"$(INTDIR)\libpathplan.pch" /YX /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /GZ /c 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\libpathplan.bsc" 
BSC32_SBRS= \
	
LIB32=link.exe -lib
LIB32_FLAGS=/nologo /out:"..\lib\Debug\libpathplan.lib" 
LIB32_OBJS= \
	"$(INTDIR)\cvt.obj" \
	"$(INTDIR)\inpoly.obj" \
	"$(INTDIR)\route.obj" \
	"$(INTDIR)\shortest.obj" \
	"$(INTDIR)\shortestpth.obj" \
	"$(INTDIR)\solvers.obj" \
	"$(INTDIR)\triang.obj" \
	"$(INTDIR)\util.obj" \
	"$(INTDIR)\visibility.obj"

"..\lib\Debug\libpathplan.lib" : "$(OUTDIR)" $(DEF_FILE) $(LIB32_OBJS)
    $(LIB32) @<<
  $(LIB32_FLAGS) $(DEF_FLAGS) $(LIB32_OBJS)
<<

!ENDIF 

.c{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<


!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("libpathplan.dep")
!INCLUDE "libpathplan.dep"
!ELSE 
!MESSAGE Warning: cannot find "libpathplan.dep"
!ENDIF 
!ENDIF 


!IF "$(CFG)" == "libpathplan - Win32 Release" || "$(CFG)" == "libpathplan - Win32 Debug"
SOURCE=.\cvt.c

"$(INTDIR)\cvt.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\inpoly.c

"$(INTDIR)\inpoly.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\route.c

"$(INTDIR)\route.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\shortest.c

"$(INTDIR)\shortest.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\shortestpth.c

"$(INTDIR)\shortestpth.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\solvers.c

"$(INTDIR)\solvers.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\triang.c

"$(INTDIR)\triang.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\util.c

"$(INTDIR)\util.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\visibility.c

"$(INTDIR)\visibility.obj" : $(SOURCE) "$(INTDIR)"



!ENDIF 


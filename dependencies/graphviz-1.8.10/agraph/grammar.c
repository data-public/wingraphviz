
/*  A Bison parser, made from grammar.y
    by GNU Bison version 1.28  */

#define YYBISON 1  /* Identify Bison output.  */

#define	T_graph	257
#define	T_node	258
#define	T_edge	259
#define	T_digraph	260
#define	T_subgraph	261
#define	T_strict	262
#define	T_edgeop	263
#define	T_list	264
#define	T_attr	265
#define	T_atom	266

#line 1 "grammar.y"

/*
    This software may only be used by you under license from AT&T Corp.
    ("AT&T").  A copy of AT&T's Source Code Agreement is available at
    AT&T's Internet website having the URL:
    <http://www.research.att.com/sw/tools/graphviz/license/source.html>
    If you received this software without first entering into a license
    with AT&T, you have an infringing copy of this software and cannot use
    it without violating AT&T's intellectual property rights.
*/


#include <stdio.h>  /* SAFE */
#include <aghdr.h>	/* SAFE */

#ifdef _WIN32
#define gettxt(a,b)	(b)
#endif

static char Key[] = "key";

typedef union s {					/* possible items in generic list */
    Agnode_t		*n;
    Agraph_t		*subg;
    Agedge_t		*e;
    Agsym_t			*asym;	/* bound attribute */
    char			*name;	/* unbound attribute */

    struct item_s	*list;	/* list-of-lists (for edgestmt) */
} val_t;

typedef struct item_s {		/* generic list */
    int				tag;	/* T_node, T_subgraph, T_edge, T_attr */
    val_t			u;		/* primary element */
    char			*str;	/* secondary value - port or attr value */

    struct item_s	*next;
}

item;

typedef struct list_s {		/* maintain head and tail ptrs for fast append */
    item			*first;
    item			*last;
}

list_t;

/* functions */
static void appendnode(char *name, char *port);
static void attrstmt(int tkind, char *macroname);
static void startgraph(char *name, int directed, int strict);
static void bufferedges(void);
static void newedge(Agnode_t *t, char *tport, Agnode_t *h, char *hport, char *key);
static void edgerhs(Agnode_t *n, char *tport, item *hlist, char *key);
static void appendattr(char *name, char *value);
static void bindattrs(int kind);
static void applyattrs(void *obj);
static void endgraph(void);
static void endnode(void);
static void endedge(void);

static void opensubg(char *name);
static void closesubg(void);

/* global */
static Agraph_t *G;


#line 65 "grammar.y"
typedef union	{
    int				i;
    char			*str;

    struct Agnode_s	*n;
} YYSTYPE;
#include <stdio.h>

#ifndef __cplusplus
#ifndef __STDC__
#define const
#endif
#endif



#define	YYFINAL		74
#define	YYFLAG		-32768
#define	YYNTBASE	22

#define YYTRANSLATE(x) ((unsigned)(x) <= 266 ? ag_yytranslate[x] : 54)

static const char ag_yytranslate[] = {
                                         0,
                                         2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
                                         2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
                                         2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
                                         2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
                                         2,     2,     2,    16,     2,     2,     2,     2,     2,     2,
                                         2,     2,     2,     2,     2,     2,     2,    17,    15,     2,
                                         18,     2,     2,    21,     2,     2,     2,     2,     2,     2,
                                         2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
                                         2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
                                         19,     2,    20,     2,     2,     2,     2,     2,     2,     2,
                                         2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
                                         2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
                                         2,     2,    13,     2,    14,     2,     2,     2,     2,     2,
                                         2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
                                         2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
                                         2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
                                         2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
                                         2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
                                         2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
                                         2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
                                         2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
                                         2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
                                         2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
                                         2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
                                         2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
                                         2,     2,     2,     2,     2,     1,     3,     4,     5,     6,
                                         7,     8,     9,    10,    11,    12
                                     };

#if YYDEBUG != 0
static const short ag_yyprhs[] = {
                                     0,
                                     0,     3,     5,     6,    10,    14,    16,    17,    19,    20,
                                     22,    24,    26,    27,    30,    32,    34,    35,    38,    41,
                                     45,    47,    49,    50,    55,    56,    58,    62,    65,    68,
                                     69,    73,    75,    77,    79,    81,    84,    85,    87,    88,
                                     93,    95,    96,    98,   102,   104,   106,   110,   113,   115,
                                     116,   120,   123,   125,   126,   128,   130
                                 };

static const short ag_yyrhs[] = {
                                    24,
                                    23,     0,     1,     0,     0,    13,    28,    14,     0,    26,
                                    27,    25,     0,    12,     0,     0,     8,     0,     0,     3,
                                    0,     6,     0,    29,     0,     0,    29,    31,     0,    31,
                                    0,    15,     0,     0,    39,    30,     0,    32,    30,     0,
                                    33,    34,    42,     0,    36,     0,    50,     0,     0,     9,
                                    35,    33,    34,     0,     0,    37,     0,    36,    16,    37,
                                    0,    12,    38,     0,    17,    12,     0,     0,    40,    41,
                                    43,     0,    49,     0,     3,     0,     4,     0,     5,     0,
                                    12,    18,     0,     0,    43,     0,     0,    42,    19,    44,
                                    20,     0,    45,     0,     0,    46,     0,    45,    53,    46,
                                    0,    47,     0,    48,     0,    12,    18,    12,     0,    21,
                                    12,     0,    47,     0,     0,    52,    51,    23,     0,     7,
                                    12,     0,     7,     0,     0,    15,     0,    16,     0,     0
                                };

#endif

#if YYDEBUG != 0
static const short ag_yyrline[] = {
                                      0,
                                      82,    83,    84,    87,    89,    92,    92,    94,    94,    96,
                                      96,    98,    98,   100,   100,   102,   102,   104,   105,   108,
                                      112,   112,   114,   114,   115,   119,   119,   121,   124,   125,
                                      128,   129,   132,   133,   134,   137,   138,   141,   141,   143,
                                      145,   145,   147,   148,   151,   151,   153,   156,   159,   162,
                                      162,   165,   166,   167,   170,   170,   170
                                  };

#endif


#if YYDEBUG != 0 || defined (YYERROR_VERBOSE)

static const char * const ag_yytname[] = {   "$","error","$undefined.","T_graph",
        "T_node","T_edge","T_digraph","T_subgraph","T_strict","T_edgeop","T_list","T_attr",
        "T_atom","'{'","'}'","';'","','","':'","'='","'['","']'","'@'","graph","body",
        "hdr","optgraphname","optstrict","graphtype","optstmtlist","stmtlist","optsemi",
        "stmt","compound","simple","rcompound","@1","nodelist","node","optport","attrstmt",
        "attrtype","optmacroname","optattr","attrlist","optattrdefs","attrdefs","attritem",
        "attrassignment","attrmacro","graphattrdefs","subgraph","@2","optsubghdr","optseparator", NULL
                                         };

#endif

static const short ag_yyr1[] = {
                                   0,
                                   22,    22,    22,    23,    24,    25,    25,    26,    26,    27,
                                   27,    28,    28,    29,    29,    30,    30,    31,    31,    32,
                                   33,    33,    35,    34,    34,    36,    36,    37,    38,    38,
                                   39,    39,    40,    40,    40,    41,    41,    42,    42,    43,
                                   44,    44,    45,    45,    46,    46,    47,    48,    49,    51,
                                   50,    52,    52,    52,    53,    53,    53
                               };

static const short ag_yyr2[] = {
                                   0,
                                   2,     1,     0,     3,     3,     1,     0,     1,     0,     1,
                                   1,     1,     0,     2,     1,     1,     0,     2,     2,     3,
                                   1,     1,     0,     4,     0,     1,     3,     2,     2,     0,
                                   3,     1,     1,     1,     1,     2,     0,     1,     0,     4,
                                   1,     0,     1,     3,     1,     1,     3,     2,     1,     0,
                                   3,     2,     1,     0,     1,     1,     0
                               };

static const short ag_yydefact[] = {
                                       0,
                                       2,     8,     0,     0,    13,     1,    10,    11,     7,    33,
                                       34,    35,    53,    30,     0,    12,    15,    17,    25,    21,
                                       26,    17,    37,    49,    32,    22,    50,     6,     5,    52,
                                       0,     0,    28,     4,    14,    16,    19,    23,    39,     0,
                                       18,     0,    39,     0,    29,    47,    54,    20,    38,    30,
                                       27,    36,     0,    31,    51,    25,    42,    24,     0,     0,
                                       0,    57,    43,    45,    46,    48,    40,    55,    56,     0,
                                       44,     0,     0,     0
                                   };

static const short ag_yydefgoto[] = {
                                        72,
                                        6,     3,    29,     4,     9,    15,    16,    37,    17,    18,
                                        19,    39,    47,    20,    21,    33,    22,    23,    43,    48,
                                        49,    61,    62,    63,    24,    65,    25,    26,    44,    27,
                                        70
                                    };

static const short ag_yypact[] = {
                                     16,
                                     -32768,-32768,     1,    15,    -2,-32768,-32768,-32768,    11,-32768,
                                     -32768,-32768,    17,     8,     6,    -2,-32768,    12,    19,    14,
                                     -32768,    12,    20,-32768,-32768,-32768,-32768,-32768,-32768,-32768,
                                     21,    22,-32768,-32768,-32768,-32768,-32768,-32768,-32768,    23,
                                     -32768,    13,-32768,     1,-32768,-32768,    -3,    18,-32768,    24,
                                     -32768,-32768,    18,    25,-32768,    19,    -6,-32768,    27,    26,
                                     28,    -8,-32768,-32768,-32768,-32768,-32768,-32768,-32768,    -6,
                                     -32768,    36,    39,-32768
                                 };

static const short ag_yypgoto[] = {
                                      -32768,
                                      -4,-32768,-32768,-32768,-32768,-32768,-32768,    29,    30,-32768,
                                      -5,   -13,-32768,-32768,     7,-32768,-32768,-32768,-32768,     9,
                                      10,-32768,-32768,   -21,   -57,-32768,-32768,-32768,-32768,-32768,
                                      -32768
                                  };


#define	YYLAST		53


static const short ag_yytable[] = {
                                      64,
                                      10,    11,    12,    13,    13,    59,    68,    69,    50,    14,
                                      -54,   -41,    64,     5,    60,    -3,     1,     7,    -9,    34,
                                      8,    -9,    28,     2,    31,    32,    36,    38,    30,    40,
                                      52,    42,    45,    46,    50,    73,    57,    66,    74,    55,
                                      31,    56,    58,   -38,    32,    35,    51,    67,    71,     0,
                                      41,    53,    54
                                  };

static const short ag_yycheck[] = {
                                      57,
                                      3,     4,     5,     7,     7,    12,    15,    16,    12,    12,
                                      13,    20,    70,    13,    21,     0,     1,     3,     3,    14,
                                      6,     6,    12,     8,    17,    18,    15,     9,    12,    16,
                                      18,    12,    12,    12,    12,     0,    19,    12,     0,    44,
                                      17,    47,    56,    19,    18,    16,    40,    20,    70,    -1,
                                      22,    43,    43
                                  };

/* -*-C-*-  Note some compilers choke on comments on `#line' lines.  */
/* This file comes from bison-1.28.  */

/* Skeleton output parser for bison,
   Copyright (C) 1984, 1989, 1990 Free Software Foundation, Inc.
 
   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.
 
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
 
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.  */

/* As a special exception, when this file is copied by Bison into a
   Bison output file, you may use that output file without restriction.
   This special exception was added by the Free Software Foundation
   in version 1.24 of Bison.  */

/* This is the parser code that is written into each bison parser
  when the %semantic_parser declaration is not specified in the grammar.
  It was written by Richard Stallman by simplifying the hairy parser
  used when %semantic_parser is specified.  */

#ifndef YYSTACK_USE_ALLOCA
#ifdef alloca
#define YYSTACK_USE_ALLOCA
#else /* alloca not defined */
#ifdef __GNUC__
#define YYSTACK_USE_ALLOCA
#define alloca __builtin_alloca
#else /* not GNU C.  */
#if (!defined (__STDC__) && defined (sparc)) || defined (__sparc__) || defined (__sparc) || defined (__sgi) || (defined (__sun) && defined (__i386))
#define YYSTACK_USE_ALLOCA
#include <alloca.h>
#else /* not sparc */
/* We think this test detects Watcom and Microsoft C.  */
/* This used to test MSDOS, but that is a bad idea
since that symbol is in the user namespace.  */
#if (defined (_MSDOS) || defined (_MSDOS_)) && !defined (__TURBOC__)
#if 0 /* No need for malloc.h, which pollutes the namespace;
instead, just don't use alloca.  */
#include <malloc.h>
#endif
#else /* not MSDOS, or __TURBOC__ */
#if defined(_AIX)

/* I don't know what this was needed for, but it pollutes the namespace.
So I turned it off.   rms, 2 May 1997.  */
/* #include <malloc.h>  */
#pragma alloca
#define YYSTACK_USE_ALLOCA
#else /* not MSDOS, or __TURBOC__, or _AIX */
#if 0
#ifdef __hpux /* haible@ilog.fr says this works for HPUX 9.05 and up,
and on HPUX 10.  Eventually we can turn this on.  */
#define YYSTACK_USE_ALLOCA
#define alloca __builtin_alloca
#endif /* __hpux */
#endif
#endif /* not _AIX */
#endif /* not MSDOS, or __TURBOC__ */
#endif /* not sparc */
#endif /* not GNU C */
#endif /* alloca not defined */
#endif /* YYSTACK_USE_ALLOCA not defined */

#ifdef YYSTACK_USE_ALLOCA
#define YYSTACK_ALLOC alloca
#else
#define YYSTACK_ALLOC malloc
#endif

/* Note: there must be only one dollar sign in this file.
   It is replaced by the list of actions, each action
   as one case of the switch.  */

#define ag_yyerrok		(ag_yyerrstatus = 0)
#define ag_yyclearin	(ag_yychar = YYEMPTY)
#define YYEMPTY		-2
#define YYEOF		0
#define YYACCEPT	goto ag_yyacceptlab
#define YYABORT 	goto ag_yyabortlab
#define YYERROR		goto ag_yyerrlab1
/* Like YYERROR except do call ag_yyerror.
   This remains here temporarily to ease the
   transition to the new meaning of YYERROR, for GCC.
   Once GCC version 2 has supplanted version 1, this can go.  */
#define YYFAIL		goto ag_yyerrlab
#define YYRECOVERING()  (!!ag_yyerrstatus)
#define YYBACKUP(token, value) \
do								\
if (ag_yychar == YYEMPTY && ag_yylen == 1)				\
{ ag_yychar = (token), ag_yylval = (value);			\
ag_yychar1 = YYTRANSLATE (ag_yychar);				\
YYPOPSTACK;						\
goto ag_yybackup;						\
}								\
else								\
{ ag_yyerror ("syntax error: cannot back up"); YYERROR; }	\
while (0)

#define YYTERROR	1
#define YYERRCODE	256

#ifndef YYPURE
#define YYLEX		ag_yylex()
#endif

#ifdef YYPURE
#ifdef YYLSP_NEEDED
#ifdef YYLEX_PARAM
#define YYLEX		ag_yylex(&ag_yylval, &ag_yylloc, YYLEX_PARAM)
#else
#define YYLEX		ag_yylex(&ag_yylval, &ag_yylloc)
#endif
#else /* not YYLSP_NEEDED */
#ifdef YYLEX_PARAM
#define YYLEX		ag_yylex(&ag_yylval, YYLEX_PARAM)
#else
#define YYLEX		ag_yylex(&ag_yylval)
#endif
#endif /* not YYLSP_NEEDED */
#endif

/* If nonreentrant, generate the variables here */

#ifndef YYPURE

int	ag_yychar;			/*  the lookahead symbol		*/

YYSTYPE	ag_yylval;			/*  the semantic value of the		*/

/*  lookahead symbol			*/

#ifdef YYLSP_NEEDED
YYLTYPE ag_yylloc;			/*  location data for the lookahead	*/

/*  symbol				*/
#endif

int ag_yynerrs;			/*  number of parse errors so far       */

#endif  /* not YYPURE */

#if YYDEBUG != 0
int ag_yydebug;			/*  nonzero means print parse trace	*/

/* Since this is uninitialized, it does not stop multiple parsers
   from coexisting.  */
#endif

/*  YYINITDEPTH indicates the initial size of the parser's stacks	*/

#ifndef	YYINITDEPTH
#define YYINITDEPTH 200
#endif

/*  YYMAXDEPTH is the maximum size the stacks can grow to
    (effective only if the built-in stack extension method is used).  */

#if YYMAXDEPTH == 0
#undef YYMAXDEPTH
#endif

#ifndef YYMAXDEPTH
#define YYMAXDEPTH 10000
#endif

/* Define __ag_yy_memcpy.  Note that the size argument
   should be passed with type unsigned int, because that is what the non-GCC
   definitions require.  With GCC, __builtin_memcpy takes an arg
   of type size_t, but it can handle unsigned int.  */

#if __GNUC__ > 1		/* GNU C and GNU C++ define this.  */
#define __ag_yy_memcpy(TO,FROM,COUNT)	__builtin_memcpy(TO,FROM,COUNT)
#else				/* not GNU C or C++ */
#ifndef __cplusplus

/* This is the most reliable way to avoid incompatibilities
in available built-in functions on various systems.  */
static void
__ag_yy_memcpy (to, from, count)
char *to;

char *from;

unsigned int count;

{
    register char *f = from;
    register char *t = to;
    register int i = count;

    while (i-- > 0)
        *t++ = *f++;
}

#else /* __cplusplus */

/* This is the most reliable way to avoid incompatibilities
in available built-in functions on various systems.  */
static void
__ag_yy_memcpy (char *to, char *from, unsigned int count) {
    register char *t = to;
    register char *f = from;
    register int i = count;

    while (i-- > 0)
        *t++ = *f++;
}

#endif
#endif


/* The user can define YYPARSE_PARAM as the name of an argument to be passed
   into ag_yyparse.  The argument should have type void *.
   It should actually point to an object.
   Grammar actions can access the variable by casting it
   to the proper pointer type.  */

#ifdef YYPARSE_PARAM
#ifdef __cplusplus
#define YYPARSE_PARAM_ARG void *YYPARSE_PARAM
#define YYPARSE_PARAM_DECL
#else /* not __cplusplus */
#define YYPARSE_PARAM_ARG YYPARSE_PARAM
#define YYPARSE_PARAM_DECL void *YYPARSE_PARAM;
#endif /* not __cplusplus */
#else /* not YYPARSE_PARAM */
#define YYPARSE_PARAM_ARG
#define YYPARSE_PARAM_DECL
#endif /* not YYPARSE_PARAM */

/* Prevent warning if -Wstrict-prototypes.  */
#ifdef __GNUC__
#ifdef YYPARSE_PARAM
int ag_yyparse (void *);

#else
int ag_yyparse (void);

#endif
#endif

int
ag_yyparse(YYPARSE_PARAM_ARG)
YYPARSE_PARAM_DECL
{
    register int ag_yystate;
    register int ag_yyn;
    register short *ag_yyssp;
    register YYSTYPE *ag_yyvsp;
    int ag_yyerrstatus;	/*  number of tokens to shift before error messages enabled */
    int ag_yychar1 = 0;		/*  lookahead token as an internal (translated) token number */

    short	ag_yyssa[YYINITDEPTH];	/*  the state stack			*/
    YYSTYPE ag_yyvsa[YYINITDEPTH];	/*  the semantic value stack		*/

    short *ag_yyss = ag_yyssa;		/*  refer to the stacks thru separate pointers */
    YYSTYPE *ag_yyvs = ag_yyvsa;	/*  to allow ag_yyoverflow to reallocate them elsewhere */

#ifdef YYLSP_NEEDED

    YYLTYPE ag_yylsa[YYINITDEPTH];	/*  the location stack			*/
    YYLTYPE *ag_yyls = ag_yylsa;
    YYLTYPE *ag_yylsp;

#define YYPOPSTACK   (ag_yyvsp--, ag_yyssp--, ag_yylsp--)
#else
#define YYPOPSTACK   (ag_yyvsp--, ag_yyssp--)
#endif

    int ag_yystacksize = YYINITDEPTH;
    int ag_yyfree_stacks = 0;

#ifdef YYPURE

    int ag_yychar;
    YYSTYPE ag_yylval;
    int ag_yynerrs;
#ifdef YYLSP_NEEDED

    YYLTYPE ag_yylloc;
#endif
#endif

    YYSTYPE ag_yyval;		/*  the variable used to return		*/
    /*  semantic values from the action	*/
    /*  routines				*/

    int ag_yylen;

#if YYDEBUG != 0

    if (ag_yydebug)
    fprintf(stderr, "Starting parse\n");

#endif

    ag_yystate = 0;

    ag_yyerrstatus = 0;

    ag_yynerrs = 0;

    ag_yychar = YYEMPTY;		/* Cause a token to be read.  */

    /* Initialize stack pointers.
       Waste one element of value and location stack
       so that they stay on the same level as the state stack.
       The wasted elements are never initialized.  */

    ag_yyssp = ag_yyss - 1;

    ag_yyvsp = ag_yyvs;

#ifdef YYLSP_NEEDED

    ag_yylsp = ag_yyls;

#endif

    /* Push a new state, which is found in  ag_yystate  .  */
    /* In all cases, when you get here, the value and location stacks
       have just been pushed. so pushing a state here evens the stacks.  */

    ag_yynewstate:

    *++ag_yyssp = ag_yystate;

    if (ag_yyssp >= ag_yyss + ag_yystacksize - 1) {
        /* Give user a chance to reallocate the stack */
        /* Use copies of these so that the &'s don't force the real ones into memory. */
        YYSTYPE *ag_yyvs1 = ag_yyvs;
        short *ag_yyss1 = ag_yyss;
#ifdef YYLSP_NEEDED

        YYLTYPE *ag_yyls1 = ag_yyls;
#endif

        /* Get the current used size of the three stacks, in elements.  */
        int size = ag_yyssp - ag_yyss + 1;

#ifdef ag_yyoverflow
        /* Each stack pointer address is followed by the size of
        the data in use in that stack, in bytes.  */
#ifdef YYLSP_NEEDED
        /* This used to be a conditional around just the two extra args,
        but that might be undefined if ag_yyoverflow is a macro.  */
        ag_yyoverflow("parser stack overflow",
                      &ag_yyss1, size * sizeof (*ag_yyssp),
                      &ag_yyvs1, size * sizeof (*ag_yyvsp),
                      &ag_yyls1, size * sizeof (*ag_yylsp),
                      &ag_yystacksize);
#else
        ag_yyoverflow("parser stack overflow",
                      &ag_yyss1, size * sizeof (*ag_yyssp),
                      &ag_yyvs1, size * sizeof (*ag_yyvsp),
                      &ag_yystacksize);
#endif

            ag_yyss = ag_yyss1;
            ag_yyvs = ag_yyvs1;
#ifdef YYLSP_NEEDED

            ag_yyls = ag_yyls1;
#endif
#else /* no ag_yyoverflow */
        /* Extend the stack our own way.  */

        if (ag_yystacksize >= YYMAXDEPTH) {
                ag_yyerror("parser stack overflow");

                if (ag_yyfree_stacks) {
                    free (ag_yyss);
                    free (ag_yyvs);
#ifdef YYLSP_NEEDED

                    free (ag_yyls);
#endif

                }

                return 2;
            }

            ag_yystacksize *= 2;

            if (ag_yystacksize > YYMAXDEPTH)
                ag_yystacksize = YYMAXDEPTH;

#ifndef YYSTACK_USE_ALLOCA

            ag_yyfree_stacks = 1;

#endif

            ag_yyss = (short *) YYSTACK_ALLOC (ag_yystacksize * sizeof (*ag_yyssp));

            __ag_yy_memcpy ((char *)ag_yyss, (char *)ag_yyss1,
                            size * (unsigned int) sizeof (*ag_yyssp));

            ag_yyvs = (YYSTYPE *) YYSTACK_ALLOC (ag_yystacksize * sizeof (*ag_yyvsp));

            __ag_yy_memcpy ((char *)ag_yyvs, (char *)ag_yyvs1,
                            size * (unsigned int) sizeof (*ag_yyvsp));

#ifdef YYLSP_NEEDED

            ag_yyls = (YYLTYPE *) YYSTACK_ALLOC (ag_yystacksize * sizeof (*ag_yylsp));

            __ag_yy_memcpy ((char *)ag_yyls, (char *)ag_yyls1,
                            size * (unsigned int) sizeof (*ag_yylsp));

#endif
#endif /* no ag_yyoverflow */

            ag_yyssp = ag_yyss + size - 1;

            ag_yyvsp = ag_yyvs + size - 1;

#ifdef YYLSP_NEEDED

            ag_yylsp = ag_yyls + size - 1;

#endif

#if YYDEBUG != 0

            if (ag_yydebug)
                fprintf(stderr, "Stack size increased to %d\n", ag_yystacksize);

#endif

            if (ag_yyssp >= ag_yyss + ag_yystacksize - 1)
                YYABORT;
        }

#if YYDEBUG != 0
    if (ag_yydebug)
    fprintf(stderr, "Entering state %d\n", ag_yystate);

#endif

    goto ag_yybackup;

    ag_yybackup:

    /* Do appropriate processing given the current state.  */
    /* Read a lookahead token if we need one and don't already have one.  */
    /* ag_yyresume: */

    /* First try to decide what to do without reference to lookahead token.  */

    ag_yyn = ag_yypact[ag_yystate];

    if (ag_yyn == YYFLAG)
        goto ag_yydefault;

        /* Not known => get a lookahead token if don't already have one.  */

        /* ag_yychar is either YYEMPTY or YYEOF
           or a valid token in external form.  */

        if (ag_yychar == YYEMPTY) {
#if YYDEBUG != 0

            if (ag_yydebug)
                    fprintf(stderr, "Reading a token: ");

#endif

                ag_yychar = YYLEX;
            }

    /* Convert token to internal form (in ag_yychar1) for indexing tables with */

    if (ag_yychar <= 0)		/* This means end of input. */
    {
    ag_yychar1 = 0;
    ag_yychar = YYEOF;		/* Don't call YYLEX any more */

#if YYDEBUG != 0

    if (ag_yydebug)
            fprintf(stderr, "Now at end of input.\n");

#endif

    } else {
        ag_yychar1 = YYTRANSLATE(ag_yychar);

#if YYDEBUG != 0

        if (ag_yydebug) {
            fprintf (stderr, "Next token is %d (%s", ag_yychar, ag_yytname[ag_yychar1]);
            /* Give the individual parser a way to print the precise meaning
               of a token, for further debugging info.  */
#ifdef YYPRINT

            YYPRINT (stderr, ag_yychar, ag_yylval);
#endif

            fprintf (stderr, ")\n");
        }

#endif

    }

    ag_yyn += ag_yychar1;

    if (ag_yyn < 0 || ag_yyn > YYLAST || ag_yycheck[ag_yyn] != ag_yychar1)
    goto ag_yydefault;

    ag_yyn = ag_yytable[ag_yyn];

    /* ag_yyn is what to do for this token type in this state.
       Negative => reduce, -ag_yyn is rule number.
       Positive => shift, ag_yyn is new state.
         New state is final state => don't bother to shift,
         just return success.
       0, or most negative number => error.  */

    if (ag_yyn < 0) {
        if (ag_yyn == YYFLAG)
                goto ag_yyerrlab;

            ag_yyn = -ag_yyn;

            goto ag_yyreduce;

        } else if (ag_yyn == 0)
        goto ag_yyerrlab;

        if (ag_yyn == YYFINAL)
            YYACCEPT;

            /* Shift the lookahead token.  */

#if YYDEBUG != 0

            if (ag_yydebug)
                fprintf(stderr, "Shifting token %d (%s), ", ag_yychar, ag_yytname[ag_yychar1]);

#endif

                /* Discard the token being shifted unless it is eof.  */
                if (ag_yychar != YYEOF)
                    ag_yychar = YYEMPTY;

                    *++ag_yyvsp = ag_yylval;

#ifdef YYLSP_NEEDED

                    *++ag_yylsp = ag_yylloc;

#endif

                    /* count tokens shifted since error; after three, turn off error status.  */
                    if (ag_yyerrstatus)
                        ag_yyerrstatus--;

                        ag_yystate = ag_yyn;

                        goto ag_yynewstate;

                        /* Do the default action for the current state.  */

    ag_yydefault:

                        ag_yyn = ag_yydefact[ag_yystate];

                        if (ag_yyn == 0)
                            goto ag_yyerrlab;

                            /* Do a reduction.  ag_yyn is the number of a rule to reduce with.  */

    ag_yyreduce:
                            ag_yylen = ag_yyr2[ag_yyn];

                            if (ag_yylen > 0)
                                ag_yyval = ag_yyvsp[1-ag_yylen]; /* implement default value of the action */

#if YYDEBUG != 0

                                if (ag_yydebug) {
                                    int i;

                                    fprintf (stderr, "Reducing via rule %d (line %d), ",
                                             ag_yyn, ag_yyrline[ag_yyn]);

                                        /* Print the symbols being reduced, and their result.  */

                                        for (i = ag_yyprhs[ag_yyn]; ag_yyrhs[i] > 0; i++)
                                            fprintf (stderr, "%s ", ag_yytname[ag_yyrhs[i]]);

                                        fprintf (stderr, " -> %s\n", ag_yytname[ag_yyr1[ag_yyn]]);
                                    }

#endif


    switch (ag_yyn) {

    case 1:
#line 82 "grammar.y"

    {endgraph();
            ;
            break;
        }

        case 2:
#line 83 "grammar.y"

        {if (G) {
                agclose(G);
                G = Ag_G_global = NIL(Agraph_t*);
            };

            break;
        }

        case 5:
#line 89 "grammar.y"

        {startgraph(ag_yyvsp[0].str,ag_yyvsp[-1].i,ag_yyvsp[-2].i);
            ;
            break;
        }

        case 6:
#line 92 "grammar.y"

        {ag_yyval.str=ag_yyvsp[0].str;
            ;
            break;
        }

        case 7:
#line 92 "grammar.y"

        {ag_yyval.str=0;
            ;
            break;
        }

        case 8:
#line 94 "grammar.y"

        {ag_yyval.i=1;
            ;
            break;
        }

        case 9:
#line 94 "grammar.y"

        {ag_yyval.i=0;
            ;
            break;
        }

        case 10:
#line 96 "grammar.y"

        {ag_yyval.i = 0;
            ;
            break;
        }

        case 11:
#line 96 "grammar.y"

        {ag_yyval.i = 1;
            ;
            break;
        }

        case 20:
#line 109 "grammar.y"

        {if (ag_yyvsp[-1].i)
            endedge();
            else
                endnode();

            ;
            break;
        }

        case 23:
#line 114 "grammar.y"

        {bufferedges();
            ;
            break;
        }

        case 24:
#line 114 "grammar.y"

        {ag_yyval.i = 1;
            ;
            break;
        }

        case 25:
#line 115 "grammar.y"

        {ag_yyval.i = 0;
            ;
            break;
        }

        case 28:
#line 121 "grammar.y"

        {appendnode(ag_yyvsp[-1].str,ag_yyvsp[0].str);
            ;
            break;
        }

        case 29:
#line 124 "grammar.y"

        {ag_yyval.str = ag_yyvsp[0].str;
            ;
            break;
        }

        case 30:
#line 125 "grammar.y"

        {ag_yyval.str = NIL(char*);
            ;
            break;
        }

        case 31:
#line 128 "grammar.y"

        {attrstmt(ag_yyvsp[-2].i,ag_yyvsp[-1].str);
            ;
            break;
        }

        case 32:
#line 129 "grammar.y"

        {attrstmt(T_graph,NIL(char*));
            ;
            break;
        }

        case 33:
#line 132 "grammar.y"

        {ag_yyval.i = T_graph;
            ;
            break;
        }

        case 34:
#line 133 "grammar.y"

        {ag_yyval.i = T_node;
            ;
            break;
        }

        case 35:
#line 134 "grammar.y"

        {ag_yyval.i = T_edge;
            ;
            break;
        }

        case 36:
#line 137 "grammar.y"

        {ag_yyval.str = ag_yyvsp[-1].str;
            ;
            break;
        }

        case 37:
#line 138 "grammar.y"

        {ag_yyval.str = NIL(char*);
            ;
            break;
        }

        case 47:
#line 153 "grammar.y"

        {appendattr(ag_yyvsp[-2].str,ag_yyvsp[0].str);
            ;
            break;
        }

        case 48:
#line 156 "grammar.y"

        {appendattr(ag_yyvsp[0].str,NIL(char*));
            ;
            break;
        }

        case 50:
#line 162 "grammar.y"

        {opensubg(ag_yyvsp[0].str);
            ;
            break;
        }

        case 51:
#line 162 "grammar.y"

        {closesubg();
            ;
            break;
        }

        case 52:
#line 165 "grammar.y"

        {ag_yyval.str=ag_yyvsp[0].str;
            ;
            break;
        }

        case 53:
#line 166 "grammar.y"

        {ag_yyval.str=NIL(char*);
            ;
            break;
        }

        case 54:
#line 167 "grammar.y"

        {ag_yyval.str=NIL(char*);
            ;
            break;
        }
    }

    /* the action file gets copied in in place of this dollarsign */
    
    ag_yyvsp -= ag_yylen;

    ag_yyssp -= ag_yylen;

#ifdef YYLSP_NEEDED

    ag_yylsp -= ag_yylen;

#endif

#if YYDEBUG != 0

    if (ag_yydebug) {
    short *ssp1 = ag_yyss - 1;
    fprintf (stderr, "state stack now");

        while (ssp1 != ag_yyssp)
            fprintf (stderr, " %d", *++ssp1);

        fprintf (stderr, "\n");
    }

#endif

    *++ag_yyvsp = ag_yyval;

#ifdef YYLSP_NEEDED

    ag_yylsp++;

    if (ag_yylen == 0) {
    ag_yylsp->first_line = ag_yylloc.first_line;
    ag_yylsp->first_column = ag_yylloc.first_column;
    ag_yylsp->last_line = (ag_yylsp-1)->last_line;
        ag_yylsp->last_column = (ag_yylsp-1)->last_column;
        ag_yylsp->text = 0;

    } else {
        ag_yylsp->last_line = (ag_yylsp+ag_yylen-1)->last_line;
        ag_yylsp->last_column = (ag_yylsp+ag_yylen-1)->last_column;
    }

#endif

    /* Now "shift" the result of the reduction.
       Determine what state that goes to,
       based on the state we popped back to
       and the rule number reduced by.  */

    ag_yyn = ag_yyr1[ag_yyn];

    ag_yystate = ag_yypgoto[ag_yyn - YYNTBASE] + *ag_yyssp;

    if (ag_yystate >= 0 && ag_yystate <= YYLAST && ag_yycheck[ag_yystate] == *ag_yyssp)
    ag_yystate = ag_yytable[ag_yystate];
    else
        ag_yystate = ag_yydefgoto[ag_yyn - YYNTBASE];

        goto ag_yynewstate;

    ag_yyerrlab:   /* here on detecting error */

        if (! ag_yyerrstatus)
            /* If not already recovering from an error, report this error.  */
        {
            ++ag_yynerrs;

#ifdef YYERROR_VERBOSE

            ag_yyn = ag_yypact[ag_yystate];

                if (ag_yyn > YYFLAG && ag_yyn < YYLAST) {
                    int size = 0;
                    char *msg;
                    int x, count;

                    count = 0;
                    /* Start X at -ag_yyn if nec to avoid negative indexes in ag_yycheck.  */

                    for (x = (ag_yyn < 0 ? -ag_yyn : 0);
                            x < (sizeof(ag_yytname) / sizeof(char *)); x++)
                        if (ag_yycheck[x + ag_yyn] == x)
                            size += strlen(ag_yytname[x]) + 15, count++;

                    msg = (char *) malloc(size + 15);

                    if (msg != 0) {
                        strcpy(msg, "parse error");

                        if (count < 5) {
                            count = 0;

                            for (x = (ag_yyn < 0 ? -ag_yyn : 0);
                                    x < (sizeof(ag_yytname) / sizeof(char *)); x++)
                                if (ag_yycheck[x + ag_yyn] == x) {
                                    strcat(msg, count == 0 ? ", expecting `" : " or `");
                                    strcat(msg, ag_yytname[x]);
                                    strcat(msg, "'");
                                    count++;
                                }
                        }

                        ag_yyerror(msg);
                        free(msg);

                    } else
                        ag_yyerror ("parse error; also virtual memory exceeded");

                } else
#endif /* YYERROR_VERBOSE */

                    ag_yyerror("parse error");
            }

    goto ag_yyerrlab1;

    ag_yyerrlab1:   /* here on error raised explicitly by an action */

    if (ag_yyerrstatus == 3) {
    /* if just tried and failed to reuse lookahead token after an error, discard it.  */

    /* return failure if at end of input */

    if (ag_yychar == YYEOF)
            YYABORT;

#if YYDEBUG != 0

        if (ag_yydebug)
            fprintf(stderr, "Discarding token %d (%s).\n", ag_yychar, ag_yytname[ag_yychar1]);

#endif

        ag_yychar = YYEMPTY;
    }

    /* Else will try to reuse lookahead token
       after shifting the error token.  */

    ag_yyerrstatus = 3;		/* Each real token shifted decrements this */

    goto ag_yyerrhandle;

    ag_yyerrdefault:  /* current state does not do anything special for the error token. */

#if 0
    /* This is wrong; only states that explicitly want error tokens
       should shift them.  */
    ag_yyn = ag_yydefact[ag_yystate];  /* If its default is to accept any token, ok.  Otherwise pop it.*/

    if (ag_yyn)
    goto ag_yydefault;

#endif

    ag_yyerrpop:   /* pop the current state because it cannot handle the error token */

    if (ag_yyssp == ag_yyss)
        YYABORT;

        ag_yyvsp--;

        ag_yystate = *--ag_yyssp;

#ifdef YYLSP_NEEDED

        ag_yylsp--;

#endif

#if YYDEBUG != 0

        if (ag_yydebug) {
            short *ssp1 = ag_yyss - 1;
            fprintf (stderr, "Error: state stack now");

                while (ssp1 != ag_yyssp)
                    fprintf (stderr, " %d", *++ssp1);

                fprintf (stderr, "\n");
            }

#endif

    ag_yyerrhandle:

    ag_yyn = ag_yypact[ag_yystate];

    if (ag_yyn == YYFLAG)
    goto ag_yyerrdefault;

    ag_yyn += YYTERROR;

    if (ag_yyn < 0 || ag_yyn > YYLAST || ag_yycheck[ag_yyn] != YYTERROR)
        goto ag_yyerrdefault;

        ag_yyn = ag_yytable[ag_yyn];

        if (ag_yyn < 0) {
            if (ag_yyn == YYFLAG)
                    goto ag_yyerrpop;

                ag_yyn = -ag_yyn;

                goto ag_yyreduce;

            } else if (ag_yyn == 0)
            goto ag_yyerrpop;

            if (ag_yyn == YYFINAL)
                YYACCEPT;

#if YYDEBUG != 0

                if (ag_yydebug)
                    fprintf(stderr, "Shifting error token, ");

#endif

                    *++ag_yyvsp = ag_yylval;

#ifdef YYLSP_NEEDED

                    *++ag_yylsp = ag_yylloc;

#endif

                    ag_yystate = ag_yyn;

                    goto ag_yynewstate;

    ag_yyacceptlab:
                    /* YYACCEPT comes here.  */
                    if (ag_yyfree_stacks) {
                        free (ag_yyss);
                            free (ag_yyvs);
#ifdef YYLSP_NEEDED

                            free (ag_yyls);
#endif

                        }

    return 0;

    ag_yyabortlab:
    /* YYABORT comes here.  */

    if (ag_yyfree_stacks) {
    free (ag_yyss);
        free (ag_yyvs);
#ifdef YYLSP_NEEDED

        free (ag_yyls);
#endif

    }

    return 1;
}
#line 172 "grammar.y"


#define NILitem  NIL(item*)

/* globals */
static	Agraph_t	*Subgraph;	/* most recent subgraph that was opened */

static	Agdisc_t	*Disc;		/* discipline passed to agread or agconcat */

static	list_t	Nodelist,Edgelist,Attrlist;

static item *newitem(int tag, void *p0, char *p1) {
    item	*rv = agalloc(G,sizeof(item));
    rv->tag = tag;
    rv->u.name = (char*)p0;
    rv->str = p1;
    return rv;
}

static item *cons_node(Agnode_t *n, char *port) {
    return newitem(T_node,n,port);
}

static item *cons_attr(char *name, char *value) {
    return newitem(T_atom,name,value);
}

static item *cons_list(item *list) {
    return newitem(T_list,list,NIL(char*));
}

static item *cons_subg(Agraph_t *subg) {
    return newitem(T_subgraph,subg,NIL(char*));
}

#ifdef NOTDEF
static item *cons_edge(Agedge_t *e) {
    return newitem(T_edge,e,NIL(char*));
}

#endif

static void delete_items(item *ilist) {
    item	*p,*pn;

    for (p = ilist; p; p = pn) {
        pn = p->next;

        switch(p->tag) {

                case T_list:
                delete_items(p->u.list);
                break;

                case T_atom:

                case T_attr:
                agstrfree(G,p->str);
                break;
        }

        agfree(G,p);
    }
}

static void deletelist(list_t *list) {
    delete_items(list->first);
    list->first = list->last = NILitem;
}

#ifdef NOTDEF
static void listins(list_t *list, item *v) {
    v->next = list->first;
    list->first = v;

    if (list->last == NILitem)
        list->last = v;
}

#endif

static void listapp(list_t *list, item *v) {
    if (list->last)
        list->last->next = v;

    list->last = v;

    if (list->first == NILitem)
        list->first = v;
}


/* attrs */
static void appendattr(char *name, char *value) {
    item		*v;

    assert(value != NIL(char*));
    v = cons_attr(name,value);
    listapp(&Attrlist,v);
}

static void bindattrs(int kind) {
    item		*aptr;
    char		*name;

    for (aptr = Attrlist.first; aptr; aptr = aptr->next) {
        assert(aptr->tag == T_atom);	/* signifies unbound attr */
        name = aptr->u.name;

        if ((kind == AGEDGE) && streq(name,Key))
            continue;

        if ((aptr->u.asym = agattr(G,kind,name,NIL(char*))) == NILsym)
            aptr->u.asym = agattr(G,kind,name,"");

        aptr->tag = T_attr;				/* signifies bound attr */

        agstrfree(G,name);
    }
}

static void applyattrs(void *obj) {
    item		*aptr;

    for (aptr = Attrlist.first; aptr; aptr = aptr->next) {
        if (aptr->tag == T_attr) {
            if (aptr->u.asym) {
                agxset(obj,aptr->u.asym,aptr->str);
            }

        } else {
            assert(AGTYPE(obj) == AGEDGE);
            assert(aptr->tag == T_atom);
            assert(streq(aptr->u.name,Key));
        }
    }
}

static void nomacros(void) {
    agerror(AGERROR_UNIMPL,"attribute macros");
}

static void attrstmt(int tkind, char *macroname) {
    item			*aptr;
    int				kind;

    /* creating a macro def */

    if (macroname)
        nomacros();

    /* invoking a macro def */
    for (aptr = Attrlist.first; aptr; aptr = aptr->next)
        if (aptr->str == NIL(char*))
            nomacros();

    switch(tkind) {

            case T_graph:
            kind = AGRAPH;
            break;

            case T_node:
            kind = AGNODE;
            break;

            case T_edge:
            kind = AGEDGE;
            break;

            default :
            abort();
    }

    bindattrs(kind);	/* set up defaults for new attributes */

    for (aptr = Attrlist.first; aptr; aptr = aptr->next)
        agattr(G,kind,aptr->u.asym->name,aptr->str);

    deletelist(&Attrlist);
}

/* nodes */

static void appendnode(char *name, char *port) {
    item		*elt;

    elt = cons_node(agnode(G,name,TRUE),port);
    listapp(&Nodelist,elt);
    agstrfree(G,name);
}

/* apply current optional attrs to Nodelist and clean up lists */
static void endnode() {
    item	*ptr;

    bindattrs(AGNODE);

    for (ptr = Nodelist.first; ptr; ptr = ptr->next)
        applyattrs(ptr->u.n);

    deletelist(&Nodelist);

    deletelist(&Attrlist);
}

/* edges - store up node/subg lists until optional edge key can be seen */

static void bufferedges() {
    item	*v;

    if (Nodelist.first) {
        v = cons_list(Nodelist.first);
        Nodelist.first = Nodelist.last = NILitem;

    } else {
        v = cons_subg(Subgraph);
        Subgraph = NIL(Agraph_t*);
    }

    listapp(&Edgelist,v);
}

static void endedge(void) {
    char			*key;
    item			*aptr,*tptr,*p;

    Agnode_t		*t;
    Agraph_t		*subg;

    bufferedges();	/* pick up the terminal nodelist or subg */
    bindattrs(AGEDGE);

    /* look for "key" pseudo-attribute */
    key = NIL(char*);

    for (aptr = Attrlist.first; aptr; aptr = aptr->next) {
        if ((aptr->tag == T_atom) && streq(aptr->u.name,Key))
            key = aptr->str;
    }

    /* can make edges with node lists or subgraphs */
    for (p = Edgelist.first; p->next; p = p->next) {
        if (p->tag == T_subgraph) {
            subg = p->u.subg;

            for (t = agfstnode(subg); t; t = agnxtnode(t))
                edgerhs(agsubnode(G,t,FALSE),NIL(char*),p->next,key);

        } else {
            for (tptr = p->u.list; tptr; tptr = tptr->next)
                edgerhs(tptr->u.n,tptr->str,p->next,key);
        }
    }

    deletelist(&Edgelist);
    deletelist(&Attrlist);
}

static void edgerhs(Agnode_t *tail, char *tport, item *hlist, char *key) {
    Agnode_t		*head;
    Agraph_t		*subg;
    item			*hptr;

    if (hlist->tag == T_subgraph) {
        subg = hlist->u.subg;

        for (head = agfstnode(subg); head; head = agnxtnode(head))
            newedge(tail,tport,agsubnode(G,head,FALSE),NIL(char*),key);

    } else {
        for (hptr = hlist->u.list; hptr; hptr = hptr->next)
            newedge(tail,tport,agsubnode(G,hptr->u.n,FALSE),hptr->str,key);
    }
}

static void mkport(Agedge_t *e, char *name, char *val) {
    Agsym_t *attr;

    if (val) {
        if ((attr = agattr(G,AGEDGE,name,NIL(char*))) == NILsym)
            attr = agattr(G,AGEDGE,name,"");

        agxset(e,attr,val);
    }
}

static void newedge(Agnode_t *t, char *tport, Agnode_t *h, char *hport, char *key) {
    Agedge_t 	*e;

    e = agedge(t,h,key,TRUE);

    if (e) {		/* can fail if graph is strict and t==h */
        mkport(e,"tailport",tport);
        mkport(e,"headport",hport);
        applyattrs(e);
    }
}

/* graphs and subgraphs */


static void startgraph(char *name, int directed, int strict) {
    static Agdesc_t	req;	/* get rid of warnings */

    if (G == NILgraph) {
        req.directed = directed;
        req.strict = strict;
        req.flatlock = FALSE;
        req.maingraph = TRUE;
        Ag_G_global = G = agopen(name,req,Disc);

    } else {
        Ag_G_global = G;
    }

    agstrfree(NIL(Agraph_t*),name);
}

static void endgraph() {
    aglexeof();
    aginternalmapclearlocalnames(G);
}

static void opensubg(char *name) {
    G = agsubg(G,name,TRUE);
    agstrfree(G,name);
}

static void closesubg() {
    Subgraph = G;

    if ((G = agparent(G)) == NIL(Agraph_t*))
        ag_yyerror("libgraph: parser lost root graph\n");
}

extern void *ag_yyin;
Agraph_t *agconcat(Agraph_t *g, void *chan, Agdisc_t *disc) {
    ag_yyin = chan;
    G = g;
    Ag_G_global = NILgraph;
    Disc = (disc? disc :  &AgDefaultDisc);
    aglexinit(Disc, chan);
    ag_yyparse();
    return Ag_G_global;
}

Agraph_t *agread(void *fp, Agdisc_t *disc) {
    return agconcat(NILgraph,fp,disc);
}

/*
    This software may only be used by you under license from AT&T Corp.
    ("AT&T").  A copy of AT&T's Source Code Agreement is available at
    AT&T's Internet website having the URL:
    <http://www.research.att.com/sw/tools/graphviz/license/source.html>
    If you received this software without first entering into a license
    with AT&T, you have an infringing copy of this software and cannot use
    it without violating AT&T's intellectual property rights.
*/
#include <aghdr.h>

#ifdef DMALLOC
#include "dmalloc.h"
#endif

static char DRName[] = "_AG_pending";

typedef struct symlist_s {
    Agsym_t *sym;

    struct symlist_s *link;
}

symlist_t;

typedef struct {
    Dtlink_t	link;
    Agobj_t		*key;	/* i.e. the root obj */
    Agobj_t		*obj;	/* may be a subgraph obj */
    symlist_t	*symlist;
    int			callback_kind;
}

pending_cb_t;

typedef struct {
    Agrec_t		h;

    struct {
        Dict_t *g,*n,*e;
    }

    pending;
}

dirtyset_t;

static void clean_symlist(pending_cb_t *pcb) {
    Agraph_t		*g;
    symlist_t		*s,*t;

    g = agroot(agraphof(pcb->obj));

    for (s = pcb->symlist; s; s = t) {
        t = s->link;
        agfree(g,s);
    }
}

static void freef(Dict_t *dict, void *ptr, Dtdisc_t *disc) {
    pending_cb_t	*pcb;

    NOTUSED(dict);
    NOTUSED(disc);
    pcb = ptr;
    clean_symlist(pcb);
    agfree(agraphof(pcb->obj),pcb);
}

static Dtdisc_t Disc = {
                           offsetof(pending_cb_t,key),	/* sort by 'key' */
                           sizeof(Agobj_t*),
                           0, /* link offset */
                           NIL(Dtmake_f),
                           freef,
                           NIL(Dtcompar_f),
                           NIL(Dthash_f)
                       };

static Dict_t *dictof(dirtyset_t *ds, Agobj_t *obj) {
    Dict_t	**dict_ref = NIL(Dict_t**);

    switch(AGTYPE(obj)) {

            case AGRAPH:
            dict_ref = &(ds->pending.g);
            break;

            case AGNODE:
            dict_ref = &(ds->pending.n);
            break;

            case AGINEDGE:

            case AGOUTEDGE:
            dict_ref = &(ds->pending.e);
            break;
    }

    if (*dict_ref == NIL(Dict_t*))
        *dict_ref = agdtopen(agraphof(obj),&Disc,Dttree);

    return *dict_ref;
}

static Agobj_t* genkey(Agobj_t *obj) {
    switch (AGTYPE(obj)) {

            case AGRAPH:
            return obj;

            case AGNODE:
            return (Agobj_t*)agsubnode(agroot(agraphof(obj)),(Agnode_t*)obj,FALSE);

            case AGINEDGE:

            case AGOUTEDGE:
            return (Agobj_t*)agsubedge(agroot(agraphof(obj)),(Agedge_t*)obj,FALSE);
    }

    return NIL(Agobj_t*);
}

static pending_cb_t *lookup(Dict_t *dict, Agobj_t *obj) {
    pending_cb_t	key, *rv;

    key.key = genkey(obj);
    rv = (pending_cb_t*) dtsearch(dict,&key);
    return rv;
}

static void resolve(Dict_t *dict, pending_cb_t *handle, Agobj_t *obj, int cbkind, Agsym_t *optsym) {
    symlist_t	*sym,*nsym,*psym;

    switch (cbkind) {

            case CB_INITIALIZE:	/* dominate CB_UPDATE */
            clean_symlist(handle);
            handle->callback_kind = cbkind;
            break;

            case CB_DELETION:	/* cancels all pending callbacks */
            dtdelete(dict,handle);
            break;

            case CB_UPDATE:		/* least strength */

            if (handle->callback_kind == CB_UPDATE) {
                psym = NIL(symlist_t*);

                for (sym = handle->symlist; sym; psym = sym, sym = sym->link)
                    if (sym->sym == optsym)
                        break;

                if (sym == NIL(symlist_t*)) {
                    nsym = agalloc(agraphof(obj),sizeof(symlist_t));
                    nsym->sym = optsym;

                    if (psym)
                        psym->link = nsym;
                    else
                        handle->symlist = nsym;
                }

                /* else we already have a callback registered */
            }

            /* else update is dominated by CB_INITIALIZE or CB_DELETION */
            break;
    }
}

static void insert(Dict_t *dict, Agobj_t *obj, int cbkind, Agsym_t *optsym) {
    pending_cb_t	*handle;
    handle = agalloc(agraphof(obj),sizeof(pending_cb_t));
    handle->obj = obj;
    handle->key = genkey(obj);

    if (optsym) {
        handle->symlist = (symlist_t*)agalloc(agraphof(obj),sizeof(symlist_t));
        handle->symlist->sym = optsym;
    }

    handle->callback_kind = cbkind;
    dtinsert(dict,handle);
}

void agrecord_callback(Agobj_t *obj, int kind, Agsym_t *optsym) {
    dirtyset_t		*dirty;
    Dict_t			*dict;
    pending_cb_t	*handle;
    Agraph_t		*g;

    g = agraphof(obj);
    dirty = (dirtyset_t*) agbindrec(g,DRName,sizeof(dirtyset_t),FALSE);
    dict = dictof(dirty,obj);

    if ((handle = lookup(dict,obj)))
        resolve(dict,handle,obj,kind,optsym);
    else
        insert(dict,obj,kind,optsym);
}

static void cb(Dict_t *dict) {
    pending_cb_t	*pcb;
    Agraph_t		*g;
    symlist_t		*psym;
    Agcbstack_t		*stack;

    if (dict)
        while ((pcb = (pending_cb_t*)dtfirst(dict))) {
            g = agraphof(pcb->obj);
            stack = g->clos->cb;

            switch(pcb->callback_kind) {

                    case CB_INITIALIZE:
                    aginitcb(pcb->obj,stack);
                    break;

                    case CB_UPDATE:

                    for (psym = pcb->symlist; psym; psym = psym->link)
                        agupdcb(pcb->obj, psym->sym, stack);

                    break;

                    case CB_DELETION:
                    agdelcb(pcb->obj,stack);

                    break;
            }

            dtdelete(dict,pcb);
        }
}

static void agrelease_callbacks(Agraph_t *g) {
    dirtyset_t *dirty;

    if (NOT(g->clos->callbacks_enabled)) {
        g->clos->callbacks_enabled = TRUE;
        dirty = (dirtyset_t*) agbindrec(g,DRName,sizeof(dirtyset_t),FALSE);
        cb (dirty->pending.g);
        cb (dirty->pending.n);
        cb (dirty->pending.e);
    }
}

int agcallbacks(Agraph_t *g, int flag) {
#ifdef OLD

    if (flag && NOT(g->clos->callbacks_enabled))
        agrelease_callbacks(g);
    else if (NOT(flag) && g->clos->callbacks_enabled)
        g->clos->callbacks_enabled = FALSE;

#else

    if (flag && NOT(g->clos->callbacks_enabled))
        agrelease_callbacks(g);

    if (g->clos->callbacks_enabled) {
        g->clos->callbacks_enabled = flag;
        return TRUE;
    }

    g->clos->callbacks_enabled = flag;
    return FALSE;
#endif
}

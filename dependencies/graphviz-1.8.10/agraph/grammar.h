typedef union	{
    int				i;
    char			*str;

    struct Agnode_s	*n;
}

YYSTYPE;
#define	T_graph	257
#define	T_node	258
#define	T_edge	259
#define	T_digraph	260
#define	T_subgraph	261
#define	T_strict	262
#define	T_edgeop	263
#define	T_list	264
#define	T_attr	265
#define	T_atom	266


extern YYSTYPE ag_yylval;

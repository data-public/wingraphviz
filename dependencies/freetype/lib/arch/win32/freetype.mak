# Microsoft Developer Studio Generated NMAKE File, Based on freetype.dsp
!IF "$(CFG)" == ""
CFG=freetype - Win32 Debug
!MESSAGE No configuration specified. Defaulting to freetype - Win32 Debug.
!ENDIF 

!IF "$(CFG)" != "freetype - Win32 Release" && "$(CFG)" != "freetype - Win32 Debug"
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "freetype.mak" CFG="freetype - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "freetype - Win32 Release" (based on "Win32 (x86) Static Library")
!MESSAGE "freetype - Win32 Debug" (based on "Win32 (x86) Static Library")
!MESSAGE 
!ERROR An invalid configuration is specified.
!ENDIF 

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE 
NULL=nul
!ENDIF 

CPP=cl.exe
RSC=rc.exe

!IF  "$(CFG)" == "freetype - Win32 Release"

OUTDIR=.\Release
INTDIR=.\Release

ALL : "..\..\..\..\lib\Release\freetype.lib"


CLEAN :
	-@erase "$(INTDIR)\freetype.obj"
	-@erase "$(INTDIR)\Ftxcmap.obj"
	-@erase "$(INTDIR)\ftxerr18.obj"
	-@erase "$(INTDIR)\Ftxgasp.obj"
	-@erase "$(INTDIR)\Ftxkern.obj"
	-@erase "$(INTDIR)\ftxpost.obj"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "..\..\..\..\lib\Release\freetype.lib"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP_PROJ=/nologo /MT /W3 /GX /O1 /I "." /I "..\.." /I "..\..\extend" /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /c 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\freetype.bsc" 
BSC32_SBRS= \
	
LIB32=link.exe -lib
LIB32_FLAGS=/nologo /out:"..\..\..\..\lib\Release\freetype.lib" 
LIB32_OBJS= \
	"$(INTDIR)\freetype.obj" \
	"$(INTDIR)\Ftxcmap.obj" \
	"$(INTDIR)\ftxerr18.obj" \
	"$(INTDIR)\Ftxgasp.obj" \
	"$(INTDIR)\Ftxkern.obj" \
	"$(INTDIR)\ftxpost.obj"

"..\..\..\..\lib\Release\freetype.lib" : "$(OUTDIR)" $(DEF_FILE) $(LIB32_OBJS)
    $(LIB32) @<<
  $(LIB32_FLAGS) $(DEF_FLAGS) $(LIB32_OBJS)
<<

!ELSEIF  "$(CFG)" == "freetype - Win32 Debug"

OUTDIR=.\Debug
INTDIR=.\Debug

ALL : "..\..\..\..\lib\Debug\freetype.lib"


CLEAN :
	-@erase "$(INTDIR)\freetype.obj"
	-@erase "$(INTDIR)\Ftxcmap.obj"
	-@erase "$(INTDIR)\ftxerr18.obj"
	-@erase "$(INTDIR)\Ftxgasp.obj"
	-@erase "$(INTDIR)\Ftxkern.obj"
	-@erase "$(INTDIR)\ftxpost.obj"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(INTDIR)\vc60.pdb"
	-@erase "..\..\..\..\lib\Debug\freetype.lib"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP_PROJ=/nologo /MTd /W3 /GX /Zi /O1 /I "." /I "..\.." /I "..\..\extend" /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /c 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\freetype.bsc" 
BSC32_SBRS= \
	
LIB32=link.exe -lib
LIB32_FLAGS=/nologo /out:"..\..\..\..\lib\Debug\freetype.lib" 
LIB32_OBJS= \
	"$(INTDIR)\freetype.obj" \
	"$(INTDIR)\Ftxcmap.obj" \
	"$(INTDIR)\ftxerr18.obj" \
	"$(INTDIR)\Ftxgasp.obj" \
	"$(INTDIR)\Ftxkern.obj" \
	"$(INTDIR)\ftxpost.obj"

"..\..\..\..\lib\Debug\freetype.lib" : "$(OUTDIR)" $(DEF_FILE) $(LIB32_OBJS)
    $(LIB32) @<<
  $(LIB32_FLAGS) $(DEF_FLAGS) $(LIB32_OBJS)
<<

!ENDIF 

.c{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<


!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("freetype.dep")
!INCLUDE "freetype.dep"
!ELSE 
!MESSAGE Warning: cannot find "freetype.dep"
!ENDIF 
!ENDIF 


!IF "$(CFG)" == "freetype - Win32 Release" || "$(CFG)" == "freetype - Win32 Debug"
SOURCE=.\freetype.c

"$(INTDIR)\freetype.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\Ftxcmap.c

"$(INTDIR)\Ftxcmap.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\ftxerr18.c

"$(INTDIR)\ftxerr18.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\Ftxgasp.c

"$(INTDIR)\Ftxgasp.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\Ftxkern.c

"$(INTDIR)\Ftxkern.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\ftxpost.c

"$(INTDIR)\ftxpost.obj" : $(SOURCE) "$(INTDIR)"



!ENDIF 


// test.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "test.h"

#include <objbase.h>
#include <atomic>
#include "WinGraphViz.h"

#pragma comment( lib, "gdiplus.lib" ) 
#include <gdiplus.h> 


#define MAX_LOADSTRING 100

// Global Variables:
HINSTANCE hInst;								// current instance
TCHAR szTitle[MAX_LOADSTRING];					// The title bar text
TCHAR szWindowClass[MAX_LOADSTRING];			// the main window class name

// Forward declarations of functions included in this code module:
ATOM				MyRegisterClass(HINSTANCE hInstance);
BOOL				InitInstance(HINSTANCE, int);
LRESULT CALLBACK	WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK	About(HWND, UINT, WPARAM, LPARAM);

//const BSTR	sGraphString = _T("digraph G { bgcolor = \"transparent\"; center = \"true\"; aspect = \"1.35\"; splines = \"true\"; size = \"6.2, 4.2\"; A -> B -> C -> A; B-> D; }");
//const BSTR	sGraphString = _T("digraph G {A -> B -> C -> A; C -> D; }");
const BSTR	sGraphString = _T("digraph G{	bgcolor = \"transparent\";	center = \"true\";	size = \"6.2, 3.0\";	_RESERVED__________THE_END[label = \"Project End\", style = filled, fillcolor = \"lightblue\"];	_RESERVED__________START[label = \"Project Start\", style = filled, fillcolor = \"lightblue\"];	FOLDER1[shape = record, style = filled, fillcolor = \"lightgray\", label = FOLDER1]; FOLDER2[shape = record, style = filled, fillcolor = \"lightgray\" label = \"FOLDER2\"];		FOLDER3[shape = record, style = filled, fillcolor = \"lightgray\" label = \"FOLDER3\"];	FOLDER4[shape = record, style = filled, fillcolor = \"lightgray\"label = \"FOLDER4\"]; FOLDER5[shape = record, style = filled, fillcolor = \"lightgray\"label = \"FOLDER5\"];		FOLDER6[shape = record, style = filled, fillcolor = \"lightgray\"label = \"FOLDER6\"];	FOLDER7[shape = record, style = filled, fillcolor = \"lightgray\"label = \"FOLDER7\"];	FOLDER5->FOLDER1;	FOLDER6->FOLDER1;	FOLDER4->FOLDER2;	FOLDER5->FOLDER3;	FOLDER5->FOLDER4;	FOLDER7->FOLDER5; FOLDER2->FOLDER6; _RESERVED__________START->FOLDER7;	FOLDER1->_RESERVED__________THE_END;	FOLDER3->_RESERVED__________THE_END;}");

IDOT * mpIDOT = 0;
IBinaryImage * mpImage = 0;

static void errMsg(HWND hWnd, HRESULT hr, const std::wstring& err = L"Cannot generate Image")
{
	std::wstring lError = err + L", GraphViz error: " + std::to_wstring(hr);
	IErrorInfo* lInfo;
	hr = GetErrorInfo(0, &lInfo);
	if (lInfo) {
		BSTR msg;
		hr = lInfo->GetDescription(&msg);
		if (!FAILED(hr))
			lError.append(L"\r\n").append(msg);
	}
	MessageBox(hWnd, lError.c_str(), _T("Graph error"), MB_ICONWARNING);
}

class _img {
	Gdiplus::GdiplusStartupInput gdiplusStartupInput;
	ULONG_PTR gdiplusToken;
	std::atomic<Gdiplus::Image*> image;
public:
	_img() : image(0) {
		Gdiplus::GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL);
	}
	~_img() {
		unload();
		// Shutdown Gdiplus 
		Gdiplus::GdiplusShutdown(gdiplusToken);
	}

	operator Gdiplus::Image*() { return image; }
	operator bool() { return image.operator void *() != nullptr; }
	void load(const wchar_t* filename) {
		unload();
		image = Gdiplus::Image::FromFile(filename);
	}
	void unload() {
		Gdiplus::Image* old = std::atomic_exchange<Gdiplus::Image*>(&image, nullptr);
		if (old)
			delete old;
	}
};
static _img sImage;

static void doTest(HWND hWnd)
{
	HRESULT hr;
	BSTR svgResult;

	if (!mpIDOT)
	{
		hr = CoCreateInstance(CLSID_DOT, NULL, CLSCTX_ALL, IID_IDOT,
			reinterpret_cast<void**>(&mpIDOT));
		if (FAILED(hr)) {
			MessageBox(hWnd, _T("Cannot create IDOT instance, is WinGraphViz registered ?"),
				_T("Graph error"), MB_ICONWARNING);
			return;
		}
	}

	if (!mpImage)
	{
		// WinGraphviz.BinaryImage
		hr = CoCreateInstance(CLSID_BinaryImage, NULL, CLSCTX_ALL, IID_IBinaryImage,
			reinterpret_cast<void**>(&mpImage));
		if (FAILED(hr)) {
			MessageBox(hWnd, _T("Cannot create WinGraphviz.BinaryImage instance, is WinGraphViz registered ?"),
				_T("Graph error"), MB_ICONWARNING);
			return;
		}
	}

	hr = mpIDOT->ToJPEG(sGraphString, &mpImage);
	// JCC BUG ???
	if (FAILED(hr))
	{
		hr = mpIDOT->ToJPEG(sGraphString, &mpImage);
	}
	if (FAILED(hr))
	{
		errMsg(hWnd, hr);
		hr = mpIDOT->ToSvg(sGraphString, &svgResult);
		if (FAILED(hr))
			hr = mpIDOT->ToSvg(sGraphString, &svgResult);
		if (FAILED(hr))
			MessageBox(hWnd, _T("ToSvg failed"), _T("Graph error"), MB_ICONWARNING);
		return;
	}
	VARIANT_BOOL r;
	const BSTR lFile = _T("_graph.jpg" );
	hr = mpImage->Save(lFile, &r);
	if (FAILED(hr))
	{
		errMsg(hWnd, hr);
		return;
	}
	if (r != TRUE)
	{
		MessageBox(hWnd, _T("Save() failed"), _T("Graph error"), MB_ICONWARNING);
		return;
	}

	// Load the image 
	sImage.load(lFile);
	Gdiplus::Image* image = sImage;
	if (!image)
	{
		MessageBox(hWnd, _T("Cannot load image"), _T("GDI+ error"), MB_ICONWARNING);
		return;
	}

	GUID guid;
	image->GetRawFormat(&guid);
	TCHAR strguid[40];
	StringFromGUID2(guid, strguid, sizeof(strguid));
	if (guid == Gdiplus::ImageFormatJPEG)
		printf("The format is JPEG.\n");
	else
		MessageBox(hWnd, strguid, _T("GDI+ image format"), MB_ICONWARNING);

	RedrawWindow(hWnd, 0, 0, RDW_INTERNALPAINT);
}

void drawImage(HDC hdc)
{
	using namespace Gdiplus;
	Graphics g(hdc);
	//Pen pen();
	g.DrawImage(sImage, 0, 0);
}

int APIENTRY _tWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPTSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

 	// TODO: Place code here.
	MSG msg;
	HACCEL hAccelTable;

	// Initialize global strings
	LoadString(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
	LoadString(hInstance, IDC_TEST, szWindowClass, MAX_LOADSTRING);
	MyRegisterClass(hInstance);

	// Perform application initialization:
	if (!InitInstance (hInstance, nCmdShow))
	{
		return FALSE;
	}

	hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_TEST));

	// Main message loop:
	while (GetMessage(&msg, NULL, 0, 0))
	{
		if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	return (int) msg.wParam;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEX wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style			= CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc	= WndProc;
	wcex.cbClsExtra		= 0;
	wcex.cbWndExtra		= 0;
	wcex.hInstance		= hInstance;
	wcex.hIcon			= LoadIcon(hInstance, MAKEINTRESOURCE(IDI_TEST));
	wcex.hCursor		= LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground	= (HBRUSH)(COLOR_WINDOW+1);
	wcex.lpszMenuName	= MAKEINTRESOURCE(IDC_TEST);
	wcex.lpszClassName	= szWindowClass;
	wcex.hIconSm		= LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

	return RegisterClassEx(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   HWND hWnd;

   hInst = hInstance; // Store instance handle in our global variable

   hWnd = CreateWindow(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
      CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, NULL, NULL, hInstance, NULL);

   if (!hWnd)
   {
      return FALSE;
   }

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND	- process the application menu
//  WM_PAINT	- Paint the main window
//  WM_DESTROY	- post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	int wmId, wmEvent;
	PAINTSTRUCT ps;
	HDC hdc;

	switch (message)
	{
	case WM_COMMAND:
		wmId    = LOWORD(wParam);
		wmEvent = HIWORD(wParam);
		// Parse the menu selections:
		switch (wmId)
		{
		case IDM_ABOUT:
			DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
			break;
		case IDM_EXIT:
			DestroyWindow(hWnd);
			break;
		case ID_FILE_TEST:
			doTest(hWnd);
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
		}
		break;
	case WM_PAINT:
		hdc = BeginPaint(hWnd, &ps);
		// TODO: Add any drawing code here...
		if (sImage) {
			drawImage(hdc);
		}
		EndPaint(hWnd, &ps);
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}

// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
		{
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		break;
	}
	return (INT_PTR)FALSE;
}

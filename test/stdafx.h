// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"

#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers
// Windows Header Files:
#include <windows.h>

// C RunTime Header Files
#include <stdlib.h>
#include <malloc.h>
#include <memory.h>
#include <tchar.h>


// TODO: reference additional headers your program requires here
#include <string>
static const IID IID_IDOT = { 0xA1080582, 0xD33F, 0x486E, { 0xBD, 0x5F, 0x58, 0x19, 0x89, 0xA3, 0xC7, 0xE9 } };
static const CLSID CLSID_DOT = { 0x55811839, 0x47B0, 0x4854, { 0x81, 0xB5, 0x0A, 0x00, 0x31, 0xB8, 0xAC, 0xEC } };
static const IID IID_IBinaryImage = { 0xFFF6CEBE, 0xBD9B, 0x4C3A, { 0xA2, 0x74, 0x12, 0xE6, 0x00, 0xB6, 0xBD, 0x10 } };
static const CLSID CLSID_BinaryImage = { 0x6B3F62C8, 0x80CE, 0x470D, { 0xB2, 0xE4, 0x0B, 0xA4, 0x2A, 0x03, 0x52, 0x28 } };
